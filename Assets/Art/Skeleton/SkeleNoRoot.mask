%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: SkeleNoRoot
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: skeleton_sniper_model
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root
    m_Weight: 0
  - m_Path: skeleton_sniper_model_skeleton/root/bip_footIK.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_footIK.L/bip_foot.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_footIK.L/bip_foot.L/bip_foot.L_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_footIK.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_footIK.R/bip_foot.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_footIK.R/bip_foot.R/bip_foot.R_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKElbow.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKElbow.L/bip_IKElbow.L_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKElbow.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKElbow.R/bip_IKElbow.R_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_index_0.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_index_0.L/bip_index_1.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_index_0.L/bip_index_1.L/bip_index_2.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_index_0.L/bip_index_1.L/bip_index_2.L/bip_index_2.L_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_middle_0.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_middle_0.L/bip_middle_1.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_middle_0.L/bip_middle_1.L/bip_middle_2.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_middle_0.L/bip_middle_1.L/bip_middle_2.L/bip_middle_2.L_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_pinky_0.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_pinky_0.L/bip_pinky_1.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_pinky_0.L/bip_pinky_1.L/bip_pinky_2.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_pinky_0.L/bip_pinky_1.L/bip_pinky_2.L/bip_pinky_2.L_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_ring_0.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_ring_0.L/bip_ring_1.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_ring_0.L/bip_ring_1.L/bip_ring_2.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_ring_0.L/bip_ring_1.L/bip_ring_2.L/bip_ring_2.L_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_thumb_0.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_thumb_0.L/bip_thumb_1.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_thumb_0.L/bip_thumb_1.L/bip_thumb_2.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.L/bip_thumb_0.L/bip_thumb_1.L/bip_thumb_2.L/bip_thumb_2.L_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_index_0.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_index_0.R/bip_index_1.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_index_0.R/bip_index_1.R/bip_index_2.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_index_0.R/bip_index_1.R/bip_index_2.R/bip_index_2.R_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_middle_0.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_middle_0.R/bip_middle_1.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_middle_0.R/bip_middle_1.R/bip_middle_2.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_middle_0.R/bip_middle_1.R/bip_middle_2.R/bip_middle_2.R_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_pinky_0.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_pinky_0.R/bip_pinky_1.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_pinky_0.R/bip_pinky_1.R/bip_pinky_2.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_pinky_0.R/bip_pinky_1.R/bip_pinky_2.R/bip_pinky_2.R_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_ring_0.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_ring_0.R/bip_ring_1.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_ring_0.R/bip_ring_1.R/bip_ring_2.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_ring_0.R/bip_ring_1.R/bip_ring_2.R/bip_ring_2.R_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_thumb_0.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_thumb_0.R/bip_thumb_1.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_thumb_0.R/bip_thumb_1.R/bip_thumb_2.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKHand.R/bip_thumb_0.R/bip_thumb_1.R/bip_thumb_2.R/bip_thumb_2.R_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKKnee.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKKnee.L/bip_IKKnee.L_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKKnee.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_IKKnee.R/bip_IKKnee.R_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_hip.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_hip.L/bip_thigh.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_hip.L/bip_thigh.L/bip_shin.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_hip.L/bip_thigh.L/bip_shin.L/bip_shin.L_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_hip.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_hip.R/bip_thigh.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_hip.R/bip_thigh.R/bip_shin.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_hip.R/bip_thigh.R/bip_shin.R/bip_shin.R_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.L/bip_shoulder.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.L/bip_shoulder.L/bip_upperArm.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.L/bip_shoulder.L/bip_upperArm.L/bip_foreArm.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.L/bip_shoulder.L/bip_upperArm.L/bip_foreArm.L/weapon_bone.L
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.L/bip_shoulder.L/bip_upperArm.L/bip_foreArm.L/weapon_bone.L/weapon_bone.L_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.R/bip_shoulder.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.R/bip_shoulder.R/bip_upperArm.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.R/bip_shoulder.R/bip_upperArm.R/bip_foreArm.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.R/bip_shoulder.R/bip_upperArm.R/bip_foreArm.R/weapon_bone.R
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_collar.R/bip_shoulder.R/bip_upperArm.R/bip_foreArm.R/weapon_bone.R/weapon_bone.R_end
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_neck
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_neck/bip_neckupper
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_neck/bip_neckupper/bip_head
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_neck/bip_neckupper/bip_head/bip_jaw
    m_Weight: 1
  - m_Path: skeleton_sniper_model_skeleton/root/bip_pelvis/bip_spine_0/bip_spine_1/bip_spine_2/bip_spine_3/bip_neck/bip_neckupper/bip_head/bip_jaw/bip_jaw_end
    m_Weight: 1
