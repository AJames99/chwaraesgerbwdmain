#ifndef __C_HATCH_UTILS_
#define __C_HATCH_UTILS_

#ifndef PI_FLOAT
#define PI_FLOAT 3.141592f
#endif // PI_FLOAT

#ifndef Rad2Deg
#define Rad2Deg PI_FLOAT / 180.f
#endif

#ifndef Deg2Rad
#define Deg2Rad 180.f / PI_FLOAT
#endif

#ifndef BASEFOV_CHWSGRBWD
#define BASEFOV_CHWSGRBWD 120.0f
#endif

// Enables hue shifting:
#ifndef CROSSHATCH_USEHUESHIFT
//#define CROSSHATCH_USEHUESHIFT
#endif

// Enables crosshatch strength modifying:
#ifndef CROSSHATCH_USESTRENGTH
//#define CROSSHATCH_USESTRENGTH
#endif

#define CROSSHATCH_VIGNETTE_VERTICALBIAS 0.92

// X = hue shift per 1.0 change in value
// Y = saturation shift per 1.0 change in value
half2 _HueShiftParams;
//float _BaseVignetteMult;
//float _VignettePower;
// XY = translation offset, Z = rotation
float3 _GlobalSamplingOffset;
// Attempts to pin pattern sampling in place using camera rotation
float2 _GlobalCameraRotSampleOffset;
float _CrosshatchTiling;
float _CrosshatchHighlightTiling;

sampler2D _CrosshatchMapDiagonal;
float _CrosshatchPatternShift;
float _CrosshatchPatternBoundary;

float _CrosshatchHighlightsShift;
float _CrosshatchHighlightsBoundary;

// Should work for calculating relative FOV; change in size of 90 fov to 120 fov is the same for
// vertical and horizontal? V != H but the multiplicative factor V_mf == H_mf?? Double check
//inline half GetCameraFOV(in float cameraT)
//{
//	return atan(1.0f / cameraT) * 2.0f * Rad2Deg;
//}

//inline void ScaleVSCoordsByFOV(inout float2 uv)
//{
//	float fov = GetCameraFOV(unity_CameraProjection._m11);
//	uv *= float2(BASEFOV_CHWSGRBWD / fov, BASEFOV_CHWSGRBWD / fov);	
//}

#ifdef CROSSHATCH_USEHUESHIFT
// Takes an HSV colour and hue shifts it
inline void HueShiftColour(inout half3 colourHSV) {
	half deviationFromMiddle = clamp(colourHSV.z - 0.5, -1, 1);
	
	// Shift by deviation * hueShift, wrap to 0-1 range
	colourHSV.x = colourHSV.x + (deviationFromMiddle * _HueShiftParams.x);
	colourHSV.x -= floor(colourHSV.x);
	
	colourHSV.y = clamp(colourHSV.y * ((deviationFromMiddle * _HueShiftParams.y) + 1), 0, 1);
}
#endif

//inline float GetVignette(float2 uv) {
//	//uv.y *= CROSSHATCH_VIGNETTE_VERTICALBIAS;
//	float2 uvmod = uv * (1.0 - uv.yx);
//	float intensity = uvmod.x*uvmod.y * _BaseVignetteMult;
//	return pow(intensity, _VignettePower);
//}

// Takes 0-1 float2 UV and adjusts it such that the aspect ratio is considered
// i.e. sampling an image with these UVs will result in a SQUARE image, not a stretched one
float2 GetCHSamplingCoord(float2 uv) {
	uv.x *= (_ScreenParams.x / _ScreenParams.y);

	uv = (uv + _GlobalCameraRotSampleOffset) * _CrosshatchTiling;

	// Rotate by _GlobalSamplingOffset.z - randomised rotation offset
	{
		float sinX = sin(_GlobalSamplingOffset.z);
		float cosX = cos(_GlobalSamplingOffset.z);
		float2x2 rotMat = float2x2(cosX, sinX, -sinX, cosX);
		uv = mul(uv, rotMat);
	}

	// Offset also by the periodically randomised offset for that "re-drawn" pattern look
	uv += _GlobalSamplingOffset;

	return uv;
}
/*
	"ExpBlend" Function
	c = (2 / (1 - s)) - 1
	f(x,n) = (x^c)/(n^(c-1))

	for x <= p:
		f(x,p)
	for x > p:
		1-f(1-x, 1-p)

	branching is avoided via some clever int trickery
*/
inline float ExpBlend(float x, float p, float s)
{
	float c = (2 / (1 - s)) - 1;

	return pow(x,c) / pow(p, c-1);
}

// Gets a factor to be multiplied onto the lighting values used to determine crosshatch strength.
// Based on occlusion mapping and extra params.
float GetCrosshatchShadingFactor(float occlusionLevel, float bias, float steepness)
{
	float diff = occlusionLevel - bias;
	int j = sign(diff);
	int i = min(j, 0);
	int k = (i * 2) + 1;
	i *= -1;

	return saturate(i + k * ExpBlend(i + k * occlusionLevel, i + k * bias, steepness));
}

float CalculateLuminance(float3 colourLinear)
{
  // https://en.wikipedia.org/wiki/Relative_luminance
  return colourLinear.x * 0.2126 +
    colourLinear.y * 0.7152 +
    colourLinear.z * 0.0722;
}

float2 RotateUV90(float2 uv) {
	float sinX = sin(PI_FLOAT / 2);
	float cosX = cos(PI_FLOAT / 2);
	float2x2 rotMat = float2x2(cosX, sinX, -sinX, cosX);
	uv = mul(uv, rotMat);
	return uv;
}

float2 GetCrosshatching(float lightingValueDiffuse, float lightingValueSpecular, float2 chUV, float shift)
{
	float hatchBlend = (lightingValueDiffuse * shift) + (1 - shift);
	
	// Sample the hatch map for diagonal and mixed hatching
	float hatchDiagonal = tex2D(_CrosshatchMapDiagonal, chUV).a;
	float hatchMixed = tex2D(_CrosshatchMapDiagonal, RotateUV90(chUV)).a;
	hatchMixed = (max(hatchMixed, hatchDiagonal) + ((hatchMixed + hatchDiagonal) / 2)) / 2;

	// Sample again for the specular highlights with different tiling
	float hatchDiagonalSpec = tex2D(_CrosshatchMapDiagonal, chUV * _CrosshatchHighlightTiling).a;
	float hatchMixedSpec = tex2D(_CrosshatchMapDiagonal, RotateUV90(chUV * _CrosshatchHighlightTiling)).a;
	hatchMixedSpec = (max(hatchMixedSpec, hatchDiagonalSpec) + ((hatchMixedSpec + hatchDiagonalSpec) / 2)) / 2;

	float patternBlend = smoothstep(
		_CrosshatchPatternShift + _CrosshatchPatternBoundary,
		_CrosshatchPatternShift - _CrosshatchPatternBoundary,
		hatchBlend
	);
	float finalHatchSample = lerp(hatchDiagonal, hatchMixed, saturate(patternBlend));
	return float2(
		step(finalHatchSample, hatchBlend),
		step((1.001 - hatchMixedSpec) * (1 - smoothstep(_CrosshatchHighlightsShift - _CrosshatchHighlightsBoundary, _CrosshatchHighlightsShift + _CrosshatchHighlightsBoundary, lightingValueSpecular)), lightingValueSpecular)
	);
}

#endif // ifndef __C_HATCH_UTILS_