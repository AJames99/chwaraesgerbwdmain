//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

void MainLight_half(float3 WorldPos, float2 lightmapUV, float3 WorldNrm, out half3 Direction, out half3 Color, out half DistanceAtten, out half ShadowAtten)
{
#ifdef SHADERGRAPH_PREVIEW
	Direction = half3(0.5, 0.5, 0);
	Color = 1;
	DistanceAtten = 1;
	ShadowAtten = 1;
#else

	// OLD
#if SHADOWS_SCREEN
	// Shadow cascades are defined
	half4 clipPos = TransformWorldToHClip(WorldPos);
	half4 shadowCoord = ComputeScreenPos(clipPos);
#else
	// Shadow cascades are NOT defined
	half4 shadowCoord = TransformWorldToShadowCoord(WorldPos);
#endif

	//Light mainLight = GetMainLight(shadowCoord, WorldPos, shadowMask);
	Light mainLight = GetMainLight(shadowCoord);
	Direction = mainLight.direction;
	Color = mainLight.color;
	DistanceAtten = mainLight.distanceAttenuation;
	ShadowAtten = mainLight.shadowAttenuation;
	/*
#if defined(_SHADOW_MASK_DISTANCE)
#define SHADOWS_SHADOWMASK
#endif


	//#if defined(LIGHTMAP_ON) && defined(SHADOWS_SHADOWMASK)
#if defined(LIGHTMAP_ON)
	//ShadowAtten = UnitySampleBakedOcclusion(lightmapUV.xy, WorldPos).r;
	//ShadowAtten = GetMainLightShadowParams().x;
	//ShadowAtten = SampleScreenSpaceShadowmap(shadowCoord); //Black
	//ShadowAtten = MainLightRealtimeShadow(shadowCoord); //Realtime only
	half4 shadowMask = SAMPLE_SHADOWMASK(lightmapUV.xy);
	//ShadowAtten = BakedShadow(shadowMask, unity_ProbesOcclusion); //BakedShadow takes (half4 shadowMask, half4 occlusionProbeChannels)
	//ShadowAtten = MainLightShadow(shadowCoord, WorldPos, shadowMask, unity_ProbesOcclusion);
	
	ShadowAtten = SampleLightmap(lightmapUV.xy, WorldNrm);
#endif
*/

#endif
}