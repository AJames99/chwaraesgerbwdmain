using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering.RendererUtils;

public class CHLightingPrePass : ScriptableRenderPass {
	// The profiler tag that will show up in the frame debugger.
	const string ProfilerTag = "Crosshatch Lighting Prepass";

	// We will store our pass settings in this variable.
	CHAutoExposureSRPFeature.PassSettings passSettings;

	RTHandle colorBuffer;
	RenderTargetIdentifier temporaryBuffer;
	int temporaryBufferID = Shader.PropertyToID("Result");

	//Material material;

	RenderTargetIdentifier lightingDataRTIdentifier;
	static RenderTexture baseRenderRT;
	static RenderTexture averageLumRT;
	//static RenderTexture temporaryRT;

	ComputeShader exposureComputeShader;
	int kernelId;
	uint groupSizeX, groupSizeY;
	
	// It is good to cache the shader property IDs here.
	static readonly int BlurStrengthProperty = Shader.PropertyToID("_BlurStrength");

	//static ShaderTagId CHPrePassShaderTagID = new ShaderTagId("CHPrePass");
	static ShaderTagId CHPrePassShaderNameID = new ShaderTagId("CHLightingInfo");

	// The constructor of the pass. Here you can set any material properties that do not need to be updated on a per-frame basis.
	public CHLightingPrePass(CHAutoExposureSRPFeature.PassSettings passSettings) {
		this.passSettings = passSettings;

		// Set the render pass event.
		renderPassEvent = passSettings.renderPassEvent;

		// We create a material that will be used during our pass. You can do it like this using the 'CreateEngineMaterial' method, giving it
		// a shader path as an input or you can use a 'public Material material;' field in your pass settings and access it here through 'passSettings.material'.
		//if (material == null) material = CoreUtils.CreateEngineMaterial("Hidden/Box Blur");

		// Set any material properties based on our pass settings. 
		//material.SetInt(BlurStrengthProperty, passSettings.blurStrength);
		//lightingDataRT = passSettings.lightingDataRT;
		Camera mainCam = Camera.main;
		baseRenderRT =	new RenderTexture(mainCam.pixelWidth, mainCam.pixelHeight, 1, RenderTextureFormat.ARGB32);
		baseRenderRT.enableRandomWrite = true;
		baseRenderRT.Create();

		//temporaryRT =	new RenderTexture(mainCam.pixelWidth, mainCam.pixelHeight, 1, RenderTextureFormat.ARGB32);
		//temporaryRT.enableRandomWrite = true;
		//temporaryRT.Create();

		averageLumRT = passSettings.averageRT;

		exposureComputeShader = passSettings.exposureCalcComputeShader;

		Shader.SetGlobalTexture("_BaseRender", baseRenderRT);

		kernelId = exposureComputeShader.FindKernel("CalculateExposure");

		exposureComputeShader.GetKernelThreadGroupSizes(kernelId, out uint sizeX, out uint sizeY, out var _);
		groupSizeX = sizeX;
		groupSizeY = sizeY;
	}

	// Gets called by the renderer before executing the pass.
	// Can be used to configure render targets and their clearing state.
	// Can be user to create temporary render target textures.
	// If this method is not overriden, the render pass will render to the active camera render target.
	public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData) {
		// Grab the camera target descriptor. We will use this when creating a temporary render texture.
		RenderTextureDescriptor descriptor = renderingData.cameraData.cameraTargetDescriptor;

		// Set the number of depth bits we need for our temporary render texture.
		descriptor.depthBufferBits = 0;

		// Enable these if your pass requires access to the CameraDepthTexture or the CameraNormalsTexture.
		ConfigureInput(ScriptableRenderPassInput.Depth);
		ConfigureInput(ScriptableRenderPassInput.Normal);

		// Grab the color buffer from the renderer camera color target.
		colorBuffer = renderingData.cameraData.renderer.cameraColorTargetHandle;

		// Create a temporary render texture using the descriptor from above.
		descriptor.enableRandomWrite = true;
		descriptor.msaaSamples = 1;
		descriptor.useMipMap = true;
		cmd.GetTemporaryRT(temporaryBufferID, descriptor, FilterMode.Bilinear);
		temporaryBuffer = new RenderTargetIdentifier(temporaryBufferID);

		// Set RT dimensions to camera dimensions
		//lightingDataRT.width = renderingData.cameraData.camera.pixelWidth;
		//lightingDataRT.height = renderingData.cameraData.camera.pixelHeight;
		//lightingDataRTIdentifier = new RenderTargetIdentifier(passSettings.lightingDataRT);
	}

	// The actual execution of the pass. This is where custom rendering occurs.
	public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData) {
		// Grab a command buffer. We put the actual execution of the pass inside of a profiling scope.
		CommandBuffer cmd = CommandBufferPool.Get("Crosshatch PrePass Commands");
		using (new ProfilingScope(cmd, new ProfilingSampler(ProfilerTag))) {
			Blit(cmd, colorBuffer, baseRenderRT);
			Blit(cmd, colorBuffer, temporaryBuffer);

			// Should already be set
			//exposureComputeShader.SetTexture(kernelId, "Result", temporaryBuffer);
			//exposureComputeShader.SetTexture(kernelId, "Result", temporaryRT);
			cmd.SetComputeTextureParam(exposureComputeShader, kernelId, "Result", temporaryBuffer);
			cmd.DispatchCompute(exposureComputeShader, kernelId,
				Mathf.CeilToInt(baseRenderRT.width / groupSizeX),
				Mathf.CeilToInt(baseRenderRT.height / groupSizeY),
				1);

			Blit(cmd, temporaryBuffer, averageLumRT);

			cmd.SetGlobalTexture("_AverageLuminanceImage", averageLumRT);
		}

		// Execute the command buffer and release it.
		context.ExecuteCommandBuffer(cmd);
		CommandBufferPool.Release(cmd);

		/*Texture2D tex = new Texture2D(1, 1, TextureFormat.RGBA32, false);
		RenderTexture.active = lightingDataRT;
		Rect readPic = new Rect(0, 0, lightingDataRT.width, lightingDataRT.height);
		tex.ReadPixels(readPic, 0, 0);
		tex.Apply();
		Shader.SetGlobalFloat("_TESTLuminance", tex.GetPixel(0,0).r);*/
		//Shader.SetGlobalTexture("_CrosshatchRT", lightingDataRT);
	}

	// Called when the camera has finished rendering.
	// Here we release/cleanup any allocated resources that were created by this pass.
	// Gets called for all cameras i na camera stack.
	public override void OnCameraCleanup(CommandBuffer cmd) {
		if (cmd == null) throw new ArgumentNullException("cmd");

		// Since we created a temporary render texture in OnCameraSetup, we need to release the memory here to avoid a leak.
		cmd.ReleaseTemporaryRT(temporaryBufferID);

		//Shader.SetGlobalTexture("_CrosshatchRT", lightingDataRT);
	}
}