using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering.RendererUtils;

public class BlitMainToOtherPass : ScriptableRenderPass {
	// The profiler tag that will show up in the frame debugger.
	const string ProfilerTag = "BlitMainToOtherPass";

	// We will store our pass settings in this variable.
	BlitMainToLightingData.PassSettings passSettings;

	RTHandle colorBuffer;
	static RenderTexture blitToRT;
	static string blitToName;

	// The constructor of the pass. Here you can set any material properties that do not need to be updated on a per-frame basis.
	public BlitMainToOtherPass(BlitMainToLightingData.PassSettings passSettings) {
		this.passSettings = passSettings;

		// Set the render pass event.
		renderPassEvent = passSettings.renderPassEvent;

		Camera mainCam = Camera.main;
		blitToRT = new RenderTexture(mainCam.pixelWidth, mainCam.pixelHeight, 1, RenderTextureFormat.ARGB32);
		//blitToRT.name = blitToName;
		if (!blitToRT.Create()) {
			Debug.LogError("Couldn't create RT " + "_LightingDataImage");
		}
		Shader.SetGlobalTexture("_LightingDataImage", blitToRT);
	}

	// Gets called by the renderer before executing the pass.
	// Can be used to configure render targets and their clearing state.
	// Can be user to create temporary render target textures.
	// If this method is not overriden, the render pass will render to the active camera render target.
	public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData) {
		// Enable these if your pass requires access to the CameraDepthTexture or the CameraNormalsTexture.
		//ConfigureInput(ScriptableRenderPassInput.Depth);
		//ConfigureInput(ScriptableRenderPassInput.Normal);

		// Grab the color buffer from the renderer camera color target.
		colorBuffer = renderingData.cameraData.renderer.cameraColorTargetHandle;
	}

	// The actual execution of the pass. This is where custom rendering occurs.
	public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData) {
		// Grab a command buffer. We put the actual execution of the pass inside of a profiling scope.
		CommandBuffer cmd = CommandBufferPool.Get("Crosshatch PrePass Commands");
		using (new ProfilingScope(cmd, new ProfilingSampler(ProfilerTag))) {
			Blit(cmd, colorBuffer, blitToRT);
			cmd.SetGlobalTexture("_LightingDataImage", blitToRT);
		}

		// Execute the command buffer and release it.
		context.ExecuteCommandBuffer(cmd);
		CommandBufferPool.Release(cmd);
	}

	public override void OnCameraCleanup(CommandBuffer cmd) {
		
	}
}