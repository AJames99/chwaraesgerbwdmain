using UnityEngine;
using UnityEngine.Rendering.Universal;

public class BlitMainToLightingData : ScriptableRendererFeature {

	// All this needs to do is take the rendered lighting data from the pass' shader
	// and run it via unity's histogram compute shader!!
	// take the result value and set that as a global param for the ch shader

	[System.Serializable]
	public class PassSettings {
		// Where/when the render pass should be injected during the rendering process.
		public RenderPassEvent renderPassEvent = RenderPassEvent.BeforeRenderingOpaques;

		// additional properties ...
	}

	// References to our pass and its settings.
	BlitMainToOtherPass pass;
	public PassSettings passSettings = new();

	// Gets called every time serialization happens.
	// Gets called when you enable/disable the renderer feature.
	// Gets called when you change a property in the inspector of the renderer feature.
	public override void Create() {
		// Pass the settings as a parameter to the constructor of the pass.
		pass = new BlitMainToOtherPass(passSettings);
	}

	// Injects one or multiple render passes in the renderer.
	// Gets called when setting up the renderer, once per-camera.
	// Gets called every frame, once per-camera.
	// Will not be called if the renderer feature is disabled in the renderer inspector.
	public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData) {
		// Here you can queue up multiple passes after each other.
		renderer.EnqueuePass(pass);
	}
}