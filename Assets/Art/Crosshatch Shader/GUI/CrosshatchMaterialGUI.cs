using UnityEditor;
using UnityEngine;

public class CrosshatchMaterialGUI : ShaderGUI {
	public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties) {
		base.OnGUI(materialEditor, properties);
	}

	public override void ValidateMaterial(Material material) {
		base.ValidateMaterial(material);

		material.EnableKeyword("_NORMALMAP");
	}

	public override void OnClosed(Material material) {
		base.OnClosed(material);

		material.EnableKeyword("_NORMALMAP");
	}
}