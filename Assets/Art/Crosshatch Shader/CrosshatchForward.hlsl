//#ifndef UNIVERSAL_FORWARD_LIT_PASS_INCLUDED
//#define UNIVERSAL_FORWARD_LIT_PASS_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
#include "CrosshatchUtils.hlsl"

// GLES2 has limited amount of interpolators
#if defined(_PARALLAXMAP) && !defined(SHADER_API_GLES)
#define REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR
#endif

#if (defined(_NORMALMAP) || (defined(_PARALLAXMAP) && !defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR))) || defined(_DETAIL)
#define REQUIRES_WORLD_SPACE_TANGENT_INTERPOLATOR
#endif

struct Attributes
{
	float4 positionOS   : POSITION;
	float3 normalOS     : NORMAL;
	float4 tangentOS    : TANGENT;
	float2 texcoord     : TEXCOORD0;
	float2 lightmapUV   : TEXCOORD1;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
	float2 uv                       : TEXCOORD0;
	DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

#if defined(REQUIRES_WORLD_SPACE_POS_INTERPOLATOR)
	float3 positionWS               : TEXCOORD2;
#endif

	float3 normalWS                 : TEXCOORD3;
#if defined(REQUIRES_WORLD_SPACE_TANGENT_INTERPOLATOR)
	float4 tangentWS                : TEXCOORD4;    // xyz: tangent, w: sign
#endif
	float3 viewDirWS                : TEXCOORD5;

	half4 fogFactorAndVertexLight   : TEXCOORD6; // x: fogFactor, yzw: vertex light

#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
	float4 shadowCoord              : TEXCOORD7;
#endif

#if defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	float3 viewDirTS                : TEXCOORD8;
#endif

	float4 positionCS               : SV_POSITION;
	UNITY_VERTEX_INPUT_INSTANCE_ID
		UNITY_VERTEX_OUTPUT_STEREO
};

struct LightingInfo {
	half3 lightDiffuse;
	half3 lightSpecular;
	// For understanding the intensity of the light
	half lightDiffuseIntensity;
	half lightSpecularIntensity;
};

//sampler2D _CrosshatchMapDiagonal;
sampler2D _CrosshatchMapMixed;
texture2D _RampTexture;
sampler2D _SpecularMap;
SamplerState _SamplerState_Linear_Clamp_sampler;
SamplerState _SamplerState_Point_Repeat_sampler;
float _SpecMultipler;
float _CrosshatchShift;
float _CrosshatchShiftAdditive;
float _CrosshatchTilingOverrideMultiplier;
//float _CrosshatchPatternShift;
//float _CrosshatchPatternBoundary;
// float _CrosshatchHighlightsShift;
// float _CrosshatchHighlightsBoundary;
float _RimStrength;
float _AmbientLightInfluence;
float _ShallowNormalLightingBoost;
float _ShallowNormalSharpness;
float2 _UniqueObjOffset;
float _CrosshatchStrength;
float _OcclusionStrengthNonAmbient;

float _CrosshatchOcclusionBias;
float _CrosshatchOcclusionSteepness;
float _CrosshatchOcclusionInfluence;
float _CrosshatchOcclusionOffset;

half3 Unity_ColorspaceConversion_RGB_HSV_half(half3 In)
{
	half4 K = half4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	half4 P = lerp(half4(In.bg, K.wz), half4(In.gb, K.xy), step(In.b, In.g));
	half4 Q = lerp(half4(P.xyw, In.r), half4(In.r, P.yzx), step(P.x, In.r));
	half D = Q.x - min(Q.w, Q.y);
	half  E = 1e-10;
	return half3(abs(Q.z + (Q.w - Q.y) / (6.0 * D + E)), D / (Q.x + E), Q.x);
}

half3 Unity_ColorspaceConversion_HSV_RGB_half(half3 In)
{
	half4 K = half4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	half3 P = abs(frac(In.xxx + K.xyz) * 6.0 - K.www);
	return In.z * lerp(K.xxx, saturate(P - K.xxx), In.y);
}

void InitializeInputData(Varyings input, half3 normalTS, out InputData inputData)
{
	inputData = (InputData)0;

#if defined(REQUIRES_WORLD_SPACE_POS_INTERPOLATOR)
	inputData.positionWS = input.positionWS;
#endif

	half3 viewDirWS = SafeNormalize(input.viewDirWS);
#if defined(_NORMALMAP) || defined(_DETAIL)
	float sgn = input.tangentWS.w;      // should be either +1 or -1
	float3 bitangent = sgn * cross(input.normalWS.xyz, input.tangentWS.xyz);
	inputData.normalWS = TransformTangentToWorld(normalTS, half3x3(input.tangentWS.xyz, bitangent.xyz, input.normalWS.xyz));
#else
	inputData.normalWS = input.normalWS;
#endif

	inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
	inputData.viewDirectionWS = viewDirWS;

#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
	inputData.shadowCoord = input.shadowCoord;
#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
	inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);
#else
	inputData.shadowCoord = float4(0, 0, 0, 0);
#endif

	inputData.fogCoord = input.fogFactorAndVertexLight.x;
	inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
	inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
	inputData.normalizedScreenSpaceUV = GetNormalizedScreenSpaceUV(input.positionCS);
	inputData.shadowMask = SAMPLE_SHADOWMASK(input.lightmapUV);
}


///////////////////////////////////////////////////////////////////////////////
//                        Lighting functions                                 //
///////////////////////////////////////////////////////////////////////////////

LightingInfo GetLightingStuff(InputData inputData, SurfaceData surfaceData/*, float occlusionMapSample*/)
{
	LightingInfo output;

	BRDFData brdfData;

	// NOTE: can modify alpha
	InitializeBRDFData(surfaceData.albedo, surfaceData.metallic, surfaceData.specular, surfaceData.smoothness, surfaceData.alpha, brdfData);


	BRDFData brdfDataClearCoat = (BRDFData)0;
#if defined(_CLEARCOAT) || defined(_CLEARCOATMAP)
	// base brdfData is modified here, rely on the compiler to eliminate dead computation by InitializeBRDFData()
	InitializeBRDFDataClearCoat(surfaceData.clearCoatMask, surfaceData.clearCoatSmoothness, brdfData, brdfDataClearCoat);
#endif

	// #### SHADOWS ####
	// To ensure backward compatibility we have to avoid using shadowMask input, as it is not present in older shaders
#if defined(SHADOWS_SHADOWMASK) && defined(LIGHTMAP_ON)
	half4 shadowMask = inputData.shadowMask;
#elif !defined (LIGHTMAP_ON)
	half4 shadowMask = unity_ProbesOcclusion;
#else
	half4 shadowMask = half4(1, 1, 1, 1);
#endif
	// #### MAIN LIGHT ####
	Light mainLight = GetMainLight(inputData.shadowCoord, inputData.positionWS, shadowMask);

	// #### AMBIENT OCCLUSION ####
#if defined(_SCREEN_SPACE_OCCLUSION)
	AmbientOcclusionFactor aoFactor = GetScreenSpaceAmbientOcclusion(inputData.normalizedScreenSpaceUV);
	mainLight.color *= aoFactor.directAmbientOcclusion;
	surfaceData.occlusion = min(surfaceData.occlusion, aoFactor.indirectAmbientOcclusion);
#endif

	/*
	MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI);
	half3 color = GlobalIllumination(brdfData, brdfDataClearCoat, surfaceData.clearCoatMask,
		inputData.bakedGI, surfaceData.occlusion,
		inputData.normalWS, inputData.viewDirectionWS);
	color += LightingPhysicallyBased(brdfData, brdfDataClearCoat,
		mainLight,
		inputData.normalWS, inputData.viewDirectionWS,
		surfaceData.clearCoatMask, 0);
	*/

	// N dot L Main
	half mainLightingFactorDiff = saturate(dot(inputData.normalWS, mainLight.direction)) * mainLight.distanceAttenuation * mainLight.shadowAttenuation;
	half3 lightingDiffuse = mainLight.color * mainLightingFactorDiff;

	// N dot H main
	half mainLightingFactorSpec = pow(
		saturate(dot(normalize(normalize(inputData.viewDirectionWS) + mainLight.direction), inputData.normalWS)), 
		max(surfaceData.smoothness * 100, 0)
	);
	mainLightingFactorSpec += pow((1.0 - saturate(dot(normalize(inputData.normalWS), normalize(inputData.viewDirectionWS)))), 1 / _RimStrength);	// Fresnel
	mainLightingFactorSpec *= mainLight.distanceAttenuation * mainLight.shadowAttenuation; // Attenuation
	half3 lightingSpecular = mainLight.color * mainLightingFactorSpec;

	// #### ADDITIONAL LIGHTS ####
	half3 additionalLightingFactor = 0, additionalLightingFactorSpec = 0;
#ifdef _ADDITIONAL_LIGHTS
	uint pixelLightCount = GetAdditionalLightsCount();
	for (uint lightIndex = 0u; lightIndex < pixelLightCount; ++lightIndex)
	{
		Light light = GetAdditionalLight(lightIndex, inputData.positionWS, shadowMask);

		//maybe remove this? idk
#if defined(_SCREEN_SPACE_OCCLUSION)
		light.color *= aoFactor.directAmbientOcclusion;
#endif
		//additionalLightingFactor += saturate(dot(inputData.normalWS, light.direction))  * light.distanceAttenuation * light.shadowAttenuation;
		//additionalLightingFactor += (light.color.r + light.color.g + light.color.b) / 3  * light.distanceAttenuation * light.shadowAttenuation; // v2 - dont bother with dot h
		//additionalLightingFactor += saturate(dot(inputData.normalWS, light.direction))  * light.distanceAttenuation * light.shadowAttenuation * (light.color.r + light.color.g + light.color.b) / 3; //v3 works but with no coloured point lights
		additionalLightingFactor += saturate(dot(inputData.normalWS, light.direction))  * light.distanceAttenuation * light.shadowAttenuation * light.color;

		half3 lightFactorSpec = pow(
			saturate(dot(normalize(normalize(inputData.viewDirectionWS) + light.direction), inputData.normalWS)),	// might need to remove the direction part?
			max(surfaceData.smoothness * 100, 0)
		);
		lightFactorSpec += pow((1.0 - saturate(dot(normalize(inputData.normalWS), normalize(inputData.viewDirectionWS)))), 1 / _RimStrength);	// Fresnel
		lightFactorSpec *= light.distanceAttenuation * light.shadowAttenuation * light.color; // Attenuation

		additionalLightingFactorSpec += lightFactorSpec;

		// this doesnt seem to be the dynamic shadows
		/*colour += LightingPhysicallyBased(brdfData, brdfDataClearCoat,
			light,
			inputData.normalWS, inputData.viewDirectionWS,
			surfaceData.clearCoatMask, specularHighlightsOff);*/
			//todo do light stuff
	}
#endif

	//add me back in
#ifdef _ADDITIONAL_LIGHTS_VERTEX
	color += inputData.vertexLighting * brdfData.diffuse;
#endif

	// V1
	//output.lightDiffuse = additionalLightingFactor + mainLightingFactorDiff;
	//
	//output.lightSpecular = additionalLightingFactorSpec + mainLightingFactorSpec;
	//output.lightSpecular *= SurfaceData.emission;
	// V2 - point colour should work

	// Full colour
	//float offFactor = saturate((occlusionMapSample * _OcclusionStrength) - _OcclusionStrength + 1) * _AmbientLightInfluence;

	//output.lightDiffuse = ((lightingDiffuse + additionalLightingFactor) * offFactor) + (inputData.bakedGI.rgb * offFactor * _AmbientLightInfluence);/* + (UNITY_LIGHTMODEL_AMBIENT.xyz);*/ /** _AmbientLightInfluence*/
	//output.lightDiffuse = (lightingDiffuse + additionalLightingFactor) + (inputData.bakedGI.rgb * offFactor);/* + (UNITY_LIGHTMODEL_AMBIENT.xyz);*/ /** _AmbientLightInfluence*/
	output.lightDiffuse = (lightingDiffuse + additionalLightingFactor) + (inputData.bakedGI.rgb * _AmbientLightInfluence);
	output.lightSpecular = lightingSpecular + additionalLightingFactorSpec;

	// Intensities
	output.lightDiffuseIntensity = Unity_ColorspaceConversion_RGB_HSV_half(output.lightDiffuse).z;
	output.lightSpecularIntensity = Unity_ColorspaceConversion_RGB_HSV_half(output.lightSpecular).z;

	return output;
}

///////////////////////////////////////////////////////////////////////////////
//                  Vertex and Fragment functions                            //
///////////////////////////////////////////////////////////////////////////////

// Used in Standard (Physically Based) shader
Varyings LitPassVertex(Attributes input)
{
	Varyings output = (Varyings)0;

	UNITY_SETUP_INSTANCE_ID(input);
	UNITY_TRANSFER_INSTANCE_ID(input, output);
	UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

	VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

	// normalWS and tangentWS already normalize.
	// this is required to avoid skewing the direction during interpolation
	// also required for per-vertex lighting and SH evaluation
	VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

	half3 viewDirWS = GetWorldSpaceViewDir(vertexInput.positionWS);
	half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
	half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

	output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);

	// already normalized from normal transform to WS.
	output.normalWS = normalInput.normalWS;
	output.viewDirWS = viewDirWS;
#if defined(REQUIRES_WORLD_SPACE_TANGENT_INTERPOLATOR) || defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	real sign = input.tangentOS.w * GetOddNegativeScale();
	half4 tangentWS = half4(normalInput.tangentWS.xyz, sign);
#endif
#if defined(REQUIRES_WORLD_SPACE_TANGENT_INTERPOLATOR)
	output.tangentWS = tangentWS;
#endif

#if defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	half3 viewDirTS = GetViewDirectionTangentSpace(tangentWS, output.normalWS, viewDirWS);
	output.viewDirTS = viewDirTS;
#endif

	OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
	OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

	output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);

#if defined(REQUIRES_WORLD_SPACE_POS_INTERPOLATOR)
	output.positionWS = vertexInput.positionWS;
#endif

#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
	output.shadowCoord = GetShadowCoord(vertexInput);
#endif

	output.positionCS = vertexInput.positionCS;

	return output;
}

// Used in Standard (Physically Based) shader
half4 LitPassFragment(Varyings input) : SV_Target
{
	UNITY_SETUP_INSTANCE_ID(input);
	UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

#if defined(_PARALLAXMAP)
#if defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	half3 viewDirTS = input.viewDirTS;
#else
	half3 viewDirTS = GetViewDirectionTangentSpace(input.tangentWS, input.normalWS, input.viewDirWS);
#endif
	ApplyPerPixelDisplacement(viewDirTS, input.uv);
#endif

	SurfaceData surfaceData;
	InitializeStandardLitSurfaceData(input.uv, surfaceData);

	InputData inputData;
	InitializeInputData(input, surfaceData.normalTS, inputData);
	
	LightingInfo lighting = GetLightingStuff(inputData, surfaceData);

#if !defined(_SHALLOWNORMALBOOSTENABLED_OFF)
	// Boost lighting based on fragment normal dot view dir
	// Parameter is in a -1 to 1 scale
	//half shallowAngleIntensity = (1 - dot(input.normalWS, input.viewDirWS)) * _ShallowNormalLightingBoost;
	half shallowAngleIntensity = pow(1 - dot(normalize(input.normalWS), normalize(input.viewDirWS)), _ShallowNormalSharpness) * _ShallowNormalLightingBoost;
	lighting.lightDiffuseIntensity += shallowAngleIntensity;
	//return half4(max(lighting.lightDiffuseIntensity, 0), 0.0, min(lighting.lightDiffuseIntensity, 0),1.0);
	//return half4(max(shallowAngleIntensity, 0), 0.0, abs(min(shallowAngleIntensity, 0)), 1.0);
#endif

	// --- TODO: input.uv is prbably the same as this?? ----
	// -----------------
	// New UV coordinate system:
	float2 screenUVBase = input.positionCS.xy / _ScreenParams.xy;

	//half4 prepassLightingData = tex2D(_CrosshatchRT, screenUVBase);
	//lighting.lightDiffuseIntensity = prepassLightingData.r;
	//lighting.lightSpecularIntensity = prepassLightingData.g;
	//color = tex2D(_CrosshatchRT, screenUVBase);

	half4 color = half4(surfaceData.albedo, surfaceData.alpha); // Moved to point sample

	// Previous method of getting view space coords:
	//float3 positionVS_base = TransformWorldToView(input.positionWS) / input.positionCS.w;
	// new method is now up top

	// PositionVS is only used for sampling (currently); modify it however is desired
	float2 screenUV = GetCHSamplingCoord(screenUVBase);

	// Offset by the object offset (prevents continuum of pattern between touching objects),
	screenUV += _UniqueObjOffset;

	// lighting.colour is kinda borked rn, so saturate it //23/01/2021 should be fixed?
	//half3 sampledSpecular = tex2D(_SpecularMap, input.uv) * _SpecMultipler * _SpecColor * saturate(lighting.lightSpecular);
	half3 sampledSpecular = tex2D(_SpecularMap, input.uv) * _SpecMultipler * _SpecColor * lighting.lightSpecular;
	half sampledSpecularIntensity = tex2D(_SpecularMap, input.uv) * _SpecMultipler * lighting.lightSpecularIntensity;

	// Lighting ramp
	// LOD is important here, to stop the texture being treated as distance-LOD and lowering the samplingresolution 
	half4 rampDiffuse = SAMPLE_TEXTURE2D_LOD(_RampTexture, _SamplerState_Linear_Clamp_sampler, float2(lighting.lightDiffuseIntensity, 1), 0);
	//half4 rampSpecular = SAMPLE_TEXTURE2D_LOD(_RampTexture, _SamplerState_Linear_Clamp_sampler, float2(lighting.lightSpecularIntensity * lighting.lightDiffuseIntensity, 0), 0); //v1
	half4 rampSpecular = SAMPLE_TEXTURE2D_LOD(_RampTexture, _SamplerState_Linear_Clamp_sampler, float2(sampledSpecularIntensity * lighting.lightDiffuseIntensity, 0), 0); //v2

	// color += rampSpecular 	// This is the previous method; simply add the specular.

	// For crosshatching
	float occlusion = saturate(_OcclusionMap.Sample(sampler_OcclusionMap, input.uv).r + _CrosshatchOcclusionOffset);
	float dramaticShadingFactor = GetCrosshatchShadingFactor(occlusion, _CrosshatchOcclusionBias, _CrosshatchOcclusionSteepness);
	dramaticShadingFactor = (_CrosshatchOcclusionInfluence * dramaticShadingFactor) + 1 - _CrosshatchOcclusionInfluence;

	// Moved this to CrosshatchUtils
	/*float hatchBlend = ((rampSpecular + rampDiffuse) * _CrosshatchShift * dramaticShadingFactor) + _CrosshatchShiftAdditive;
	float hatchDiagonal = tex2D(_CrosshatchMapDiagonal, screenUV).a;
	float hatchMixed = tex2D(_CrosshatchMapMixed, screenUV).a;
	//float hatchSample = lerp(hatchDiagonal, hatchMixed, saturate((hatchBlend + _CrosshatchPatternShift - 0.5) * _CrosshatchPatternBoundary));
	// Blends between the diagonal and crossed patterns based on a smooth-ish boundary.
	float patternBlend = smoothstep(
		_CrosshatchPatternShift + _CrosshatchPatternBoundary,
		_CrosshatchPatternShift - _CrosshatchPatternBoundary,
		hatchBlend
	);
	float hatchSample = lerp(hatchDiagonal, hatchMixed, saturate(patternBlend));
	hatchSample = step(hatchSample, hatchBlend);
	*/
	float lightingValueCH = ((rampSpecular + rampDiffuse) * dramaticShadingFactor);
	float2 hatchSample = GetCrosshatching(lightingValueCH, rampSpecular, screenUV * _CrosshatchTilingOverrideMultiplier, _CrosshatchShift);

	/*
	// Apply strength modifier
	#ifdef CROSSHATCH_USESTRENGTH
	hatchSample = (hatchSample * _CrosshatchStrength) - _CrosshatchStrength + 1;
	hatchMixed = (hatchMixed * _CrosshatchStrength) - _CrosshatchStrength + 1;
	#endif
	
	// Apply Hue Shift
	#ifdef CROSSHATCH_USEHUESHIFT
	half3 colourHSV = Unity_ColorspaceConversion_RGB_HSV_half(color); // Get HSV colour of albedo
	half originalValue = colourHSV.z;
	colourHSV.z = lighting.lightDiffuseIntensity; // Multiply value by lighting intensity
	colourHSV.z += lighting.lightSpecularIntensity;
	HueShiftColour(colourHSV);
	color.rgb = Unity_ColorspaceConversion_HSV_RGB_half(half3(colourHSV.rg, originalValue));
	#endif
	*/

	// OLD
	//color += rampSpecular * smoothstep(hatchMixed * step(rampSpecular, 0.9), 0.45, 0.55);				// v1
	//color += rampSpecular * step(hatchDiagonal * (1 - smoothstep(0.5, 0.55, rampSpecular)), 0.5);		// v2
	//color += step(hatchDiagonal * (1 - smoothstep(0.5, 0.55, rampSpecular)), rampSpecular);						// v3
	//color += step(hatchMixed * (1 - smoothstep(_CrosshatchHighlightsShift - _CrosshatchHighlightsBoundary, _CrosshatchHighlightsShift + _CrosshatchHighlightsBoundary, rampSpecular)), rampSpecular); // v4 parameterised, removed sampledSpecular
	
	// NEW
	// Highlight CH
	//color += step((1.001 - hatchMixed) * (1 - smoothstep(_CrosshatchHighlightsShift - _CrosshatchHighlightsBoundary, _CrosshatchHighlightsShift + _CrosshatchHighlightsBoundary, rampSpecular)), rampSpecular); // v5.1 - fixed speckles
	// Shade CH
	//color *= hatchSample;

	color += hatchSample.y;
	color *= hatchSample.x;
	
	// Experimental - adding in point light colours by removing their value/luminance component
	half3 pureLightingColour = Unity_ColorspaceConversion_RGB_HSV_half(lighting.lightDiffuse + sampledSpecular);
	pureLightingColour.z = 1.0; // Set saturation to 1
	pureLightingColour = Unity_ColorspaceConversion_HSV_RGB_half(pureLightingColour);
	color *= half4(pureLightingColour, 1.0);
	

	color.rgb = MixFog(color.rgb, inputData.fogCoord);
	//color.a = OutputAlpha(color.a, _Surface); // This always seems to assume we're not an alpha-using surface; removed.

	color.a = surfaceData.alpha;
	clip(color.a - _Cutoff);

	return color;
}
/*
half4 ApplyCrosshatchFragment(Varyings input) : SV_Target {
	// just read back for now
	float2 screenUVBase = input.positionCS.xy / _ScreenParams.xy;
	half4 lightingDataSample = tex2D(_LightingDataImage, screenUVBase);
	half4 baseColourSample = tex2D(_BaseRender, screenUVBase);
	float averageLuminance = max(tex2D(_AverageLuminanceImage, screenUVBase).r, 0.0001);

	// PositionVS is only used for sampling (currently); modify it however is desired
	float2 screenUV = GetCHSamplingCoord(screenUVBase);

	// Offset by the object offset (prevents continuum of pattern between touching objects),
	screenUV += _UniqueObjOffset;


	// y = -(2x - 1)^2 + 1
	//float totalLighting = lightingDataSample.r;
	float t = 0.1;
	float totalLighting = (t * (lightingDataSample.r / averageLuminance)) + ((1 - t) * lightingDataSample);

	float hatchBlend = (totalLighting) * _CrosshatchShift;
	float hatchDiagonal = tex2D(_CrosshatchMapDiagonal, screenUV).a;
	float hatchMixed = tex2D(_CrosshatchMapMixed, screenUV).a;
	//float hatchSample = lerp(hatchDiagonal, hatchMixed, saturate((hatchBlend + _CrosshatchPatternShift - 0.5) * _CrosshatchPatternBoundary));
	// Blends between the diagonal and crossed patterns based on a smooth-ish boundary.
	float patternBlend = smoothstep(
		_CrosshatchPatternShift + _CrosshatchPatternBoundary,
		_CrosshatchPatternShift - _CrosshatchPatternBoundary,
		hatchBlend
	);
	float hatchSample = lerp(hatchDiagonal, hatchMixed, saturate(patternBlend));
	hatchSample = step(hatchSample, hatchBlend);

	baseColourSample *= hatchSample;
	//baseColourSample.rgb = totalLighting;
	return baseColourSample;
}
*/

/*
half4 DrawLightingData(Varyings input) : SV_Target {
		UNITY_SETUP_INSTANCE_ID(input);
	UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

#if defined(_PARALLAXMAP)
#if defined(REQUIRES_TANGENT_SPACE_VIEW_DIR_INTERPOLATOR)
	half3 viewDirTS = input.viewDirTS;
#else
	half3 viewDirTS = GetViewDirectionTangentSpace(input.tangentWS, input.normalWS, input.viewDirWS);
#endif
	ApplyPerPixelDisplacement(viewDirTS, input.uv);
#endif

	SurfaceData surfaceData;
	InitializeStandardLitSurfaceData(input.uv, surfaceData);

	InputData inputData;
	InitializeInputData(input, surfaceData.normalTS, inputData);

 	LightingInfo lighting = GetLightingStuff(inputData, surfaceData);

#if !defined(_SHALLOWNORMALBOOSTENABLED_OFF)
	// Boost lighting based on fragment normal dot view dir
	// Parameter is in a -1 to 1 scale
	half shallowAngleIntensity = pow(1 - dot(normalize(input.normalWS), normalize(input.viewDirWS)), _ShallowNormalSharpness) * _ShallowNormalLightingBoost;
	lighting.lightDiffuseIntensity += shallowAngleIntensity;
#endif

	half3 sampledSpecular = tex2D(_SpecularMap, input.uv) * _SpecMultipler * _SpecColor * lighting.lightSpecular;
	half sampledSpecularIntensity = tex2D(_SpecularMap, input.uv) * _SpecMultipler * lighting.lightSpecularIntensity;

	// Lighting ramp
	// LOD is important here, to stop the texture being treated as distance-LOD and lowering the samplingresolution 
	half4 rampDiffuse = SAMPLE_TEXTURE2D_LOD(_RampTexture, _SamplerState_Linear_Clamp_sampler, float2(lighting.lightDiffuseIntensity, 1), 0);
	//half4 rampSpecular = SAMPLE_TEXTURE2D_LOD(_RampTexture, _SamplerState_Linear_Clamp_sampler, float2(lighting.lightSpecularIntensity * lighting.lightDiffuseIntensity, 0), 0); //v1
	half4 rampSpecular = SAMPLE_TEXTURE2D_LOD(_RampTexture, _SamplerState_Linear_Clamp_sampler, float2(sampledSpecularIntensity * lighting.lightDiffuseIntensity, 0), 0); //v2

	half rampDiffuseAvg = dot(rampDiffuse.rgb, half3(1,1,1)) / 3;
	half rampSpecAvg = dot(rampSpecular.rgb, half3(1,1,1)) / 3;

	return half4(rampDiffuseAvg, rampSpecAvg, 0.0, 1.0);
}*/

//#endif
