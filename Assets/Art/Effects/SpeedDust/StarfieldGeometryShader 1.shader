// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Geometry/StarFieldShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_ExtrusionFactor("Extrusion factor", float) = 0
		_Velocity("Reference velocity", Vector) = (0,0,0)
		_ParticleScale("Particle size", float) = 3
		_DebugThing("thing", float) = 0
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			// make fog work
			//#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2g
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct g2f
			{
				float2 uv : TEXCOORD0;
				//UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _ExtrusionFactor;
			int _StarCount;
			float3 _Velocity;
			float _StarThreshold;
			float _ParticleScale;
			float _DebugThing;

			v2g vert(appdata v)
			{
				v2g o;
				o.vertex = v.vertex;
				o.uv = v.uv;
				o.normal = v.normal;
				return o;
			}

			float pseudorandomPos(float3 position) {
				float xt = (cos(radians(360 * 8 * position.x)) + 1) / 2;
				float yt = (sin(radians(360 * 8 * position.y)) + 1) / 2;
				float zt = abs(sin(radians(360 * 8 * position.z))) % 1;
				return (xt + yt + zt) % 1;
			}

			float4 billboard(float4 positionIn) {
				return mul(UNITY_MATRIX_P,
					mul(UNITY_MATRIX_MV, float4(0.0, 0.0, 0.0, 1.0))
					+ float4(positionIn.x, positionIn.y, 0.0, 0.0)
					* float4(_ParticleScale, _ParticleScale, 1.0, 1.0));
			}

			/*float4 rotatePointAbout(float4 vert, float3 Axis, float radians) {
				float s = sin(radians);
				float c = cos(radians);
				float one_minus_c = 1.0 - c;
				float3x3 rot_mat =
				{ one_minus_c * Axis.x * Axis.x + c, one_minus_c * Axis.x * Axis.y - Axis.z * s, one_minus_c * Axis.z * Axis.x + Axis.y * s,
					one_minus_c * Axis.x * Axis.y + Axis.z * s, one_minus_c * Axis.y * Axis.y + c, one_minus_c * Axis.y * Axis.z - Axis.x * s,
					one_minus_c * Axis.z * Axis.x - Axis.y * s, one_minus_c * Axis.y * Axis.z + Axis.x * s, one_minus_c * Axis.z * Axis.z + c
				};

				return float4(mul(rot_mat, vert.xyz), vert.w);
			}*/

			#define PLANESCALE 0.05

			[maxvertexcount(6)]
			void geom(triangle v2g IN[3], inout TriangleStream<g2f> triStream)
			{
				const float4 quadPosTwoD[6] = {
					float4(-1.0,-1.0,0,0) * PLANESCALE,
					float4(-1.0,1.0,0,0) * PLANESCALE,
					float4(1.0,1.0,0,0) * PLANESCALE, // end quad 1
					float4(-1.0,-1.0,0,0) * PLANESCALE,
					float4(1.0,1.0,0,0) * PLANESCALE,
					float4(1.0,-1.0,0,0) * PLANESCALE //end quad 2
				};

				g2f o;
				
				float4 origin = IN[0].vertex * 2;
				//float rotation = radians(_DebugThing);
				float rotation = -atan2(origin.x, origin.y);

				// 2 quads
				for (int i = 0; i < 2; i++) {
					// 3 verts
					for (int v = 0; v < 3; v++) {
						
						//float4 lookingAtVelVector = float4(mul(rot_mat, quadPosTwoD[(3 * i) + v].xyz), 1.0);
						float4 lookingAtVelVector = quadPosTwoD[(3 * i) + v];
						
						//o.vertex = billboard(quadPosTwoD[(3 * i) + v]) + origin;
						//o.vertex = UnityObjectToClipPos(quadPosTwoD[(3 * i) + v] + origin);
						//o.vertex = billboard(quadPosTwoD[(3 * i) + v]) + UnityObjectToClipPos(origin);
						o.vertex = UnityObjectToClipPos(lookingAtVelVector + origin);
						o.uv = TRANSFORM_TEX(IN[i].uv, _MainTex);
						o.color = fixed4(1.0, 1.0, 1.0, 1.0);
						triStream.Append(o);
					}

					triStream.RestartStrip();
				}

				/*float4 barycentre = (IN[0].vertex + IN[1].vertex + IN[2].vertex) / 3;
				float3 normal = (IN[0].normal + IN[1].normal + IN[2].normal) / 3;
				float pseudorand = pseudorandomPos(mul(unity_ObjectToWorld, barycentre));

				for (int i = 0; i < 3; i++)	{
					int next = (i + 1) % 3;
					o.vertex = UnityObjectToClipPos(IN[i].vertex);
					o.uv = TRANSFORM_TEX(IN[i].uv, _MainTex);
					o.color = fixed4(0.0, 0.0, 0.0, 1.0);
					triStream.Append(o);

					o.vertex = UnityObjectToClipPos(barycentre + float4(normal, 0.0) * _ExtrusionFactor * pseudorand);
					o.uv = TRANSFORM_TEX(IN[i].uv, _MainTex);
					o.color = fixed4(1.0, 1.0, 1.0, 1.0);
					triStream.Append(o);

					o.vertex = UnityObjectToClipPos(IN[next].vertex);
					o.uv = TRANSFORM_TEX(IN[next].uv, _MainTex);
					o.color = fixed4(0.0, 0.0, 0.0, 1.0);
					triStream.Append(o);

					triStream.RestartStrip();
				}*/

				triStream.RestartStrip();
			}

			fixed4 frag(g2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv) * i.color;
			// apply fog
			//UNITY_APPLY_FOG(i.fogCoord, col);
			return col;
		}
		ENDCG
	}
	}
}