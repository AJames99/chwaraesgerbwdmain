using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

// Define the Volume Component for the custom post processing effect 
[System.Serializable, VolumeComponentMenuForRenderPipeline("CustomPostProcess/Vignette", typeof(UniversalRenderPipeline))]
public class VignettePostProcessEffect : VolumeComponent, IPostProcessComponent {
	//[Tooltip("Controls the strength of the vignette")]
	[SerializeField]
	public NoInterpClampedFloatParameter vignetteMult = new NoInterpClampedFloatParameter(30, 0, 30);
	[SerializeField]
	public NoInterpClampedFloatParameter vignettePower = new NoInterpClampedFloatParameter(0, 0, 1);

	public bool IsActive() => true;

	public bool IsTileCompatible() => true;
}