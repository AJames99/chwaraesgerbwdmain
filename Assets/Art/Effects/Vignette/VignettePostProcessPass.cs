using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

[System.Serializable]
public class VignettePostProcessPass : ScriptableRenderPass {
	// Used to render from camera to post processings
	// back and forth, until we render the final image to
	// the camera
	RenderTargetIdentifier source;
	RenderTargetIdentifier destinationA;
	RenderTargetIdentifier destinationB;
	RenderTargetIdentifier latestDest;

	readonly int temporaryRTIdA = Shader.PropertyToID("_TempRT");
	readonly int temporaryRTIdB = Shader.PropertyToID("_TempRTB");

	private Material m_Material;

	public VignettePostProcessPass() {
		// Set the render pass event
		renderPassEvent = RenderPassEvent.BeforeRenderingPostProcessing;
	}

	// Called when the effect is added, called from the ChwaraeSgerbwdPostProcessRenderer::AddRenderPasses
	public void Setup() {
		m_Material = CoreUtils.CreateEngineMaterial("Hidden/VignetteEffect");
	}

	public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor) {
		base.Configure(cmd, cameraTextureDescriptor);
	}

	public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData) {
		// Grab the camera target descriptor. We will use this when creating a temporary render texture.
		RenderTextureDescriptor descriptor = renderingData.cameraData.cameraTargetDescriptor;
		descriptor.depthBufferBits = 0;

		var renderer = renderingData.cameraData.renderer;
		source = renderer.cameraColorTargetHandle;

		// Create a temporary render texture using the descriptor from above.
		cmd.GetTemporaryRT(temporaryRTIdA, descriptor, FilterMode.Bilinear);
		destinationA = new RenderTargetIdentifier(temporaryRTIdA);
		cmd.GetTemporaryRT(temporaryRTIdB, descriptor, FilterMode.Bilinear);
		destinationB = new RenderTargetIdentifier(temporaryRTIdB);
	}

	// The actual execution of the pass. This is where custom rendering occurs.
	public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData) {
		// Skipping post processing rendering inside the scene View
		if (renderingData.cameraData.isSceneViewCamera)
			return;

		CommandBuffer cmd = CommandBufferPool.Get("Custom Post Processing");
		cmd.Clear();

		// This holds all the current Volumes information
		// which we will need later
		var stack = VolumeManager.instance.stack;

		#region Local Methods

		// Swaps render destinations back and forth, so that
		// we can have multiple passes and similar with only a few textures
		void BlitTo(Material mat, int pass = 0) {
			var first = latestDest;
			var last = first == destinationA ? destinationB : destinationA;
			Blit(cmd, first, last, mat, pass);

			latestDest = last;
		}

		#endregion

		// Starts with the camera source
		latestDest = source;

		//---Custom effect here---
		var customEffect = stack.GetComponent<VignettePostProcessEffect>();
		// Only process if the effect is active
		if (customEffect.IsActive()) {
			//if (!m_Material) {
			//	m_Material = CoreUtils.CreateEngineMaterial("Hidden/VignetteEffect");
			//}

			// P.s. optimize by caching the property ID somewhere else
			m_Material.SetFloat(Shader.PropertyToID("_BaseVignetteMult"), customEffect.vignetteMult.value);
			m_Material.SetFloat(Shader.PropertyToID("_VignettePower"), customEffect.vignettePower.value);
			//material.SetColor(Shader.PropertyToID("_OverlayColor"), customEffect.overlayColor.value);

			BlitTo(m_Material);
		}

		// Add any other custom effect/component you want, in your preferred order
		// Custom effect 2, 3 , ...


		// DONE! Now that we have processed all our custom effects, applies the final result to camera
		Blit(cmd, latestDest, source);

		context.ExecuteCommandBuffer(cmd);
		CommandBufferPool.Release(cmd);
	}

	//Cleans the temporary RTs when we don't need them anymore
	public override void OnCameraCleanup(CommandBuffer cmd) {
		cmd.ReleaseTemporaryRT(temporaryRTIdA);
		cmd.ReleaseTemporaryRT(temporaryRTIdB);
	}
}