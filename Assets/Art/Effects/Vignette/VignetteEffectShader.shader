Shader "Hidden/VignetteEffect"
{
	Properties 
	{
	    _MainTex ("Main Texture", 2D) = "white" {}
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" "RenderPipeline" = "UniversalPipeline" }
		
		Pass
		{
            HLSLPROGRAM
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/SurfaceInput.hlsl"
			#include "../../Crosshatch Shader/CrosshatchUtils.hlsl"
            
			#pragma vertex vert
			#pragma fragment frag
			
            TEXTURE2D(_MainTex);
            SAMPLER(sampler_MainTex);
            
			float _BaseVignetteMult;
			float _VignettePower;
			//sampler2D _CrosshatchMapDiagonal;
			sampler2D _CrosshatchMapMixed;
			//float _CrosshatchShift;
			//float _CrosshatchPatternShift;
			//float _CrosshatchPatternBoundary;
           
            struct Attributes
            {
                float4 positionOS       : POSITION;
                float2 uv               : TEXCOORD0;
            };

            struct Varyings
            {
                float2 uv        : TEXCOORD0;
                float4 vertex : SV_POSITION;
                UNITY_VERTEX_OUTPUT_STEREO
            };
            
            
            Varyings vert(Attributes input)
            {
                Varyings output = (Varyings)0;
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

                VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
                output.vertex = vertexInput.positionCS;
                output.uv = input.uv;
                
                return output;
            }
            
            float4 frag (Varyings input) : SV_Target 
            {
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv);

				// Calculate vignette coefficient
				input.uv *= float2(1,0.92);
				float2 uvmod = input.uv * (1.0 - input.uv.yx);
				float intensity = uvmod.x*uvmod.y * _BaseVignetteMult;
				intensity = pow(intensity, _VignettePower);

				float2 CHatchUV = GetCHSamplingCoord(input.uv);

				float hatchBlend = intensity;
				float hatchDiagonal = tex2D(_CrosshatchMapDiagonal, CHatchUV).a;
				float hatchMixed = tex2D(_CrosshatchMapMixed, CHatchUV).a;
				// Blends between the diagonal and crossed patterns based on a smooth-ish boundary.
				float patternBlend = smoothstep(
					_CrosshatchPatternShift + _CrosshatchPatternBoundary,
					_CrosshatchPatternShift - _CrosshatchPatternBoundary,
					hatchBlend
				);
				float hatchSample = lerp(hatchDiagonal, hatchMixed, saturate(patternBlend));
				hatchSample = step(hatchSample, hatchBlend);

				color.rgb *= hatchSample;
				return color;
            }
            
			ENDHLSL
		}
	} 
	FallBack "Diffuse"
}