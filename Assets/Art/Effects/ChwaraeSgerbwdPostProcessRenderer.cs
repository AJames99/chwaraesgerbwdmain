using UnityEngine.Rendering.Universal;

[System.Serializable]
public class ChwaraeSgerbwdPostProcessRenderer : ScriptableRendererFeature {
	VignettePostProcessPass pass;

	public override void Create() {
		pass = new VignettePostProcessPass();
	}

	public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData) {
		pass.Setup();
		renderer.EnqueuePass(pass);
	}
}