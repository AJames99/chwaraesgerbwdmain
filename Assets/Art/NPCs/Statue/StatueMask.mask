%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: StatueMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Blade Unbevelled
    m_Weight: 0
  - m_Path: CharArmature
    m_Weight: 1
  - m_Path: CharArmature/Root
    m_Weight: 1
  - m_Path: CharArmature/Root/ElbowIKTarget.L
    m_Weight: 1
  - m_Path: CharArmature/Root/ElbowIKTarget.L/ElbowIKTarget.L_end
    m_Weight: 1
  - m_Path: CharArmature/Root/ElbowIKTarget.R
    m_Weight: 1
  - m_Path: CharArmature/Root/ElbowIKTarget.R/ElbowIKTarget.R_end
    m_Weight: 1
  - m_Path: CharArmature/Root/FootIK.L
    m_Weight: 1
  - m_Path: CharArmature/Root/FootIK.L/Toes.L
    m_Weight: 1
  - m_Path: CharArmature/Root/FootIK.L/Toes.L/Toes.L_end
    m_Weight: 1
  - m_Path: CharArmature/Root/FootIK.R
    m_Weight: 1
  - m_Path: CharArmature/Root/FootIK.R/Toes.R
    m_Weight: 1
  - m_Path: CharArmature/Root/FootIK.R/Toes.R/Toes.R_end
    m_Weight: 1
  - m_Path: CharArmature/Root/HandIK.L
    m_Weight: 1
  - m_Path: CharArmature/Root/HandIK.L/Hand.L
    m_Weight: 1
  - m_Path: CharArmature/Root/HandIK.L/Hand.L/Hand.L_end
    m_Weight: 1
  - m_Path: CharArmature/Root/HandIK.R
    m_Weight: 1
  - m_Path: CharArmature/Root/HandIK.R/Hand.R
    m_Weight: 1
  - m_Path: CharArmature/Root/HandIK.R/Hand.R/Weapon.R
    m_Weight: 1
  - m_Path: CharArmature/Root/HandIK.R/Hand.R/Weapon.R/Weapon.R_end
    m_Weight: 1
  - m_Path: CharArmature/Root/KneeIKTarget.L
    m_Weight: 1
  - m_Path: CharArmature/Root/KneeIKTarget.L/KneeIKTarget.L_end
    m_Weight: 1
  - m_Path: CharArmature/Root/KneeIKTarget.R
    m_Weight: 1
  - m_Path: CharArmature/Root/KneeIKTarget.R/KneeIKTarget.R_end
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Neck
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Neck/Head
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Shoulder.L
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Shoulder.L/UpperArm.L
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Shoulder.L/UpperArm.L/ForeArm.L
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Shoulder.L/UpperArm.L/ForeArm.L/ForeArm.L_end
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Shoulder.R
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Shoulder.R/UpperArm.R
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Shoulder.R/UpperArm.R/ForeArm.R
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Chest/Shoulder.R/UpperArm.R/ForeArm.R/ForeArm.R_end
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Thigh.L
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Thigh.L/Calf.L
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Thigh.L/Calf.L/Calf.L_end
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Thigh.R
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Thigh.R/Calf.R
    m_Weight: 1
  - m_Path: CharArmature/Root/Pelvis/Thigh.R/Calf.R/Calf.R_end
    m_Weight: 1
  - m_Path: CharModel
    m_Weight: 1
  - m_Path: GuanDaoArmature
    m_Weight: 1
  - m_Path: GuanDaoArmature/GuanDaoMain
    m_Weight: 1
  - m_Path: GuanDaoArmature/GuanDaoMain/GuanDaoMain_end
    m_Weight: 1
  - m_Path: GuanDaoBlade
    m_Weight: 1
  - m_Path: GuanDaoPole
    m_Weight: 1
