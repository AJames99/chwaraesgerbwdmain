//name: TerrainSplatmapCrosshatch
//fragment: SplatmapFragmentCrosshatch

#pragma vertex SplatmapVert
#pragma fragment SplatmapFragmentCrosshatch

#ifndef UNIVERSAL_TERRAIN_LIT_PASSES_INCLUDED
#define UNIVERSAL_TERRAIN_LIT_PASSES_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/UnityGBuffer.hlsl"
#include "../../Crosshatch Shader/CrosshatchUtils.hlsl"

#if defined(UNITY_INSTANCING_ENABLED) && defined(_TERRAIN_INSTANCED_PERPIXEL_NORMAL)
#define ENABLE_TERRAIN_PERPIXEL_NORMAL
#endif

#ifdef UNITY_INSTANCING_ENABLED
TEXTURE2D(_TerrainHeightmapTexture);
TEXTURE2D(_TerrainNormalmapTexture);
SAMPLER(sampler_TerrainNormalmapTexture);
#endif

//UNITY_INSTANCING_BUFFER_START(Terrain)
//UNITY_DEFINE_INSTANCED_PROP(float4, _TerrainPatchInstanceData)  // float4(xBase, yBase, skipScale, ~)
//UNITY_INSTANCING_BUFFER_END(Terrain)

#ifdef _ALPHATEST_ON
TEXTURE2D(_TerrainHolesTexture);
SAMPLER(sampler_TerrainHolesTexture);

void ClipHoles(float2 uv)
{
	float hole = SAMPLE_TEXTURE2D(_TerrainHolesTexture, sampler_TerrainHolesTexture, uv).r;
	clip(hole == 0.0f ? -1 : 1);
}
#endif

struct Attributes
{
	float4 positionOS : POSITION;
	float3 normalOS : NORMAL;
	float2 texcoord : TEXCOORD0;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
	float4 uvMainAndLM              : TEXCOORD0; // xy: control, zw: lightmap
#ifndef TERRAIN_SPLAT_BASEPASS
	float4 uvSplat01                : TEXCOORD1; // xy: splat0, zw: splat1
	float4 uvSplat23                : TEXCOORD2; // xy: splat2, zw: splat3
#endif

#if defined(_NORMALMAP) && !defined(ENABLE_TERRAIN_PERPIXEL_NORMAL)
	float4 normal                   : TEXCOORD3;    // xyz: normal, w: viewDir.x
	float4 tangent                  : TEXCOORD4;    // xyz: tangent, w: viewDir.y
	float4 bitangent                : TEXCOORD5;    // xyz: bitangent, w: viewDir.z
#else
	float3 normal                   : TEXCOORD3;
	float3 viewDir                  : TEXCOORD4;
	half3 vertexSH                  : TEXCOORD5; // SH
#endif

	half4 fogFactorAndVertexLight   : TEXCOORD6; // x: fogFactor, yzw: vertex light
	float3 positionWS               : TEXCOORD7;
#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
	float4 shadowCoord              : TEXCOORD8;
#endif
	float4 clipPos                  : SV_POSITION;
	UNITY_VERTEX_OUTPUT_STEREO
};

struct LightingInfo {
	half3 lightDiffuse;
	half3 lightSpecular;
	// For understanding the intensity of the light
	half lightDiffuseIntensity;
	half lightSpecularIntensity;
};

//sampler2D _CrosshatchMapDiagonal;
sampler2D _CrosshatchMapMixed;
texture2D _RampTexture;
sampler2D _SpecularMap;
SamplerState _SamplerState_Linear_Clamp_sampler;
SamplerState _SamplerState_Point_Repeat_sampler;
float _SpecMultipler;
float _CrosshatchShift;
//float _CrosshatchPatternShift;
//float _CrosshatchPatternBoundary;
//float _CrosshatchHighlightsShift;
//float _CrosshatchHighlightsBoundary;
float _CrosshatchTilingOverrideMultiplier;
float _RimStrength;
float _AmbientLightInfluence;
float _CrosshatchStrength;

half3 Unity_ColorspaceConversion_RGB_HSV_half(half3 In)
{
	half4 K = half4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	half4 P = lerp(half4(In.bg, K.wz), half4(In.gb, K.xy), step(In.b, In.g));
	half4 Q = lerp(half4(P.xyw, In.r), half4(In.r, P.yzx), step(P.x, In.r));
	half D = Q.x - min(Q.w, Q.y);
	half  E = 1e-10;
	return half3(abs(Q.z + (Q.w - Q.y) / (6.0 * D + E)), D / (Q.x + E), Q.x);
}

half3 Unity_ColorspaceConversion_HSV_RGB_half(half3 In)
{
	half4 K = half4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	half3 P = abs(frac(In.xxx + K.xyz) * 6.0 - K.www);
	return In.z * lerp(K.xxx, saturate(P - K.xxx), In.y);
}

void InitializeInputData(Varyings IN, half3 normalTS, out InputData input)
{
	input = (InputData)0;

	input.positionWS = IN.positionWS;
	half3 SH = half3(0, 0, 0);

#if defined(_NORMALMAP) && !defined(ENABLE_TERRAIN_PERPIXEL_NORMAL)
	half3 viewDirWS = half3(IN.normal.w, IN.tangent.w, IN.bitangent.w);
	input.normalWS = TransformTangentToWorld(normalTS, half3x3(-IN.tangent.xyz, IN.bitangent.xyz, IN.normal.xyz));
	SH = SampleSH(input.normalWS.xyz);
#elif defined(ENABLE_TERRAIN_PERPIXEL_NORMAL)
	half3 viewDirWS = IN.viewDir;
	float2 sampleCoords = (IN.uvMainAndLM.xy / _TerrainHeightmapRecipSize.zw + 0.5f) * _TerrainHeightmapRecipSize.xy;
	half3 normalWS = TransformObjectToWorldNormal(normalize(SAMPLE_TEXTURE2D(_TerrainNormalmapTexture, sampler_TerrainNormalmapTexture, sampleCoords).rgb * 2 - 1));
	half3 tangentWS = cross(GetObjectToWorldMatrix()._13_23_33, normalWS);
	input.normalWS = TransformTangentToWorld(normalTS, half3x3(-tangentWS, cross(normalWS, tangentWS), normalWS));
	SH = SampleSH(input.normalWS.xyz);
#else
	half3 viewDirWS = IN.viewDir;
	input.normalWS = IN.normal;
	SH = IN.vertexSH;
#endif

#if SHADER_HINT_NICE_QUALITY
	viewDirWS = SafeNormalize(viewDirWS);
#endif

	input.normalWS = NormalizeNormalPerPixel(input.normalWS);

	input.viewDirectionWS = viewDirWS;

#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
	input.shadowCoord = IN.shadowCoord;
#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
	input.shadowCoord = TransformWorldToShadowCoord(input.positionWS);
#else
	input.shadowCoord = float4(0, 0, 0, 0);
#endif

	input.fogCoord = IN.fogFactorAndVertexLight.x;
	input.vertexLighting = IN.fogFactorAndVertexLight.yzw;

	input.bakedGI = SAMPLE_GI(IN.uvMainAndLM.zw, SH, input.normalWS);
	input.normalizedScreenSpaceUV = GetNormalizedScreenSpaceUV(IN.clipPos);
	input.shadowMask = SAMPLE_SHADOWMASK(IN.uvMainAndLM.zw)
}


///////////////////////////////////////////////////////////////////////////////
//                        Lighting functions                                 //
///////////////////////////////////////////////////////////////////////////////

LightingInfo GetLightingStuff(InputData inputData, SurfaceData surfaceData)
{
	LightingInfo output;

	BRDFData brdfData;

	// NOTE: can modify alpha
	InitializeBRDFData(surfaceData.albedo, surfaceData.metallic, surfaceData.specular, surfaceData.smoothness, surfaceData.alpha, brdfData);


	BRDFData brdfDataClearCoat = (BRDFData)0;
#if defined(_CLEARCOAT) || defined(_CLEARCOATMAP)
	// base brdfData is modified here, rely on the compiler to eliminate dead computation by InitializeBRDFData()
	InitializeBRDFDataClearCoat(surfaceData.clearCoatMask, surfaceData.clearCoatSmoothness, brdfData, brdfDataClearCoat);
#endif

	// #### SHADOWS ####
	// To ensure backward compatibility we have to avoid using shadowMask input, as it is not present in older shaders
#if defined(SHADOWS_SHADOWMASK) && defined(LIGHTMAP_ON)
	half4 shadowMask = inputData.shadowMask;
#elif !defined (LIGHTMAP_ON)
	half4 shadowMask = unity_ProbesOcclusion;
#else
	half4 shadowMask = half4(1, 1, 1, 1);
#endif
	// #### MAIN LIGHT ####
	Light mainLight = GetMainLight(inputData.shadowCoord, inputData.positionWS, shadowMask);

	// #### AMBIENT OCCLUSION ####
#if defined(_SCREEN_SPACE_OCCLUSION)
	AmbientOcclusionFactor aoFactor = GetScreenSpaceAmbientOcclusion(inputData.normalizedScreenSpaceUV);
	mainLight.color *= aoFactor.directAmbientOcclusion;
	surfaceData.occlusion = min(surfaceData.occlusion, aoFactor.indirectAmbientOcclusion);
#endif

	/*
	MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI);
	half3 color = GlobalIllumination(brdfData, brdfDataClearCoat, surfaceData.clearCoatMask,
		inputData.bakedGI, surfaceData.occlusion,
		inputData.normalWS, inputData.viewDirectionWS);
	color += LightingPhysicallyBased(brdfData, brdfDataClearCoat,
		mainLight,
		inputData.normalWS, inputData.viewDirectionWS,
		surfaceData.clearCoatMask, 0);
	*/

	// N dot L Main
	half mainLightingFactorDiff = saturate(dot(inputData.normalWS, mainLight.direction)) * mainLight.distanceAttenuation * mainLight.shadowAttenuation;
	half3 lightingDiffuse = mainLight.color * mainLightingFactorDiff;

	// N dot H main
	half mainLightingFactorSpec = pow(
		saturate(dot(normalize(normalize(inputData.viewDirectionWS) + mainLight.direction), inputData.normalWS)),
		max(surfaceData.smoothness * 100, 0)
	);
	mainLightingFactorSpec += pow((1.0 - saturate(dot(normalize(inputData.normalWS), normalize(inputData.viewDirectionWS)))), 1 / _RimStrength);	// Fresnel
	mainLightingFactorSpec *= mainLight.distanceAttenuation * mainLight.shadowAttenuation; // Attenuation
	half3 lightingSpecular = mainLight.color * mainLightingFactorSpec;

	// #### ADDITIONAL LIGHTS ####
	half3 additionalLightingFactor = 0, additionalLightingFactorSpec = 0;
#ifdef _ADDITIONAL_LIGHTS
	uint pixelLightCount = GetAdditionalLightsCount();
	for (uint lightIndex = 0u; lightIndex < pixelLightCount; ++lightIndex)
	{
		Light light = GetAdditionalLight(lightIndex, inputData.positionWS, shadowMask);

		//maybe remove this? idk
#if defined(_SCREEN_SPACE_OCCLUSION)
		light.color *= aoFactor.directAmbientOcclusion;
#endif
		//additionalLightingFactor += saturate(dot(inputData.normalWS, light.direction))  * light.distanceAttenuation * light.shadowAttenuation;
		//additionalLightingFactor += (light.color.r + light.color.g + light.color.b) / 3  * light.distanceAttenuation * light.shadowAttenuation; // v2 - dont bother with dot h
		//additionalLightingFactor += saturate(dot(inputData.normalWS, light.direction))  * light.distanceAttenuation * light.shadowAttenuation * (light.color.r + light.color.g + light.color.b) / 3; //v3 works but with no coloured point lights
		additionalLightingFactor += saturate(dot(inputData.normalWS, light.direction))  * light.distanceAttenuation * light.shadowAttenuation * light.color;

		half3 lightFactorSpec = pow(
			saturate(dot(normalize(normalize(inputData.viewDirectionWS) + light.direction), inputData.normalWS)),	// might need to remove the direction part?
			max(surfaceData.smoothness * 100, 0)
		);
		lightFactorSpec += pow((1.0 - saturate(dot(normalize(inputData.normalWS), normalize(inputData.viewDirectionWS)))), 1 / _RimStrength);	// Fresnel
		lightFactorSpec *= light.distanceAttenuation * light.shadowAttenuation * light.color; // Attenuation

		additionalLightingFactorSpec += lightFactorSpec;

		// this doesnt seem to be the dynamic shadows
		/*colour += LightingPhysicallyBased(brdfData, brdfDataClearCoat,
			light,
			inputData.normalWS, inputData.viewDirectionWS,
			surfaceData.clearCoatMask, specularHighlightsOff);*/
			//todo do light stuff
	}
#endif

	//add me back in
//#ifdef _ADDITIONAL_LIGHTS_VERTEX
//	color += inputData.vertexLighting * brdfData.diffuse;
//#endif

	// V1
	//output.colour = colour + surfaceData.emission;
	//output.light = additionalLightingFactor + mainLightingFactorDiff;
	//output.lightSpecular = additionalLightingFactorSpec + mainLightingFactorSpec;
	// V2 - point colour should work

	// Full colour
	output.lightDiffuse = lightingDiffuse + additionalLightingFactor + inputData.bakedGI.rgb * _AmbientLightInfluence;
	output.lightSpecular = lightingSpecular + additionalLightingFactorSpec;

	// Intensities
	output.lightDiffuseIntensity = Unity_ColorspaceConversion_RGB_HSV_half(output.lightDiffuse).z;
	output.lightSpecularIntensity = Unity_ColorspaceConversion_RGB_HSV_half(output.lightSpecular).z;

	return output;
}

#ifndef TERRAIN_SPLAT_BASEPASS

void SplatmapMix(float4 uvMainAndLM, float4 uvSplat01, float4 uvSplat23, inout half4 splatControl, out half weight, out half4 mixedDiffuse, out half4 defaultSmoothness, inout half3 mixedNormal)
{
	half4 diffAlbedo[4];

	diffAlbedo[0] = SAMPLE_TEXTURE2D(_Splat0, sampler_Splat0, uvSplat01.xy);
	diffAlbedo[1] = SAMPLE_TEXTURE2D(_Splat1, sampler_Splat0, uvSplat01.zw);
	diffAlbedo[2] = SAMPLE_TEXTURE2D(_Splat2, sampler_Splat0, uvSplat23.xy);
	diffAlbedo[3] = SAMPLE_TEXTURE2D(_Splat3, sampler_Splat0, uvSplat23.zw);

	// This might be a bit of a gamble -- the assumption here is that if the diffuseMap has no
	// alpha channel, then diffAlbedo[n].a = 1.0 (and _DiffuseHasAlphaN = 0.0)
	// Prior to coming in, _SmoothnessN is actually set to max(_DiffuseHasAlphaN, _SmoothnessN)
	// This means that if we have an alpha channel, _SmoothnessN is locked to 1.0 and
	// otherwise, the true slider value is passed down and diffAlbedo[n].a == 1.0.
	defaultSmoothness = half4(diffAlbedo[0].a, diffAlbedo[1].a, diffAlbedo[2].a, diffAlbedo[3].a);
	defaultSmoothness *= half4(_Smoothness0, _Smoothness1, _Smoothness2, _Smoothness3);

#ifndef _TERRAIN_BLEND_HEIGHT
	// 20.0 is the number of steps in inputAlphaMask (Density mask. We decided 20 empirically)
	half4 opacityAsDensity = saturate((half4(diffAlbedo[0].a, diffAlbedo[1].a, diffAlbedo[2].a, diffAlbedo[3].a) - (half4(1.0, 1.0, 1.0, 1.0) - splatControl)) * 20.0);
	opacityAsDensity += 0.001h * splatControl;      // if all weights are zero, default to what the blend mask says
	half4 useOpacityAsDensityParam = { _DiffuseRemapScale0.w, _DiffuseRemapScale1.w, _DiffuseRemapScale2.w, _DiffuseRemapScale3.w }; // 1 is off
	splatControl = lerp(opacityAsDensity, splatControl, useOpacityAsDensityParam);
#endif

	// Now that splatControl has changed, we can compute the final weight and normalize
	weight = dot(splatControl, 1.0h);

#ifdef TERRAIN_SPLAT_ADDPASS
	clip(weight <= 0.005h ? -1.0h : 1.0h);
#endif

#ifndef _TERRAIN_BASEMAP_GEN
	// Normalize weights before lighting and restore weights in final modifier functions so that the overal
	// lighting result can be correctly weighted.
	splatControl /= (weight + HALF_MIN);
#endif

	mixedDiffuse = 0.0h;
	mixedDiffuse += diffAlbedo[0] * half4(_DiffuseRemapScale0.rgb * splatControl.rrr, 1.0h);
	mixedDiffuse += diffAlbedo[1] * half4(_DiffuseRemapScale1.rgb * splatControl.ggg, 1.0h);
	mixedDiffuse += diffAlbedo[2] * half4(_DiffuseRemapScale2.rgb * splatControl.bbb, 1.0h);
	mixedDiffuse += diffAlbedo[3] * half4(_DiffuseRemapScale3.rgb * splatControl.aaa, 1.0h);

#ifdef _NORMALMAP
	half3 nrm = 0.0f;
	nrm += splatControl.r * UnpackNormalScale(SAMPLE_TEXTURE2D(_Normal0, sampler_Normal0, uvSplat01.xy), _NormalScale0);
	nrm += splatControl.g * UnpackNormalScale(SAMPLE_TEXTURE2D(_Normal1, sampler_Normal0, uvSplat01.zw), _NormalScale1);
	nrm += splatControl.b * UnpackNormalScale(SAMPLE_TEXTURE2D(_Normal2, sampler_Normal0, uvSplat23.xy), _NormalScale2);
	nrm += splatControl.a * UnpackNormalScale(SAMPLE_TEXTURE2D(_Normal3, sampler_Normal0, uvSplat23.zw), _NormalScale3);

	// avoid risk of NaN when normalizing.
#if HAS_HALF
	nrm.z += 0.01h;
#else
	nrm.z += 1e-5f;
#endif

	mixedNormal = normalize(nrm.xyz);
#endif
}

#endif

#ifdef _TERRAIN_BLEND_HEIGHT
void HeightBasedSplatModify(inout half4 splatControl, in half4 masks[4])
{
	// heights are in mask blue channel, we multiply by the splat Control weights to get combined height
	half4 splatHeight = half4(masks[0].b, masks[1].b, masks[2].b, masks[3].b) * splatControl.rgba;
	half maxHeight = max(splatHeight.r, max(splatHeight.g, max(splatHeight.b, splatHeight.a)));

	// Ensure that the transition height is not zero.
	half transition = max(_HeightTransition, 1e-5);

	// This sets the highest splat to "transition", and everything else to a lower value relative to that, clamping to zero
	// Then we clamp this to zero and normalize everything
	half4 weightedHeights = splatHeight + transition - maxHeight.xxxx;
	weightedHeights = max(0, weightedHeights);

	// We need to add an epsilon here for active layers (hence the blendMask again)
	// so that at least a layer shows up if everything's too low.
	weightedHeights = (weightedHeights + 1e-6) * splatControl;

	// Normalize (and clamp to epsilon to keep from dividing by zero)
	half sumHeight = max(dot(weightedHeights, half4(1, 1, 1, 1)), 1e-6);
	splatControl = weightedHeights / sumHeight.xxxx;
}
#endif

void SplatmapFinalColor(inout half4 color, half fogCoord)
{
	color.rgb *= color.a;

#ifndef TERRAIN_GBUFFER // Technically we don't need fogCoord, but it is still passed from the vertex shader.

#ifdef TERRAIN_SPLAT_ADDPASS
	color.rgb = MixFogColor(color.rgb, half3(0, 0, 0), fogCoord);
#else
	color.rgb = MixFog(color.rgb, fogCoord);
#endif

#endif
}

// Could probably avoid all this and just call the original? since redef error now appears, we can assume TerrainInstancing is now accessible here
void TerrainInstancingCH(inout float4 positionOS, inout float3 normal, inout float2 uv)
{
#ifdef UNITY_INSTANCING_ENABLED
	float2 patchVertex = positionOS.xy;
	float4 instanceData = UNITY_ACCESS_INSTANCED_PROP(Terrain, _TerrainPatchInstanceData);

	float2 sampleCoords = (patchVertex.xy + instanceData.xy) * instanceData.z; // (xy + float2(xBase,yBase)) * skipScale
	float height = UnpackHeightmap(_TerrainHeightmapTexture.Load(int3(sampleCoords, 0)));

	positionOS.xz = sampleCoords * _TerrainHeightmapScale.xz;
	positionOS.y = height * _TerrainHeightmapScale.y;

#ifdef ENABLE_TERRAIN_PERPIXEL_NORMAL
	normal = float3(0, 1, 0);
#else
	normal = _TerrainNormalmapTexture.Load(int3(sampleCoords, 0)).rgb * 2 - 1;
#endif
	uv = sampleCoords * _TerrainHeightmapRecipSize.zw;
#endif
}

void TerrainInstancingCH(inout float4 positionOS, inout float3 normal)
{
	float2 uv = { 0, 0 };
	TerrainInstancingCH(positionOS, normal, uv);
}

///////////////////////////////////////////////////////////////////////////////
//                  Vertex and Fragment functions                            //
///////////////////////////////////////////////////////////////////////////////

// Used in Standard Terrain shader
Varyings SplatmapVert(Attributes v)
{
	Varyings o = (Varyings)0;

	UNITY_SETUP_INSTANCE_ID(v);
	UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
	TerrainInstancing(v.positionOS, v.normalOS, v.texcoord);

	VertexPositionInputs Attributes = GetVertexPositionInputs(v.positionOS.xyz);

	o.uvMainAndLM.xy = v.texcoord;
	o.uvMainAndLM.zw = v.texcoord * unity_LightmapST.xy + unity_LightmapST.zw;
#ifndef TERRAIN_SPLAT_BASEPASS
	o.uvSplat01.xy = TRANSFORM_TEX(v.texcoord, _Splat0);
	o.uvSplat01.zw = TRANSFORM_TEX(v.texcoord, _Splat1);
	o.uvSplat23.xy = TRANSFORM_TEX(v.texcoord, _Splat2);
	o.uvSplat23.zw = TRANSFORM_TEX(v.texcoord, _Splat3);
#endif

	half3 viewDirWS = GetWorldSpaceViewDir(Attributes.positionWS);
#if !SHADER_HINT_NICE_QUALITY
	viewDirWS = SafeNormalize(viewDirWS);
#endif

#if defined(_NORMALMAP) && !defined(ENABLE_TERRAIN_PERPIXEL_NORMAL)
	float4 vertexTangent = float4(cross(float3(0, 0, 1), v.normalOS), 1.0);
	VertexNormalInputs normalInput = GetVertexNormalInputs(v.normalOS, vertexTangent);

	o.normal = half4(normalInput.normalWS, viewDirWS.x);
	o.tangent = half4(normalInput.tangentWS, viewDirWS.y);
	o.bitangent = half4(normalInput.bitangentWS, viewDirWS.z);
#else
	o.normal = TransformObjectToWorldNormal(v.normalOS);
	o.viewDir = viewDirWS;
	o.vertexSH = SampleSH(o.normal);
#endif
	o.fogFactorAndVertexLight.x = ComputeFogFactor(Attributes.positionCS.z);
	o.fogFactorAndVertexLight.yzw = VertexLighting(Attributes.positionWS, o.normal.xyz);
	o.positionWS = Attributes.positionWS;
	o.clipPos = Attributes.positionCS;

#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
	o.shadowCoord = GetShadowCoord(Attributes);
#endif

	return o;
}

void ComputeMasks(out half4 masks[4], half4 hasMask, Varyings IN)
{
	masks[0] = 0.5h;
	masks[1] = 0.5h;
	masks[2] = 0.5h;
	masks[3] = 0.5h;

#ifdef _MASKMAP
	masks[0] = lerp(masks[0], SAMPLE_TEXTURE2D(_Mask0, sampler_Mask0, IN.uvSplat01.xy), hasMask.x);
	masks[1] = lerp(masks[1], SAMPLE_TEXTURE2D(_Mask1, sampler_Mask0, IN.uvSplat01.zw), hasMask.y);
	masks[2] = lerp(masks[2], SAMPLE_TEXTURE2D(_Mask2, sampler_Mask0, IN.uvSplat23.xy), hasMask.z);
	masks[3] = lerp(masks[3], SAMPLE_TEXTURE2D(_Mask3, sampler_Mask0, IN.uvSplat23.zw), hasMask.w);
#endif

	masks[0] *= _MaskMapRemapScale0.rgba;
	masks[0] += _MaskMapRemapOffset0.rgba;
	masks[1] *= _MaskMapRemapScale1.rgba;
	masks[1] += _MaskMapRemapOffset1.rgba;
	masks[2] *= _MaskMapRemapScale2.rgba;
	masks[2] += _MaskMapRemapOffset2.rgba;
	masks[3] *= _MaskMapRemapScale3.rgba;
	masks[3] += _MaskMapRemapOffset3.rgba;
}

// Used in Standard Terrain shader
#ifdef TERRAIN_GBUFFER //probably not gonna be this?
FragmentOutput SplatmapFragmentCrosshatch(Varyings IN)
#else
half4 SplatmapFragmentCrosshatch(Varyings IN) : SV_TARGET
#endif
{
#ifdef _ALPHATEST_ON
	ClipHoles(IN.uvMainAndLM.xy);
#endif

	half3 normalTS = half3(0.0h, 0.0h, 1.0h);
#ifdef TERRAIN_SPLAT_BASEPASS
	half3 albedo = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uvMainAndLM.xy).rgb;
	half smoothness = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uvMainAndLM.xy).a;
	half metallic = SAMPLE_TEXTURE2D(_MetallicTex, sampler_MetallicTex, IN.uvMainAndLM.xy).r;
	half alpha = 1;
	half occlusion = 1;
#else

	half4 hasMask = half4(_LayerHasMask0, _LayerHasMask1, _LayerHasMask2, _LayerHasMask3);
	half4 masks[4];
	ComputeMasks(masks, hasMask, IN);

	float2 splatUV = (IN.uvMainAndLM.xy * (_Control_TexelSize.zw - 1.0f) + 0.5f) * _Control_TexelSize.xy;
	half4 splatControl = SAMPLE_TEXTURE2D(_Control, sampler_Control, splatUV);

#ifdef _TERRAIN_BLEND_HEIGHT
	// disable Height Based blend when there are more than 4 layers (multi-pass breaks the normalization)
	if (_NumLayersCount <= 4)
		HeightBasedSplatModify(splatControl, masks);
#endif

	half weight;
	half4 mixedDiffuse;
	half4 defaultSmoothness;
	SplatmapMix(IN.uvMainAndLM, IN.uvSplat01, IN.uvSplat23, splatControl, weight, mixedDiffuse, defaultSmoothness, normalTS);
	half3 albedo = mixedDiffuse.rgb;

	half4 defaultMetallic = half4(_Metallic0, _Metallic1, _Metallic2, _Metallic3);
	half4 defaultOcclusion = half4(_MaskMapRemapScale0.g, _MaskMapRemapScale1.g, _MaskMapRemapScale2.g, _MaskMapRemapScale3.g) +
							half4(_MaskMapRemapOffset0.g, _MaskMapRemapOffset1.g, _MaskMapRemapOffset2.g, _MaskMapRemapOffset3.g);

	half4 maskSmoothness = half4(masks[0].a, masks[1].a, masks[2].a, masks[3].a);
	defaultSmoothness = lerp(defaultSmoothness, maskSmoothness, hasMask);
	half smoothness = dot(splatControl, defaultSmoothness);

	half4 maskMetallic = half4(masks[0].r, masks[1].r, masks[2].r, masks[3].r);
	defaultMetallic = lerp(defaultMetallic, maskMetallic, hasMask);
	half metallic = dot(splatControl, defaultMetallic);

	half4 maskOcclusion = half4(masks[0].g, masks[1].g, masks[2].g, masks[3].g);
	defaultOcclusion = lerp(defaultOcclusion, maskOcclusion, hasMask);
	half occlusion = dot(splatControl, defaultOcclusion);
	half alpha = weight;
#endif

	InputData inputData;
	InitializeInputData(IN, normalTS, inputData);

#ifdef TERRAIN_GBUFFER

	BRDFData brdfData;
	InitializeBRDFData(albedo, metallic, /* specular */ half3(0.0h, 0.0h, 0.0h), smoothness, alpha, brdfData);

	// Baked lighting.
	
	half4 color;
	Light mainLight = GetMainLight(inputData.shadowCoord, inputData.positionWS, inputData.shadowMask);
	MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, inputData.shadowMask);
	color.rgb = GlobalIllumination(brdfData, inputData.bakedGI, occlusion, inputData.normalWS, inputData.viewDirectionWS);
	color.a = alpha;
	SplatmapFinalColor(color, inputData.fogCoord);
	

	// Dynamic lighting: emulate SplatmapFinalColor() by scaling gbuffer material properties. This will not give the same results
	// as forward renderer because we apply blending pre-lighting instead of post-lighting.
	// Blending of smoothness and normals is also not correct but close enough?
	brdfData.diffuse.rgb *= alpha;
	brdfData.specular.rgb *= alpha;
	inputData.normalWS = inputData.normalWS * alpha;
	smoothness *= alpha;

	return BRDFDataToGbuffer(brdfData, inputData, smoothness, color.rgb);

#else

	//half4 color = UniversalFragmentPBR(inputData, albedo, metallic, /* specular */ half3(0.0h, 0.0h, 0.0h), smoothness, occlusion, /* emission */ half3(0, 0, 0), alpha);
	SurfaceData surfaceData;
	//InitializeStandardLitSurfaceData(splatUV, surfaceData);
	surfaceData.albedo = albedo;
	surfaceData.specular = surfaceData.metallic = metallic;
	surfaceData.occlusion = occlusion;
	surfaceData.alpha = alpha;
	surfaceData.smoothness = smoothness;
	surfaceData.normalTS = normalTS;


	LightingInfo lighting = GetLightingStuff(inputData, surfaceData);
	half4 color = half4(surfaceData.albedo, surfaceData.alpha); // Moved to point sample

	/*float2 positionVS_base = (TransformWorldToView(IN.positionWS) / IN.clipPos.w).xy;
	float2 positionVS = (positionVS_base + _GlobalCameraRotSampleOffset) * _CrosshatchTiling; // was IN.positionCS.w, changed -> clipPos.w

	positionVS += _GlobalSamplingOffset;

	// Rotate by _GlobalSamplingOffset.z - randomised rotation offset
	{
		float sinX = sin(_GlobalSamplingOffset.z); // move these trigs to z/w or move matrix to a float4 global
		float cosX = cos(_GlobalSamplingOffset.z);
		float2x2 rotMat = float2x2(cosX, sinX, -sinX, cosX);
		positionVS.xy = mul(positionVS.xy, rotMat);
	}*/
	float2 screenUVBase = IN.clipPos.xy / _ScreenParams.xy;

	// PositionVS is only used for sampling (currently); modify it however is desired
	float2 screenUV = GetCHSamplingCoord(screenUVBase);

	half3 sampledSpecular = surfaceData.specular * lighting.lightSpecular;
	half sampledSpecularIntensity = surfaceData.specular * lighting.lightSpecularIntensity;

	// Lighting ramp
	half4 rampDiffuse = SAMPLE_TEXTURE2D_LOD(_RampTexture, _SamplerState_Linear_Clamp_sampler, float2(lighting.lightDiffuseIntensity, 1), 0);
	half4 rampSpecular = SAMPLE_TEXTURE2D_LOD(_RampTexture, _SamplerState_Linear_Clamp_sampler, float2(sampledSpecularIntensity * lighting.lightDiffuseIntensity, 0), 0);
	
	// For crosshatching
	/*float hatchBlend = (rampSpecular + rampDiffuse) * _CrosshatchShift;
	float hatchDiagonal = tex2D(_CrosshatchMapDiagonal, screenUV).a;
	float hatchMixed = tex2D(_CrosshatchMapMixed, screenUV).a;
	float patternBlend = smoothstep(
		_CrosshatchPatternShift + _CrosshatchPatternBoundary,
		_CrosshatchPatternShift - _CrosshatchPatternBoundary,
		hatchBlend
	);
	float hatchSample = lerp(hatchDiagonal, hatchMixed, saturate(patternBlend));
	hatchSample = step(hatchSample, hatchBlend);

	// Apply strength modifier
	#ifdef CROSSHATCH_USESTRENGTH
	hatchSample = (hatchSample * _CrosshatchStrength) - _CrosshatchStrength + 1;
	hatchMixed = (hatchMixed * _CrosshatchStrength) - _CrosshatchStrength + 1;
	#endif

	// Apply Hue Shift
	#ifdef CROSSHATCH_USEHUESHIFT
	half3 colourHSV = Unity_ColorspaceConversion_RGB_HSV_half(color); // Get HSV colour of albedo
	half originalValue = colourHSV.z;
	colourHSV.z = lighting.lightDiffuseIntensity; // Multiply value by lighting intensity
	colourHSV.z += lighting.lightSpecularIntensity;
	HueShiftColour(colourHSV);
	color.rgb = Unity_ColorspaceConversion_HSV_RGB_half(half3(colourHSV.rg, originalValue));
	#endif

	color += step((1.001 - hatchMixed) * (1 - smoothstep(_CrosshatchHighlightsShift - _CrosshatchHighlightsBoundary, _CrosshatchHighlightsShift + _CrosshatchHighlightsBoundary, rampSpecular)), rampSpecular); // v5.1 - fixed speckles
	color *= hatchSample;
	*/
	float lightingValueCH = (rampSpecular + rampDiffuse);
	float2 hatchSample = GetCrosshatching(lightingValueCH, rampSpecular, _CrosshatchTilingOverrideMultiplier * screenUV, _CrosshatchShift);
	color += hatchSample.y;
	color *= hatchSample.x;

	// Experimental - adding in point light colours by removing their value/luminance component
	half3 pureLightingColour = Unity_ColorspaceConversion_RGB_HSV_half(lighting.lightDiffuse + sampledSpecular);
	pureLightingColour.z = 1.0;
	pureLightingColour = Unity_ColorspaceConversion_HSV_RGB_half(pureLightingColour);
	color *= half4(pureLightingColour, 1.0);

	//color.rgb = MixFog(color.rgb, inputData.fogCoord);
	//SplatmapFinalColor(color, inputData.fogCoord); //Leaving this in for now

	// Might need to alpha clip terrain holes? Idk.

	return half4(color.rgb, 1.0h);
	//return half4(hatchDiagonal, 0.0h, 0.0h, 1.0h);
#endif
}

// Shadow pass

// Shadow Casting Light geometric parameters. These variables are used when applying the shadow Normal Bias and are set by UnityEngine.Rendering.Universal.ShadowUtils.SetupShadowCasterConstantBuffer in com.unity.render-pipelines.universal/Runtime/ShadowUtils.cs
// For Directional lights, _LightDirection is used when applying shadow Normal Bias.
// For Spot lights and Point lights, _LightPosition is used to compute the actual light direction because it is different at each shadow caster geometry vertex.
float3 _LightDirection;
float3 _LightPosition;

struct AttributesLean
{
	float4 position     : POSITION;
	float3 normalOS       : NORMAL;
#ifdef _ALPHATEST_ON
	float2 texcoord     : TEXCOORD0;
#endif
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct VaryingsLean
{
	float4 clipPos      : SV_POSITION;
#ifdef _ALPHATEST_ON
	float2 texcoord     : TEXCOORD0;
#endif
	UNITY_VERTEX_OUTPUT_STEREO
};

VaryingsLean ShadowPassVertex(AttributesLean v)
{
	VaryingsLean o = (VaryingsLean)0;
	UNITY_SETUP_INSTANCE_ID(v);
	TerrainInstancingCH(v.position, v.normalOS);

	float3 positionWS = TransformObjectToWorld(v.position.xyz);
	float3 normalWS = TransformObjectToWorldNormal(v.normalOS);

#if _CASTING_PUNCTUAL_LIGHT_SHADOW
	float3 lightDirectionWS = normalize(_LightPosition - positionWS);
#else
	float3 lightDirectionWS = _LightDirection;
#endif

	float4 clipPos = TransformWorldToHClip(ApplyShadowBias(positionWS, normalWS, lightDirectionWS));

#if UNITY_REVERSED_Z
	clipPos.z = min(clipPos.z, UNITY_NEAR_CLIP_VALUE);
#else
	clipPos.z = max(clipPos.z, UNITY_NEAR_CLIP_VALUE);
#endif

	o.clipPos = clipPos;

#ifdef _ALPHATEST_ON
	o.texcoord = v.texcoord;
#endif

	return o;
}

half4 ShadowPassFragment(VaryingsLean IN) : SV_TARGET
{
#ifdef _ALPHATEST_ON
	ClipHoles(IN.texcoord);
#endif
	return 0;
}

// Depth pass

VaryingsLean DepthOnlyVertex(AttributesLean v)
{
	VaryingsLean o = (VaryingsLean)0;
	UNITY_SETUP_INSTANCE_ID(v);
	UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
	TerrainInstancingCH(v.position, v.normalOS);
	o.clipPos = TransformObjectToHClip(v.position.xyz);
#ifdef _ALPHATEST_ON
	o.texcoord = v.texcoord;
#endif
	return o;
}

half4 DepthOnlyFragment(VaryingsLean IN) : SV_TARGET
{
#ifdef _ALPHATEST_ON
	ClipHoles(IN.texcoord);
#endif
#ifdef SCENESELECTIONPASS
// We use depth prepass for scene selection in the editor, this code allow to output the outline correctly
return half4(_ObjectId, _PassValue, 1.0, 1.0);
#endif
	return 0;
}


// DepthNormal pass
struct AttributesDepthNormal
{
	float4 positionOS : POSITION;
	float3 normalOS : NORMAL;
	float2 texcoord : TEXCOORD0;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct VaryingsDepthNormal
{
	float4 uvMainAndLM              : TEXCOORD0; // xy: control, zw: lightmap
#ifndef TERRAIN_SPLAT_BASEPASS
	float4 uvSplat01                : TEXCOORD1; // xy: splat0, zw: splat1
	float4 uvSplat23                : TEXCOORD2; // xy: splat2, zw: splat3
#endif

#if defined(_NORMALMAP) && !defined(ENABLE_TERRAIN_PERPIXEL_NORMAL)
	float4 normal                   : TEXCOORD3;    // xyz: normal, w: viewDir.x
	float4 tangent                  : TEXCOORD4;    // xyz: tangent, w: viewDir.y
	float4 bitangent                : TEXCOORD5;    // xyz: bitangent, w: viewDir.z
#else
	float3 normal                   : TEXCOORD3;
#endif

	float4 clipPos                  : SV_POSITION;
	UNITY_VERTEX_OUTPUT_STEREO
};

VaryingsDepthNormal DepthNormalOnlyVertex(AttributesDepthNormal v)
{
	VaryingsDepthNormal o = (VaryingsDepthNormal)0;

	UNITY_SETUP_INSTANCE_ID(v);
	UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
	TerrainInstancingCH(v.positionOS, v.normalOS, v.texcoord);

	VertexPositionInputs Attributes = GetVertexPositionInputs(v.positionOS.xyz);

	o.uvMainAndLM.xy = v.texcoord;
	o.uvMainAndLM.zw = v.texcoord * unity_LightmapST.xy + unity_LightmapST.zw;
#ifndef TERRAIN_SPLAT_BASEPASS
	o.uvSplat01.xy = TRANSFORM_TEX(v.texcoord, _Splat0);
	o.uvSplat01.zw = TRANSFORM_TEX(v.texcoord, _Splat1);
	o.uvSplat23.xy = TRANSFORM_TEX(v.texcoord, _Splat2);
	o.uvSplat23.zw = TRANSFORM_TEX(v.texcoord, _Splat3);
#endif

#if defined(_NORMALMAP) && !defined(ENABLE_TERRAIN_PERPIXEL_NORMAL)
	half3 viewDirWS = GetWorldSpaceViewDir(Attributes.positionWS);
#if !SHADER_HINT_NICE_QUALITY
	viewDirWS = SafeNormalize(viewDirWS);
#endif
	float4 vertexTangent = float4(cross(float3(0, 0, 1), v.normalOS), 1.0);
	VertexNormalInputs normalInput = GetVertexNormalInputs(v.normalOS, vertexTangent);

	o.normal = half4(normalInput.normalWS, viewDirWS.x);
	o.tangent = half4(normalInput.tangentWS, viewDirWS.y);
	o.bitangent = half4(normalInput.bitangentWS, viewDirWS.z);
#else
	o.normal = TransformObjectToWorldNormal(v.normalOS);
#endif

	o.clipPos = Attributes.positionCS;
	return o;
}

half4 DepthNormalOnlyFragment(VaryingsDepthNormal IN) : SV_TARGET
{
	#ifdef _ALPHATEST_ON
		ClipHoles(IN.uvMainAndLM.xy);
	#endif

	half3 normalWS = IN.normal;
	return float4(PackNormalOctRectEncode(TransformWorldToViewDir(normalWS, true)), 0.0, 0.0);
}

#endif
