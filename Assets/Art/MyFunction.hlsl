//UNITY_SHADER_NO_UPGRADE
#ifndef MYHLSLINCLUDE_INCLUDED
#define MYHLSLINCLUDE_INCLUDED

void MyFunction_float(float2 UV, out float2 Out) {
	Out = UV * 40;
}

#endif //MYHLSL_INCLUDED