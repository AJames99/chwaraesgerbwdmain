using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioBundleAbility", menuName = "ScriptableObjects/AudioBundle/AudioBundleAbility", order = 1)]
public class AudioBundleAbility : ScriptableObject {
	public AudioClip[] abilityClips_activate;		// Triggered on ability use
	public AudioClip[] abilityClips_fireProjectile; // Triggered when a projectile is launched
	public AudioClip[] abilityClips_regenerateAmmo;
	public AudioClip[] abilityClips_ammoFull;       // Triggered when ammo is regenerated to maximum
	public AudioClip[] abilityClips_cooldownComplete;

	public float m_flActivateVolume = 1.0f;
	public float m_flFireVolume = 1.0f;
	public float m_flRegenVolume = 1.0f;
	public float m_flRegenFullVolume = 1.0f;
	public float m_flCooldownVolume = 1.0f;

	public bool m_bPlaySingleRegenOnFull = false; // Play both the regen and the regen-full sounds when full?
}
