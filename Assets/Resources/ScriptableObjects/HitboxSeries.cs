using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Describes a sequence of hitboxes/hitbox activations to be played in order.
// Great for complex melee hit systems.
public class HitboxSeries : MonoBehaviour {
	public enum TempHitboxState {
		PREACTIVE,
		ACTIVE,
		DONE
	}

	[System.Serializable]
	public class TemporaryHitbox {
		// Fixed at runtime
		public SequenceHitbox m_pHitbox;
		public float m_flActivationTime;
		public float m_flDuration;
		
		// Change at runtime
		float m_flTimer;
		TempHitboxState m_state;

		public void Init() {
			m_state = TempHitboxState.DONE;
			m_flTimer = 0;
			m_pHitbox.gameObject.SetActive(false);
			//Debug.Log("Init");
		}

		// Progresses the sequence for this box
		public void Update() {
			switch (m_state) {
				case TempHitboxState.PREACTIVE:
					if (Time.time > m_flTimer) {
						m_state = TempHitboxState.ACTIVE;
						m_pHitbox.gameObject.SetActive(true);
						m_flTimer = Time.time + m_flDuration;
						//Debug.Log("Hitbox(" + m_pHitbox.name + ") Preactive -> Active");
					}
					break;
				case TempHitboxState.ACTIVE:
					if (Time.time > m_flTimer) {
						Stop();
						//Debug.Log("Hitbox(" + m_pHitbox.name + ") Active -> Done");
					}
					break;
				case TempHitboxState.DONE:
				default:
					return;
			}
		}

		// Restarts the sequence for this box
		public void Play() {
			m_state = TempHitboxState.PREACTIVE;
			m_flTimer = Time.time + m_flActivationTime;
			m_pHitbox.ClearBlacklist();
			//Debug.Log("Hitbox(" + m_pHitbox.name + ") Played");
		}

		public void Stop() {
			m_state = TempHitboxState.DONE;
			m_flTimer = 0;
			m_pHitbox.gameObject.SetActive(false);
			m_pHitbox.ClearBlacklist();
			//Debug.Log("Hitbox(" + m_pHitbox.name + ") Stopped");
		}

		public bool IsDone() {
			return m_state == TempHitboxState.DONE;
		}
	}

	[SerializeField]
	protected List<TemporaryHitbox> m_lstHitboxSeries;

	protected bool m_bAnimating;

	public AbilityUser m_pOwner;

	// Stops the sequence early when a hit is landed
	public bool m_bStopOnFirstHit = true;

	// Allows different hitboxes in a sequence to hit the same target
	public bool m_bAllowMultipleHitsOnTarget = false;

	private void Start() {
		m_bAnimating = false;

		foreach (TemporaryHitbox pBox in m_lstHitboxSeries) {
			pBox.m_pHitbox.SetSequencer(this);
		}
	}

	public void Init(Damageable pDamageableOwner) {
		foreach (TemporaryHitbox pHitBox in m_lstHitboxSeries) {
			pHitBox.Init();
			pHitBox.m_pHitbox.SetDamageable(pDamageableOwner);
			pHitBox.m_pHitbox.SetSequencer(this);
		}
		m_bAnimating = false;
	}

	public void Play() {
		m_bAnimating = true;
		foreach (TemporaryHitbox pHitBox in m_lstHitboxSeries) {
			pHitBox.Play();
		}
	}

	public void Stop() {
		if (m_bAnimating) {
			m_bAnimating = false;
			foreach (TemporaryHitbox pHitBox in m_lstHitboxSeries) {
				pHitBox.Stop();
			}
		}
	}

	public void Update() {
		if (m_bAnimating) {
			bool bContinue = false;
			foreach (TemporaryHitbox pHitBox in m_lstHitboxSeries) {
				pHitBox.Update();
				// Set our animating flag to false if all boxes in the series are done
				if (!pHitBox.IsDone())
					bContinue = true;
			}

			if (!bContinue)
				m_bAnimating = false;
		}
	}

	public bool IsAnimating() {
		return m_bAnimating;
	}

	public void OnHit(SequenceHitbox pBox, Hitbox pOther) {
		if (m_bStopOnFirstHit) {
			Stop();
		} else if (!m_bAllowMultipleHitsOnTarget && pOther.GetDamageable()) {
			// Tell all our boxes that this target has been hit and to blacklist them
			foreach (TemporaryHitbox pTempBox in m_lstHitboxSeries) {
				pTempBox.m_pHitbox.AppendToBlackList(pOther.GetDamageable());
			}
			//Debug.Log("Blacklisted damageable " + pOther.GetDamageable().name);
		}
	}
}
