using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioBundleCharacter", menuName = "ScriptableObjects/AudioBundle/AudioBundleCharacter", order = 1)]
public class AudioBundleCharacter : ScriptableObject {
	public AudioClip[] impactWallSoft;
	public AudioClip[] impactWallHard;
	public AudioClip[] impactOtherSoft;
	public AudioClip[] impactOtherHard;
}