using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioBundleChargeableAbility", menuName = "ScriptableObjects/AudioBundle/AudioBundleChargeableAbility", order = 1)]
public class AudioBundleChargeableAbility : AudioBundleAbility {
	public AudioClip[] abilityClips_chargeBegin;
	public AudioClip[] abilityClips_chargeIncrement;
	public AudioClip[] abilityClips_chargeMax;
	public AudioClip[] abilityClips_chargeRelease;

	public float m_flChargeBeginVolume = 0.5f;
	public float m_flChargeIncrementVolume = 0.5f;
	public float m_flChargeMaxVolume = 0.5f;
	public float m_flChargeReleaseVolume = 0.4f;
}
