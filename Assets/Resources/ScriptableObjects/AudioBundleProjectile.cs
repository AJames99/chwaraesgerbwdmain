using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioBundleProjectile", menuName = "ScriptableObjects/AudioBundle/AudioBundleProjectile", order = 1)]
public class AudioBundleProjectile : ScriptableObject {
	public AudioClip[] projectileClips_hitWorld;
	public AudioClip[] projectileClips_hitEnemy;
	public float m_flHitVolumeScaleBase = 1.0f;
}
