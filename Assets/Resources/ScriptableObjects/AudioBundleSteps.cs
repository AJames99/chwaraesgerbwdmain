using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioBundleSteps", menuName = "ScriptableObjects/AudioBundle/AudioBundleSteps", order = 1)]
public class AudioBundleSteps : ScriptableObject {
	public enum StepMaterialTypes {
		sand,
		gravel,
		wood
	}
	public AudioClip[] stepsClips_Sand;        // Specifically for particular materials
	public AudioClip[] stepsClips_Gravel;
	public AudioClip[] stepsClips_Wood;
	public AudioClip[] stepsClips_footSound;    // Always plays on top of other step sounds
	public AudioClip stepClips_jump;
}
