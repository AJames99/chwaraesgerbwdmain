using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioBundlePlayer", menuName = "ScriptableObjects/AudioBundle/AudioBundlePlayer", order = 1)]
public class AudioBundlePlayer : AudioBundleCharacter {
	public AudioClip[] impactSoftShield;
	public AudioClip[] impactHardBash;

	public AudioClip m_acHitsound;
	public AudioClip m_acHitsoundCrit;

	public Vector4 m_v4HitSoundVolumeDamageMap = new Vector4(10, 100, 0.1f, 1.5f);
	public float m_flCritSoundVolume = 0.5f;
}