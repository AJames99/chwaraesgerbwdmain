﻿/*Shader "Hidden/BloomToBlackShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
		SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		//Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline"}
		Tags { "Queue" = "Overlay" "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline"}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v) {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
			// just invert the colors:
			//col.rgb = 1 - col.rgb;

			if (isnan(col.r)) {
				col = float4(0.0f, 1.0f, 0.0f, col.a);
			}
				return col;
			}
			ENDCG
		}
	}
}*/

Shader "Hidden/BloomToBlackShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}

		HLSLINCLUDE

#pragma exclude_renderers gles
#pragma target 3.5

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Shaders/PostProcessing/Common.hlsl"

#define NAN_COLOR half3(0.0, 0.0, 0.0)

		TEXTURE2D_X(_MainTex);

	half4 Frag(Varyings input) : SV_Target
	{
		UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
		half3 color = SAMPLE_TEXTURE2D_X(_MainTex, sampler_PointClamp, UnityStereoTransformScreenSpaceTex(input.uv)).xyz;

		if (AnyIsNaN(color) || AnyIsInf(color))
			color = NAN_COLOR;

		return half4(color, 1.0);
	}

		ENDHLSL
		SubShader {
		Tags{ "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" }
			LOD 100
			ZTest Always ZWrite Off Cull Off

			Pass
		{
			Name "Bloom To Black"

			HLSLPROGRAM
				#pragma vertex Vert
				#pragma fragment Frag
			ENDHLSL
		}
	}
}
