using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CydrannauEndid;
using DataBank;
using DataBank.ItemSystem;

[CreateAssetMenu(fileName = "Eitem", menuName = "ScriptableObjects/Eitemau", order = 1)]
public class Eitem : ScriptableObject{
	// Physical Item Prefab
	//[SerializeField] public GameObject m_parodEitemMaterol { get; private set; }
	// Physical Item entity name
	[SerializeField] public string m_enwEitemMaterol;

	// Item ID
	[SerializeField] public string m_enwEitem;

	// The regions this item can be equipped to
	[SerializeField] public List<ItemUserSlot.EquipRegion> m_equipRegions = new List<ItemUserSlot.EquipRegion>();

	[HideInInspector] public ItemUserSlot m_currentSlot;

	// On Equip
	public void ArRhoi(CydranEndidDefnyddiwrEitem defnyddiwr, ItemUserSlot newSlot) {
		m_currentSlot = newSlot;
		AdiwchNodweddionIDefnyddiwr(defnyddiwr);
	}

	// On Unequip
	public void ArDynnu(CydranEndidDefnyddiwrEitem defnyddiwr) {
		EntityInstancingManager.instance.CreaEitemMaterol(this, defnyddiwr);
		DilewchNodweddionODefnyddiwr(defnyddiwr);
	}

	// Add attributes to user
	public void AdiwchNodweddionIDefnyddiwr(CydranEndidDefnyddiwrEitem defnyddiwr) {
		if (m_enwEitem.Length < 1) {
			return;
		}

		CydranEndidNodweddion cydNod = defnyddiwr.m_gwasanaethau.GetEntityComponent<CydranEndidNodweddion>();
		Assets.DiffiniadEitem diffiniadEitem = GameDataBankManager.instance.m_itemDataBank.CaelDiffiniadEitem(m_enwEitem);
		if (diffiniadEitem != null) {
			foreach (Assets.AddasyddNodweddInstans instans in diffiniadEitem.NodweddionAddasyddEitem) {
				NodweddionEndid.EntityAttributeModifier addasydd = NodweddionEndid.EntityAttributeModifier.CreateModifierFromInstance(instans);
				if (addasydd != null) {
					cydNod.AdioAddasyddNodwedd(addasydd);
				}
			}
		} else {
			Debug.LogWarning("Couldn't add attributes for item of name " + m_enwEitem + ": DataBank has no item definition by that ID.");
		}
	}

	// Remove attributes from user
	public void DilewchNodweddionODefnyddiwr(CydranEndidDefnyddiwrEitem defnyddiwr) {
		CydranEndidNodweddion cydNod = defnyddiwr.m_gwasanaethau.GetEntityComponent<CydranEndidNodweddion>();
		Assets.DiffiniadEitem diffiniadEitem = GameDataBankManager.instance.m_itemDataBank.CaelDiffiniadEitem(m_enwEitem);
		foreach (Assets.AddasyddNodweddInstans instans in diffiniadEitem.NodweddionAddasyddEitem) {
			AttributeModifierEntry diffiniadAddasydd = GameDataBankManager.instance.m_attributeDataBank.GetAttributeModifier(instans.EnwAddasydd);
			cydNod.DileuAddasyddNodwedd(instans);
		}
	}

	public bool TrySwitchSlot(CydranEndidDefnyddiwrEitem defnyddiwr, ItemUserSlot slot) {
		ItemUserSlot.SlotAvailability availability;
		if (defnyddiwr.HasSlotsForItem(this, out availability, false)) {
			if (availability == ItemUserSlot.SlotAvailability.EmptySlotsAvailable) {
				defnyddiwr.CeisioRhoiEitem(this, false);
			}
			return true;
		}
		return false;
	}
}

public class SortItemByRestriction : IComparer<Eitem> {

	// X is greater than Y if it is MORE restricted - outputs 1
	public int Compare(Eitem x, Eitem y) {
		int numRegionsX = x.m_equipRegions.Count;
		int numRegionsY = y.m_equipRegions.Count;

		if (numRegionsX == numRegionsY)
			return 0;

		// No regions means unrestricted
		if (numRegionsX == 0 && numRegionsY != 0) {
			return -1;
		} else if (numRegionsX != 0 && numRegionsY == 0) {
			return 1;
		}

		return numRegionsY.CompareTo(numRegionsX);
	}
}