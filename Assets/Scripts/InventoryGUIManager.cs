using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

// Manages OnDrop functionality for the inventory background
public class InventoryGUIManager : MonoBehaviour, IDropHandler {
	// If an item is dropped onto us
	public void OnDrop(PointerEventData eventData) {
		ItemScript itemDropped = null;
		if (eventData.pointerDrag.TryGetComponent(out itemDropped)) {
			itemDropped.isFloating = false;
			((RectTransform)itemDropped.transform).SetParent((RectTransform)itemDropped.containerSlot.transform);
			((RectTransform)itemDropped.transform).SetAsLastSibling();
			((RectTransform)itemDropped.transform).anchoredPosition = Vector3.zero;
			Debug.Log("Item dropped on background.");
		}
	}
}
