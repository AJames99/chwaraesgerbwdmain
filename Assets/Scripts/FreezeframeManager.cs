using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeframeManager : MonoBehaviour {

    public static FreezeframeManager m_pManagerStatic = null;

    [SerializeField]
    float m_flFreezeTime = 0.1f;

    bool m_bIsFrozen = false;

    float m_flUnfreezeTime = -1.0f;

    float m_flCachedTimescale = 1.0f;

    FreezeframeManager() {
        if (m_pManagerStatic == null) {
            m_pManagerStatic = this;
            //Debug.Log("FFManager created.");
        }
    }

    private void Start() {
        if (m_pManagerStatic == null) {
            m_pManagerStatic = this;
           // Debug.Log("FFManager created.");
        }
    }

    private void Update() {
        if (m_bIsFrozen) {
            if (Time.realtimeSinceStartup > m_flUnfreezeTime)
                Unfreeze();
        }
    }

    public void Freeze(float freezeTime = -1) {
        if (freezeTime < 0)
            freezeTime = m_flFreezeTime;

        //Debug.Log("Freeze time: " + freezeTime);

        m_flUnfreezeTime = Time.realtimeSinceStartup + freezeTime;
        m_bIsFrozen = true;

        m_flCachedTimescale = Time.timeScale;
        Time.timeScale = 0.0f;
    }

    public void Unfreeze() {
        m_bIsFrozen = false;
        Time.timeScale = m_flCachedTimescale;
    }
}
