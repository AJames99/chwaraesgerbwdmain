using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceHitbox : Hitbox {
	HitboxSeries m_pSeries;
	public void SetSequencer(HitboxSeries pSeries) {
		m_pSeries = pSeries;
	}

	public override void OnHit(Hitbox pOther) {
		base.OnHit(pOther);
		m_pSeries.OnHit(this, pOther);
	}
}
