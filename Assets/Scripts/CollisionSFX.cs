﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionSFX : MonoBehaviour {

	[SerializeField]
	private AudioClip[] _hardImpacts;

	[SerializeField]
	[Range(0, 25)]
	private float _hardImpactVelocityThreshold = 15;

	[SerializeField]
	private AudioClip[] _softImpacts;

	[SerializeField]
	[Range(0, 25)]
	private float _softImpactVelocityThreshold = 0f;

	[SerializeField]
	private AudioClip[] _breakSounds;

	[SerializeField]
	private AudioClip[] _dragSounds;


	[SerializeField]
	[Range(0, 4)]

	private float volume_Multiplier = 1f;

	//private AudioSource audioSource;

	private bool bHardImpacts = false;
	private bool bSoftImpacts = false;
	private bool bBreakSounds = false;
	private bool bDragSounds = false;

	[SerializeField]
	private bool producesDetectableSounds = false;
	[SerializeField]
	private bool producesDetectableOnHardImpactOnly = false;
	[Range(0f, 6f)]
	[SerializeField]
	private float detectableSoundVolMultiplier = 1.0f;

	//DetectableSoundScript dss;

	// Start is called before the first frame update
	/*void Start() {
		audioSource = gameObject.AddComponent<AudioSource>();
	}*/

	/*private void Start() {
		if (producesDetectableSounds) {
			dss = gameObject.AddComponent<DetectableSoundScript>();
		}
	}*/

	// Update is called once per frame
	private void OnCollisionEnter(Collision collision) {
		float vel = collision.relativeVelocity.magnitude;
		float audioLevel = Mathf.Min(vel / 10.0f, 1.5f);
		if (vel > _hardImpactVelocityThreshold) {
			//AudioSource.PlayClipAtPoint(_hardImpacts[Mathf.RoundToInt(Random.value * (_hardImpacts.Length - 1))], collision.contacts[0].point, audioLevel);
			PlayHard(collision.contacts[0].point, audioLevel);
		} else if (vel > _softImpactVelocityThreshold) {
			//AudioSource.PlayClipAtPoint(_softImpacts[Mathf.RoundToInt(Random.value * (_softImpacts.Length - 1))], collision.contacts[0].point, audioLevel);
			PlaySoft(collision.contacts[0].point, audioLevel);
		}
	}

	public void Break() {
		if (_breakSounds.Length > 0) {
			AudioSource.PlayClipAtPoint(_breakSounds[Mathf.RoundToInt(Random.value * (_breakSounds.Length - 1))], transform.position, 0.5f * volume_Multiplier);
		}
	}

	public void PlaySoft(Vector3 point, float vol) {
		/*if (producesDetectableSounds) {
			if (!producesDetectableOnHardImpactOnly && vol > 0.05f) {
				dss.CreateDetectableSound(point, detectableSoundVolMultiplier * (vol / 2f), 0.1f);
			}
		}*/
		AudioSource.PlayClipAtPoint(_softImpacts[Mathf.RoundToInt(Random.value * (_softImpacts.Length - 1))], point, vol * volume_Multiplier);
	}
	public void PlayHard(Vector3 point, float vol) {
		/*if (producesDetectableSounds) {
			if (vol > 0.05f) {
				dss.CreateDetectableSound(point, vol * detectableSoundVolMultiplier, 0.1f);
			}
	}*/
		AudioSource.PlayClipAtPoint(_hardImpacts[Mathf.RoundToInt(Random.value * (_hardImpacts.Length - 1))], point, vol * volume_Multiplier);
	}
}
