﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour {
	[SerializeField]
	private float maxLifespan = 0.5f;

	[SerializeField]
	//private float growthRate = 1.0f;
	private float maxRadius = 8.0f;

	[SerializeField]
	private float _pushForce = 10.0f;

	[SerializeField]
	private float _verticalBoostfactor = 1.0f;

	[SerializeField]
	private int _maxDamage = 25;

	[SerializeField]
	private AudioClip[] _explosionClips;

	[SerializeField]
	private float playerFactor = 0.5f; // 0.05*/

	[SerializeField]
	private LayerMask ignoreLayers;

	private float growthRate;

	private float minPushDistClamp = 0.25f; // If an affected object within this distance, clamp the push force to the maximum.

	public int team;

	[SerializeField]
	ClipPlayer clipPlayer;

	public void Explode() {
		//AudioSource.PlayClipAtPoint(_explosionClips[_explosionClips.GetRandomIndex()], transform.position, 0.5f);
		if (clipPlayer) {
			AudioClip clip = _explosionClips[_explosionClips.GetRandomIndex()];
			clipPlayer.PlayClipAtPoint(clip, transform.position, 0.05f);
			//maxLifespan += clip.length; // OLD - extended lfietime to match audio clip, now audio clip is spawned separately
		}

		// Do collision checks ONCE (at the start), using the resultant maximum 
		Vector3 centre = transform.position;
		Collider[] hitColliders = Physics.OverlapSphere(centre, maxRadius, ~ignoreLayers, QueryTriggerInteraction.Collide);
		foreach (Collider hit in hitColliders) {
			if (hit.isTrigger) {
				Rigidbody rb;
				Vector3 distanceVec = hit.ClosestPoint(centre) - centre;

				float closeness = Mathf.Clamp(maxRadius - distanceVec.magnitude, 0, maxRadius);
				float factor = 0;
				if (closeness >= (maxRadius - minPushDistClamp)) {
					factor = 1f;
				} else {
					factor = (closeness / maxRadius);
				}

				if (hit.gameObject.TryGetComponent(out rb)) {
					Vector3 force = distanceVec.normalized;
					force *= _pushForce * factor;

					rb.AddForce(force, ForceMode.VelocityChange);

					Damageable dmg;
					if (hit.gameObject.TryGetComponent(out dmg)) {
						if (dmg.team != team) {
							dmg.Hurt(new Damage(Damage.DamageTypes.EXPLOSION, Mathf.CeilToInt(_maxDamage * factor), transform.position));
						}
					}


				} else {
					PlayerCharacter pc = hit.GetComponent<PlayerCharacter>();
					if (pc) {
						Vector3 force = distanceVec.normalized;
						force *= _pushForce * factor * playerFactor;
						force.y *= _verticalBoostfactor;

						// Is the shield in the way? Or are we on the same team?
						// This was the old code; new code defers to playercharacter for handling
						/*if (Physics.Raycast(transform.position, (hit.transform.position - transform.position).normalized, 15f, shieldLayer.value, QueryTriggerInteraction.Ignore) || team == pc.team) {
							force *= 3f;
						} else {
							pc.Hurt(new Damage(Damage.DamageTypes.DEFAULT, Mathf.CeilToInt(_maxDamage * factor)));
						}*/

						// New:
						//pc.Hurt(new Damage(Damage.DamageTypes.EXPLOSION, team == pc.team ? 0 : Mathf.CeilToInt(_maxDamage * factor), centre));
						pc.Hurt(new Damage(Damage.DamageTypes.EXPLOSION, Mathf.CeilToInt(_maxDamage * factor), centre));
						pc.Push(force, centre);
					}
				}
			}
		}
		Destroy(gameObject, maxLifespan);
	}

}
