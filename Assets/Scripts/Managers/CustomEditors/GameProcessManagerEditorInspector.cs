using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

// Long ass class name lol GaDaMEI for short.
[CustomEditor(typeof(GameProcessManager))]
public class GameProcessManagerEditorInspector : Editor
{
    public override void OnInspectorGUI() {
        GameProcessManager GameProcessManager = (GameProcessManager)target;

        //bool resultGlobalShift = EditorGUILayout.Toggle("Enable global crosshatch shifting", GameProcessManager.m_CHUseDynamicRandomSampling);
        //bool resultPerObjectShift = EditorGUILayout.Toggle("Enable per-object unique crosshatch shifting", GameProcessManager.m_CHUsePerObjectRandomSampling);
        // set GameProcessManager member vars to above bools

        if (GUILayout.Button("Restart Procesesses")) {
            GameProcessManager.RestartProcesses();
        }

        DrawDefaultInspector();
    }
}
