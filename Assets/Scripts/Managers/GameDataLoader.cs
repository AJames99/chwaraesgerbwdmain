using UnityEngine;
using System.IO;
using System;
using Assets;
using DataBank;

// When attached to a gameobject, reads the data from the gamebank on start.
// The database will basically be tied this gameobject.
public class GameDataLoader : MonoBehaviour
{
	// Should save temp data to here btw
	//UnityEngine.Windows.Directory.roamingFolder

	public string directoryPath = "";

	private void Start() {
		LoadData(Path.Combine(Application.dataPath, "gamedata.gdjs"));
	}

	public void LoadData(string directoryFull) {
		if (File.Exists(directoryFull)) {
			FileStream fileStream = File.OpenRead(directoryFull);
			GameData gameData = new GameData(fileStream, GameData.Format.Json);
			//ChwaraeSgerbwdAmlwg manifest = ChwaraeSgerbwdAmlwg.Manifest(jsonContentsManifest, directoryFull);

			// Load attributes (defaults and modifiers)
			GameDataBankManager.instance.m_attributeDataBank.LoadData(gameData.GetDiffiniadNodwedds(), gameData.GetAddasyddNodwedds());
			GameDataBankManager.instance.m_entityDataBank.LoadData("Endidau");
			GameDataBankManager.instance.m_itemDataBank.LoadData("Eitemau", gameData.GetDiffiniadEitems());
			GameDataBankManager.instance.SetGameData(gameData);
			GameProcessManager.instance.GameDatabaseLoaded();
		} else {
			Debug.LogError("Game data file doesn't exist!");
		}
	}
}
