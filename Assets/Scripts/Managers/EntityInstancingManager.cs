using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataBank;
using CydrannauEndid;


public class EntityInstanceParameters {
	public Vector3 worldPosition;
	public Quaternion worldRotation;
	public Transform transformParent;
	public static EntityInstanceParameters Params(Vector3 worldPos = new Vector3(), Quaternion worldRot = default(Quaternion), Transform parent = null) {
		return new EntityInstanceParameters {
			worldPosition = worldPos,
			worldRotation = worldRot,
			transformParent = parent
		};
	}
}

// Singleton
public class EntityInstancingManager : MonoBehaviour
{
	private static EntityInstancingManager _instance;
	public static EntityInstancingManager instance {
		get {
			if (_instance == null) {
				_instance = new GameObject().AddComponent<EntityInstancingManager>();
				_instance.name = _instance.GetType().Name;
				DontDestroyOnLoad(_instance.gameObject);
			}
			return _instance;
		}
	}

	// Start is called before the first frame update
	void Start()
    {
		if (instance != null && instance != this) {
			Destroy(this);
		}
	}

	public Endid InstantiateEntity(string entityId, EntityInstanceParameters instanceParams) {
		if (GameDataBankManager.instance == null) {
			Debug.LogError("Tried to instantiate entity while GameDataBankManager was null!");
			return null;
		}

		GameObject prefab = GameDataBankManager.instance.CeisioCaelEndid(entityId);
		if (prefab != null) {
			GameObject entityGameObject = GameProcessManager.InstantiateObject(prefab, true);
			entityGameObject.transform.parent = instanceParams.transformParent;
			entityGameObject.transform.position = instanceParams.worldPosition;
			entityGameObject.transform.rotation = instanceParams.worldRotation;
			Endid entity = entityGameObject.GetComponent<Endid>();
			if (entity != null) {
				entity.Initialise();
			} else {
				Debug.LogWarning("Instantiated entity " + entityId + " has no entity component (Endid)!");
			}
			entityGameObject.SetActive(true);
			return entityGameObject.GetComponent<Endid>();
		} else {
			Debug.LogError("Couldn't instantiate object of entity ID " + entityId);
		}

		return null;
	}

	// Materialises and item, spawning its physicalised object in the world at the specified position and carrying across any runtime changes to its item definition.
	public void CreaEitemMaterol(Eitem eitem, Vector3 position) {
		Endid endidNewydd = InstantiateEntity(eitem.m_enwEitemMaterol, EntityInstanceParameters.Params(position));
		if (endidNewydd) {
			CydranEndidEitemMaterol eitemMaterol = endidNewydd.m_utils.GetEntityComponent<CydranEndidEitemMaterol>();
			if (eitemMaterol) {
				eitemMaterol.UpdateItemDef(eitem);
			} else {
				Debug.LogError("Unable to fully initialise physicalised item of name " + eitem.m_enwEitem + " and physicalised entity ID " + eitem.m_enwEitemMaterol + "; Spawned entity did not have a physical item component.");
			}
		} else {
			Debug.LogError("Unable to create physicalisd item of name " + eitem.m_enwEitem + " and physicalised entity ID " + eitem.m_enwEitemMaterol + "; Entity could not be instantiated.");
		}
	}

	// Materialises and item, spawning its physicalised object in the world by the item's owner and carrying across any runtime changes to its item definition.
	public void CreaEitemMaterol(Eitem eitem, CydranEndidDefnyddiwrEitem defnyddiwr) {
		Vector2 randomOffset = Random.insideUnitCircle * 3.0f;
		Vector3 worldPos = defnyddiwr.gameObject.transform.position + new Vector3(randomOffset.x, 3.0f, randomOffset.y);
		CreaEitemMaterol(eitem, worldPos);
	}
}
