using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using DataBank;

// Manages processes for setting data on shaders and materials in a scene.
// IMPORTANT: If the component is disabled, it will appear to work in the editor BUT NOT THE player!
[ExecuteAlways, UnityEditor.InitializeOnLoad]
public class GameProcessManager : MonoBehaviour {

	private static GameProcessManager _instance = null;
	public static GameProcessManager instance {
		get {
			if (_instance == null) {
				_instance = (GameProcessManager)FindObjectOfType(typeof(GameProcessManager));
			}
			return _instance;
		}
		private set { _instance = value; }
	}

	static GameProcessManager() {
		UnityEditor.EditorApplication.playModeStateChanged += OnPlayModeChanged;
		SceneManager.activeSceneChanged += OnSceneChanged;
		UnityEditor.EditorApplication.update += OnEditorLoaded;
		UnityEditor.EditorApplication.update += EditorUpdate;
	}

	[System.Flags]
	public enum ProcessTypeFlags : byte {
		None = 0,
		OnInstantiate = 1 << 1,
		OnAwake = 1 << 2,
		OnUpdate = 1 << 3,
		OnUpdateTimer = 1 << 4,
		OnValidateInspectorChanges = 1 << 5,
		OnDatabaseLoaded = 1 << 6,
		OnEditorStartup = 1 << 7
	};

	// Used to gather any special data before processing.
	// AllSceneObjects: gathers all the scene's objects (Expensive, only done if any processes exist w/ this type)
	public enum ProcessSpecialInputType : byte {
		FIRST = 0,
		None = 0,
		AllSceneObjects,
		COUNT
	};

	public class ManagerProcess {
		public ProcessSpecialInputType m_specialInputType;
		public ManagerProcess(ProcessTypeFlags processTypeFlags, ProcessSpecialInputType specialInputType = ProcessSpecialInputType.None) {
			m_specialInputType = specialInputType;
			m_processFlags = processTypeFlags;
		}

		public delegate void PerformProcess_GameObject(GameObject gameObject);
		public delegate void PerformProcess();
		public PerformProcess_GameObject m_processDelegateGameObject;
		public PerformProcess m_processDelegate;

		public ProcessTypeFlags m_processFlags;

		public float m_timerMax;
	}

	// -- Members --
	Dictionary<ProcessTypeFlags, List<ManagerProcess>> m_processes = new Dictionary<ProcessTypeFlags, List<ManagerProcess>>();
	Dictionary<ProcessTypeFlags, uint> m_processTypeCounts = new Dictionary<ProcessTypeFlags, uint>();
	Dictionary<ProcessSpecialInputType, uint> m_processInputTypeCounts = new Dictionary<ProcessSpecialInputType, uint>();
	Dictionary<ManagerProcess, float> m_processTimers = new Dictionary<ManagerProcess, float>();

	// Contains all the game objects in the scene, if needed
	[HideInInspector]
	public List<GameObject> m_allGameObjects = new List<GameObject>();

	bool m_bHasInstanceProcess = false;
	bool m_bHasTimerProcess = false;
	bool m_bHasOnValidateProcess = false;
	bool m_bHasUpdateProcess = false; // Update processes and Update-timer processes
	float m_fRealTimeLast = 0.0f;

	#region EditorParams
	[Header("Crosshatch Global Params")]
	[SerializeField, InspectorName("Crosshatch Diagonal Texture")]
	Texture2D m_CHDiagonalTexture;						// Tiling should be (1,1) and offset (0,0) for both. Handle tiling manually in shader.

	[SerializeField, InspectorName("Crosshatch Bidirectional/X Texture")]
	Texture2D m_CHCrossedTexture;
	
	[SerializeField, InspectorName("Crosshatch Tiling Factor")]
	[Range(0.1f, 25.0f)]
	float m_CrosshatchTilingFactor = 2.0f;

	[Range(0.05f, 25.0f)]
	[SerializeField, InspectorName("Crosshatch Highlight Tiling Factor (Relative to base tiling factor)")]
	float m_CrosshatchHighlightTilingFactor = 2.0f;

	[Range(-1.0f, 1.0f)]
	[SerializeField, InspectorName("Crosshatch Pattern Bias")]
	float m_CHPatternBias = 0.5f;

	[Range(0f, 1.0f)]
	[SerializeField, InspectorName("Crosshatch Boundary Softness")]
	float m_CHBoundarySoftness = 0.15f;

	[Range(-1.0f, 1.0f)]
	[SerializeField, InspectorName("Crosshatch Highlight Bias")]
	float m_CHHighlightBias = 1.0f;

	[Range(-1.0f, 1.0f)]
	[SerializeField, InspectorName("Crosshatch Highlight Boundary Softness")]
	float m_CHHighlightBoundarySoftness = 0.2f;

	[SerializeField, InspectorName("Crosshatch & Lighting Ramp Texture")]
	Texture2D m_CHRampTexture;

	[Range(0.0f, 10.0f)]
	[SerializeField, InspectorName("Rim Lighting Strength")]
	float m_CHRimStrength = 0.5f;

	[Range(0.0f, 10.0f)]
	[SerializeField, InspectorName("Crosshatch Point Light Attenuation Boost"), Tooltip("Multiplier for the attenuation value of point lights, to make them more effective at illuminating CH shaded objects")]
	float m_CHPointLightAttenBoost = 1.0f;

	[Header("Crosshatch Global Dynamic Params")]
	[SerializeField, Tooltip("The interval in seconds that the global offset for crosshatch sampling should be randomised, to give a jittery hand drawn effect"), Range(0.01f, 5.0f)]
	float m_CHDynamicRandomSamplingOffsetInterval = 0.4f;
	[SerializeField, Tooltip("The maximum absolute shift the global offset can be (i.e. 2 will result in values between -2 and 2")]
	Vector2 m_CHDynamicRandomSamplingOffsetMax = new Vector2(1,1);
	[SerializeField, Tooltip("The maximum rotation shift the global offset can be (degrees)")]
	float m_CHDynamicRandomSamplingOffsetRotation = 15.0f;
	[SerializeField, Tooltip("Whether to enable offsetting all crosshatch sampling by a random position and rotation")]
	bool m_CHUseDynamicRandomSampling;
	[SerializeField, Tooltip("Whether to enable the per-object unique offset")]
	bool m_CHUsePerObjectRandomSampling;
	[SerializeField, Tooltip("Whether to enable offsetting crosshatch sampling by the camera's rotation")]
	bool m_CHUseCamRotationOffset;
	[SerializeField, Tooltip("Amount of hue shift relative to value"), Range(-1.5f, 1.5f)]
	float m_CHHueShiftHue;
	[SerializeField, Tooltip("Amount of saturation shift relative to value"), Range(-1.5f, 1.5f)]
	float m_CHHueShiftSaturation;
	[SerializeField, Tooltip("Strength of crosshatching (reduces opacity)"), Range(0.0f,1.0f)]
	float m_CHCrosshatchStrength;

	[Header("Debug Tools")]
	[SerializeField, Tooltip("Enables the debug entity component (automatically added, loaded from entity data JSON)")]
	bool m_enableEntityComponentDebug;

	/*[Header("Camera Movement")]
	[SerializeField, Tooltip("The maximum distance the camera's 'delayed' position can be away from the camera's current position")]
	float m_cameraDelayDistanceMax = 1.0f;
	[SerializeField, Tooltip("The rate at which the camera's delayed position catches up to the camera's current position (in units/second)")]
	float m_cameraDelayCatchupRate = 10.0f;
	[SerializeField, Tooltip("The scale of the camera-delay-based vignette offset; also applied to all CH pattern offsets")]
	float m_vignetteCameraDelayInfluence = 0.1f;
	// Inverse camera movement tracking so we can shift screen-space things and make them feel responsive;
	Vector3 m_cameraPosDelayed;
	Quaternion m_cameraRotDelayed;
	Vector2 m_vignetteOffset = new Vector2();*/
	#endregion

	void CheckHasProcessTypes() {
		m_bHasInstanceProcess = m_processTypeCounts[ProcessTypeFlags.OnInstantiate] > 0;
		m_bHasTimerProcess = m_processTypeCounts[ProcessTypeFlags.OnUpdateTimer] > 0;
		m_bHasOnValidateProcess = m_processTypeCounts[ProcessTypeFlags.OnValidateInspectorChanges] > 0;
		m_bHasUpdateProcess = m_bHasTimerProcess | m_processTypeCounts[ProcessTypeFlags.OnUpdate] > 0;
	}

	void AddProcess(ManagerProcess process) {	
		foreach (ProcessTypeFlags flagValue in System.Enum.GetValues(typeof(ProcessTypeFlags))) {
			if (flagValue.Equals(ProcessTypeFlags.None)) {
				continue;
			}

			if (process.m_processFlags.HasFlag(flagValue)) {
				m_processTypeCounts[flagValue]++;
				m_processes[flagValue].Add(process);
			}
		}

		m_processInputTypeCounts[process.m_specialInputType]++;

		CheckHasProcessTypes();

		if (process.m_processFlags.HasFlag(ProcessTypeFlags.OnUpdateTimer)) {
			m_processTimers.Add(process, process.m_timerMax);
		}
	}

	void RemoveProcess(ManagerProcess process) {
		// If the count is 0 and this object already existed in the dictionary,
		// Something has gone horribly wrong! Avoid underflow
		//Assert.AreNotEqual(0, m_processTypeCounts[process.GetProcessType()]);
		//Assert.AreNotEqual(0, m_processInputTypeCounts[process.m_specialInputType]);

		foreach (ProcessTypeFlags flagValue in System.Enum.GetValues(typeof(ProcessTypeFlags))) {
			if (flagValue.Equals(ProcessTypeFlags.None)) {
				continue;
			}

			if (process.m_processFlags.HasFlag(flagValue)) {
				m_processTypeCounts[flagValue]--;
				m_processes[flagValue].Remove(process);
			}
		}

		m_processInputTypeCounts[process.m_specialInputType]--;

		if (process.m_processFlags.HasFlag(ProcessTypeFlags.OnUpdateTimer)) {
			m_processTimers.Remove(process);
		}

		CheckHasProcessTypes();
	}

	public static void OnEditorLoaded() {
		Debug.Log("OnEditorLoaded");

		instance.InitialiseManager();

		UnityEditor.EditorApplication.update -= OnEditorLoaded;
	}

	public static void OnSceneChanged(Scene current, Scene next) {
		Debug.Log("OnSceneChanged");
		instance.InitialiseManager();
	}

	public static void OnPlayModeChanged(UnityEditor.PlayModeStateChange state) {
		if (state == UnityEditor.PlayModeStateChange.EnteredEditMode || state == UnityEditor.PlayModeStateChange.ExitingPlayMode) {
			Debug.Log("OnPlayModeChanged (" + state.ToString() + ")");
			instance.InitialiseManager();
		}
	}

	public void RunProcesses(ProcessTypeFlags flags) {
		foreach (ProcessTypeFlags flag in System.Enum.GetValues(typeof(ProcessTypeFlags))) {
			if (!flag.Equals(ProcessTypeFlags.None) && flags.HasFlag(flag) && m_processes.ContainsKey(flag)) {
				foreach (ManagerProcess process in m_processes[flag]) {
					process.m_processDelegate.Invoke();
				}
			}
		}
	}

	public void RunProcesses(ProcessTypeFlags flags, GameObject gameObject) {
		foreach (ProcessTypeFlags flag in System.Enum.GetValues(typeof(ProcessTypeFlags))) {
			if (!flag.Equals(ProcessTypeFlags.None) && flags.HasFlag(flag) && m_processes.ContainsKey(flag)) {
				foreach (ManagerProcess process in m_processes[flag]) {
					process.m_processDelegateGameObject.Invoke(gameObject);
				}
			}
		}
	}

	public void RunProcesses(ProcessTypeFlags flags, float deltaTime) {
		foreach (ProcessTypeFlags flag in System.Enum.GetValues(typeof(ProcessTypeFlags))) {
			if (!flag.Equals(ProcessTypeFlags.None) && flags.HasFlag(flag) && m_processes.ContainsKey(flag)) {
				foreach (ManagerProcess process in m_processes[flag]) {
					m_processTimers[process] += deltaTime;
					if (m_processTimers[process] > process.m_timerMax) {
						process.m_processDelegate.Invoke();

						// Version 1: uses while loops (scary, inefficient)
						// While-loop to avoid rapidly-unwinding timer issues when awakening a slept PC 
						//while (m_processTimers[process] > process.m_timerMax) {
						//	m_processTimers[process] -= process.m_timerMax;
						//}
						// Version 2: uses good ol' float maths
						m_processTimers[process] = ((m_processTimers[process] / process.m_timerMax) % 1) * process.m_timerMax;
					}
				}
			}
		}
	}

	// called from GameDataBankManager
	public void GameDatabaseLoaded() {
		RunProcesses(ProcessTypeFlags.OnDatabaseLoaded);
	}

	public void RestartProcesses() {
		InitialiseManager();	
	}

	public void RegatherSceneGameObjects() {
		GatherAllSceneObjects();
	}

	private void InitialiseManager() {
		SingletonCheck();
		InitProcesses();

		// Manage data and on-start processes: ------
		// If we have any "on all objects" input type processes, gather all the scene's objects into the member var
		if (m_processInputTypeCounts[ProcessSpecialInputType.AllSceneObjects] > 0) {
			GatherAllSceneObjects();
		}
	}

	private void Start() {
		Application.targetFrameRate = 240;
	}

	void Awake() {
		RunProcesses(ProcessTypeFlags.OnAwake);
	}

	public void InitProcesses() {
		// Remove existing everythings.
		ClearAllData();

		m_fRealTimeLast = Time.realtimeSinceStartup;

		// Add all types!
		foreach (ProcessTypeFlags flagValue in System.Enum.GetValues(typeof(ProcessTypeFlags))) {
			if (!flagValue.Equals(ProcessTypeFlags.None)) {
				m_processTypeCounts.Add(flagValue, 0);
			}
		}
		for (ProcessSpecialInputType idx = ProcessSpecialInputType.FIRST; idx < ProcessSpecialInputType.COUNT; idx++) {
			m_processInputTypeCounts.Add(idx, 0);
		}
		// Prep the process dictionary
		foreach (ProcessTypeFlags flagValue in System.Enum.GetValues(typeof(ProcessTypeFlags))) {
			if (flagValue.Equals(ProcessTypeFlags.None)) {
				continue;
			}

			if (m_processes.ContainsKey(flagValue)) {
				m_processes.Remove(flagValue);
			}
			m_processes.Add(flagValue, new List<ManagerProcess>());
		}

		// Add processes here ---------
		if (m_CHUsePerObjectRandomSampling) {
			// (If applicable) Set the object ID field on a newly instantiated object's material's shader
			ManagerProcess processSetupObjectIDInstantiate = new ManagerProcess(ProcessTypeFlags.OnInstantiate);
			processSetupObjectIDInstantiate.m_processDelegateGameObject = SetShaderParamObjectID;
			AddProcess(processSetupObjectIDInstantiate);

			// Finds all material instances of the crosshatch shader and assigns the object ID field (at start level)
			ManagerProcess processSetupObjectIDStart = new ManagerProcess(ProcessTypeFlags.OnAwake | ProcessTypeFlags.OnEditorStartup, ProcessSpecialInputType.AllSceneObjects);
			processSetupObjectIDStart.m_processDelegate = SetAllObjectShaderParamObjectID;
			AddProcess(processSetupObjectIDStart);
		}

		#region cameradelayedtrackingfeature
		/*ManagerProcessOnUpdate processCameraDelayedTracking = new ManagerProcessOnUpdate();
		processCameraDelayedTracking.m_processDelegate = delegate {
			// CALCULATE CAMERA MOVEMENT HERE:
			// must be 0-1 range, must be very dampened
			// movement must respected axes and thus be relative to camera
			Camera cam = Camera.main;
			Vector3 translationDiff = cam.transform.position - m_cameraPosDelayed;
			float translationDiffSq = translationDiff.sqrMagnitude;
			if (translationDiffSq > m_cameraDelayDistanceMax * m_cameraDelayDistanceMax) {
				m_cameraPosDelayed = cam.transform.position - (translationDiff.normalized * m_cameraDelayDistanceMax);
			} else {
				//m_cameraPosDelayed = m_cameraPosDelayed.LerpAdditive(cam.transform.position, Time.deltaTime * m_cameraDelayCatchupRate);
				//m_cameraPosDelayed = m_cameraPosDelayed.LerpAdditive(cam.transform.position, m_cameraDelayCatchupRate * Time.deltaTime / translationDiffSq);
				m_cameraPosDelayed = Vector3.Lerp(m_cameraPosDelayed, cam.transform.position, Time.deltaTime * m_cameraDelayCatchupRate);
			}



			// Get this delta vector in the local space of the camera:
			Vector3 localSpaceDelta = cam.transform.InverseTransformDirection(cam.transform.position - m_cameraPosDelayed);
			m_vignetteOffset = localSpaceDelta * m_vignetteCameraDelayInfluence;

			//Debug.Log(localSpaceDelta.sqrMagnitude);

			//Debug.Log(localSpaceDelta);
			Debug.DrawLine(m_cameraPosDelayed, cam.transform.position, Color.yellow);
			Debug.DrawLine(cam.transform.position, cam.transform.position + cam.transform.TransformDirection(localSpaceDelta), Color.blue);

			Shader.SetGlobalVector("_CHVignetteCameraDelayOffset", new Vector3(localSpaceDelta.x, localSpaceDelta.y, 0.0f));
		};
		AddProcess(processCameraDelayedTracking);*/
		#endregion

		if (m_CHUseDynamicRandomSampling) {
			// Set the global offset param for the CH shader (affects all objects)
			ManagerProcess processUpdateGlobalCHOffset = new ManagerProcess(ProcessTypeFlags.OnUpdateTimer | ProcessTypeFlags.OnEditorStartup | ProcessTypeFlags.OnAwake);
			processUpdateGlobalCHOffset.m_timerMax = m_CHDynamicRandomSamplingOffsetInterval;
			processUpdateGlobalCHOffset.m_processDelegate = UpdateGlobalCHOffsetParam;
			AddProcess(processUpdateGlobalCHOffset);
		}

		// Set global CH params when they're changed in the properties. (TODO switch to scriptable objects?)
		ManagerProcess processUpdateGlobalCHParams = new ManagerProcess(ProcessTypeFlags.OnValidateInspectorChanges | ProcessTypeFlags.OnEditorStartup);
		processUpdateGlobalCHParams.m_processDelegate = UpdateGlobalCHParamsFromProperties;
		AddProcess(processUpdateGlobalCHParams);

		if (m_CHUseCamRotationOffset) {
			ManagerProcess processUpdateCHCameraOffset = new ManagerProcess(ProcessTypeFlags.OnUpdate);
			processUpdateCHCameraOffset.m_processDelegate = UpdateGlobalCHCameraSampleOffset;
			AddProcess(processUpdateCHCameraOffset);
		}

		// If enabled, adds the debug entity component to all valid gameobjects with an entityloader
		if (m_enableEntityComponentDebug) {
			ManagerProcess processAddDebugComponent = new ManagerProcess(ProcessTypeFlags.OnAwake, ProcessSpecialInputType.AllSceneObjects);
			processAddDebugComponent.m_processDelegate = AddDebugComponentToAll;
			AddProcess(processAddDebugComponent);

			ManagerProcess processAddDebugComponentOnInstance = new ManagerProcess(ProcessTypeFlags.OnInstantiate);
			processAddDebugComponentOnInstance.m_processDelegateGameObject = AddDebugComponentOnInstantiate;
			AddProcess(processAddDebugComponentOnInstance);
		}

		// Load some testbed data entities!
		ManagerProcess processSpawnActor = new ManagerProcess(ProcessTypeFlags.OnDatabaseLoaded);
		processSpawnActor.m_processDelegate = delegate {
			EntityInstancingManager.instance.InstantiateEntity("ChwaraewrEsgyrn", EntityInstanceParameters.Params());
		};
		AddProcess(processSpawnActor);

		// ---------------------------- (End Processes)

		RunProcesses(ProcessTypeFlags.OnEditorStartup);
	}

	void ClearAllData() {
		m_bHasInstanceProcess = m_bHasTimerProcess = m_bHasOnValidateProcess = m_bHasUpdateProcess = false;
		m_processTypeCounts.Clear();
		m_processInputTypeCounts.Clear();
		m_processTimers.Clear();
		m_allGameObjects.Clear();

		foreach (ProcessTypeFlags flagValue in System.Enum.GetValues(typeof(ProcessTypeFlags))) {
			if (flagValue.Equals(ProcessTypeFlags.None)) {
				continue;
			}
			if (m_processes.ContainsKey(flagValue)) {
				m_processes[flagValue].Clear();
			}
		}
	}

	// Contains code that should be run on both in-game update and editor update
	// deltaTime: pass in, DO NOT USE Time.deltaTime from inside the method; editor updates will not provide the right value
	private void OnUpdate(float deltaTime) {
		if (!m_bHasUpdateProcess)
			return;

		RunProcesses(ProcessTypeFlags.OnUpdate);

		if (!m_bHasTimerProcess)
			return;

		RunProcesses(ProcessTypeFlags.OnUpdateTimer, deltaTime);
	}

	// Only called in-game on update
	private void Update() {
		OnUpdate(Time.deltaTime);
	}

	// Only called in editor on update
	static void EditorUpdate() {
		// Don't run if the regular Update is running
		if (!Application.isPlaying) {
			instance.OnUpdate(Time.realtimeSinceStartup - instance.m_fRealTimeLast);
			instance.m_fRealTimeLast = Time.realtimeSinceStartup;
		}
	}

	void OnInstantiate(GameObject gameObject) {
		RunProcesses(ProcessTypeFlags.OnInstantiate, gameObject);
	}

	// Not called in release!
	private void OnValidate() {
		SingletonCheck();

		if (m_bHasOnValidateProcess) {
			RunProcesses(ProcessTypeFlags.OnValidateInspectorChanges);
		}
	}

	private void SingletonCheck() {
		if (instance != null && instance != this) {
			Destroy(this);
		} else {
			instance = this;
		}
	}

	// For this whole system to work, Unity's own Instantiate MUST NEVER BE CALLED
	// except for here, obvs. 
	public static GameObject InstantiateObject(GameObject gameObject, bool startDisabled = false) {
		GameObject instancedObject = Instantiate(gameObject, Vector3.zero, Quaternion.identity);
		instancedObject.SetActive(!startDisabled);
		if (instance.m_bHasInstanceProcess) {
			instance.OnInstantiate(instancedObject);
		}

		return instancedObject;

		// If the full object list ever needs to be updated at all,
		// include this line. AND WRITE A DESTROY/REMOVE FUNCTION
		// in a similar style to this.
		//instance.m_allGameObjects.Add(gameObject);
	}

	public static GameObject InstantiateObject(GameObject gameObject, Transform parent, bool startDisabled = false) {
		GameObject instancedObject = Instantiate(gameObject, Vector3.zero, Quaternion.identity, parent);
		instancedObject.SetActive(!startDisabled);

		if (instance.m_bHasInstanceProcess) {
			instance.OnInstantiate(instancedObject);
		}

		return instancedObject;
		//instance.m_allGameObjects.Add(gameObject);
	}

	public static GameObject InstantiateEmpty(string objectName, bool startDisabled = false) {
		GameObject newObject = new GameObject(objectName);
		newObject.SetActive(!startDisabled);
		return newObject;
	}

	public static GameObject InstantiateEmpty(string objectName, Transform parent, bool startDisabled = false) {
		GameObject newObject = new GameObject(objectName);
		newObject.transform.SetParent(parent, false);
		newObject.SetActive(!startDisabled);
		return newObject;
	}

	// Naturally, this is OMEGA expensive. Only call on start or if ABSOLUTELY needed.
	void GatherAllSceneObjects() {
		m_allGameObjects.Clear();

		List<GameObject> rootGameObjects = new List<GameObject>();
		Scene scene = SceneManager.GetActiveScene();
		if (scene != null && scene.isLoaded) {
			scene.GetRootGameObjects(rootGameObjects);
			foreach (GameObject gameObject in rootGameObjects) {
				GatherSceneObjectRecursive(gameObject);
			}
		}
	}

	// Recursively gets the child objects of a gameobject
	void GatherSceneObjectRecursive(GameObject gameObject) {
		m_allGameObjects.Add(gameObject);
		int childCount = gameObject.transform.childCount;
		if (childCount > 0) {
			for (int i = 0; i < childCount; i++) {
				GatherSceneObjectRecursive(gameObject.transform.GetChild(i).gameObject);
			}
		}
	}

	void SetAllObjectShaderParamObjectID() {
		if (instance.m_allGameObjects != null) {
			foreach (GameObject gameObject in instance.m_allGameObjects) {
				SetShaderParamObjectID(gameObject);
			}
		}
	}

	// Finds the unique ID param in the object's renderer(s) and sets it
	void SetShaderParamObjectID(GameObject gameObject) {
		const string paramName = "_UniqueObjOffset";
		const float offsetScale = 1.0f;

		MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
		if (renderer) {
			Vector2 uniqueObjectOffset = new Vector2(Mathf.Sin(gameObject.GetInstanceID() * 0.33f), Mathf.Cos(gameObject.GetInstanceID() * 0.15f)) * offsetScale;
			Material[] materials = renderer.materials;
			foreach (Material material in materials) {
				if (material.HasVector(paramName)) {
					material.SetVector(paramName, uniqueObjectOffset);
					Debug.Log("Offset for " + gameObject.name + ": " + material.GetVector(paramName));
				}
			}
			return;
		}

		SkinnedMeshRenderer skinnedRenderer = gameObject.GetComponent<SkinnedMeshRenderer>();
		if (skinnedRenderer) {
			// Previously used trig funcs on gameObject.GetInstanceID(), now uses random
			Vector2 uniqueObjectOffset = (UnityEngine.Random.insideUnitCircle + UnityEngine.Random.insideUnitCircle) * offsetScale;
			Material[] materials = skinnedRenderer.materials;
			foreach (Material material in materials) {
				if (material.HasVector(paramName)) {
					material.SetVector(paramName, uniqueObjectOffset);
				}
			}
			return;
		}
	}

	// Periodically update the global sampling offset for all crosshatch shaders
	// This effectively "redraws" the hatching pattern to give the impression
	// that it's hand drawn
	void UpdateGlobalCHOffsetParam() {
		const string paramName = "_GlobalSamplingOffset";
		Vector3 offset = UnityEngine.Random.insideUnitCircle * m_CHDynamicRandomSamplingOffsetMax;
		offset.z = UnityEngine.Random.Range(Mathf.Deg2Rad * -m_CHDynamicRandomSamplingOffsetRotation, Mathf.Deg2Rad * m_CHDynamicRandomSamplingOffsetRotation);
		Shader.SetGlobalVector(paramName, offset);
	}

	// Whenever the relevant params on here are changed, update the global shader CrossHatch params
	void UpdateGlobalCHParamsFromProperties() {
		Shader.SetGlobalTexture("_CrosshatchMapDiagonal", m_CHDiagonalTexture);
		Shader.SetGlobalTexture("_CrosshatchMapMixed", m_CHCrossedTexture);
		Shader.SetGlobalTexture("_RampTexture", m_CHRampTexture);
		Shader.SetGlobalFloat("_CrosshatchTiling", m_CrosshatchTilingFactor);
		Shader.SetGlobalFloat("_CrosshatchHighlightTiling", m_CrosshatchHighlightTilingFactor);
		//Shader.SetGlobalFloat("_CrosshatchShift", m_CHShadingShift);
		Shader.SetGlobalFloat("_CrosshatchPatternShift", m_CHPatternBias);
		Shader.SetGlobalFloat("_CrosshatchPatternBoundary", m_CHBoundarySoftness);
		Shader.SetGlobalFloat("_CrosshatchHighlightsShift", m_CHHighlightBias);
		Shader.SetGlobalFloat("_CrosshatchHighlightsBoundary", m_CHHighlightBoundarySoftness);
		Shader.SetGlobalFloat("_RimStrength", m_CHRimStrength);
		Shader.SetGlobalVector("_HueShiftParams", new Vector2(m_CHHueShiftHue, m_CHHueShiftSaturation));
		Shader.SetGlobalFloat("_CrosshatchStrength", m_CHCrosshatchStrength);
		Shader.SetGlobalFloat("_CrosshatchPointLightAttenBoost", m_CHPointLightAttenBoost);

		Debug.Log("~~~CH Params updated!");
	}

	void UpdateGlobalCHCameraSampleOffset() {
		// Pope's Method "DitherOffset = ScreenSize * CameraRotation / CameraFov"
		
		Camera mainCam = Camera.main;
		if(!mainCam)
			return;

		// Represent yaw in X, and pitch in Y since yaw affects X-pos of sampling, and pitch Y-pos
		Vector3 cameraEuler = mainCam.transform.rotation.eulerAngles;
		Vector2 camYawPitch = new Vector3(cameraEuler.y, cameraEuler.x);
		// Crossing between positive and negative values (at 0?) (or at certain angle looping thresholds) causes issues
		// Investigate why this occurs!!
		camYawPitch.x = ExtensionMethods.Wrap180(camYawPitch.x);
		camYawPitch.y = ExtensionMethods.Wrap180(camYawPitch.y);

		// Get FOV(h,v) in degrees to calculate rotation as a proportion of it
		float fovVertical = mainCam.fieldOfView;
		float cameraHeightAt1 = Mathf.Tan(fovVertical * Mathf.Deg2Rad * 0.5f);
		float fovHorizontalRadians = Mathf.Atan(cameraHeightAt1 * mainCam.aspect) * 2;
		float fovHorizontal = Mathf.Rad2Deg * fovHorizontalRadians;

		float aspectRatio = mainCam.aspect;
		Vector2 sampleOffset = camYawPitch / new Vector2(fovHorizontal, fovVertical) * new Vector2(aspectRatio, aspectRatio);
		
		// Negate Y due to coordinate space changes
		sampleOffset.y *= -1;

		Shader.SetGlobalVector("_GlobalCameraRotSampleOffset", sampleOffset);
	}

	void AddDebugComponentToAll() {
		if (GameProcessManager.instance.m_allGameObjects != null) {
			foreach (GameObject gameObject in GameProcessManager.instance.m_allGameObjects) {
				TryAddDebugComponent(gameObject);
			}
		}
	}

	void AddDebugComponentOnInstantiate(GameObject gameObject) {
		TryAddDebugComponent(gameObject);
	}

	void TryAddDebugComponent(GameObject gameObject) {
		if (gameObject != null) {

		}
	}
}
