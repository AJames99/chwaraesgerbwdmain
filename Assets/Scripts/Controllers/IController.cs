using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Really REALLY bad design practice but trying to make FPSController 
// inherit from Kinematic caused a lot of traversal issues, so here's
// an adapter.
public interface IController {

	public void Push(Vector3 pushVector, Vector3? point = null);

	// Bad naming scheme. This actually adds to the velocity directly without accounting for mass.
	public void AddForce(Vector3 force, bool explosion = false);

	public Vector3 GetVelocity();

	public void SetVelocity(Vector3 velocity);
}
