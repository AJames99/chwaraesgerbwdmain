﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class KinematicController : Controller, IController {

	#region Drag Drop
	// Collision resolving is done with respect to this volume

	[SerializeField]
	protected CapsuleCollider _collisionVolume;

	//[SerializeField]
	public Transform _camTransform; //"eyes"

	// Collision will not happen with these layers
	// One of them has to be this controller's own layer
	[SerializeField]
	protected LayerMask _excludedLayers;
	
	[SerializeField]
	protected List<Transform> _groundedRayPositions;

	[SerializeField]
	protected double _slopePenetrationIgnoreLimit = 0.015;

	[SerializeField]
	protected float maxSlopeAngle = 45f;

	[SerializeField]
	protected float minimumVelocityBounce = 20.0f;

	[SerializeField]
	protected float restitutionFactor = 0.5f;

	//[SerializeField]
	public float SENSITIVITY = 100f;

	#endregion

	#region Movement Parameters

	// The controller can collide with colliders within this radius
	protected const float Radius = 2f;

	// Ad-hoc approach to make the controller accelerate faster
	//protected const float GroundAccelerationCoeff = 500.0f;
	//protected const float GroundAccelerationCoeff = 150.0f; // TF2 version
	// WAS 150, now 250
	[SerializeField]
	protected float GroundAccelerationCoeff = 250.0f; // TF2 version

	// How fast the controller accelerates while it's not grounded
	[SerializeField]
	protected float AirAccelCoeff = 3f; // TF2 Version
	[SerializeField]
	protected float AirDecelCoeff = 8f; // TF2 Version

	// Along a dimension, we can't go faster than this
	// This dimension is relative to the controller, not global
	// Meaning that "max speend along X" means "max speed along 'right side' of the controller"
	//protected float MaxSpeedAlongOneDimension = 8f;
	//protected const float MaxSpeedDefault = 8f;
	public float MaxSpeedDefault = 10f; // Upped to 10 from 8
	public float MaxSpeedSideways = 8f;
	public float MaxSpeedBackwards = 4f;

	// How fast the controller decelerates on the grounded
	// Was previously 9, decreased to 3f
	//protected const float Friction = 4.5f; // TF2 version
	[SerializeField]
	protected float Friction = 3f; // TF2 version
									   //protected const float SlidingFriction = 0.05f; // TF2 version

	// Stop if under this speed
	protected const float FrictionSpeedThresholdStop = 0.5f;

	// Use extra friction multiplier if under this speed
	protected const float FrictionSpeedThresholdRoughen = 3f;

	// Push force given when jumping
	// - Used also to determine whether or not to ground us when sliding across a surface.
	//protected const float JumpStrength = 8f;
	[SerializeField]
	protected float JumpStrength = 10f;

	// yeah...
	[SerializeField]
	public float GravityAmount = 24f;


	// How precise the controller can change direction while not grounded 
	protected const float AirControlPrecision = 8f; // 16

	// Should we increase air control/direction change when moving forward only?
	//protected const bool bForwardAirControlAddition = false; // true

	// When moving only forward, increase air control dramatically
	protected const float AirControlAdditionForward = 2f; //8

	// Prevents the projection of velocity onto the ground normal plane that stops us bouncing down slopes etc. Used to allow the player to be launched vertically when grounded.
	protected bool beingLaunched = false;

	protected bool isRamping = false; // Causes sliding state when going up ramps
									//protected float rampThreshold = JumpStrength + 0.05f;
	protected float rampThreshold;

	#endregion

	#region Fields
	// The real velocity of this controller
	protected Vector3 _velocity;
	//protected Vector3 velocityForNextFrame {
	//	set {
	//		_velocityForNextFrame = value;
	//		_velocityNextFrameDirty = true;
	//	}
	//	get {
	//		return _velocityForNextFrame;
	//	}
	//}
	//private Vector3 _velocityForNextFrame;
	//private bool _velocityNextFrameDirty = false;

	public override Vector3 GetVelocity() { return _velocity; }
	public override void SetVelocity(Vector3 vel) { _velocity = vel; }

	protected float verticalVelocityBeforeLanding;

	[HideInInspector]
	public bool _isGrounded = false;

	protected Collider m_pCollidersThis;

	// Raw input taken with GetAxisRaw()
	//protected Vector3 _moveInput;

	// Vertical look
	[HideInInspector]
	public float _pitch = 0; // We keep track of this stat since we want to clamp it
	public Vector2 pitchLimit = new Vector2(-360, 360);

	[HideInInspector]
	public float _yaw = 0;

	protected readonly Collider[] _overlappingColliders = new Collider[30]; // More may be needed in future?
	protected Transform _ghostJumpRayPosition; // Removed for now - ruins downhill sliding
											 // Old stat was (0, -0.5, -0.75)

	// Some information to persist
	protected bool _isGroundedInPrevFrame;
	protected bool _isGonnaJump;
	protected Vector3 _wishDirDebug;

	protected const float _rampDoubleJumpTimeMax = 0.3f; // If a 2nd jump occurs within X seconds, convert some horizontal to vertical speed
	protected float _timeSinceLastJump = 0.0f;

	// Was the last jump a ramp/double jump? If so, don't supply the down-jump boost.
	protected bool _lastjumpwasramp = false;

	protected Vector3[] velocityReadings;
	protected const int NUMREADINGS = 60;

	//protected float verticalVelocityBeforeLanding = 0f; // What was our vertical velocity last frame?

	[HideInInspector]
	public bool pitchShouldAffectModel {
		get {
			return _pitchShouldAffectModel;
		}
		set {
			if (value != _pitchShouldAffectModel) {
				_pitchShouldAffectModel = value;
				ResetPitchRotation();
				//Debug.Log("Reset pitch rot");
			}
		}
	}

	void ResetPitchRotation() {
		transformForPitch.localRotation = defaultPitchObjectRot;
	}

	private bool _pitchShouldAffectModel = false;
	[SerializeField]
	protected Transform transformForPitch = null;
	protected Quaternion defaultPitchObjectRot;

	#endregion

	#region InputsVariables
	public Vector2 m_Look;
	public Vector2 m_Move;
	#endregion

	//protected Vector3 _averageVelocity;
	public Vector3 averageVelocity {
		get {
			Vector3 avg = Vector3.zero;
			for (int i = 0; i < NUMREADINGS; i++) {
				avg += velocityReadings[i];
			}
			avg /= NUMREADINGS;
			return avg;
		}
	}

	protected override void Start() {
		base.Start();

		_ghostJumpRayPosition = _groundedRayPositions.Last();

		rampThreshold = JumpStrength * 0.8f;

		velocityReadings = new Vector3[NUMREADINGS];
		for (int i = 0; i < NUMREADINGS; i++) {
			velocityReadings[i] = Vector3.zero;
		}

		if(transformForPitch)
			defaultPitchObjectRot = transformForPitch.localRotation; // Cache this.

		Collider[] arrColliders = GetComponents<Collider>();
		foreach (Collider pCollider in arrColliders) {
			if (!pCollider.isTrigger) {
				m_pCollidersThis = pCollider;
				break;
			}
		}
	}

	public override void Update() {
		base.Update();
		// Wrap the angle
		_pitch.Wrap180();
		_yaw.Wrap180();

		_pitch = Mathf.Clamp(_pitch, pitchLimit.x, pitchLimit.y);

		// 200 IS THE SENS, MOVE IT TO CONFIG 
		//_pitch += (m_Look.y * -SENSITIVITY);
		
		_pitch += m_Look.y * -SENSITIVITY;
		_yaw += m_Look.x * SENSITIVITY;

		_camTransform.localRotation = Quaternion.Euler(Vector3.right * _pitch);

		if (pitchShouldAffectModel && transformForPitch) {
			transformForPitch.localRotation = defaultPitchObjectRot * Quaternion.Euler(Vector3.right * _pitch);
			Debug.DrawLine(transformForPitch.position, transformForPitch.position + transformForPitch.forward * 30.0f, Color.white);
		}
		transform.localRotation = Quaternion.Euler(Vector3.up * _yaw);

		//_camTransform.Rotate(_camTransform.forward, -_currentRot, Space.World);
	}


	protected int velRecCounter = 0;
	protected virtual void FixedUpdate() {

		//if (_velocityNextFrameDirty) {
		//	_velocity = _velocityForNextFrame.Copy();
		//	_velocityForNextFrame = Vector3.zero;
		//	_velocityNextFrameDirty = false;
		//	//Debug.Log("Made velocity clean on " + name);
		//}

		// Grab the vertical velocity to calculate the impact speed if necessary
		if (!_isGrounded)
			verticalVelocityBeforeLanding = _velocity.VerticalComponent() < 0 ? _velocity.VerticalComponent() : verticalVelocityBeforeLanding;

		//Average speed readings
		velocityReadings[velRecCounter] = _velocity.ToHorizontal();
		if (++velRecCounter > (NUMREADINGS - 1)) {
			velRecCounter = 0;
		}

		var dt = Time.fixedDeltaTime;

		// The player is attempting to go in this direction
		Vector3 wishDir = new Vector3(m_Move.x, 0f, m_Move.y);
		wishDir = _camTransform.TransformDirectionHorizontal(wishDir);
		_wishDirDebug = wishDir;//.ToHorizontal();

		Vector3 groundNormal;
		_isGrounded = IsGrounded(out groundNormal);
		_isGrounded = beingLaunched ? false : _isGrounded;

		bool bounceForNext = false;

		if (_isGrounded) {
			if (_isGroundedInPrevFrame) {
				// Don't apply friction if just landed or about to jump (essentially, bhopping)
				if (!_isGonnaJump && !beingLaunched) {
					ApplyFriction(ref _velocity, dt, wishDir);
				}
			} else {
				if (-verticalVelocityBeforeLanding > minimumVelocityBounce) {
					bounceForNext = true;
				}
			}

			if (!_isGonnaJump) {
				Accelerate(ref _velocity, wishDir, GroundAccelerationCoeff, dt);
			}
			
			// Crop up horizontal velocity component
			if (!beingLaunched) {
				_velocity = Vector3.ProjectOnPlane(_velocity, groundNormal);
			}

			if (!_isGroundedInPrevFrame && !_isGonnaJump) {
				_lastjumpwasramp = false;
			}

			// JUMP
			if (_isGonnaJump && !isRamping) {
				_velocity += groundNormal * JumpStrength; // New: GroundNormal only

				// The minimum time needed between jumps to reset them (Too frequently will not allow the timer to be reset)
				if (_timeSinceLastJump > 0.1f) {
					_timeSinceLastJump = 0.0f;
				}
			}
		} else {
			if (_timeSinceLastJump <= 10f) {
				_timeSinceLastJump += dt;
			}
			// If the input doesn't have the same facing with the current velocity
			// then slow down instead of speeding up (and use appropriate coefficient)
			var coeff = Vector3.Dot(_velocity, wishDir) > 0 ? AirAccelCoeff : AirDecelCoeff;

			Accelerate(ref _velocity, wishDir, coeff, dt);

			if (Mathf.Abs(m_Move.x) > 0.0001) {  // Pure side velocity doesn't allow air control
				ApplyAirControl(ref _velocity, wishDir, dt);
			}

			_velocity += Gravity.Down * (GravityAmount * dt);
		}

		var displacement = _velocity * dt;

		// If we're moving too fast, make sure we don't hollow through any collider
		if (displacement.magnitude > _collisionVolume.radius) {
			ClampDisplacement(ref _velocity, ref displacement, transform.position);
		}

		transform.position += displacement;

		var collisionDisplacement = ResolveCollisions(ref _velocity);
		transform.position += collisionDisplacement;


		if (beingLaunched) {
			beingLaunched = false;
		}

		if (bounceForNext) {
			AddForce(new Vector3(0, -verticalVelocityBeforeLanding * restitutionFactor, 0), true);
			//_isGrounded = false;
			Debug.Log("Bounced up: " + _velocity.y.ToString());
		}

		_isGroundedInPrevFrame = _isGrounded;
		//_velocityForNextFrame = _velocity.Copy();
	}

	protected void Accelerate(ref Vector3 playerVelocity, Vector3 accelDir, float accelCoeff, float dt) {
		// How much speed we already have in the direction we want to speed up

		// CPMA Style - Acceleration in direction of vel = 0, at 90° = 1, at 180° (Braking) = 2
		float projSpeed = Vector3.Dot(playerVelocity, accelDir);

		// Fucking floating point imprecision I swear to GAWD (was selecting wrong movespeed set because m_Move.x == 0.1E-0billion whatever fuck
		float speedToUse = MaxSpeedDefault;
		if (m_Move.y < 0) {
			speedToUse = MaxSpeedBackwards;
		} else if (Mathf.Abs(m_Move.x) > 0.05f) {
			speedToUse = MaxSpeedSideways;
		}

		// How much speed we need to add (in that direction) to reach max speed
		var addSpeed = speedToUse - projSpeed;
		if (addSpeed <= 0) {
			return;
		}

		// How much we are gonna increase our speed
		// maxSpeed * dt => the real deal. a = v / t
		// accelCoeff => ad hoc approach to make it feel better
		var accelAmount = accelCoeff * speedToUse * dt;

		// If we are accelerating more than in a way that we exceed maxSpeedInOneDimension, crop it to max
		if (accelAmount > addSpeed) {
			accelAmount = addSpeed;
		}

		playerVelocity += accelDir * accelAmount;
	}

	protected void ApplyFriction(ref Vector3 playerVelocity, float dt, Vector3 wishdir) {
		var speed = playerVelocity.magnitude;
		if (speed <= 0.00001) {
			return;
		}

		float dot = (wishdir.magnitude > 0.1f) ? Mathf.Max(0f, Vector3.Dot(wishdir, playerVelocity)) : 1f;

		var downLimit = Mathf.Max(speed, FrictionSpeedThresholdStop); // Don't drop below threshold.
																	  // If we're crouching and going beyond the minimum slide limit and we allow sliding, use sliding friction

		float fric = Friction;
		if (speed < FrictionSpeedThresholdRoughen) {
			fric *= Friction * 5f;
		}

		var dropAmount = speed - (downLimit * fric * dt * dot);
		if (dropAmount < 0) {
			dropAmount = 0;
		}

		playerVelocity *= (dropAmount / speed); // Reduce the velocity by a certain percent
	}

	protected void ApplyAirControl(ref Vector3 playerVelocity, Vector3 accelDir, float dt) {
		// This only happens in the horizontal plane
		// TODO: Verify that these work with various gravity values
		var playerDirHorz = playerVelocity.ToHorizontal().normalized;
		var playerSpeedHorz = playerVelocity.ToHorizontal().magnitude;

		var dot = Vector3.Dot(playerDirHorz, accelDir);
		if (dot > 0) {
			var k = AirControlPrecision * dot * dot * dt;

			// Like CPMA, increased direction change rate when we only hold W
			// If we want pure forward movement, we have much more air control
			var isPureForward = Mathf.Abs(m_Move.x) < 0.0001 && Mathf.Abs(m_Move.y) > 0;
			if (isPureForward) {
				k *= AirControlAdditionForward;
			}

			// A little bit closer to accelDir
			playerDirHorz = playerDirHorz * playerSpeedHorz + accelDir * k;
			playerDirHorz.Normalize();

			// Assign new direction, without touching the vertical speed
			playerVelocity = (playerDirHorz * playerSpeedHorz).ToHorizontal() + Gravity.Up * playerVelocity.VerticalComponent();
		}

	}

	// Calculates the displacement required in order not to be in a world collider
	protected Vector3 ResolveCollisions(ref Vector3 playerVelocity) {
		// Get nearby colliders
		Physics.OverlapSphereNonAlloc(transform.position, Radius + 0.1f,
			_overlappingColliders, ~_excludedLayers, QueryTriggerInteraction.Ignore);

		var totalDisplacement = Vector3.zero;
		var checkedColliderIndices = new HashSet<int>();

		// If the player is intersecting with that environment collider, separate them
		for (var i = 0; i < _overlappingColliders.Length; i++) {
			// Two player colliders shouldn't resolve collision with the same environment collider
			if (checkedColliderIndices.Contains(i)) {
				continue;
			}

			var envColl = _overlappingColliders[i];

			// Skip empty slots
			if (envColl == null || envColl.isTrigger || envColl == m_pCollidersThis) {
				continue;
			}

			//if(envColl.gameObject.layer == 0)
			//	Debug.Log("ResolveCollisions on " + name + " hit a default layer obj: " + envColl.name + "'s collider of type " + envColl.ToString() + " while self collider is " + m_pCollidersThis.ToString());

			Vector3 collisionNormal;
			float collisionDistance;
			if (Physics.ComputePenetration(
				_collisionVolume, _collisionVolume.transform.position, _collisionVolume.transform.rotation,
				envColl, envColl.transform.position, envColl.transform.rotation,
				out collisionNormal, out collisionDistance)) {
				// Ignore very small penetrations
				// Required for standing still on slopes
				// ... still far from perfect though
				if (collisionDistance < _slopePenetrationIgnoreLimit) {
					continue;
				}

				checkedColliderIndices.Add(i);

				// Shift out of the collider
				totalDisplacement += collisionNormal * collisionDistance;

				// Clip down the velocity component which is in the direction of penetration
				playerVelocity -= Vector3.Project(playerVelocity, collisionNormal);
			}
		}

		// It's better to be in a clean state in the next resolve call
		for (var i = 0; i < _overlappingColliders.Length; i++) {
			_overlappingColliders[i] = null;
		}

		return totalDisplacement;
	}

	// If one of the rays hit, we're considered to be grounded
	protected bool IsGrounded(out Vector3 groundNormal) {
		// If vertical speed (w.r.t. gravity direction) is greater than our jump velocity, don't ground us.
		// This allows for sliding up ramps when moving fast
		// If we allow downward slide, the player can slide when their vertical velocity is downward
		float verticalSpeed = _velocity.VerticalComponent();
		bool isGrounded = false;
		groundNormal = -Gravity.Down;

		if (verticalSpeed > rampThreshold || (beingLaunched && verticalSpeed > 0)) {
			return false; //- now removed, when sliding up a ramp we'll be in the slide state from now on
						  //isRamping = true;
		} else {
			//isRamping = false;

			foreach (var t in _groundedRayPositions) {
				// The last one is reserved for ghost jumps
				// Don't check that one if already on the ground
				// Ghost jump is typically behind the player and below so that we can check if we've recently jumped etc.
				if (t == _ghostJumpRayPosition && isGrounded) {
					continue;
				}

				RaycastHit[] hits = Physics.RaycastAll(t.position, Gravity.Down, 0.51f, ~_excludedLayers, QueryTriggerInteraction.Ignore);
				//RaycastHit hit;
				//if (Physics.Raycast(t.position, Gravity.Down, out hit, 0.51f, ~_excludedLayers, QueryTriggerInteraction.Ignore)) {
				foreach(RaycastHit hit in hits) {
					if (hit.collider == m_pCollidersThis)
						continue;

					if (Vector3.Angle(Gravity.Up, hit.normal) <= maxSlopeAngle) {
						groundNormal = hit.normal;
						//isGrounded = true;

						return true;
					}
				}
			}
			//}
		}
		return isGrounded;
	}

	// If there's something between the current position and the next, clamp displacement
	protected void ClampDisplacement(ref Vector3 playerVelocity, ref Vector3 displacement, Vector3 playerPosition) {
		RaycastHit[] hits = Physics.RaycastAll(playerPosition, playerVelocity.normalized, displacement.magnitude, ~_excludedLayers);
		foreach (RaycastHit hit in hits) {
			if (hit.collider != m_pCollidersThis) {
				displacement = hit.point - playerPosition;
				return;
			}
		}
	}

	// Handy when testing
	public void ResetAt(Transform t) {
		transform.position = t.position + Vector3.up * 0.5f;
		//_camTransform.position = _defaultCamPos;
		_velocity = t.TransformDirection(Vector3.forward);
	}

	public override void AddForce(Vector3 force, bool explosion = false) {
		//_velocity += force;
		//velocityForNextFrame += _velocity + force;
		//velocityForNextFrame += force;
		_velocity += force;
		beingLaunched = true;
	}

	public override void MakeInput(ControllerInputs type, Vector3 value) {
	}

	public override void MakeInput(ControllerInputs type, Vector2 value) {
		switch (type) {
			case ControllerInputs.LOOK:
				//m_Look = value;
				_yaw = value.x;
				_pitch = value.y;
				break;
			case ControllerInputs.MOVE:
				m_Move = value;
				break;
			case ControllerInputs.MOVEWORLDSPACE:
				Vector3 inLocalSpace = _camTransform.InverseTransformDirectionHorizontal(new Vector3(value.x, 0f, value.y));
				m_Move = new Vector2(inLocalSpace.x, inLocalSpace.z);
				break;
		}
	}

	public override void MakeInput(ControllerInputs type, float value) {
	}

	public override void MakeInput(ControllerInputs type, bool value) {
		switch (type) {
			case ControllerInputs.JUMP:
				_isGonnaJump = value;
				break;
		}
	}

	public override void MakeInput(ControllerInputs type) {

	}

	public override void Push(Vector3 pushVector, Vector3? point = null) {
		AddForce(pushVector);
	}
}
