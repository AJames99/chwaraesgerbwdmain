﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

//public class TPSController : MonoBehaviour, IController {
public class TPSController : Controller {

	#region Drag Drop
	// Collision resolving is done with respect to this volume

	[SerializeField]
	protected Transform _camTransform;
	
	[SerializeField]
	protected Transform _camBoomTransform;

	[SerializeField]
	private CapsuleCollider _collisionVolume;

	// Collision will not happen with these layers
	// One of them has to be this controller's own layer
	[SerializeField]
	private LayerMask _excludedLayers;

	[SerializeField]
	private bool _debugInfo;

	[SerializeField]
	private List<Transform> _groundedRayPositions;

	[SerializeField]
	private double _slopePenetrationIgnoreLimit = 0.015;

	[SerializeField]
	private float maxSlopeAngle = 45f;


	[SerializeField]
	private Transform _spawnPoint;

	[SerializeField]
	private float SENSITIVITY = 100f;

	//[SerializeField]
	//private Vector2 _dashStrength = new Vector2(15.0f, 5.0f);

	//private bool canDash = true;
	//private bool canDashReset = true;
	//[SerializeField]
	//private float _dashCooldownMinimum = 0.5f;

	[SerializeField]
	private Animator cameraAnimator;

	[SerializeField]
	private float moveBobMaxVelocity = 10.0f;

	[SerializeField]
	private float landImpactMaxVelocity = 20.0f;

	[SerializeField]
	protected float restitutionFactor = 0.75f;
	
	[SerializeField]
	protected float minimumVelocityBounce = 10.0f;

	[Header("Speed Lines")]
	[SerializeField]
	private Transform speedLinesEffect;
	private Material speedLinesEffectMat;
	private const string speedEffectHandle = "_Speed";
	private const string speedAngleHandle = "_Dot";
	[SerializeField]
	private Vector3 speedLinesEffectCylinderBaseScale = new Vector3(1.0f, 1.0f, 3.0f);
	[SerializeField]
	float speedLinesRotateRate = 45f;

	[SerializeField]
	private Vector2 speedLinesVelocityRange = new Vector2(0, 10);
	private Vector2 speedLinesVelocityRangeSqr;

	/*[Header("FoV Settings")]
	[SerializeField]
	private bool _enableDynamicFoV = false;
	[SerializeField]
	[Range(25.0f, 120.0f)]
	private float _minFov = 75f;
	[SerializeField]
	[Range(25.0f, 120.0f)]
	private float _maxFov = 90f;
	[SerializeField]
	[Range(1.0f, 100.0f)]
	private float _fovChangeRate = 15.0f;
	[SerializeField]
	[Range(2.0f, 50.0f)]
	private float _minSpeed_Fov = 5.0f;
	[SerializeField]
	[Range(2.0f, 50.0f)]
	private float _maxSpeed_Fov = 20.0f;*/
	#endregion

	#region Camera
	[Header("TPS Camera")]
	[SerializeField]
	[Range(0.0f, 15.0f)]
	protected float CameraOrbitDistanceMax = 5.0f;
	[SerializeField]
	protected Vector2 CameraOrbitXYOffset;

	[SerializeField]
	LayerMask CameraOcclusionLayers; // Layers that can occlude the camera and force it to move closer to the player.

	Transform _mainCamera;
	#endregion

	#region Movement Parameters

	// The controller can collide with colliders within this radius
	private const float Radius = 2f;

	// Ad-hoc approach to make the controller accelerate faster
	//private const float GroundAccelerationCoeff = 500.0f;
	//private const float GroundAccelerationCoeff = 150.0f; // TF2 version
	// WAS 150, now 250
	private const float GroundAccelerationCoeff = 250.0f; // TF2 version // Makes stopping a little too easy...
	protected Vector4 AccelVelocityMultiplierBounds = new Vector4(15.0f, 50.0f, 1.0f, 0.05f); 	// Attenuate our control down when at high speeds

	// How fast the controller accelerates while it's not grounded
	private const float AirAccelCoeff = 3f; // TF2 Version
	private const float AirDecelCoeff = 8f; // TF2 Version

	// Along a dimension, we can't go faster than this
	// This dimension is relative to the controller, not global
	// Meaning that "max speend along X" means "max speed along 'right side' of the controller"
	//private float MaxSpeedAlongOneDimension = 8f;
	//private const float MaxSpeedDefault = 8f;
	private const float MaxSpeedDefault = 10f; // Upped to 10 from 8
	private const float MaxSpeedSideways = 8f;
	private const float MaxSpeedBackwards = 7f;

	// How fast the controller decelerates on the grounded
	// Was previously 9, decreased to 3f
	//private const float Friction = 4.5f; // TF2 version
	//private const float Friction = 3f; // TF2 version
	private const float Friction = 2f; // Lowered version for punishing over-speeding
									   //private const float SlidingFriction = 0.05f; // TF2 version

	// Stop if under this speed
	private const float FrictionSpeedThresholdStop = 0.5f;

	// Use extra friction multiplier if under this speed
	private const float FrictionSpeedThresholdRoughen = 3f;

	// Push force given when jumping
	// - Used also to determine whether or not to ground us when sliding across a surface.
	//private const float JumpStrength = 8f;
	private const float JumpStrength = 10f;

	// yeah...
	private const float GravityAmount = 24f;


	// How precise the controller can change direction while not grounded 
	private const float AirControlPrecision = 8f; // 16

	// Should we increase air control/direction change when moving forward only?
	//private const bool bForwardAirControlAddition = false; // true

	// When moving only forward, increase air control dramatically
	private const float AirControlAdditionForward = 2f; //8

	// Prevents the projection of velocity onto the ground normal plane that stops us bouncing down slopes etc. Used to allow the player to be launched vertically when grounded.
	private bool beingLaunched = false;

	private bool isRamping = false; // Causes sliding state when going up ramps
									//private float rampThreshold = JumpStrength + 0.05f;
	private float rampThreshold = JumpStrength * 0.8f;
	#endregion

	#region Fields
	// The real velocity of this controller
	private Vector3 _velocity;
	//protected Vector3 velocityForNextFrame {
	//	set {
	//		_velocityForNextFrame = value;
	//		_velocityNextFrameDirty = true;
	//	}
	//	get {
	//		return _velocityForNextFrame;
	//	}
	//}
	//private Vector3 _velocityForNextFrame;
	//private bool _velocityNextFrameDirty = false;

	public override Vector3 GetVelocity() { return _velocity; }
	public override void SetVelocity(Vector3 vel) { _velocity = vel; }

	[HideInInspector]
	public bool _isGrounded = false;

	// Raw input taken with GetAxisRaw()
	//private Vector3 _moveInput;

	// Camera
	private float _pitch = 0; // We keep track of this stat since we want to clamp it
	private float _yaw = 0;
	// Player model etc.
	private float _pitchPlayer = 0;
	private float _yawPlayer = 0;

	private readonly Collider[] _overlappingColliders = new Collider[30]; // More may be needed in future?
	private Transform _ghostJumpRayPosition; // Removed for now - ruins downhill sliding
											 // Old stat was (0, -0.5, -0.75)

	// Some information to persist
	private bool _isGroundedInPrevFrame;
	private bool _isGonnaJump;
	private Vector3 _wishDirDebug;

	private const float _rampDoubleJumpTimeMax = 0.3f; // If a 2nd jump occurs within X seconds, convert some horizontal to vertical speed
	private float _timeSinceLastJump = 0.0f;

	// Was the last jump a ramp/double jump? If so, don't supply the down-jump boost.
	private bool _lastjumpwasramp = false;

	Vector3[] velocityReadings;
	private const int NUMREADINGS = 60;

	private float degressToCompensate = 0f; // Used to do DUSK-like camera flipping
	private const float recoveryRate = 1000f; // How fast is the flip?

	[HideInInspector]
	public float degreesThisJump = 0f; // How much have we spun this jump?


	private float moveBobBlend = 0; // What's the move-bob blend so far?
	private float moveBobBlendRate = 1.0f; // The rate of move-bob blending per second

	private float verticalVelocityBeforeLanding = 0f; // What was our vertical velocity last frame?

	#endregion

	#region InputsVariables
	private Vector2 m_Look;
	private Vector2 m_Move;
	#endregion

	[Header("Audio")]
	[SerializeField]
	AudioBundleSteps stepClips;
	//AudioSource reusableSource;
	AudioBundleSteps.StepMaterialTypes currentStepMaterial = AudioBundleSteps.StepMaterialTypes.sand;	// Which material are we standing on? (Sand, gravel, wood etc)
	float distanceCoveredSinceLastStep = 0f;
	float nextLandSound = 0f;
	float stepMinTimer;
	[SerializeField]
	float stepMinInterval = 0.1f;
	[SerializeField]
	float stepDistance = 0.5f;

	//private Vector3 _averageVelocity;
	public Vector3 averageVelocity {
		get {
			Vector3 avg = Vector3.zero;
			for (int i = 0; i < NUMREADINGS; i++) {
				avg += velocityReadings[i];
			}
			avg /= NUMREADINGS;
			return avg;
		}
	}

	public Vector3 GetLookVector() {
		return _camTransform.forward;
	}

	protected override void Start() {
		base.Start();

		_ghostJumpRayPosition = _groundedRayPositions.Last();

		velocityReadings = new Vector3[NUMREADINGS];
		for (int i = 0; i < NUMREADINGS; i++) {
			velocityReadings[i] = Vector3.zero;
		}

		Application.targetFrameRate = 240;

		_mainCamera = Camera.main.transform;

		if (speedLinesEffect) {
			// Get the speed effect material and parameter
			speedLinesEffectMat = speedLinesEffect.GetComponent<MeshRenderer>().material;
			speedLinesEffectMat.SetFloat(speedEffectHandle, 0f);
			
			// Adjust the scaling to fit the aspect ratio
			speedLinesEffect.localScale = speedLinesEffectCylinderBaseScale;
			speedLinesEffect.localScale = Vector3.Scale(speedLinesEffect.localScale, new Vector3(Camera.main.aspect, 1.0f, 1.0f));

			// Squares for cheaper magnitudes
			speedLinesVelocityRangeSqr = Vector3.Scale(speedLinesVelocityRange, speedLinesVelocityRange);
		}

		Cursor.lockState = CursorLockMode.Locked; // Keep doing this. We don't want cursor anywhere just yet

		// Offset.
		//_camTransform.localPosition = new Vector3(CameraOrbitXYOffset.x, CameraOrbitXYOffset.y, -CameraOrbitDistanceMax);
		_camBoomTransform.localPosition = new Vector3(CameraOrbitXYOffset.x, CameraOrbitXYOffset.y, -CameraOrbitDistanceMax);


		m_pDamageable = GetComponentInChildren<Damageable>();
		// Use the monk abilityset for now.
		m_pAbilityUser = new MonkAbilityUser(this, m_pDamageable);
		m_pAbilityGUIManager.m_pOwnerAbilityUser = m_pAbilityUser;
		m_pAbilityUser.m_pGUIManager = m_pAbilityGUIManager;
		m_pAbilityUser.m_pGUIManager.CreateAbilityIcons();
	}

	#region inputfunctions
	//public override void MakeInput(ControllerInputs type) {
	//	base.MakeInput(type);
	//}

	public void OnPrimary(InputValue value) {
		if (m_bIsInventoryOpen)
			return;
		MakeInput(ControllerInputs.ABILITY1, value.isPressed);
	}

	public void OnSecondary(InputValue value) {
		if (m_bIsInventoryOpen)
			return;
		MakeInput(ControllerInputs.ABILITY2, value.isPressed);
	}

	public void OnTertiary(InputValue value) {
		if (m_bIsInventoryOpen)
			return;
		MakeInput(ControllerInputs.ABILITY3, value.isPressed);
	}

	public void OnQuaternary(InputValue value) {
		if (m_bIsInventoryOpen)
			return;
		MakeInput(ControllerInputs.ABILITY4, value.isPressed);
	}

	public void OnMove(InputValue value) {
		if (m_bIsInventoryOpen) {
			m_Move = new Vector2(0, 0);
			return;
		}
		m_Move = value.Get<Vector2>();
	}

	public void OnLook(InputValue value) {
		if (m_bIsInventoryOpen)
			return;
		m_Look = value.Get<Vector2>() * Time.timeScale;
	}

	public void OnJump(InputValue value) {
		if (m_bIsInventoryOpen) {
			_isGonnaJump = false;
			return;
		}
		_isGonnaJump = value.Get<float>() > 0.5f;
	}

	public void OnDash(Vector2 v2DashStrength) {
		//Vector3 gn;
		//if (!IsGrounded(out gn)) {
		//Vector3 braking = _camTransform.forward;
		float dot = (Vector3.Dot(-_camTransform.forward, _velocity.normalized) + 1) / 2f;
		//Debug.Log(dot);

		if (_isGrounded) {
			_velocity += (transform.forward * v2DashStrength.x) + (Vector3.up * v2DashStrength.y);
		} else {
			// Partially cancel our existing velocity if we dash in the opposite direction of our current vel.
			_velocity += (_camTransform.forward * dot * _velocity.magnitude);
			_velocity += (_camTransform.forward * v2DashStrength.x);
		}

		beingLaunched = true;
	}

	#endregion

	//private void DashReset() {
	//	canDashReset = true;
	//}

	// Only for debug drawing
	private void OnGUI() {
		if (!_debugInfo) {
			return;
		}

		// Print current horizontal speed
		var ups = _velocity;
		ups.y = 0;
		GUI.Box(new Rect(Screen.width / 2f - 50, Screen.height / 2f + 50, 100, 40),
			(Mathf.Round(ups.magnitude * 100) / 100).ToString());

		Vector3 gn;
		GUI.Box(new Rect(50, 300, 150, 40),
			"Is Grounded: " + IsGrounded(out gn));

		// Draw horizontal speed as a line
		var mid = new Vector2(Screen.width / 2, Screen.height / 2); // Should remain integer division, otherwise GUI drawing gets screwed up
		var v = _camTransform.InverseTransformDirectionHorizontal(_velocity) * _velocity.WithY(0).magnitude * 10f;
		if (v.WithY(0).magnitude > 0.0001) {
			Drawing.DrawLine(mid, mid + Vector2.up * -v.z + Vector2.right * v.x, Color.red, 3f);
		}

		// Draw input direction
		var w = _camTransform.InverseTransformDirectionHorizontal(_wishDirDebug) * 100;
		if (w.magnitude > 0.001) {
			Drawing.DrawLine(mid, mid + Vector2.up * -w.z + Vector2.right * w.x, Color.blue, 2f);
		}
	}

	public override void Update() {
		base.Update();
		//Cursor.lockState = CursorLockMode.Locked; // Keep doing this. We don't want cursor anywhere just yet
		if (Cursor.lockState != CursorLockMode.Locked)
			m_Look = Vector2.zero;

		// Don't allow jumps while ramping, only true player-input sliding
		/*if (Input.GetKeyDown(KeyCode.Space) && !_isGonnaJump) {
			_isGonnaJump = true;
		} else if (Input.GetKeyUp(KeyCode.Space)) {
			_isGonnaJump = false;
		}*/ // --> moved to OnJump

		// Wrap the angle
		if (_pitch > 180f) {
			_pitch = -360 + _pitch;
		} else if (_pitch < -180f) {
			_pitch = 360 + _pitch;
		}

		// 200 IS THE SENS, MOVE IT TO CONFIG 
		//_pitch += (m_Look.y * -SENSITIVITY);
		float delta = m_Look.y * -SENSITIVITY;

		//_pitch = Mathf.Clamp(_pitch, -87, 87);

		//_pitch = Mathf.Clamp(_pitch, -360, 360);

		if (degressToCompensate > 0f) {
			float degDelta = recoveryRate * Time.deltaTime;

			// Prevent overcompensation
			if (_pitch + degDelta > 90f || _pitch + degDelta < 0f) {
				_pitch += degDelta;
				degressToCompensate -= degDelta;
			} else {
				degressToCompensate = 0f;
				Debug.Log("Reset degree count");
			}
		}

		if (_isGrounded && !_isGonnaJump) {

			// OLD - Rotates to -87°
			/*if (_pitch > 87f || _pitch < -87f) {
				_pitch += 720f * Time.deltaTime;
			}*/

			if ((_pitch > 90f || _pitch < -90f) && delta > 0f) {
				_pitch += delta;
			} else if (_pitch + delta > -87f && _pitch + delta < 87f) {
				_pitch += delta;
			}

		} else {
			_pitch += delta;
		}

		if (!_isGrounded) {
			// Counting spins
			degreesThisJump += delta;
		} else {
			degreesThisJump = 0f;
		}

		_yaw += (_pitch < 90f && _pitch > -90) ? m_Look.x * SENSITIVITY : m_Look.x * -SENSITIVITY;

		_camTransform.localRotation = Quaternion.Euler(Vector3.right * _pitch);
		//_camTransform
		transform.localRotation = Quaternion.Euler(Vector3.up * _yaw);

		//speedLinesEffect.position = _camTransform.position;
		speedLinesEffect.position = _mainCamera.position;
		speedLinesEffect.AdditiveLookat(speedLinesEffect.position + _velocity, Vector3.up, speedLinesRotateRate * Time.deltaTime);
		speedLinesEffectMat.SetFloat(speedEffectHandle, _velocity.sqrMagnitude.RemapClamped(speedLinesVelocityRangeSqr.x, speedLinesVelocityRangeSqr.y, 0f, 1f));
		speedLinesEffectMat.SetFloat(speedAngleHandle, Vector3.Dot(_camTransform.forward, speedLinesEffect.forward));

		//_camTransform.Rotate(_camTransform.forward, -_currentRot, Space.World);

		// Check for camera occlusion and adjust.
		RaycastHit info;
		Vector3 vCameraBoomExtent = _camTransform.rotation * new Vector3(CameraOrbitXYOffset.x, CameraOrbitXYOffset.y, -CameraOrbitDistanceMax);
		//Debug.DrawLine(_camTransform.position, _camTransform.position + vCameraBoomExtent, Color.green);
		if (Physics.Raycast(_camTransform.position, vCameraBoomExtent.normalized, out info, vCameraBoomExtent.magnitude, CameraOcclusionLayers.value, QueryTriggerInteraction.Ignore)) {
			_camBoomTransform.localPosition = new Vector3(CameraOrbitXYOffset.x, CameraOrbitXYOffset.y, -CameraOrbitDistanceMax).normalized * Mathf.Clamp(info.distance, 0, CameraOrbitDistanceMax);
		} else {
			_camBoomTransform.localPosition = new Vector3(CameraOrbitXYOffset.x, CameraOrbitXYOffset.y, -CameraOrbitDistanceMax);
		}
	}


	private int velRecCounter = 0;
	private void FixedUpdate() {
		//if (_velocityNextFrameDirty) {
		//	_velocity = _velocityForNextFrame.Copy();
		//	_velocityForNextFrame = Vector3.zero;
		//	_velocityNextFrameDirty = false;
		//}

		Vector3 positionBefore = transform.position.WithY(0);

		// Grab the vertical velocity to calculate the impact speed if necessary
		if (!_isGrounded)
			verticalVelocityBeforeLanding = _velocity.VerticalComponent() < 0 ? _velocity.VerticalComponent() : verticalVelocityBeforeLanding;

		//Average speed readings
		velocityReadings[velRecCounter] = _velocity.ToHorizontal();
		if (++velRecCounter > (NUMREADINGS - 1)) {
			velRecCounter = 0;
		}

		// Reset player - for testing
		/*if (Input.GetKeyDown(KeyCode.L)) {
			Gravity.Set(Vector3.down);
			transform.position = _spawnPoint.position;
			_velocity = Vector3.forward;
		}*/



		var dt = Time.fixedDeltaTime;


		// The player is attempting to go in this direction
		Vector3 wishDir = new Vector3(m_Move.x, 0f, m_Move.y);
		wishDir = _camTransform.TransformDirectionHorizontal(wishDir);
		_wishDirDebug = wishDir;//.ToHorizontal();

		Vector3 groundNormal;
		_isGrounded = IsGrounded(out groundNormal);
		_isGrounded = beingLaunched ? false : _isGrounded;

		// Upon landing, if we're beyond the normal limits, set the degrees we need to rotate by to reach forward facing angle
		if (!_isGroundedInPrevFrame && _isGrounded && (_pitch > 87f || _pitch < -87f)) {
			// If positive
			if (_pitch > 0) {
				degressToCompensate = 360f - _pitch;
			} else {
				degressToCompensate = -_pitch;
			}
			Debug.Log("Pitch target set: " + degressToCompensate);
		}

		bool bounceForNext = false;

		if (_isGrounded) {
			//if (!canDash && canDashReset) {
			//	canDash = true;
			//}

			//if (_isGroundedInPrevFrame && !_isGonnaJump && !beingLaunched) {
			if (_isGroundedInPrevFrame) {
				// Don't apply friction if just landed or about to jump (essentially, bhopping)
				if (!_isGonnaJump && !beingLaunched) {
					ApplyFriction(ref _velocity, dt, wishDir);
				}
			} else {
				// Play the industry-standard landing impact animation if applicable
				cameraAnimator?.SetFloat("LandImpactBlend", Mathf.Min(-verticalVelocityBeforeLanding / landImpactMaxVelocity, 1)); // ?????

				//Debug.Log(verticalVelocityBeforeLanding);
				cameraAnimator?.SetTrigger("Land");
			}

			if (-verticalVelocityBeforeLanding > minimumVelocityBounce) {
				bounceForNext = true;
			}

			/*if (isRamping) {
				_velocity += Gravity.Down * (GravityAmount * dt);

			} else */
			if (!_isGonnaJump) {
				Accelerate(ref _velocity, wishDir, GroundAccelerationCoeff, dt);
			}
			// Crop up horizontal velocity component - JULY9 - REMOVED THIS AS IT PREVENTS LAUNCHING
			if (!beingLaunched) {
				_velocity = Vector3.ProjectOnPlane(_velocity, groundNormal);
			}

			if (!_isGroundedInPrevFrame && !_isGonnaJump) {
				_lastjumpwasramp = false;
			}

			// JUMP
			if (_isGonnaJump && !isRamping) {


				// How strong is our jump? If we're jumping up a slope we can do a Quake Ramp Jump
				/*if (playerSocket.CanQuakeRampBounce.resultantvalue) {
					// Ramp jump converts some horizontal to vertical speed
					if (_timeSinceLastJump <= _rampJumpTimeMax) {
						_velocity = new Vector3(_velocity.x * 0.8f, _velocity.y, _velocity.z * 0.8f);
						_velocity += -Gravity.Down * JumpStrength * 1.15f;
						_lastjumpwasramp = true;

					} else if (_timeSinceLastJump >= _doubleJumpDownTimeMin && !_lastjumpwasramp) {
						// If we're jumping down a slope (longer time between jumps), we boost horizontally in our move dir
						// Increase horizontal speed

						// Clamp downward velocity to 0, otherwise slopes get buggy :(
						_velocity = _velocity.WithY(Mathf.Max(_velocity.y, 0f));
						_velocity += -Gravity.Down * JumpStrength;
						_velocity += Vector3.ProjectOnPlane(_velocity, groundNormal).normalized * 3.0f;

						_lastjumpwasramp = false;
					} else {
						_velocity += -Gravity.Down * JumpStrength;
						_lastjumpwasramp = false;
					}
				} else {*/

				//}

				/*if (_timeSinceLastJump <= _rampDoubleJumpTimeMax && !_lastjumpwasramp) {
					// Clamp downward velocity to 0, otherwise slopes get buggy :(
					_velocity = _velocity.WithY(Mathf.Max(_velocity.y, 0f));
					_velocity += -Gravity.Down * JumpStrength;
					_lastjumpwasramp = true;
				} else {
					_lastjumpwasramp = false;
				}*/

				//_velocity += -Gravity.Down * JumpStrength; // Normal jump
				// NEW add velocity that goes down the ramp like in Quake
				// Is 0 if the ground normal is just flat, increases to a max of 2.5 ups at 90° (which is impossible to hit really)
				//_velocity += groundNormal * 2.5f * (1 - Mathf.Abs(Vector3.Dot(groundNormal, Vector3.up))); // Ramp jump
				_velocity += groundNormal * JumpStrength; // New: GroundNormal only

				m_pDamageable.reusableAudioSource.PlayOneShot(stepClips.stepClips_jump, 0.3f);

				// The minimum time needed between jumps to reset them (Too frequently will not allow the timer to be reset)
				if (_timeSinceLastJump > 0.1f) {
					_timeSinceLastJump = 0.0f;
				}
			}
		} else {
			if (_timeSinceLastJump <= 10f) {
				_timeSinceLastJump += dt;
			}
			// If the input doesn't have the same facing with the current velocity
			// then slow down instead of speeding up (and use appropriate coefficient)
			var coeff = Vector3.Dot(_velocity, wishDir) > 0 ? AirAccelCoeff : AirDecelCoeff;

			Accelerate(ref _velocity, wishDir, coeff, dt);

			if (Mathf.Abs(m_Move.x) > 0.0001) {  // Pure side velocity doesn't allow air control
				ApplyAirControl(ref _velocity, wishDir, dt);
			}

			_velocity += Gravity.Down * (GravityAmount * dt);
		}

		var displacement = _velocity * dt;

		// If we're moving too fast, make sure we don't hollow through any collider
		if (displacement.magnitude > _collisionVolume.radius) {
			ClampDisplacement(ref _velocity, ref displacement, transform.position);
		}

		transform.position += displacement;

		var collisionDisplacement = ResolveCollisions(ref _velocity);
		transform.position += collisionDisplacement;


		if (beingLaunched) {
			beingLaunched = false;
		}

		// Move bob blending
		float moveBobBlendTarget = _velocity.WithY(0f).magnitude / moveBobMaxVelocity * (_isGrounded ? 1 : 0);  // If we're grounded, set the target to the proportion of our xz-movespeed to the max move speed
		moveBobBlend = moveBobBlend.InterpAdditive(moveBobBlendTarget, moveBobBlendRate, Time.fixedDeltaTime);  // Additively interpolate
		cameraAnimator?.SetFloat("MoveBlend", moveBobBlend);


		/*if (_enableDynamicFoV) {
			float targetFoV;
			float speed = _velocity.ToHorizontal().magnitude;
			if (speed <= _minSpeed_Fov) {
				targetFoV = _minFov;
			} else if (speed >= _maxSpeed_Fov) {
				targetFoV = _maxFov;
			} else {
				targetFoV = speed.Remap(_minSpeed_Fov, _maxSpeed_Fov, _minFov, _maxFov);
			}

			float diff = targetFoV - camera.fieldOfView;
			// If the difference isn't negligible...
			if (Mathf.Abs(diff) >= 0.01f) {
				// If the difference is smaller than the rate of change, just set it to the target
				if (Mathf.Abs(diff) <= _fovChangeRate * Time.deltaTime) {
					camera.fieldOfView = targetFoV;
				} else {
					if (Mathf.Sign(diff) > 0) {
						camera.fieldOfView += _fovChangeRate * Time.deltaTime;
					} else {
						camera.fieldOfView -= _fovChangeRate * Time.deltaTime;
					}
				}
			}
		}*/

		/*float distanceCoveredSinceLastStep = 0f;
		float stepMinTimer;
		const float stepMinInterval = 0.1f;
		const float stepDistance = 0.5f;*/

		if (_isGrounded) {
			// Increment the distance tracker for footstep sounds
			distanceCoveredSinceLastStep += Mathf.Abs((positionBefore - transform.position.WithY(0)).magnitude);

			if (!_isGroundedInPrevFrame && Time.time > nextLandSound) {
				// Landing SFX
				m_pDamageable.reusableAudioSource.PlayOneShot(stepClips.stepClips_jump, 0.3f);
				switch (currentStepMaterial) {
					default:
						m_pDamageable.reusableAudioSource.PlayOneShot(stepClips.stepsClips_Sand[stepClips.stepsClips_Sand.GetRandomIndex()], 0.6f);
						break;
				}

				// Only allow a landing sound once every 0.2 seconds to avoid situations where poor collision can repeatedly unground/ground the player.
				nextLandSound = Time.time + 0.2f;
			}
		}
		
		if (distanceCoveredSinceLastStep > stepDistance && Time.fixedTime > stepMinTimer && _isGrounded) {
			switch (currentStepMaterial) {
				default:
				m_pDamageable.reusableAudioSource.PlayOneShot(stepClips.stepsClips_Sand[stepClips.stepsClips_Sand.GetRandomIndex()], 0.6f);
				m_pDamageable.reusableAudioSource.PlayOneShot(stepClips.stepsClips_footSound[stepClips.stepsClips_footSound.GetRandomIndex()], 0.3f);
				distanceCoveredSinceLastStep = 0f;
				stepMinTimer = Time.fixedTime + stepMinInterval;
					break;
			}
		}

		if(bounceForNext) {
			AddForce(new Vector3(0, -verticalVelocityBeforeLanding * restitutionFactor, 0), true);
			_isGrounded = false;
			//Debug.Log("Bounced up: " + (-verticalVelocityBeforeLanding * restitutionFactor).ToString());
		}

		_isGroundedInPrevFrame = _isGrounded;
	}

	private void Accelerate(ref Vector3 playerVelocity, Vector3 accelDir, float accelCoeff, float dt) {
		// How much speed we already have in the direction we want to speed up

		// CPMA Style - Acceleration in direction of vel = 0, at 90° = 1, at 180° (Braking) = 2
		float projSpeed = Vector3.Dot(playerVelocity, accelDir);

		// To punish players for speeding without a plan, divide their accel coeff somewhat
		if (_isGrounded) {
			accelCoeff *= playerVelocity.magnitude.RemapClamped(AccelVelocityMultiplierBounds.x, AccelVelocityMultiplierBounds.y, AccelVelocityMultiplierBounds.z, AccelVelocityMultiplierBounds.w);
		}

		float speedToUse = MaxSpeedDefault;
		if (m_Move.y < 0) {
			speedToUse = MaxSpeedBackwards;
		} else if (m_Move.x != 0) {
			speedToUse = MaxSpeedSideways;
		}

		// How much speed we need to add (in that direction) to reach max speed
		var addSpeed = speedToUse - projSpeed;
		if (addSpeed <= 0) {
			return;
		}

		// How much we are gonna increase our speed
		// maxSpeed * dt => the real deal. a = v / t
		// accelCoeff => ad hoc approach to make it feel better
		var accelAmount = accelCoeff * speedToUse * dt;

		// If we are accelerating more than in a way that we exceed maxSpeedInOneDimension, crop it to max
		if (accelAmount > addSpeed) {
			accelAmount = addSpeed;
		}

		playerVelocity += accelDir * accelAmount;
	}

	private void ApplyFriction(ref Vector3 playerVelocity, float dt, Vector3 wishdir) {
		var speed = playerVelocity.magnitude;
		if (speed <= 0.00001) {
			return;
		}

		float dot = (wishdir.magnitude > 0.1f) ? Mathf.Max(0f, Vector3.Dot(wishdir, playerVelocity)) : 1f;

		var downLimit = Mathf.Max(speed, FrictionSpeedThresholdStop); // Don't drop below threshold.
																	  // If we're crouching and going beyond the minimum slide limit and we allow sliding, use sliding friction

		float fric = Friction;
		if (speed < FrictionSpeedThresholdRoughen) {
			fric *= Friction * 5f;
		}

		var dropAmount = speed - (downLimit * fric * dt * dot);
		if (dropAmount < 0) {
			dropAmount = 0;
		}

		playerVelocity *= (dropAmount / speed); // Reduce the velocity by a certain percent
	}

	private void ApplyAirControl(ref Vector3 playerVelocity, Vector3 accelDir, float dt) {
		// This only happens in the horizontal plane
		// TODO: Verify that these work with various gravity values
		var playerDirHorz = playerVelocity.ToHorizontal().normalized;
		var playerSpeedHorz = playerVelocity.ToHorizontal().magnitude;

		var dot = Vector3.Dot(playerDirHorz, accelDir);
		if (dot > 0) {
			var k = AirControlPrecision * dot * dot * dt;

			// Like CPMA, increased direction change rate when we only hold W
			// If we want pure forward movement, we have much more air control
			var isPureForward = Mathf.Abs(m_Move.x) < 0.0001 && Mathf.Abs(m_Move.y) > 0;
			if (isPureForward) {
				k *= AirControlAdditionForward;
			}

			// A little bit closer to accelDir
			playerDirHorz = playerDirHorz * playerSpeedHorz + accelDir * k;
			playerDirHorz.Normalize();

			// Assign new direction, without touching the vertical speed
			playerVelocity = (playerDirHorz * playerSpeedHorz).ToHorizontal() + Gravity.Up * playerVelocity.VerticalComponent();
		}
	}

	/*private void ApplySlideControl(ref Vector3 playerVelocity, Vector3 accelDir, float dt) {
		// This only happens in the horizontal plane
		var playerDirHorz = playerVelocity.ToHorizontal().normalized;
		var playerSpeedHorz = playerVelocity.ToHorizontal().magnitude;

		var dot = Vector3.Dot(playerDirHorz, accelDir);
		if (dot > 0) {
			//16f was air control precision
			var k = 1f * dot * dot * dt;

			// (Mae'n fel Slash o Quake: Champions)
			// If we want pure forward movement, we have much more air control

	// A little bit closer to accelDir
	playerDirHorz = playerDirHorz* playerSpeedHorz + accelDir* k;
	playerDirHorz.Normalize();

			// Assign new direction, without touching the vertical speed
			playerVelocity = (playerDirHorz* playerSpeedHorz).ToHorizontal() + Gravity.Up* playerVelocity.VerticalComponent();
}

	}*/

	// Calculates the displacement required in order not to be in a world collider
	private Vector3 ResolveCollisions(ref Vector3 playerVelocity) {
		// Get nearby colliders
		Physics.OverlapSphereNonAlloc(transform.position, Radius + 0.1f,
			_overlappingColliders, ~_excludedLayers, QueryTriggerInteraction.Ignore);

		var totalDisplacement = Vector3.zero;
		var checkedColliderIndices = new HashSet<int>();

		// If the player is intersecting with that environment collider, separate them
		for (var i = 0; i < _overlappingColliders.Length; i++) {
			// Two player colliders shouldn't resolve collision with the same environment collider
			if (checkedColliderIndices.Contains(i)) {
				continue;
			}

			var envColl = _overlappingColliders[i];

			// Skip empty slots
			if (envColl == null || envColl.isTrigger) {
				continue;
			}

			Vector3 collisionNormal;
			float collisionDistance;
			if (Physics.ComputePenetration(
				_collisionVolume, _collisionVolume.transform.position, _collisionVolume.transform.rotation,
				envColl, envColl.transform.position, envColl.transform.rotation,
				out collisionNormal, out collisionDistance)) {
				// Ignore very small penetrations
				// Required for standing still on slopes
				// ... still far from perfect though
				if (collisionDistance < _slopePenetrationIgnoreLimit) {
					continue;
				}

				checkedColliderIndices.Add(i);

				// Shift out of the collider
				totalDisplacement += collisionNormal * collisionDistance;

				// Clip down the velocity component which is in the direction of penetration
				playerVelocity -= Vector3.Project(playerVelocity, collisionNormal);
			}
		}

		// (holy shit this was dumb)
		// It's better to be in a clean state in the next resolve call
		for (var i = 0; i < _overlappingColliders.Length; i++) {
			_overlappingColliders[i] = null;
		}

		return totalDisplacement;
	}

	// If one of the rays hit, we're considered to be grounded
	private bool IsGrounded(out Vector3 groundNormal) {
		// If vertical speed (w.r.t. gravity direction) is greater than our jump velocity, don't ground us.
		// This allows for sliding up ramps when moving fast
		// If we allow downward slide, the player can slide when their vertical velocity is downward
		float verticalSpeed = _velocity.VerticalComponent();
		bool isGrounded = false;
		groundNormal = -Gravity.Down;

		// Moving vertically fast enough allows us to slide up ramps.
		// If we're being launched, then we need to return False if we're going upwards at all to avoid unwanted groundings when being launched.
		if (verticalSpeed > rampThreshold || (beingLaunched && verticalSpeed > 0)) {
			return false; //- now removed, when sliding up a ramp we'll be in the slide state from now on
						  //isRamping = true;
		} else {
			//isRamping = false;

			foreach (var t in _groundedRayPositions) {
				// The last one is reserved for ghost jumps
				// Don't check that one if already on the ground
				// Ghost jump is typically behind the player and below so that we can check if we've recently jumped etc.
				if (t == _ghostJumpRayPosition && isGrounded) {
					continue;
				}

				RaycastHit hit;
				if (Physics.Raycast(t.position, Gravity.Down, out hit, 0.51f, ~_excludedLayers, QueryTriggerInteraction.Ignore)) {
					if (Vector3.Angle(Gravity.Up, hit.normal) <= maxSlopeAngle) {
						groundNormal = hit.normal;
						//isGrounded = true;

						return true;
					}
				}
			}
			//}
		}
		return isGrounded;
	}

	// If there's something between the current position and the next, clamp displacement
	private void ClampDisplacement(ref Vector3 playerVelocity, ref Vector3 displacement, Vector3 playerPosition) {
		RaycastHit hit;
		if (Physics.Raycast(playerPosition, playerVelocity.normalized, out hit, displacement.magnitude, ~_excludedLayers)) {
			displacement = hit.point - playerPosition;
		}
	}

	// Handy when testing
	public void ResetAt(Transform t) {
		transform.position = t.position + Vector3.up * 0.5f;
		//_camTransform.position = _defaultCamPos;
		_velocity = t.TransformDirection(Vector3.forward);
	}

	public override void AddForce(Vector3 force, bool explosion = false) {
		_velocity += force;
		beingLaunched = true;
	}

	public override void Push(Vector3 pushVector, Vector3? point = null) {
		AddForce(pushVector, false);
	}

	public void SetCameraAnimatorTrigger(string name) {
		cameraAnimator?.SetTrigger(name);
	}

	public void SetCameraAnimatorBool(string name, bool value) {
		cameraAnimator?.SetBool(name, value);
	}

	public void SetCameraAnimatorFloat(string name, float value) {
		cameraAnimator?.SetFloat(name, value);
	}
}
