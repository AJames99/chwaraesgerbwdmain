﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class Controller : MonoBehaviour, IController {
	public enum ControllerInputs {
		MOVE,
		MOVEWORLDSPACE,
		JUMP,
		LOOK,
		ABILITY1,
		ABILITY2,
		ABILITY3,
		ABILITY4,
	}

	public Dictionary<ControllerInputs, bool> m_dicInputsHeld = new Dictionary<ControllerInputs, bool>();

	[HideInInspector]
	public bool m_bIsInventoryOpen;

	public AbilitySlot.SLOTBINDING_t GetSlotForInput(ControllerInputs input) {
		switch (input) {
			case ControllerInputs.ABILITY1:
				return AbilitySlot.SLOTBINDING_t.PRIMARY;
			case ControllerInputs.ABILITY2:
				return AbilitySlot.SLOTBINDING_t.SECONDARY;
			case ControllerInputs.ABILITY3:
				return AbilitySlot.SLOTBINDING_t.TERTIARY;
			case ControllerInputs.ABILITY4:
				return AbilitySlot.SLOTBINDING_t.QUATERNARY;
			
			default:
				return AbilitySlot.SLOTBINDING_t.PRIMARY;
		}
	}

	protected virtual void Start() {
		m_dicInputsHeld.Add(ControllerInputs.ABILITY1, false);
		m_dicInputsHeld.Add(ControllerInputs.ABILITY2, false);
		m_dicInputsHeld.Add(ControllerInputs.ABILITY3, false);
		m_dicInputsHeld.Add(ControllerInputs.ABILITY4, false);
		m_dicInputsHeld.Add(ControllerInputs.JUMP, false);

		m_bIsInventoryOpen = false;

		if(GetComponent<PlayerInput>())
			m_pInputActionMap = GetComponent<PlayerInput>().currentActionMap;
	}

	protected AbilityUser m_pAbilityUser;
	protected Damageable m_pDamageable;
	protected InputActionMap m_pInputActionMap;

	[SerializeField]
	protected AbilityGUIManager m_pAbilityGUIManager = null;

	public AbilityUser GetAbilityUser() {
		return m_pAbilityUser;
	}

	public Damageable GetDamageable() {
		return m_pDamageable;
	}

	public InputActionMap GetInputActions() {
		return m_pInputActionMap;
	}

	public virtual void MakeInput(ControllerInputs type, Vector3 value) {}
	public virtual void MakeInput(ControllerInputs type, Vector2 value) {}
	public virtual void MakeInput(ControllerInputs type, float value) {}
	public virtual void MakeInput(ControllerInputs type, bool value) {
		if(value)
			m_pAbilityUser.UseAbility(GetSlotForInput(type));
		else
			m_pAbilityUser.OnStopUsingAbility(GetSlotForInput(type));

		if (m_dicInputsHeld.ContainsKey(type)) {
			if (!m_dicInputsHeld[type] && value)
				m_pAbilityUser.OnBeginUsingAbility(GetSlotForInput(type));

			m_dicInputsHeld[type] = value;
		}
	}
	public virtual void MakeInput(ControllerInputs type) { }

	public abstract void Push(Vector3 pushVector, Vector3? point = null);
	public abstract void AddForce(Vector3 force, bool explosion = false);
	public abstract Vector3 GetVelocity();

	public abstract void SetVelocity(Vector3 vel);

	public virtual void Update() {
		if (null != m_pAbilityUser) {
			m_pAbilityUser.Update();
			foreach (ControllerInputs input in m_dicInputsHeld.Keys) {
				if (m_dicInputsHeld[input])
					m_pAbilityUser.UseAbility(GetSlotForInput(input));
			}
		}
	}
}
