﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireScript : MonoBehaviour {

	public const string FLAMMABLE_TAG = "Flammable";

	// Decides whether or not we're burning, which temperature sources we can accept, and ability to spread
	private float _temperature = 0.0f;

	// Should we instantiate ignited *immediately*?
	[SerializeField]
	private bool _startOnFire = false;

	// The temperature needed to start burning
	[SerializeField]
	private float _burnThreshold = 3.0f;

	// The temperature needed to stop burning ("chemical energy")
	[SerializeField]
	private float _extinguishThreshold = 15.0f;

	// The passive increase in temperature while we're burning
	[SerializeField]
	private float _temperatureDeltaWhileBurning = 1.0f;

	// Are we currently on fire?
	[HideInInspector]
	public bool _isBurning;

	// Have we completed the burning process, and are no longer ignitable?
	private bool _isExtinguished;

	// Should burning mean we can no longer accept temperature increases from other sources?
	//[SerializeField]
	private bool _allowExternalSourcesWhileBurning = false;

	// The rate at which we increase neighbouring objects' temperatures if we're burning
	const float _spreadRate = 0.25f;

	// This allows other FireScripts to check if ours is valid or not (i.e. has all the necessary data and settings etc)
	bool _scriptIsValid = false;

	List<Collider> _triggerList = new List<Collider>();
	List<FireScript> _otherFlammables = new List<FireScript>();

	// The mesh to emit fiery particles from
	[SerializeField]
	private MeshRenderer _emissionMesh;

	[SerializeField]
	private SkinnedMeshRenderer _emissionSkinnedMesh;

	[SerializeField]
	private bool _useSkinnedMesh = false;

	// Spreads fire to intersecting objects
	[SerializeField]
	Collider _fireColliderTrigger;

	[SerializeField]
	private GameObject _fireParticleSysPrefab;

	ParticleSystem _masterFireSystem;
	//Transform _stretchedFlames;

	List<Material> objectMaterials;

	[SerializeField]
	Material _blackMat;

	// The sound effect/s this object will use when burning (chosen at random from the list)
	[SerializeField]
	private List<AudioClip> _burnSoundClips;

	AudioSource _soundSource;
	// Lerp between 0 and this over an amount of time when beginning to burn
	const float maxVol = 2.0f;

	const float _maxLightIntensity = 25.0f;
	const float _lightJitter = 2.0f;
	private Light _fireLight;

	private Material _blackMatInstance;

	private void Start() {
		if (!_fireColliderTrigger) {
			_fireColliderTrigger = GetComponent<SphereCollider>();
		}
		if (!_emissionMesh && !_useSkinnedMesh) {
			_emissionMesh = GetComponent<MeshRenderer>();
		}
	}

	UnityEngine.Rendering.CommandBuffer cb;
	void Awake() {
		Start();

		if ((_emissionMesh && !_useSkinnedMesh || _emissionSkinnedMesh && _useSkinnedMesh) && _fireColliderTrigger && _temperatureDeltaWhileBurning >= 0.0f && _extinguishThreshold >= 0.0f && _burnThreshold >= 0.0f) {
			_scriptIsValid = true;
			_fireColliderTrigger.tag = FLAMMABLE_TAG; // Just in case we forgot hehe
			SetupParticleSystem();

			if (_useSkinnedMesh) {
				objectMaterials = new List<Material>(_emissionSkinnedMesh.materials);
				_blackMatInstance = new Material(_blackMat);
				objectMaterials.Add(_blackMatInstance);
				_emissionSkinnedMesh.materials = objectMaterials.ToArray();
			} else {


				// Workaround for drawing burn effect on mesh with >1 materials
				if (_emissionMesh.materials.Length > 1) {
					GameObject duplicateMeshObj = new GameObject("Duplicate");
					duplicateMeshObj.transform.parent = transform;
					duplicateMeshObj.transform.localPosition = Vector3.zero;
					duplicateMeshObj.transform.localRotation = Quaternion.identity;
					duplicateMeshObj.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
					MeshRenderer duplicateMeshRenderer = duplicateMeshObj.AddComponent<MeshRenderer>();

					_blackMatInstance = new Material(_blackMat);
					Material[] blackMats = new Material[_emissionMesh.materials.Length];
					for (int i = 0; i < blackMats.Length; i++) {
						blackMats[i] = _blackMatInstance;
					}
					duplicateMeshRenderer.materials = blackMats;
					duplicateMeshObj.AddComponent<MeshFilter>().sharedMesh = GetComponent<MeshFilter>().sharedMesh;
				} else {
					objectMaterials = new List<Material>(_emissionMesh.materials);
					_blackMatInstance = new Material(_blackMat);
					objectMaterials.Add(_blackMatInstance);
					_emissionMesh.materials = objectMaterials.ToArray();
				}
			}

			// Sound
			//_soundSource = gameObject.AddComponent<AudioSource>();
			_soundSource = _masterFireSystem.gameObject.AddComponent<AudioSource>();
			_soundSource.loop = true;
			_soundSource.volume = 0.0f;
			_burnSoundClips.Shuffle();
			_soundSource.clip = _burnSoundClips[0];
			_soundSource.spatialize = true;
			_soundSource.spatialBlend = 1.0f;
			//...

			// Light
			// Attempt to find the point light on the particle system (Make sure SetupParticleSystem is done beforehand)
			_fireLight = _masterFireSystem.GetComponent<Light>();
			if (_fireLight) {
				_fireLight.intensity = 0.0f;
			}
			//...

			if (_startOnFire) {
				Ignite();
			}
		} else {
			Debug.LogError("Oy vey! You've not set up the flammable object properly! You need: \nAn emission mesh for the particle system...\nA trigger collider\nA temperature delta while burning, extinguish, and burn threshold >= 0!");
		}
	}

	void SetupParticleSystem() {
		GameObject _fireSys = Instantiate(_fireParticleSysPrefab, this.transform);
		_fireSys.transform.localPosition = ((SphereCollider)_fireColliderTrigger).center;
		_masterFireSystem = _fireSys.GetComponent<ParticleSystem>();

		// Set the mesh
		ParticleSystem.ShapeModule sm = _masterFireSystem.shape;

		if (_useSkinnedMesh && _emissionSkinnedMesh) {
			sm.skinnedMeshRenderer = _emissionSkinnedMesh;
		} else if (!_useSkinnedMesh && _emissionMesh) {
			sm.meshRenderer = _emissionMesh;
		} else {
			Debug.LogError("Neither a skinned mesh nor a mesh has been set!!");
		}


		// Set the orientation of the stretched flames to (0,90,0) globally
		//_stretchedFlames = _masterFireSystem.transform.Find("particle fire stretch");
		//ParticleSystem stretchedFlameSystem = stretchedFlames.GetComponent<ParticleSystem>();
		//_stretchedFlames.rotation = Quaternion.Euler(0f, 90f, 0f);

	}


	//float DEBUG_lastFrameTemp = 0.0f;
	// Update is called once per frame
	float burnProgress = 0.0f;
	float noise;
	void Update() {
		if (!_isExtinguished) {
			if (_isBurning) {

				// To stop the rotation affecting the velocity over lifetime bit in the system
				_masterFireSystem.transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);

				IncreaseTemperature(Time.deltaTime * _temperatureDeltaWhileBurning);
				if (_temperature >= _extinguishThreshold) {
					Extinguish();
				}
				// Blacken the colour over time, 0-1 between burn threshold and extinguish threshold
				burnProgress = (_temperature - _burnThreshold) / (_extinguishThreshold - _burnThreshold);
				_blackMatInstance.SetFloat("BurnShaderLerp", burnProgress);
				//Debug.Log("Lerp progress: " + burnProgress);


				Random.InitState(System.DateTime.Now.Millisecond);
				noise = (Random.value - 0.5f) * _lightJitter;
				// {x<0.25: x*maxVol, 0.75<x, x*
				if (burnProgress < 0.25f) {
					//_soundSource.volume = 4 * burnProgress * maxVol;
					_soundSource.volume = burnProgress * maxVol + 0.1f;
					//_fireLight.intensity = (4 * burnProgress * _maxLightIntensity) + noise;
					_fireLight.intensity = burnProgress * _maxLightIntensity + noise;
				} else if (burnProgress > 0.75f) {
					//_soundSource.volume = (-4 * burnProgress * maxVol) + 4;
					_soundSource.volume = -(burnProgress * maxVol) + maxVol + 0.1f;
					//_fireLight.intensity = (-4 * burnProgress * _maxLightIntensity) + 4 + noise;
					_fireLight.intensity = -(burnProgress * _maxLightIntensity) + _maxLightIntensity; // No noise
				} else {// else... vol = 1
						//_fireLight.intensity = (1 * _maxLightIntensity) + noise;
					_fireLight.intensity = (_maxLightIntensity / 4) + noise;
				}

			} else {
				if (_temperature >= _burnThreshold) {
					Ignite();
				}
			}
			// Output temp to log if our temp has changed at all since last frame
			/*if (_temperature != DEBUG_lastFrameTemp) {
				Debug.Log(_temperature);
			}
			DEBUG_lastFrameTemp = _temperature;*/
		}
	}

	/*private void OnPostRender() {
		Camera.main.RemoveAllCommandBuffers();
		var cb = new UnityEngine.Rendering.CommandBuffer();
		cb.name = "outline";
		for (int i = 0; i < _emissionMesh.materials.Length; i++) {
			cb.DrawRenderer(_emissionMesh, _blackMatInstance, i);
		}
		Camera.main.AddCommandBuffer(UnityEngine.Rendering.CameraEvent.AfterEverything, cb);
	}*/

	public void Ignite() {
		if (!_isExtinguished) {
			_isBurning = true;
			_temperature = _burnThreshold; // As a contingency
			_masterFireSystem.Play();
			//Debug.Log("Ignited!");
			_soundSource.Play();
		}
	}

	public void Extinguish() {
		_isExtinguished = true;
		_isBurning = false;
		_masterFireSystem.Stop();
		//Debug.Log("Extinguished!");
		_soundSource.Stop();
	}

	public void IncreaseTemperature(float t) {
		if (!_isExtinguished) {
			if (_temperature + t >= _burnThreshold && !_isBurning) {
				_temperature = _burnThreshold;
				Ignite();
			} else {
				_temperature += t;
			}
		}
	}

	// Once per physics frame?
	private void FixedUpdate() {
		// If we're burning...
		if (_isBurning) {
			// Upright our flames (so the emitter cone points up) TODO: might be easier to change the shape's direction? and/or lerp?
			//_stretchedFlames.rotation = Quaternion.Euler(0f, 90f, 0f);

			// Go through each of our nearby flammables and check:
			foreach (FireScript flammable in _otherFlammables) {
				// If the other isn't extinguished, and either isn't burning or allows external sources while burning, spread
				//if (!flammable._isExtinguished && (!flammable._isBurning || flammable._allowExternalSourcesWhileBurning))
				// If the other isn't extinguished nor is it burning, spread
				if (!flammable._isExtinguished && !flammable._isBurning) {
					flammable.IncreaseTemperature(_spreadRate * Time.fixedDeltaTime);
				}
			}
		}
	}

	private void OnTriggerEnter(Collider other) {
		if (!_triggerList.Contains(other)) {
			// We only want objects tagged as flammable, and that are triggers 
			if (other.CompareTag(FLAMMABLE_TAG) && other.isTrigger) {
				_triggerList.Add(other);
				_otherFlammables.Add(other.GetComponentInParent<FireScript>());
			}
		}
	}
	private void OnTriggerExit(Collider other) {
		if (_triggerList.Contains(other)) {
			// Remove the FireScript from our list
			_otherFlammables.Remove(other.GetComponentInParent<FireScript>());
			// THEN remove the collider from our list
			_triggerList.Remove(other);
			//Debug.Log("FLAMMABLE REMOVED: " + other.transform.name);
		}
	}
}
