﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class KineticShieldScript : ShieldScript {
	[SerializeField]
	private GameObject shotPrefab;

	[SerializeField]
	private Transform shotPoint;

	[SerializeField]
	private AudioClip shotSound;

	AudioSource audioSource;

	[SerializeField]
	LayerMask railableLayers;

	[SerializeField]
	int damage = 80;

	[SerializeField]
	float range = 150f;


	// Fire rate
	[SerializeField]
	float railgunCooldown = 1.5f;
	float drawCooldown = 0.5f;
	private bool canFire = false;

	// How much damage have we accumulated so far?
	[HideInInspector]
	public int accumulatedDamage = 0;

	// How much damage do we need to accumulate to get 1 rail ammo?
	[SerializeField]
	private int damageConversionThreshold = 100;


	protected override void Start() {
		base.Start();
		audioSource = gameObject.AddComponent<AudioSource>();
		/*audioSource.spatialBlend = 1f;
		audioSource.spatialize = true;*/
		audioSource.volume = 0.1f;
	}

	public override void OnShieldHit(float projectileSpeed = 0, int projectileDamage = 0, int blockLevel = 0, Projectile projectile = null) {
		base.OnShieldHit(projectileSpeed, projectileDamage, blockLevel, projectile);

		if (!projectile)
			return;

		accumulatedDamage += projectileDamage;

		if (accumulatedDamage >= damageConversionThreshold && playerCharacter.resources[PlayerCharacter.ResourceType.RAILGUNSHOTS] < playerCharacter.resourceMaxima[PlayerCharacter.ResourceType.RAILGUNSHOTS]) {
			//playerCharacter.resources[PlayerCharacter.ResourceType.RAILGUNSHOTS] += 1;
			playerCharacter.PickupResource(PlayerCharacter.ResourceType.RAILGUNSHOTS, 1);
			accumulatedDamage -= damageConversionThreshold;
		}

		accumulatedDamage = Mathf.Min(accumulatedDamage, damageConversionThreshold);
	}

	void ResetShotCooldown() {
		canFire = true;
	}

	protected override void SetActiveShield() {
		base.SetActiveShield();
		canFire = false;
		Invoke("ResetShotCooldown", drawCooldown);
	}

	protected override void OnAttack(InputValue value) {
		if (active && canFire) {
			if (playerCharacter.resources[PlayerCharacter.ResourceType.RAILGUNSHOTS] > 0) {
				audioSource.PlayOneShot(shotSound);

				GameObject instance = Instantiate(shotPrefab, shotPoint.position, shotPoint.rotation);
				LineRenderer lr = instance.GetComponent<LineRenderer>();

				Vector3 endPoint = shotPoint.position + (shotPoint.forward * range);

				RaycastHit hitInfo;
				if (Physics.Raycast(shotPoint.position, shotPoint.forward, out hitInfo, range, railableLayers, QueryTriggerInteraction.Ignore)) {
					endPoint = hitInfo.point;
					Damageable dmg;
					if (hitInfo.collider.gameObject.TryGetComponent(out dmg)) {
						if (dmg.team != playerCharacter.team) {
							dmg.Hurt(new Damage(Damage.DamageTypes.DEFAULT, damage));
						}
					}
				}

				lr.SetPosition(0, shotPoint.position);
				lr.SetPosition(1, endPoint);

				Destroy(instance, 2f);

				playerCharacter.resources[PlayerCharacter.ResourceType.RAILGUNSHOTS]--;


				canFire = false;

				Invoke("ResetShotCooldown", railgunCooldown);
			}
		}
	}
}
