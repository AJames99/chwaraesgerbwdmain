﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileGhost : MonoBehaviour {

	Animator[] animators = null;

	[SerializeField]
	GameObject childEffect;
	GameObject childInstance;
	//[SerializeField]
	//bool childInheritsScale = true;
	[SerializeField]
	bool abandonChild = true;
	[SerializeField]
	bool abandonChildrenOnHit = true;
	[SerializeField]
	bool dieOnHit = true;
	[SerializeField]
	Transform[] alwaysAbandonOnHitChildren; // If this array is populated, they get abandoned on-hit

	[SerializeField]
	float m_flDestroyAfterHitTime = 2.0f;

	[SerializeField]
	Vector3 m_vRandomTorqueOnHit = new Vector3(0, 0, 0);

	protected Collider m_pCollider;
	protected Rigidbody m_pRigidBody;

	[SerializeField]
	protected AudioBundleProjectile m_pAudioBundleProjectile;

	[HideInInspector]
	public AudioSource reusableAudioSource;

	// Stick into walls etc.
	[SerializeField]
	bool m_bStickOnHit = false;

	public void Start() {
		reusableAudioSource = GetComponent<AudioSource>();
		if (!reusableAudioSource) {
			reusableAudioSource = gameObject.AddComponent<AudioSource>();
			reusableAudioSource.rolloffMode = AudioRolloffMode.Logarithmic;
			reusableAudioSource.minDistance = 15;
			reusableAudioSource.maxDistance = 150;
			reusableAudioSource.dopplerLevel = 0;
			reusableAudioSource.spread = 0;
			reusableAudioSource.spatialBlend = 1.0f;
			reusableAudioSource.mute = reusableAudioSource.loop = reusableAudioSource.playOnAwake = false;
		}
	}

	public virtual void Awake() {
		if (animators == null)
			animators = transform.GetComponentsInChildren<Animator>();
		if (!childInstance && childEffect) {
			childInstance = Instantiate(childEffect, transform);
			if (abandonChild) {
				childInstance.transform.SetParent(null, true);
			}
		}

		m_pCollider = GetComponent<Collider>();
		if (m_pCollider)
			m_pCollider.enabled = false;

		m_pRigidBody = GetComponent<Rigidbody>();
		if (m_pRigidBody)
			m_pRigidBody.isKinematic = true;
	}

	public virtual void OnHit(Hitbox pHitboxHit = null) {

		Vector3 vEffectiveForward = transform.parent.forward;

		if (null != m_pCollider && null != m_pRigidBody && m_flDestroyAfterHitTime > 0 && !m_bStickOnHit) {
			m_pCollider.enabled = true;
			m_pRigidBody.isKinematic = false;

			//Rigidbody pParentBody;
			if (transform.parent) {

				// Shift out from wall.
				transform.position += (transform.parent.forward + m_pRigidBody.velocity) * -1;

				Vector3 vTorque = UnityEngine.Random.insideUnitSphere;
				vTorque.Scale(m_vRandomTorqueOnHit);
				m_pRigidBody.AddTorque(vTorque, ForceMode.VelocityChange);
			}
		}

		if (m_pAudioBundleProjectile.projectileClips_hitWorld.Length > 0) {
			reusableAudioSource.PlayOneShot(m_pAudioBundleProjectile.projectileClips_hitWorld[m_pAudioBundleProjectile.projectileClips_hitWorld.GetRandomIndex()], m_pAudioBundleProjectile.m_flHitVolumeScaleBase);
		}

		if (!m_bStickOnHit) {
			// Unparent us to world space
			transform.SetParent(null, true);
		}  else if (m_flDestroyAfterHitTime > 0 && pHitboxHit != null) {
			// Stick to the object we hit
			transform.SetParent(pHitboxHit.transform, true);
			transform.position += vEffectiveForward * -3;
			m_pCollider.enabled = false;
			m_pRigidBody.isKinematic = true;
		}

		foreach (Animator animator in animators) {
			animator.SetTrigger("Impact");
		}

		if (abandonChildrenOnHit) {
			for (int i = 0; i < transform.childCount; i++) {
				transform.GetChild(i).SetParent(null, true);
			}
		} else {
			if (alwaysAbandonOnHitChildren.Length > 0) {
				foreach (Transform child in alwaysAbandonOnHitChildren) {
					child.SetParent(null, true);
				}
			}
		}

		//if (dieOnHit)
		Destroy(gameObject, m_flDestroyAfterHitTime);
	}
}
