﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawnerScript : MonoBehaviour {


	[SerializeField]
	float respawnTimer;

	[SerializeField]
	PickupScript itemToSpawn;

	bool hasItem = true;

	// Start is called before the first frame update
	void Start() {

	}

	// Update is called once per frame
	void ResetItem() {
		hasItem = true;
		itemToSpawn.Reset();
	}

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Player") && hasItem) {
			PlayerCharacter pc = other.GetComponent<PlayerCharacter>();
			itemToSpawn.PickedUp(pc);
			hasItem = false;
			Invoke("ResetItem", respawnTimer);
		}
	}
}
