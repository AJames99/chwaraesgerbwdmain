﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

	private List<ShieldScript> shieldsEquipped;

	[SerializeField]
	private float swapCooldown = 0.25f;
	protected bool canSwap = true;

	private InventorySlot selected = null;

	[HideInInspector]
	public float canvasScale = 1f;
	protected CanvasScaler canvasScaler;
	
	public void RefreshCanvasScale() {
		canvasScale = canvasScaler.scaleFactor;
	}

	private bool _inventoryOpen;
	public bool inventoryOpen {
		set {
			_inventoryOpen = value;
		}
		get {
			return _inventoryOpen;
		}
	}

	float snapRadius = 0.1f;

	[SerializeField]
	InventorySlot[] slots;
	[SerializeField]
	string[] slotIdentifiers;
	// 1:1 relation
	Dictionary<string, InventorySlot> slotDict;
	Dictionary<InventorySlot, string> slotDictReverse;

	const string hotbarstr = "HOTBAR"; // e.g. HOTBAR3
	const string shieldstr = "SHIELD"; // e.g. SHIELD1

	//[SerializeField]
	public Canvas inventoryCanvas;

	[SerializeField]
	private Texture2D cursorTexture;

	[SerializeField]
	private LayerMask interactableLayers;

	[SerializeField]
	private float maxInteractionDistance = 5.0f;

	[SerializeField]
	private Damageable _owner = null;
	public Damageable owner {
		get { return _owner; }
	}

	void Awake() {
		shieldsEquipped = new List<ShieldScript>(GetComponentsInChildren<ShieldScript>());

		if(slots == null)
			slots = inventoryCanvas.GetComponentsInChildren<InventorySlot>(true);
		foreach (InventorySlot slot in slots) {
			slot.container = this;
			slot.Initialise();
		}

		Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.ForceSoftware);

		slotDict = new Dictionary<string, InventorySlot>(slotIdentifiers.Length);
		slotDictReverse = new Dictionary<InventorySlot, string>(slotIdentifiers.Length);
		int i = 0;
		foreach (string identifier in slotIdentifiers) {
			slotDict.Add(identifier, slots[i]);
			slotDictReverse.Add(slots[i++], identifier);
		}

		canvasScaler = inventoryCanvas.GetComponent<CanvasScaler>();

		ForceInventoryOpen(false);
	}

	void ForceInventoryOpen(bool state) {
		inventoryOpen = state;
		inventoryCanvas.gameObject.SetActive(state); // Switched to gameObject to properly disable all items etc not just canvassed ones.
		Cursor.visible = state;
		Cursor.lockState = state ? CursorLockMode.Confined : CursorLockMode.Locked;
		if (owner) {
			if (owner.GetCharacterController() != null) {
				((Controller)owner.GetCharacterController()).m_bIsInventoryOpen = state;
			}
		}
	}
	protected void OnInventory() {
		inventoryOpen = !inventoryOpen;
		ForceInventoryOpen(inventoryOpen);
	}

	protected void OnSlot1() {
		SelectShieldSlot(1);
	}
	protected void OnSlot2() {
		SelectShieldSlot(2);
	}
	protected void OnSlot3() {
		UseHotbarItem(3);
	}
	protected void OnSlot4() {
		UseHotbarItem(4);
	}
	protected void OnSlot5() {
		UseHotbarItem(5);
	}
	protected void SelectShieldSlot(int n) {
		InventorySlot shieldSlot = slotDict[shieldstr + n.ToString()];
		if (null != shieldSlot && null != shieldSlot?.containedItem) {
			ShieldSlotChange(shieldSlot);
		}
	}
	protected void UseHotbarItem(int n) {
		InventorySlot hbarSlot = slotDict[hotbarstr + n.ToString()];
		if (null != hbarSlot && null != hbarSlot?.containedItem) {
			if(hbarSlot.containedItem.type == ItemScript.ItemType.Consumable)
				hbarSlot.containedItem.OnUse(owner);
		}
	}

	// Used to invoke the OnSlot1/2 funcs from the shield item script
	public void ShieldPlacedInSlot(InventorySlot slotInstalledIn) {
		canSwap = true;
		ShieldSlotChange(slotInstalledIn);
	}

	protected void OnInteract() {
		Debug.DrawRay(transform.position, transform.forward * maxInteractionDistance, Color.cyan, 3.0f);
		RaycastHit[] hits = Physics.RaycastAll(transform.position, transform.forward, maxInteractionDistance, interactableLayers.value, QueryTriggerInteraction.Collide);
		PhysicalItem firstItem = null;
		foreach (RaycastHit hit in hits) {
			if (hit.transform.TryGetComponent(out firstItem)) {
				ItemScript itemPickedUp = null;
				itemPickedUp = firstItem.OnPickup();
				InventorySlot designatedSlot = GetFirstAvailableSlotForItem(itemPickedUp);
				if (designatedSlot != null) {
					designatedSlot.containedItem = itemPickedUp;    // Manages parenting etc for us.
					Destroy(firstItem.gameObject);
				} else {
					firstItem.OnFailedPickup();
					Destroy(itemPickedUp.gameObject);
				}
				return;
			}
		}
	}

	// Adds an item to a slot, which if already filled will either eject or move the item.
	// Returns state of success
	public bool AddToSlot(ItemScript item, InventorySlot slot, bool alwaysEject = false) {
		if (slot.slotType != item.slotType)
			return false;

		// Slot is already filled; move what's in it first
		if (null != slot.containedItem) {
			InventorySlot slotMoved = GetFirstAvailableSlotForItem(slot.containedItem);
			if (alwaysEject || slotMoved == null) {
				slot.containedItem.Drop();
			} else {
				slotMoved.containedItem = slot.containedItem;
			}
			slot.containedItem = null;
		}
		slot.containedItem = item;
		return true;
	}

	private void ResetCooldown() {
		canSwap = true;
	}

	// This has been removed due to there only being 2 slots now + a new slots system based on strings not ints
	// Iterates over our equipped shields and sets em to active depending on which was selected
	/*protected void ShieldSlotChange(int i) {
		if (canSwap && selected != i) {
			foreach (ShieldScript ss in shieldsEquipped) {
				if (ss.slot == i) {
					ss.SetActive(true);
				} else {
					ss.ResetAll();
					ss.SetActive(false);
				}
			}
			canSwap = false;
			selected = i;
			Invoke("ResetCooldown", swapCooldown);
		}
	}*/

	public void ShieldSlotChange(InventorySlot slotActive) {
		if (slotActive == null) {
			selected = null;			
			return;
		}

		if (canSwap && selected != slotActive) {
			foreach (InventorySlot slot in slots) {
				if (slot.slotType == InventorySlot.SlotType.SHIELD) {
					if (!slot.containedItem)
						continue;
					ShieldItem item = slot.containedItem as ShieldItem;
					ShieldScript shield = item.shieldInstance;
					if (!shield)
						continue;
					if (slotActive == slot) {
						shield.active = true;
					} else {
						shield.ResetAll();
						shield.active = false;
					}
				}
			}
			canSwap = false;
			selected = slotActive;
			Invoke("ResetCooldown", swapCooldown);
		}
	}

	public ShieldScript GetActiveShield() {
		/*foreach (ShieldScript ss in shieldsEquipped) {
			if (ss.slot == selected) {
				return ss;
			}
		}
		return null;*/
		if (selected && selected?.containedItem) {
			ShieldItem item = selected.containedItem as ShieldItem;
			ShieldScript shield = item.shieldInstance;
			return shield;
		}
		return null;
	}

	public InventorySlot GetFirstAvailableSlotForItem(ItemScript item) {
		foreach (InventorySlot slot in slots) {
			if (slot.slotType == item.slotType && !slot.containedItem) {
				return slot;
			}
		}
		return null;
	}
}