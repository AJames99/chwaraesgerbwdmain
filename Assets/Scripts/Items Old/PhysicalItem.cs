using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Represents an item in the context of the world.
// When equipped, instantiates the ItemScript object associated for inventory use
public class PhysicalItem : MonoBehaviour {
	public string itemName;
	public ItemScript itemScript;

	// For bouncing an unpickable item up in the air
	Rigidbody attachedRB = null;
	static Vector3 failedPickupBounceForces = new Vector3(1.0f, 5.0f, 1.0f);
	private void Start() {
		attachedRB = GetComponent<Rigidbody>();
	}
	public ItemScript OnPickup() {
		Debug.Log("Item picked up: " + itemName);
		return Instantiate(itemScript); // Passed out to inventory script for handling
	}
	public void OnFailedPickup() {
		Debug.Log("Item filed picked up!: " + itemName);
		if (attachedRB) {
			Vector3 bounceForce = Random.insideUnitSphere.WithY(Random.value); // X,Z are +ve or -ve, Y is only +ve
			bounceForce.Scale(failedPickupBounceForces);
			attachedRB.AddForce(bounceForce, ForceMode.Impulse); 
		}
	}
}
