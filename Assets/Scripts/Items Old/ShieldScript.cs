﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class ShieldScript : MonoBehaviour {

    // Freeze frame duration applied
    static float HITSTUNFRAMES = 0.3f;

    // Audio
    protected AudioSource _audioSource;
    [SerializeField]
    protected bool _debugInfo;

    public TPSController playerController;

    public PlayerCharacter playerCharacter;

    [SerializeField]
    protected LayerMask hittableLayers; // For bashign

    protected Animator animator;

    // Blocking params
    [HideInInspector]
    public bool inputtingBlock = false;
    [HideInInspector]
    public bool isBlocking = false;
    [HideInInspector]
    public bool isBlockingTimed = false;
    [HideInInspector]
    public bool isBlockingWithSpin = false;
    [HideInInspector]
    public bool isBashing = false;
    [HideInInspector]
    public bool canChangeBlockStates = true;

    protected float resetBlockTime = 0.2f; //Minimum time after timed blocking/parrying ends before we can change block states

    // Time window for deflections
    protected float blockParryDuration = 0.5f; // Time after which timed blocking/parrying ends

    // Used to prevent spamming the block button - minimum time after stopping blocking before we can change inputs again
    protected bool canMakeBlockInput = true;
    //private const float blockInputReset = 0.45f;
    protected const float blockInputReset = 1.5f;

    [SerializeField]
    protected float attackDuration = 0.3f;

    [SerializeField]
    protected Collider shieldCollider;

    [SerializeField]
    protected GameObject visuals;

    [SerializeField]
    protected bool canParry = true;
    [SerializeField]
    protected float parryDamageMultiplier = 2f;
    [SerializeField]
    protected float parryVelocityMultiplier = 2f;
    [SerializeField]
    private float spinDamageMultiplier = 1.25f;
    [SerializeField]
    protected float bashDelay = 0.2f;

    [SerializeField]
    // Whether or not the bash will add the owners velocity to the bashed target
    protected bool bashInheritsVelocity = true;
    [SerializeField]
    // Whether or not the bash will "deplete"/transfer the owner's velocity to the target
    protected bool bashExpendsVelocity = true;
    [SerializeField]
    // (If enabled) How much of the owner's velocity should be transferred to the target
    protected float bashVelocityTransfer = 0.7f;
    [SerializeField]
    // Whether or not bashing actually applies an opposite force or simply dissipates the owner's velocity.
    protected bool respectNewtonsSecondLaw = true;
    [SerializeField]
    // Whether or not this shield can bash static/world objects.
    protected bool canBashStatics = true;
    [SerializeField]
    protected float staticBashPushForce = 20.0f;
    protected float staticBashRayDistance = 5.0f;
    [SerializeField]
    protected Vector2 bashBaseVelocity = new Vector2(35.0f, 15.0f); // Forward, Up

    // Used to aim things
    protected Transform cameraTransf;

    //[SerializeField]
    public int slot = 1;

    private bool _active = false;
    public bool active {
        set {
            //Debug.Log("active property was called, value: " + value);
            _active = value;
            SetActiveShield();
        }
        get {
            return _active;
        }
    }

    public ItemScript associatedItem;

    [Header("Audio & Effects")]

    //[HideInInspector]
    [SerializeField]
    AudioClip audioClip_Swing;
    [SerializeField]
    AudioClip audioClip_Raise, audioClip_Lower;
    [SerializeField]
    GameObject impactFX_shieldHit, impactFX_shieldSuccess, impactFX_shieldBashSuccess;
    [SerializeField]
    Transform effectPoint;
    [SerializeField]
    float minimumTimeBetweenEffects = 0.5f;
    private float minimumTimeEffectTimer = 0f;
    [SerializeField]
    float minimumTimeBetweenSounds = 0.7f;
    private float minimumTimeSoundTimer = 0f;
    [HideInInspector]
    public AudioSource shieldAudioSource;
    [HideInInspector]
    public ClipPlayer clipPlayer;
    [SerializeField]
    protected AudioClip shieldHitClip;
    [SerializeField]
    protected AudioClip[] successClips;

    private enum effecttype {
        shieldHit, shieldSuccess, shieldBashSuccess
    }

    protected virtual void SetActiveShield() {
        //		_active = value;
        shieldCollider.enabled = _active;
        visuals.SetActive(_active);
    }

    // Used for switching weapons
    public virtual void ResetAll() {
        CancelInvoke();
        inputtingBlock = false;
        isBlocking = false;
        isBlockingTimed = false;
        isBlockingWithSpin = false;
        isBashing = false;
        canChangeBlockStates = true;
        canMakeBlockInput = true;

    }

    // Depending on the particular shield and state we're in, impacts should deliver different amounts of force.
    public virtual float GetCurrentForceMultiplier() {
        // Ordered by importance/priority descending
        switch (GetBlockLevel()) {
            case BlockLevel.BASH:
                Debug.Log("Force Multiplier: Bash");
                return 3.0f;
            case BlockLevel.TIMEDBLOCK:
                Debug.Log("Force Multiplier: Timed Block");
                return 2.0f;
            case BlockLevel.BLOCK:
                Debug.Log("Force Multiplier: Block");
                return 1.5f;
            case BlockLevel.NONE:
                Debug.Log("Force Multiplier: None");
                return 1.0f;
            default:
                Debug.Log("Force Multiplier: DEFAULT");
                return 1.0f;
        }
    }

    public enum BlockLevel {
        NONE,
        BLOCK,
        TIMEDBLOCK,
        BASH
    }

    public BlockLevel GetBlockLevel() {
        if (isBashing)
            return BlockLevel.BASH;
        else if (isBlockingTimed)
            return BlockLevel.TIMEDBLOCK;
        else if (isBlocking)
            return BlockLevel.BLOCK;
        else
            return BlockLevel.NONE;
    }

    protected virtual void OnBlock(InputValue value) {
        if (_active && !associatedItem.isInventoryOpen) {
            inputtingBlock = value.Get<float>() > 0.5f;
            CheckBlockInput();
        }
    }

    protected virtual void OnAttack(InputValue value) {
        if (_active && !associatedItem.isInventoryOpen) {
            // New behaviour removed spin, bash has the old spin behaviour
            if (!isBlocking && canChangeBlockStates && !isBashing) {
                animator.SetTrigger("Bash");
                playerController.SetCameraAnimatorTrigger("Bash");
                isBashing = true;
                canChangeBlockStates = false;
                canMakeBlockInput = false;
                Invoke("Bash", bashDelay);
                _audioSource.PlayOneShot(audioClip_Swing, 0.3f);
            }
        }
    }

    // Left click bash behaviour
    protected void Bash() {
        CancelInvoke();
        isBlocking = true;
        isBlockingTimed = true;
        //isBlockingWithSpin = true;
        Invoke("StopBlockParryWindow", attackDuration);

        shieldCollider.enabled = true;
        Invoke("StopBash", attackDuration/* + bashDelay*/);

        Vector3 bonusVelocity = Vector3.zero;
        Vector3 ownerVelocity = playerController.GetVelocity();
        if (bashInheritsVelocity) {
            bonusVelocity = ownerVelocity * Mathf.Clamp01(bashVelocityTransfer);
            if (bashExpendsVelocity) {
                if (respectNewtonsSecondLaw) {
                    ownerVelocity -= bonusVelocity;
                } else {
                    ownerVelocity *= 1 - Mathf.Clamp01(bashVelocityTransfer);
                }
            }
        }

        Vector3 basePushVel = (cameraTransf.forward * bashBaseVelocity.x) + (Vector3.up * bashBaseVelocity.y);

        //Debug.Log("Push: " + basePushVel + " + " + bonusVelocity + " = " + (bonusVelocity + basePushVel));

        bool bHitSomething = false;
        bool bHitDamageable = false;
        Collider firstWorldObjHit = null;
        bool bHitWorld = false;
        foreach (Collider col in Physics.OverlapSphere(shieldCollider.transform.position, 4.0f, hittableLayers, QueryTriggerInteraction.Ignore)) {
            //Debug.Log("Boink");

            Damageable dmg;
            if (col.TryGetComponent(out dmg)) {
                if (dmg.team != playerCharacter.team) {
                    dmg.Hurt(new Damage(Damage.DamageTypes.DEFAULT, 5));
                    dmg.Push(basePushVel + bonusVelocity);
                    bHitSomething = true;
                    bHitDamageable = true;
                }
            }

            if (col.attachedRigidbody) {
                if (col.attachedRigidbody.isKinematic) {
                    if (!bHitWorld) {
                        bHitWorld = true;
                        firstWorldObjHit = col;
                    }
                } else {
                    col.attachedRigidbody.AddForce(basePushVel + bonusVelocity, ForceMode.VelocityChange);
                }
                bHitSomething = true;
            } else if (col.gameObject.isStatic && !bHitWorld) {
                bHitWorld = true;
                firstWorldObjHit = col;
            }
        }

        //if ((bHitWorld || bHitSomething) && null != firstWorldObjHit) {
        //    ownerVelocity += (cameraTransf.position - firstWorldObjHit.ClosestPoint(cameraTransf.position + cameraTransf.forward)).normalized * staticBashPushForce;
        //}
        RaycastHit info;
        Physics.Raycast(cameraTransf.position, cameraTransf.forward, out info, staticBashRayDistance, hittableLayers, QueryTriggerInteraction.Ignore);
        if (info.collider) {
            ownerVelocity += info.normal * staticBashPushForce;
        }

        if (bHitDamageable) {
            if (FreezeframeManager.m_pManagerStatic != null) {
                FreezeframeManager.m_pManagerStatic.Freeze(0.1f);
            }
        }

        if (bHitWorld || bHitSomething)
            playerController.SetVelocity(ownerVelocity);
    }

    protected void StopBash() {
        canMakeBlockInput = true;
        canChangeBlockStates = true;
        isBashing = false;
        isBlocking = false;
        isBlockingTimed = false;
        shieldCollider.enabled = false;
    }

    protected void CheckBlockInput() {
        if (canChangeBlockStates) {
            if (inputtingBlock) {
                if (canMakeBlockInput) {
                    StartBlock();
                }
            } else if (isBlocking) {
                StopBlock();
            }
        }
    }

    protected virtual void OnGUI() {
        if (!_debugInfo)
            return;

        if (_active) {
            // Print booleans
            GUI.Box(new Rect(50, 50, 250, 40),
            ("can make block input: " + canMakeBlockInput.ToString()));
            GUI.Box(new Rect(50, 100, 250, 40),
                ("inputting block: " + inputtingBlock.ToString()));
            GUI.Box(new Rect(50, 150, 250, 40),
                ("is blocking: " + isBlocking.ToString()));
            GUI.Box(new Rect(50, 200, 250, 40),
                ("is doing timed blocked: " + isBlockingTimed.ToString()));
        }
    }


    // Ceases the parrying state, and queues up the reset block
    protected void StopBlockParryWindow() {
        isBlockingTimed = false;
        isBlockingWithSpin = false;

        // Begin the countdown to being able to put the shield away again (if block is no longer held etc etc)
        Invoke("ResetBlockStateChange", resetBlockTime);
    }
    protected virtual void StartBlock() {

        CancelInvoke("StopBlockParryWindow");
        CancelInvoke("StopBlock");
        CancelInvoke("RestBlock");

        shieldCollider.enabled = true;

        isBlocking = true;
        if (canParry) {
            isBlockingTimed = true;
            isBlockingWithSpin = true;
        }

        canChangeBlockStates = false;
        Invoke("StopBlockParryWindow", blockParryDuration);
        _audioSource.PlayOneShot(audioClip_Raise, 0.2f);

        animator.SetBool("isBlocking", true);

        canMakeBlockInput = false;
        CancelInvoke("ResetInputAbility");
    }
    protected virtual void StopBlock() {
        isBlocking = false;
        isBlockingWithSpin = false;
        isBlockingTimed = false;
        CancelInvoke("StopBlockParryWindow");

        _audioSource.PlayOneShot(audioClip_Lower, 0.2f);

        shieldCollider.enabled = false;

        animator.SetBool("isBlocking", false);
        CancelInvoke("ResetInputAbility");
        Invoke("ResetInputAbility", blockInputReset);
    }

    protected void ResetInputAbility() {
        canMakeBlockInput = true;

        // Check to see if we should update the block state now that our inputs are allowed again
        CheckBlockInput();
    }

    /// <summary>
    /// Allows the blocking (isBlocking flag) state to change
    /// </summary>
    protected void ResetBlockStateChange() {
        canChangeBlockStates = true;
        if (!inputtingBlock/* && canMakeInput*/) {
            StopBlock();
        }
    }

    // Start is called before the first frame update
    protected virtual void Start() {
        _audioSource = gameObject.AddComponent<AudioSource>();
        animator = GetComponentInChildren<Animator>();

        //shieldCollider = GetComponent<BoxCollider>();
        cameraTransf = transform.parent;
        //canMakeBlockInput = true;
        //		active = false;
    }

    // Update is called once per frame
    /*void Update() {

	}*/

    // Blocklevel: 0 for basic, 1 for minicrit, 2 for crit
    public virtual void OnShieldHit(float projectileSpeed = 0, int projectileDamage = 0, int blockLevel = 0, Projectile projectile = null) {
        //...
        if (blockLevel <= 0 && projectile) {
            projectile.OnHit(null,null);
            //Debug.Log("blockLevel was == 0");
        }

        switch (blockLevel) {
            case 0:
                PerformEffect(effecttype.shieldHit);
                break;
            case 1:
                PerformEffect(effecttype.shieldSuccess);
                break;
            case 2:
                PerformEffect(effecttype.shieldBashSuccess);
                break;
        }

        playerController.SetCameraAnimatorFloat("AdditiveImpactBlend", 1.0f);
        playerController.SetCameraAnimatorTrigger("Impact");

        // Only set the trigger if we're not whacking
        if (!isBashing)
            animator.SetTrigger("Hit");
    }

    private void OnTriggerEnter(Collider other) {
        OnCollisionEnter(other);
    }

    private void OnCollisionEnter(Collision collision) {
        OnCollisionEnter(collision.collider);
    }
    protected virtual void OnCollisionEnter(Collider collider) {
        Projectile projectileScript = collider.gameObject.GetComponent<Projectile>();
        if (projectileScript) {
            if (projectileScript.m_bActive && projectileScript.m_nTeam != PlayerCharacter.PLAYERTEAM) {
                if (isBlockingTimed) {
                    float velocityMultiplier;
                    float damageMultiplier;

                    if (isBlockingWithSpin) {
                        //normal damage for a spin block
                        //normal velocity "
                        TryPlaySoundOneShot(_audioSource, successClips[1], 0.6f);
                        playerController.AddForce((playerController.transform.position - collider.transform.position).normalized * (projectileScript.m_pDamageInfo.amount));

                        OnShieldHit(projectileScript.m_flInitSpeed, projectileScript.m_pDamageInfo.amount, 1, projectileScript);
                        damageMultiplier = spinDamageMultiplier;
                        velocityMultiplier = 1f;

                    } else {
                        TryPlaySoundOneShot(_audioSource, successClips[0], 0.15f);

                        OnShieldHit(projectileScript.m_flInitSpeed, projectileScript.m_pDamageInfo.amount, 2, projectileScript);

                        playerController.AddForce((playerController.transform.position - collider.transform.position).normalized * (projectileScript.m_pDamageInfo.amount / 3f));

                        damageMultiplier = parryDamageMultiplier;
                        velocityMultiplier = parryVelocityMultiplier;
                    }
                    // Reset the block timer, thus extending the shielding window
                    CancelInvoke("StopBlockParryWindow");
                    Invoke("StopBlockParryWindow", blockParryDuration);
                    isBlockingTimed = true;

                    // If parrying, immediately cancel anti spam timer
                    // TODO


                    // Deflect!
                    //float projectileVelocity = collision.relativeVelocity.magnitude * returnVelocityMultiplier;
                    float projectileVelocity = projectileScript.m_flInitSpeed * velocityMultiplier;
                    Vector3 Force = cameraTransf.forward * projectileVelocity;
                    /*Vector3 TargetCenter = projectileScript.shooter.position;
					float distance = (TargetCenter - projectileScript.transform.position).magnitude;
					float trajectoryAngle;

					if (ProjectileScript.CalculateTrajectory(distance, projectileVelocity, out trajectoryAngle)) {
						float trajectoryHeight = Mathf.Tan(trajectoryAngle * Mathf.Deg2Rad) * distance;
						TargetCenter.y += trajectoryHeight;
					}*/

                    projectileScript.Deflect(PlayerCharacter.PLAYERTEAM, Force, damageMultiplier);

                    ExtensionMethods.BroadcastAll("PlayerEvent", "NPC", "DEFLECT");

                } else if (isBlocking) {
                    OnShieldHit(projectileScript.m_flInitSpeed, projectileScript.m_pDamageInfo.amount, 0, projectileScript);
                    playerController.AddForce((playerController.transform.position - collider.transform.position).normalized * (projectileScript.m_pDamageInfo.amount / 1.5f));
                }

                TryPlaySoundOneShot(_audioSource, shieldHitClip, 0.05f);
            }
        }
    }

    void PerformEffect(effecttype type) {
        if (Time.time < minimumTimeEffectTimer)
            return;

        Vector3 point = effectPoint.position;
        Vector3 forward = effectPoint.forward; Vector3 up = effectPoint.up;

        switch (type) {
            case effecttype.shieldBashSuccess:
                break;
            case effecttype.shieldSuccess:
                GameObject parryEffect = Instantiate(impactFX_shieldSuccess, point + (forward) + (up), effectPoint.rotation, effectPoint);
                //parryEffect.transform.localScale *= 1.5f;
                parryEffect.transform.localScale /= parryEffect.transform.lossyScale.magnitude;
                break;
            case effecttype.shieldHit:
                //GameObject effect = Instantiate(impactFX_shieldHit, point + (forward), effectPoint.rotation);
                GameObject effect = Instantiate(impactFX_shieldHit, cameraTransf.position + (cameraTransf.forward), cameraTransf.rotation);
                break;
        }

        minimumTimeEffectTimer = Time.time + minimumTimeBetweenEffects;
    }

    void TryPlaySoundOneShot(AudioSource source, AudioClip clip, float volume) {
        if (Time.time > minimumTimeSoundTimer) {
            minimumTimeSoundTimer = Time.time + minimumTimeBetweenSounds;
            source.PlayOneShot(clip, volume);
        }
    }
}
