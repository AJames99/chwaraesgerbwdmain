﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GravityShieldScript : ShieldScript {
	[SerializeField]
	AudioClip puntClip;
	[SerializeField]
	AudioClip rechargeClip;

	[SerializeField]
	LayerMask puntIncludeLayers;

	[SerializeField]
	float maxPuntStrength = 30.0f;
	float currentPuntStrength;
	[SerializeField]
	float puntRechargeRatePerSecond = 10.0f;

	[SerializeField]
	float maxPuntDistance = 8f;

	[SerializeField]
	bool flipToResetPunt = true;

	[SerializeField]
	float puntCooldown = 0.5f;
	private bool canPunt = true;

	bool isProjectileHeld = false;
	Projectile heldProjectile = null;
	Rigidbody heldRigidBody = null;
	float holdTimeMax = 10f; // Max lifespan for a grabbed object

	[SerializeField]
	private float projectilePuntForceMultiplier = 3f;
	[SerializeField]
	private float gravityHoldDistance = 3.5f;

	public override void ResetAll() {
		base.ResetAll();
		canPunt = true;
	}

	protected override void Start() {
		base.Start();

		currentPuntStrength = maxPuntStrength;
	}

	private void Update() {
		if (currentPuntStrength < maxPuntStrength) {
			currentPuntStrength = Mathf.Min(maxPuntStrength, currentPuntStrength + (puntRechargeRatePerSecond * Time.deltaTime));
			if (Mathf.FloorToInt(Mathf.Abs(playerController.degreesThisJump) / 270f) > 0) {
				currentPuntStrength = maxPuntStrength;
				playerController.degreesThisJump = 0f;
			}

			if (currentPuntStrength == maxPuntStrength) {
				_audioSource.PlayOneShot(rechargeClip, 0.5f);
			}
		}
	}

	private void FixedUpdate() {
		if (isProjectileHeld) {
			if (heldProjectile != null) {
				//heldRigidBody.position = (transform.position + (transform.forward * gravityHoldDistance));
				heldRigidBody.MovePosition(transform.position + (playerController.GetVelocity() * Time.fixedDeltaTime) + (transform.forward * gravityHoldDistance));
				heldRigidBody.MoveRotation(Quaternion.FromToRotation(Vector3.forward, transform.forward));
			} else {
				isProjectileHeld = false;
				heldRigidBody = null;
				heldProjectile = null;
			}
		}
	}

	protected override void OnGUI() {
		if (!_debugInfo)
			return;

		if (active) {
			base.OnGUI();

			GUI.Box(new Rect(50, 250, 250, 40),
				("Punt Strength: " + Mathf.FloorToInt(currentPuntStrength) + ", degs: " + playerController.degreesThisJump));
		}
	}

	public override void OnShieldHit(float projectileSpeed = 0, int projectileDamage = 0, int blockLevel = 0, Projectile projectile = null) {
		if (!projectile)
			return;

		//base.OnShieldHit(projectileSpeed, projectileDamage, blockLevel);
		if (!isProjectileHeld) {
			heldProjectile = projectile;
			heldRigidBody = projectile.GetComponent<Rigidbody>();
			isProjectileHeld = true;
			projectile.CancelInvoke();
			projectile.m_bActive = false;
		} else {
			// Boom!
			base.OnShieldHit(projectileSpeed, projectileDamage, blockLevel, projectile);
		}
	}

	protected override void OnAttack(InputValue value) {
		if (active) {
			if (isBlocking && canPunt) {
				CancelInvoke();

				animator.SetTrigger("Hit");
				_audioSource.PlayOneShot(puntClip, 0.1f);

				canChangeBlockStates = false;
				canPunt = false;
				Invoke("ResetBlockStateChange", puntCooldown);
				Invoke("ResetPunt", puntCooldown);


				if (isProjectileHeld) {
					YeetProjectile();
				} else {
					RaycastHit hit;
					if (Physics.Raycast(cameraTransf.position, cameraTransf.forward, out hit, 5.0f, puntIncludeLayers, QueryTriggerInteraction.Ignore)) {
						//playerController.AddForce(-cameraTransf.forward * currentPuntStrength);
						playerController.AddForce((hit.normal - cameraTransf.forward) * 0.5f * currentPuntStrength);

						Damageable dmg;
						if (hit.collider.gameObject.TryGetComponent(out dmg)) {
							// hit.point is used optionally, to do an AddForceAtPoint if desired in the Damageable subclass
							dmg.Push(cameraTransf.forward * currentPuntStrength * 10f, hit.point);
						}
					}
				}

				currentPuntStrength = 0.0f;
			}
		}
	}

	protected void YeetProjectile() {
		isProjectileHeld = false;
		if (heldProjectile.gameObject) {
			heldProjectile.Deflect(playerCharacter.team, transform.forward * currentPuntStrength * projectilePuntForceMultiplier, 2.0f);
		}
		heldProjectile = null;
		heldRigidBody = null;
	}

	protected override void StopBlock() {
		base.StopBlock();
		if (isProjectileHeld) {
			YeetProjectile();
		}
	}

	protected void ResetPunt() {
		canPunt = true;
	}

	public void OnFlip() {
		if (flipToResetPunt) {
			currentPuntStrength = maxPuntStrength;
		}
	}
}