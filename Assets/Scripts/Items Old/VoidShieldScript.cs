﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class VoidShieldScript : ShieldScript {

	Queue<Projectile> storedProjectiles = new Queue<Projectile>(30);

	[Header("Void Shield Settings")]
	[SerializeField]
	float portalMaxTimeOpen = 1.5f;
	[SerializeField]
	float portalMaxTimeOpenPerProjectile = 0.2f;
	[SerializeField]
	float portalReturnFireInterval = 0.15f;
	[SerializeField]
	Transform shotPoint;
	[SerializeField]
	AudioClip portalEnterSound;
	[SerializeField]
	AudioClip portalExitSound;
	[SerializeField]
	Animator[] portalAnimators;

	bool isFiring = false;
	bool hasProjectilesStored = false; // Used to determine what blocking should do exactly

	float portalOpenTimer = 0.0f;

	protected override void StartBlock() {
		base.StartBlock();

		if (hasProjectilesStored) {
			canChangeBlockStates = false;
			CancelInvoke("ResetBlockStateChange");
			//Invoke("StopBlock", (portalReturnFireInterval * storedProjectiles.Count) + 0.5f);
			//Invoke("ResetBlockStateChange", (portalReturnFireInterval * storedProjectiles.Count) + 0.5f);
			Invoke("FireLoop", 0.1f);
			isFiring = true;
		} else {
			//Invoke("StopBlock", portalMaxTimeOpen);
			portalOpenTimer = portalMaxTimeOpen;
		}

		portalAnimators[0].SetTrigger("Vwoosh");
		portalAnimators[1].SetTrigger("Vwoosh");
	}

	protected override void OnAttack(InputValue value) {
		//base.OnAttack(value);
	}

	private void Update() {
		if (portalOpenTimer > 0f) {
			portalOpenTimer = Mathf.Max(0f, portalOpenTimer - Time.deltaTime);
			if (portalOpenTimer <= 0) {
				StopBlock();
			}
		}
	}

	void FireLoop() {
		if (storedProjectiles.Count > 0) {
			Projectile projToFire = storedProjectiles.Dequeue();
			//Rigidbody projBody = projToFire.GetComponent<Rigidbody>();

			if (!projToFire) {
				Debug.LogError("Trying to open a portal for a projectile that doesn't exist anymore!");
			} else {

				projToFire.gameObject.SetActive(true);
				projToFire.m_bActive = false;
				projToFire.transform.position = shotPoint.transform.position;
				projToFire.Deflect(playerCharacter.team, shotPoint.forward * projToFire.m_flInitSpeed, 1f);

				_audioSource.PlayOneShot(portalExitSound, 0.2f);
			}
			Invoke("FireLoop", portalReturnFireInterval);
		} else {
			Invoke("StopBlock", 0.3f);
			Invoke("ResetBlockStateChange", 0.25f);
			hasProjectilesStored = false;
			isFiring = false;
		}
	}

	public override void OnShieldHit(float projectileSpeed = 0, int projectileDamage = 0, int blockLevel = 0, Projectile projectile = null) {
		//base.OnShieldHit(projectileSpeed, projectileDamage, blockLevel, projectile); // if blocklevel is 0, blow up the projectile or whatever

		if (!projectile)
			return;

		// This is da real meat n POTATERS babyyy
		if (!isFiring) {
			projectile.m_bActive = false;
			projectile.CancelInvoke();
			storedProjectiles.Enqueue(projectile);
			projectile.gameObject.SetActive(false);
			_audioSource.PlayOneShot(portalEnterSound, 0.2f);
			hasProjectilesStored = true;
			portalOpenTimer += portalMaxTimeOpenPerProjectile;
		} else {
			projectile.OnHit(null,null);
		}
	}
}
