﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PickupScript : MonoBehaviour {

	[SerializeField]
	protected GameObject model;

	public abstract void PickedUp(PlayerCharacter pc);

	public abstract void Reset();
}
