﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailgunChargeItem : PickupScript {
	public override void PickedUp(PlayerCharacter pc) {
		if (pc.resources[PlayerCharacter.ResourceType.RAILGUNSHOTS] < pc.resourceMaxima[PlayerCharacter.ResourceType.RAILGUNSHOTS]) {
			pc.PickupResource(PlayerCharacter.ResourceType.RAILGUNSHOTS, 1);
			model.SetActive(false);
		}
	}

	public override void Reset() {
		model.SetActive(true);
	}
}
