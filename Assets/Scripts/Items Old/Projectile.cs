﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public static Projectile InstanceProjectile(Projectile pPrefab, Vector3 vSpawnPoint, Vector3 vSpawnDirection, float flVelocity, Damageable pOwner, Damage pDamageInfo, bool bIsHoming = false, Transform pHomingTransform = null) {
		Projectile pProjectileInstance = GameObject.Instantiate(pPrefab, vSpawnPoint, Quaternion.LookRotation(vSpawnDirection, Vector3.up));

		pProjectileInstance.m_nTeam = pOwner.team;
		pProjectileInstance.m_pDamageInfo = pDamageInfo;
		pProjectileInstance.m_pShooterTransform = pOwner.transform;
		pProjectileInstance.m_bIsHoming = bIsHoming;
		pProjectileInstance.m_pHomingTarget = pHomingTransform;
		pProjectileInstance.m_pOwner = pOwner;

		Rigidbody projBody = pProjectileInstance.GetComponent<Rigidbody>();
		projBody.velocity = ((Vector3)vSpawnDirection).normalized * flVelocity;
		pProjectileInstance.m_flInitSpeed = flVelocity;
		pProjectileInstance.transform.LookAt(projBody.position + projBody.velocity.normalized);

		return pProjectileInstance;
	}

	public static Projectile InstanceProjectile(string szPath, Vector3 vSpawnPoint, Vector3 vSpawnDirection, float flVelocity, Damageable pOwner, Damage pDamageInfo, bool bIsHoming = false, Transform pHomingTransform = null) {
		return InstanceProjectile(Resources.Load(szPath, typeof(Projectile)) as Projectile, vSpawnPoint, vSpawnDirection, flVelocity, pOwner, pDamageInfo, bIsHoming, pHomingTransform);
	}

	public Damageable m_pOwner = null;

	[SerializeField]
	protected float m_flLifeTime = 5.0f;

	[SerializeField]
	protected float m_flLifeTimeAfterHit = 0.5f;

	private bool _isHoming = false;
	public bool m_bIsHoming {
		get {
			return _isHoming;
		}
		set {
			_isHoming = value;
			// Trying out a Source engine style float timer system, one float comp per frame to check
			m_flHomingStartTimer = Time.time + m_flHomingDelay;
		}
	}

	[HideInInspector]
	private Transform _homingTarget = null;
	public Transform m_pHomingTarget {
		get {
			return _homingTarget;
		}
		set {
			_homingTarget = value;

		}
	}
	[SerializeField]
	protected float m_flHomingDelay = 0.0f;
	private float m_flHomingStartTimer = 0f;
	[SerializeField]
	protected float m_flHomingAnglePerSecond = 5f;

	public bool m_bActive;

	public Transform m_pShooterTransform = null;

	public int m_nTeam = Damageable.ENEMYTEAM;

	[SerializeField]
	protected LayerMask m_lmLayerMask;

	public float m_flInitSpeed = 0.0f;

	[HideInInspector]
	public Rigidbody m_pRigidBody;

	//[SerializeField]
	public Damage m_pDamageInfo;

	[SerializeField]
	GameObject projectileGhostPrefab;
	private ProjectileGhost projectileGhost;

	public GameObject explosionPrefab;

	protected virtual void Awake() {
		if (!projectileGhost)
			projectileGhost = Instantiate(projectileGhostPrefab, transform).GetComponent<ProjectileGhost>();

		m_bActive = true;
		m_pRigidBody = GetComponent<Rigidbody>();
		m_flHomingStartTimer = Time.time + m_flHomingDelay;
		Invoke("Delete", m_flLifeTime);
	}


	protected virtual void Delete() {
		if (projectileGhost)
			projectileGhost.OnHit();
		if (explosionPrefab) {
			Explode();
		}
		Destroy(gameObject);
	}

	private void FixedUpdate() {
		// I feel like nesting these ifs in order of how expensive the comparison is would be efficient?
		// i.e. bool is quicker than obj == null is quicker than float?
		if (m_bIsHoming) {
			if (m_pHomingTarget) {
				if (Time.time >= m_flHomingStartTimer) {
					AdjustHeading(m_pHomingTarget);
				}
			}
		}
	}

	void AdjustHeading(Transform target) {
		if (float.IsNaN(m_pRigidBody.velocity.x)) {
			return;
		}

		// Gently nudge the projectile in the target direction
		Vector3 toTarget = (target.position - transform.position).normalized;
		Vector3 heading = m_pRigidBody.velocity.normalized;
		//rb.velocity = Vector3.Slerp(rb.velocity.normalized, toTarget, homingStrength * Time.fixedDeltaTime) * rb.velocity.magnitude;
		float angleToTarget = Mathf.Acos(Vector3.Dot(toTarget, heading));
		Quaternion adjustRot = Quaternion.FromToRotation(heading, toTarget);

		if (angleToTarget <= m_flHomingAnglePerSecond * Time.fixedDeltaTime || adjustRot.Equals(Quaternion.identity)) {
			m_pRigidBody.velocity = m_pRigidBody.velocity.magnitude * toTarget;
		} else {
			m_pRigidBody.velocity = Quaternion.Slerp(Quaternion.identity, adjustRot, m_flHomingAnglePerSecond * Time.fixedDeltaTime / angleToTarget) * m_pRigidBody.velocity;
		}

		transform.LookAt(transform.position + m_pRigidBody.velocity);
	}

	void Explode() {
		GameObject explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
		ExplosionScript es = explosion.GetComponent<ExplosionScript>();
		es.team = m_nTeam;
		es.Explode();
	}

	public virtual void Deflect(int newteam, Vector3 newVel, float damageMultiplier) {
		m_nTeam = newteam;
		m_pRigidBody.velocity = Vector3.zero;
		m_pRigidBody.AddForce(newVel, ForceMode.VelocityChange);

		m_pDamageInfo.amount = Mathf.CeilToInt(damageMultiplier * m_pDamageInfo.amount);

		transform.LookAt(transform.position + newVel);

		// Removing homing capabilities
		m_bIsHoming = false;

		CancelInvoke("Delete");
		Invoke("Delete", m_flLifeTime);
		m_bActive = true;
	}

	public static bool CalculateTrajectory(float TargetDistance, float ProjectileVelocity, out float CalculatedAngle) {
		CalculatedAngle = 0.5f * (Mathf.Asin((-Physics.gravity.y * TargetDistance) / (ProjectileVelocity * ProjectileVelocity)) * Mathf.Rad2Deg);
		if (float.IsNaN(CalculatedAngle)) {
			CalculatedAngle = 0;
			return false;
		}
		return true;
	}

	//first-order intercept using absolute target position
	public static Vector3 FirstOrderIntercept(Vector3 shooterPosition, Vector3 shooterVelocity, float shotSpeed, Vector3 targetPosition, Vector3 targetVelocity) {
		Vector3 targetRelativePosition = targetPosition - shooterPosition;
		Vector3 targetRelativeVelocity = targetVelocity - shooterVelocity;
		float t = FirstOrderInterceptTime(shotSpeed, targetRelativePosition, targetRelativeVelocity);
		return targetPosition + t * (targetRelativeVelocity);
	}
	//first-order intercept using absolute target position
	public static Vector3 FirstOrderInterceptWithAccel(Vector3 shooterPosition, Vector3 shooterVelocity, float shotSpeed, Vector3 targetPosition, Vector3 targetVelocity, Vector3 targetAcceleration) {
		Vector3 targetRelativePosition = targetPosition - shooterPosition;
		Vector3 targetRelativeVelocity = targetVelocity - (shooterVelocity);
		float t = FirstOrderInterceptTime(shotSpeed, targetRelativePosition, targetRelativeVelocity);
		return targetPosition + (t * (targetRelativeVelocity)) + (0.5f * targetAcceleration * t * t);
	}
	//first-order intercept using relative target position
	public static float FirstOrderInterceptTime(float shotSpeed, Vector3 targetRelativePosition, Vector3 targetRelativeVelocity, Vector3? targetAcceleration = null) {
		float velocitySquared = targetRelativeVelocity.sqrMagnitude;
		if (velocitySquared < 0.001f)
			return 0f;

		float a = velocitySquared - shotSpeed * shotSpeed;

		//handle similar velocities
		if (Mathf.Abs(a) < 0.001f) {
			float t = -targetRelativePosition.sqrMagnitude /
			(
				2f * Vector3.Dot
				(
					targetRelativeVelocity,
					targetRelativePosition
				)
			);
			return Mathf.Max(t, 0f); //don't shoot back in time
		}

		float b = 2f * Vector3.Dot(targetRelativeVelocity, targetRelativePosition);
		float c = targetRelativePosition.sqrMagnitude;
		float determinant = b * b - 4f * a * c;

		if (determinant > 0f) { //determinant > 0; two intercept paths (most common)
			float t1 = (-b + Mathf.Sqrt(determinant)) / (2f * a),
					t2 = (-b - Mathf.Sqrt(determinant)) / (2f * a);
			if (t1 > 0f) {
				if (t2 > 0f)
					return Mathf.Min(t1, t2); //both are positive
				else
					return t1; //only t1 is positive
			} else
				return Mathf.Max(t2, 0f); //don't shoot back in time
		} else if (determinant < 0f) //determinant < 0; no intercept path
			return 0f;
		else //determinant = 0; one intercept path, pretty much never happens
			return Mathf.Max(-b / (2f * a), 0f); //don't shoot back in time
	}

	/*	public static Vector3 GetInterceptionWithAccel(Vector3 acceleration, Vector3 VelTargetInit, Vector3 PositionTarget, Vector3 PositionProjectile, float Velocity) {
			Vector3[] QResults = Q(acceleration, VelTargetInit, PositionTarget, PositionProjectile, Velocity);
			float[] QResultsSummed = new float[5];

			for (int i = 0; i < QResultsSummed.Length; i++) {
				QResultsSummed[i] = QResults[i].x + QResults[i].y + QResults[i].z;
			}

			float[] criticalPoints = new float[4];

			//check if one sln is <=0

			return new Vector3(); // TODOODODODODODOD
		}

		static float quadForm(float a, float b, float c, float pos)

		// Returns the coefficients of the expanded equation
		public static Vector3[] Q(Vector3 a, Vector3 Utarget, Vector3 Ptarget, Vector3 Pproj, float VMagproj) {
			Vector3[] coefficients = new Vector3[5];

			for (int i = 0; i < coefficients.Length; i++)
				coefficients[i] = new Vector3();


			// t^4
			coefficients[0].x = 0.25f * a.x * a.x;
			coefficients[0].y = 0.25f * a.y * a.y;
			coefficients[0].z = 0.25f * a.z * a.z;

			// t^3
			coefficients[1].x = a.x * Utarget.x;
			coefficients[1].y = a.y * Utarget.y;
			coefficients[1].z = a.z * Utarget.z;

			// t^2
			Vector3 P = (Ptarget - Pproj);
			coefficients[2].x = a.x * P.x;
			coefficients[2].y = a.y * P.y;
			coefficients[2].z = a.z * P.z;

			// t
			coefficients[3].x = 2 * P.x * Utarget.x;
			coefficients[3].y = 2 * P.y * Utarget.y;
			coefficients[3].z = 2 * P.z * Utarget.z;

			// c
			coefficients[4].x = P.x * P.x;
			coefficients[4].y = P.y * P.y;
			coefficients[4].z = P.z * P.z;

			return coefficients;
		}*/

	public static Vector3 MultiplyEach(Vector3 a, Vector3 b) {
		return new Vector3(a.x * a.x, a.y * a.y, a.z * a.z);
	}

	public virtual void OnHit(Hitbox pOther, Hitbox pAttacker) {
		if (m_bActive) {
			// e.g. shields
			if (!pOther) {
				Delete();
				return;
			}


			Damageable dmgable = pOther.GetDamageable();

			if (dmgable) {
				if (m_nTeam != pOther.GetTeam()) {
					//Debug.Log("OnHit on " + name + "against an enemy damageable: " + pOther.name);

					m_bActive = false;

					CancelInvoke("Delete");
					if (m_flLifeTimeAfterHit > 0) {
						Invoke("Delete", m_flLifeTimeAfterHit);
					} else {
						Delete();
					}
				}
			} else {
				if (projectileGhost) {
					projectileGhost.OnHit(pOther);
				}

				CancelInvoke("Delete");
				if (m_flLifeTimeAfterHit > 0) {
					Invoke("Delete", m_flLifeTimeAfterHit);
					m_bActive = false;
				} else {
					Delete();
				}
			}
		}
	}
}