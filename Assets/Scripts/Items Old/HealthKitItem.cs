﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthKitItem : PickupScript {

	[SerializeField]
	int healAmount = 50;

	public override void PickedUp(PlayerCharacter pc) {
		if (pc.healthPoints < pc.maxHealthPoints) {
			//pc.Heal(healAmount);
			pc.PickupResource(PlayerCharacter.ResourceType.HEALTH, healAmount);
			model.SetActive(false);
		}
	}

	public override void Reset() {
		model.SetActive(true);
	}
}
