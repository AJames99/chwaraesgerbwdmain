using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

// Represents an item in the context of the inventory.
// When dropped, creates an object with a PhysicalItem script attached.
public abstract class ItemScript : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IEquippableItem, IUsableItem {

	[HideInInspector]
	public bool isFloating;

	public enum ItemType {
		Consumable,		// Reduce stack count on use. Can be used, dropped, and picked up.
		Equippable,		// Cannot be destroyed, only equipped, unequipped, dropped, and picked up
		Quest			// Cannot be removed, destroyeed, nor dropped manually, only usable contextually.
	}
	public ItemType type;
	public InventorySlot.SlotType slotType;

	private Image image;

	public void Initialise() {
		isFloating = false;
		image = GetComponent<Image>();
		isScreenSpaceCam = container.inventoryCanvas.renderMode == RenderMode.ScreenSpaceCamera;
		if (isScreenSpaceCam)
			screenSpaceCam = container.inventoryCanvas.worldCamera;
	}

	/*public ItemScript(ItemType type) {
		isBeingDragged = false;
	}

	public ItemScript(ItemScript instanceOf) {
		type = instanceOf.type;
		itemPrefab = instanceOf.itemPrefab;
		itemName = instanceOf.itemName;
		container = instanceOf?.container;
		isBeingDragged = false;
	}*/
	[SerializeField]
	PhysicalItem itemPrefab; // Used when dropping the item;
	
	public string itemName; // Simple ID, nothing important

	[HideInInspector]
	public Inventory container; // Which inventory are we in? Needed when Use/Equip affects the stack count (managed by inv.) and other situations.
	[HideInInspector]
	public InventorySlot containerSlot;

	static Vector2 lastMousePos = Vector2.zero;

	private bool isScreenSpaceCam = false;
	Camera screenSpaceCam;

	public bool isInventoryOpen {
		get {
			return container.inventoryOpen;
		}
	}

	public void OnDrag(PointerEventData eventData) {
		//base.OnDrag(eventData);
		//		Vector2 position = eventData.position;
		//		if (isScreenSpaceCam)
		//			position = screenSpaceCam.WorldToScreenPoint(eventData.pointerCurrentRaycast.worldPosition);

		Vector2 position = Input.mousePosition;
		Vector2 delta = (position - lastMousePos) / container.canvasScale;
		transform.localPosition += new Vector3(delta.x, delta.y, 0f);
		lastMousePos = position;
	}

	public virtual void OnBeginDrag(PointerEventData eventData) {
		//base.OnBeginDrag(eventData);
		isFloating = true;
		//lastMousePos = eventData.position;
		lastMousePos = Input.mousePosition;

		container.RefreshCanvasScale();

		// Because unity lacks any UI sorting whatsoever, we have to 
		// manually unparent and shift this fucking icon to be on
		// top of everything despite all UI element alignment 
		// depending on the fucking hierarchy.
		((RectTransform)transform).SetParent((RectTransform)container.inventoryCanvas.transform, true);
		((RectTransform)transform).SetAsLastSibling();
		image.raycastTarget = false;
	}

	// Occurs after OnDrop
	public void OnEndDrag(PointerEventData eventData) {
		//base.OnEndDrag(eventData);
		// We haven't landed on anything; go back to where we were before
		if (isFloating) {
			//DROP out of inventory
			isFloating = false;
			Drop();
		}
		image.raycastTarget = true;
	}


	/// <summary>
	/// Called when the item is dropped from the inventory. Inventory should manage stack counts etc.
	/// </summary>
	public virtual void Drop() {
		if (containerSlot)
			containerSlot.RemoveItem();
		/*PhysicalItem instancedItem =*/ Instantiate(itemPrefab, container.transform.position + container.transform.forward, container.transform.rotation);
		Destroy(gameObject);
	}

	public abstract void OnEquip(Damageable owner);

	public abstract void OnUnequip(Damageable owner);

	public abstract void OnUse(Damageable owner);
}

public interface IEquippableItem {
	/// <summary>
	/// Called when the item is placed in an equippable slot
	/// </summary>
	abstract void OnEquip(Damageable owner);
	/// <summary>
	/// Called when the item is removed from an equippable slot
	/// </summary>
	abstract void OnUnequip(Damageable owner);
}

public interface IUsableItem {
	/// <summary>
	/// Called when the item is used.
	/// </summary>
	abstract void OnUse(Damageable owner);
}