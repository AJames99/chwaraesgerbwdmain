using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventorySlot : MonoBehaviour, IDropHandler {
	public enum SlotType {
		GENERIC,    // Accepts anything (normal inv. slots)
		SHIELD,     // Accepts only shields (shield slot)
		ARMOUR      // Accepts only armour
	}
	public SlotType slotType = SlotType.GENERIC;
	public Inventory container;
	private ItemScript _containedItem = null;
	public ItemScript containedItem {
		set {
			_containedItem = value;
			if (_containedItem != null) {
				// If this item already belongs somewhere, notify that slot of its remove
				if (_containedItem.containerSlot)
					_containedItem.containerSlot.RemoveItem();

				// Set its new slot to be us
				_containedItem.containerSlot = this;

				// Positioning
				_containedItem.transform.SetParent(transform);
				//_containedItem.transform.position = transform.position.WithZ(_containedItem.transform.position.z);	// Reposition, preserving Z;
				((RectTransform)_containedItem.transform).anchoredPosition = Vector3.zero;//.WithZ(_containedItem.transform.position.z);  // Reposition, preserving Z;
				_containedItem.transform.localRotation = Quaternion.Euler(Vector3.zero);//.WithZ(_containedItem.transform.position.z);  // Reposition, preserving Z;
				((RectTransform)_containedItem.transform).localScale = Vector3.one;
				_containedItem.container = container;

				// For screen space camera, z seems to get VERY confused
				_containedItem.transform.localPosition = _containedItem.transform.localPosition.WithZ(0f);

				// May be redundant
				_containedItem.Initialise();

				if (_containedItem.type == ItemScript.ItemType.Equippable) {
					_containedItem.OnUnequip(container.owner);
					_containedItem.OnEquip(container.owner);
				}
			}
		}
		get {
			return _containedItem;
		}
	}

	// To be called by the inventory manager
	public void Initialise() {
		containedItem = GetComponentInChildren<ItemScript>();
		if (containedItem) {
			containedItem.Initialise();

			// I think this is redundant, as setting containedItem automatically calls OnEquip & OnUnequip
			/*if (containedItem.type == ItemScript.ItemType.Equippable) {
				switch (slotType) {
					case SlotType.SHIELD:
						ShieldItem shield = (ShieldItem)containedItem;
						shield.OnEquip(container.owner);
						break;
					case SlotType.ARMOUR:
						break;
				}
			}*/
		}
	}

	public bool SlotAccepts(SlotType? incomingType) {
		return (incomingType == slotType || slotType == SlotType.GENERIC);
	}

	public void RemoveItem() {
		containedItem = null;
	}

	// Swaps two objects around, assuming they both have slots assigned to them
	public ItemScript SwapItem(ItemScript replacement) {
		if (containedItem == replacement)
			return replacement;

		if (replacement.containerSlot != null && SlotAccepts(replacement.slotType)) {
			ItemScript tempRef = containedItem;
			replacement.containerSlot.containedItem = tempRef;
			containedItem = replacement; // Updates slot of replacement to this
			return tempRef;
		} else {
			return null;
		}
	}

	public void OnDrop(PointerEventData eventData) {
		ItemScript item = eventData.pointerDrag.GetComponent<ItemScript>();
		//if (eventData.pointerCurrentRaycast.gameObject.TryGetComponent<InventorySlot>(out slotDroppedOn)) { // DOESNT WORK
		//if (RectTransformUtility.RectangleContainsScreenPoint(transform as RectTransform, Input.mousePosition)) { // kinda redundant, removed
		// Make sure what's been dropped is in fact an item and it matches our slot type
		if (item != null && SlotAccepts(item?.slotType) && item != containedItem) { 
			item.isFloating = false;
			if (containedItem == null) {
				// Slot is free; place us in there
				containedItem = item;
			} else {
				// Something is already in this slot!
				SwapItem(item);
			}
		} else {
			((RectTransform)item.transform).SetParent((RectTransform)item.containerSlot.transform);
			((RectTransform)item.transform).SetAsLastSibling();
			((RectTransform)item.transform).anchoredPosition = Vector3.zero;
			item.isFloating = false;
		}
	}
}