using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldItem : ItemScript {
	[SerializeField]
	ShieldScript associatedShieldPrefab;

	//[HideInInspector]
	public ShieldScript shieldInstance;
	// Set by the shield prefab, used when instantiating
	Vector3 defaultPosition = new Vector3(0f, 0f, -0.4f);

	private void Start() {
		slotType = InventorySlot.SlotType.SHIELD;
		type = ItemType.Equippable;
		//defaultPosition = shieldInstance.transform.position; // might be invalid?
	}

	public override void OnEquip(Damageable owner) {
		shieldInstance = Instantiate(associatedShieldPrefab, defaultPosition, Quaternion.Euler(Vector3.zero), container.transform);
		shieldInstance.active = true;
		shieldInstance.transform.localPosition = defaultPosition;
		shieldInstance.transform.localRotation = Quaternion.identity;
		shieldInstance.associatedItem = this;
		shieldInstance.playerCharacter = (PlayerCharacter)owner;
		shieldInstance.playerController = shieldInstance.playerCharacter.GetComponentInParent<TPSController>();
		shieldInstance.active = false;
		container.ShieldPlacedInSlot(containerSlot);
		Debug.Log("OnEquip shield");
	}

	public override void OnUnequip(Damageable owner) {
		Debug.Log("OnUnequip shield");
		container.ShieldSlotChange(null);
		if(shieldInstance)
			Destroy(shieldInstance.gameObject);
	}

	public override void OnUse(Damageable owner) { }

	public override void Drop() {
		OnUnequip(container.owner);
		base.Drop();
	}
}
