using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Literally does nothing.
public class ClutterItem : ItemScript {
	public override void OnEquip(Damageable owner) { }

	public override void OnUnequip(Damageable owner) { }

	public override void OnUse(Damageable owner) { }
}
