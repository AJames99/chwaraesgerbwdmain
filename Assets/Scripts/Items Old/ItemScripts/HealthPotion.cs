using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HealthPotion : DrinkablePotion {
	[SerializeField]
	int healAmount = 25;
	public override void Drink(Damageable owner) {
		owner.Heal(healAmount);
	}
	/*public override void OnBeginDrag(PointerEventData eventData) {
		base.OnBeginDrag(eventData);
		Debug.Log("Begun drag on health potion");
	}*/
}
