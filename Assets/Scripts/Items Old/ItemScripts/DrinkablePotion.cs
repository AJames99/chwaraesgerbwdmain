using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DrinkablePotion : ItemScript {
	[SerializeField]
	ItemScript emptyBottle = null;

	public abstract void Drink(Damageable owner);
	public override void OnUse(Damageable owner) {
		Drink(owner);
		if (emptyBottle) {
			ItemScript emptyBottleInstance = Instantiate(emptyBottle);
			container.AddToSlot(emptyBottleInstance, containerSlot);
		}
		Destroy(gameObject);
	}
	public override void OnEquip(Damageable owner) {}
	public override void OnUnequip(Damageable owner) {}
}
