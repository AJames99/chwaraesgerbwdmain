using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Describes any collider that can be used for hitting other hitboxes/hurtboxes or can receive hits.
public class Hitbox : MonoBehaviour {

	public enum HITBOX_LOCATION {
		TRIPLE,
		DOUBLE,
		SPECIAL,
		DEFAULT
	}

	public float GetMultiplier() {
		switch (m_hitboxLocation) {
			case HITBOX_LOCATION.DOUBLE:
				return 2.0f;
			case HITBOX_LOCATION.TRIPLE:
				return 3.0f;
			case HITBOX_LOCATION.DEFAULT:
			default:
				return 1.0f;
		}
	}

	[SerializeField]
	protected bool m_bCanReceiveHits = true;

	[SerializeField]
	protected bool m_bCanDeliverHits = false;

	[SerializeField]
	protected Damageable m_pDamageable;

	public bool m_bCanHitLocations = false; // Can this hitbox use location multipliers e.g. headshots?

	public virtual Damageable GetDamageable() {
		return m_pDamageable;
	}

	public virtual void SetDamageable(Damageable pDamageable) {
		m_pDamageable = pDamageable;
	}

	protected Collider m_pCollider;

	[SerializeField]
	protected int m_nDamage;

	[SerializeField]
	protected Damage.DamageTypes m_damageType;

	public HITBOX_LOCATION m_hitboxLocation = HITBOX_LOCATION.DEFAULT;

	[SerializeField]
	bool m_bDebug = false;

	const string szDebugSpherePath = "Prefab/Debug/HitSphereDebug";

	[HideInInspector]
	public bool m_bIsActive = true;

	protected bool m_bInitialised = false;

	// Damageables that we can't hit e.g. after having hit them already.
	List<Damageable> m_lstHitBlacklist;

	public void ClearBlacklist() {
		m_lstHitBlacklist.Clear();
	}

	public void AppendToBlackList(Damageable pDamageable) {
		m_lstHitBlacklist.Add(pDamageable);
	}

	public virtual bool CanHit(Damageable pDamageable) {
		return !m_lstHitBlacklist.Contains(pDamageable);
	}

	// Start is called before the first frame update
	protected virtual void Start() {
		if (!m_pDamageable)
			m_pDamageable = GetComponent<Damageable>();

		if (!m_pDamageable)
			m_pDamageable = GetComponentInParent<Damageable>();

		m_pCollider = GetComponent<Collider>();

		m_bInitialised = false;
	}

	protected virtual void Awake() {
		if (!m_bInitialised) {
			SphereCollider pSphereCollider = GetComponent<SphereCollider>();
			if (m_bDebug && pSphereCollider) {
				GameObject pDebugSphere = Instantiate(Resources.Load(szDebugSpherePath) as GameObject, transform);
				pDebugSphere.transform.localScale = Util.FromSingle(pSphereCollider.radius * 2);
				pDebugSphere.transform.localPosition = pSphereCollider.center;
			}

			if (null == m_lstHitBlacklist)
				m_lstHitBlacklist = new List<Damageable>(8);

			m_bInitialised = true;
		}
	}


	protected virtual void OnTriggerEnter(Collider other) {
		if (!m_bIsActive)
			return;

		if (/*other.isTrigger && */m_bCanDeliverHits) {
			Hitbox pHitboxOther = other.GetComponent<Hitbox>();
			if (pHitboxOther) {
				Damageable pDmgOther = pHitboxOther.GetDamageable();
				if (pHitboxOther.m_bCanReceiveHits && pDmgOther && CanHit(pDmgOther)) {
					pDmgOther.StruckByHitbox(pHitboxOther, this);
				}

				OnHit(pHitboxOther);
			}
		}
	}

	public virtual void OnHit(Hitbox pOther) {
		//Debug.Log("Hitbox on " + pOther.name + " was hit by hitbox on " + name);
	}

	// Gets the damage that this hitbox should *deal*
	public virtual Damage GetDamage(Hitbox pReceiverBox = null) {
		Damage.DamageDing ding = ding = Damage.DamageDing.NONE;
		switch (pReceiverBox.m_hitboxLocation) {
			case HITBOX_LOCATION.DOUBLE:
			case HITBOX_LOCATION.TRIPLE:
				if (m_bCanHitLocations) {
					ding = Damage.DamageDing.CRITICAL;
				} else {
					ding = Damage.DamageDing.NORMAL;
				}
				break;

			case HITBOX_LOCATION.DEFAULT:
			default:
				ding = Damage.DamageDing.NORMAL;
				break;
		}

		return new Damage(m_damageType, Mathf.CeilToInt(m_nDamage * (m_bCanHitLocations ? pReceiverBox.GetMultiplier() : 1.0f)), transform.position, ding);
	}

	public virtual int GetTeam() {
		if (!m_pDamageable)
			return -1;

		return m_pDamageable.team;
	}

	public virtual string GetOwnerName() {
		if (m_pDamageable)
			return m_pDamageable.name;
		return name;
	}
}
