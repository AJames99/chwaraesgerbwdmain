﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClipPlayer : MonoBehaviour {

	[SerializeField]
	ClipObject clipObject;

    private void Start() {
		if (!clipObject) {
			GameObject obj = (GameObject)Resources.Load("Prefab/Sound/OneShotClipLog");
			clipObject = obj.GetComponent<ClipObject>();
		}
    }

    public void PlayClipAtPoint(AudioClip clip, Vector3 position, float volume = 1f) {
		ClipObject instance = Instantiate(clipObject, position, Quaternion.identity);
		instance.volume = volume;
		instance.clip = clip;
	}
}
