﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClipObject : MonoBehaviour {
	[SerializeField]
	AudioSource audioSource;

	private AudioClip _clip;
	public AudioClip clip {
		set {
			_clip = value;
			audioSource.clip = _clip;
			audioSource.volume = volume;
			audioSource.Play();
			Destroy(gameObject, _clip.length);
		}
		get {
			return _clip;
		}
	}

	public float volume = 1f;

	private void Start() {
		audioSource = GetComponent<AudioSource>();
		audioSource.playOnAwake = false;
	}
}
