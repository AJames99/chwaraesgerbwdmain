﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities;

public class PlayerCharacter : Damageable {
	[SerializeField]
	private LayerMask damagingLayers;

	[SerializeField]
	private AudioClip hurtClip;
	[SerializeField]
	private AudioClip dieClip;

	[SerializeField]
	private AudioClip projectileHitClip;

	[SerializeField]
	private AudioClip[] resourceGrabbedClips;

	public AudioBundlePlayer audioBundlePlayer;

	[SerializeField]
	protected Slider HPSlider;

	//AudioSource audioSource;

	float barDisplayHP = 0;

	public TPSController playerController;

	[SerializeField]
	protected Transform m_pProjectileSpawnPoint;            // Where projectiles are created
	[SerializeField]
	protected Transform m_pProjectileDirectionTransform;    // The transform to use as the projectiles' initial direction

	const float blockingAngle = 90f;

	[HideInInspector]
	public Inventory shieldManager = null;

	[Header("UI")]
	[SerializeField]
	UIHealthBar healthBar = null;

	static protected PlayerCharacter pMainCharacter = null;
	public static PlayerCharacter GetMainCharacter() {
		return pMainCharacter;
	}

	public enum ResourceType {
		RAILGUNSHOTS,
		DASHCHARGES,
		HEALTH
	}

	public Dictionary<ResourceType, int> resources = new Dictionary<ResourceType, int>();
	public Dictionary<ResourceType, int> resourceMaxima = new Dictionary<ResourceType, int>();

	public override void GetProjectileFiringSetup(out Vector3 vPosition, out Vector3 vDirection) {
		if (m_pProjectileSpawnPoint && m_pProjectileDirectionTransform) {
			vPosition = m_pProjectileSpawnPoint.position;
			vDirection = m_pProjectileDirectionTransform.forward;
		} else {
			base.GetProjectileFiringSetup(out vPosition, out vDirection);
		}
	}

	// Start is called before the first frame update
	public override void Start() {
		base.Start();
		team = PLAYERTEAM;

		resourceMaxima[ResourceType.RAILGUNSHOTS] = 3;
		resourceMaxima[ResourceType.DASHCHARGES] = 2;
		resourceMaxima[ResourceType.HEALTH] = 2;
		resources[ResourceType.RAILGUNSHOTS] = 0;
		resources[ResourceType.DASHCHARGES] = 2;

		//audioSource = gameObject.AddComponent<AudioSource>();
		//audioSource.volume = 0.5f;
		reusableAudioSource = GetComponent<AudioSource>();

		playerController = GetComponentInParent<TPSController>();

		shieldManager = playerController.GetComponentInChildren<Inventory>();

		if (!pMainCharacter && tag.Equals("Player"))
			pMainCharacter = this;
	}

	private void Awake() {
		healthPoints = maxHealthPoints;
		//barDisplayHP = (healthPoints / maxHealthPoints);
		healthBar.SetHealthSnap(healthPoints);
		healthBar.maxHP = maxHealthPoints;
		healthBar.maxOverhealHP = Mathf.CeilToInt(maxHealthPoints * maxOverhealFactor);
	}

	public override void Die() {
		//HURGH
		//ARHK
		//grlbllbl....
		reusableAudioSource.PlayOneShot(dieClip, 0.2f);

		ExtensionMethods.BroadcastAll("PlayerEvent", "NPC", "DEAD");
	}

	public override void Hurt(Damage damage) {
		// For melee damage, defer to the shield if we're blocking to check for blocks
		ShieldScript currentShield = shieldManager.GetActiveShield();
		float angleOfAttack = Vector3.Angle(playerController.GetLookVector(), (damage.origin - playerController.transform.position).normalized);

		switch (damage.damageType) {
			case Damage.DamageTypes.MELEE:
			case Damage.DamageTypes.EXPLOSION:
			case Damage.DamageTypes.PROJECTILE:
				if (currentShield && angleOfAttack < blockingAngle) {
					if (currentShield.isBlocking) {
						currentShield.OnShieldHit(projectileSpeed: damage.amount, projectileDamage: damage.amount);
						goto SkipHurt;
					}
				}
				if (damage.damageType == Damage.DamageTypes.PROJECTILE)
					HitProjectile();
				reusableAudioSource.PlayOneShot(hurtClip, 0.08f);
				reusableAudioSource.PlayOneShot(projectileHitClip, 0.08f);
				break;
			default:
				reusableAudioSource.PlayOneShot(hurtClip, 0.08f);
				break;
		}

		base.Hurt(damage);
		//barDisplayHP = (float)healthPoints / maxHealthPoints;
		healthBar.SetHealth(healthPoints);

	SkipHurt:
		if (damage.origin != null) {
			playerController.AddForce(((shieldManager.GetActiveShield() ? shieldManager.GetActiveShield().transform.position : transform.position) - damage.origin).normalized * (damage.amount));
		}
	}

	public void DoHitsound(Damage pDamage) {
		switch (pDamage.m_ding) {
			case Damage.DamageDing.NORMAL: {
					reusableAudioSource.PlayOneShot(audioBundlePlayer.m_acHitsound,
						pDamage.amount.RemapClamped(audioBundlePlayer.m_v4HitSoundVolumeDamageMap.x, audioBundlePlayer.m_v4HitSoundVolumeDamageMap.y, audioBundlePlayer.m_v4HitSoundVolumeDamageMap.z, audioBundlePlayer.m_v4HitSoundVolumeDamageMap.w));
					break;
				}
			case Damage.DamageDing.CRITICAL: {
					reusableAudioSource.PlayOneShot(audioBundlePlayer.m_acHitsoundCrit, audioBundlePlayer.m_flCritSoundVolume);
					break;
				}
			case Damage.DamageDing.NONE:
			default:
				break;
		}
	}

	public override void Heal(int hp) {
		base.Heal(hp);
		barDisplayHP = (float)healthPoints / maxHealthPoints;
		//HPSlider.value = barDisplayHP;
		//healthBar.SetHealth(barDisplayHP);
		healthBar.SetHealth(healthPoints);
	}

	protected override void Update() {
		base.Update();
		if (overhealAllowed)
			healthBar.SetHealth(healthPoints, false);
	}

	public void HitProjectile() {
		reusableAudioSource.PlayOneShot(projectileHitClip, 0.075f);
		ExtensionMethods.BroadcastAll("PlayerEvent", "NPC", "HIT");
	}

	public void PickupResource(ResourceType type, int amount) {
		switch (type) {
			case ResourceType.RAILGUNSHOTS:
				resources[type] += amount;
				reusableAudioSource.PlayOneShot(resourceGrabbedClips[1], 0.1f);
				break;
			case ResourceType.DASHCHARGES:
				resources[type] += amount;
				reusableAudioSource.PlayOneShot(resourceGrabbedClips[0], 0.1f);
				break;
			case ResourceType.HEALTH:
				Heal(amount);
				reusableAudioSource.PlayOneShot(resourceGrabbedClips[2], 0.2f);
				break;
		}
	}

	public override void Push(Vector3 force, Vector3? point) {
		playerController.AddForce(force, true);
	}

	public override IController GetCharacterController() {
		return playerController;
	}

	//public override float GetForceMultiplier() {
	//	ShieldScript currentShield = shieldManager.GetActiveShield();
	//	return currentShield ? currentShield.GetCurrentForceMultiplier() : 1.0f;
	//}

	public override bool ImpactedBy(Damageable other) {
		// The player needs special behaviour because we have a shield!
		ShieldScript currentShield = shieldManager.GetActiveShield();
		bool bImpact = base.ImpactedBy(other);

		if (!currentShield)
			return false;

		// Only play the impact cue if we really hit them with enough force
		if (bImpact) {
			switch (currentShield.GetBlockLevel()) {
				case ShieldScript.BlockLevel.BASH:
					reusableAudioSource.PlayOneShot(audioBundlePlayer.impactHardBash.GetRandom<AudioClip>(), 0.4f);
					if (FreezeframeManager.m_pManagerStatic != null) {
						FreezeframeManager.m_pManagerStatic.Freeze(0.5f);
					}
					break;
				case ShieldScript.BlockLevel.TIMEDBLOCK:
					reusableAudioSource.PlayOneShot(audioBundlePlayer.impactSoftShield.GetRandom<AudioClip>(), 0.3f);
					if (FreezeframeManager.m_pManagerStatic != null) {
						FreezeframeManager.m_pManagerStatic.Freeze(0.2f);
					}
					break;
				case ShieldScript.BlockLevel.BLOCK:
					reusableAudioSource.PlayOneShot(audioBundlePlayer.impactOtherHard.GetRandom<AudioClip>(), 0.3f);
					if (FreezeframeManager.m_pManagerStatic != null) {
						FreezeframeManager.m_pManagerStatic.Freeze(0.1f);
					}
					break;
				case ShieldScript.BlockLevel.NONE:
					reusableAudioSource.PlayOneShot(audioBundlePlayer.impactOtherSoft.GetRandom<AudioClip>(), 0.5f);
					break;
			}
		}

		return bImpact;
	}

	public override void OnHitWall(float speed01 = -1) {
		base.OnHitWall();
		if (speed01 > impactSoftHardSpeedBoundary) {
			reusableAudioSource.PlayOneShot(audioBundlePlayer.impactWallHard.GetRandom<AudioClip>(), 0.5f);
		} else if (speed01 > 0) {
			reusableAudioSource.PlayOneShot(audioBundlePlayer.impactWallSoft.GetRandom<AudioClip>(), 0.5f);
		}
	}

}
