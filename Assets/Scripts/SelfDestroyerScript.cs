﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroyerScript : MonoBehaviour {
	[SerializeField] float m_lifeTime = 0.0f;

	public void Awake() {
		if (m_lifeTime > 0.0f) {
			Destroy(gameObject, m_lifeTime);
		}
	}

	public void DestroyThis() {
		Destroy(this.gameObject);
	}
}
