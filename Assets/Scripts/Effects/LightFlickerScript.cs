using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlickerScript : MonoBehaviour {

	[SerializeField]
	Vector2 intensityBounds;

	[SerializeField]
	float rateMultiplier = 10.0f;

	Light pointLight;
	private void Start() {
		pointLight = GetComponent<Light>();
	}

	void Update() {
		pointLight.intensity = Mathf.PerlinNoise(Time.time * rateMultiplier, 0.0f).Remap(0, 1, intensityBounds.x, intensityBounds.y);
	}
}
