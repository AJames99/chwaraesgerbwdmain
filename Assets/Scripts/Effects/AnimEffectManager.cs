﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// I don't know why I made this...
public abstract class AnimEffectManager : MonoBehaviour {
	protected AudioSource audioSource;

	public abstract void DoEffectFull(string tag, Vector3? positionOverride = null, Quaternion? rotationOverride = null);
	
	// For animator callbacks
	public void DoEffect(string tag) {
		DoEffectFull(tag);
	}
}
