using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ScreenshotHandler : MonoBehaviour {
	public void OnScreenshot() {
		/*Camera mainCamera = Camera.main;
		RenderTexture activeRenderTexture = RenderTexture.active;
		RenderTexture.active = mainCamera.targetTexture;
		mainCamera.Render();

		Texture2D image = new Texture2D(mainCamera.targetTexture.width, mainCamera.targetTexture.height);
		image.ReadPixels(new Rect(0, 0, mainCamera.targetTexture.width, mainCamera.targetTexture.height), 0, 0);
		image.Apply();
		RenderTexture.active = activeRenderTexture;

		byte[] bytes = image.EncodeToPNG();
		Destroy(image);*/
		//string fullURL = Application.dataPath + "/Screenshots/" + System.DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".png";
		string fullURL = "Screenshots/" + System.DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".png";
		ScreenCapture.CaptureScreenshot(fullURL);
		//File.WriteAllBytes(fullURL, bytes);

		Debug.Log("Screenshot! " + fullURL);
	}
}
