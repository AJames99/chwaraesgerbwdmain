using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshatchThresholdCameraController : MonoBehaviour
{
    [SerializeField]
    Camera m_Camera;

    [SerializeField]
    [InspectorName("Enable FOV Sliding")]
    bool m_enableFOVSliding = false;

    [SerializeField]
    [InspectorName("FOV Sliding Range")]
    Vector2 m_fovSlidingRange = new Vector2(75, 120);

    [SerializeField]
    [InspectorName("FOV Sliding Rate")]
    [Range(0.0f, 1.5f)]
    float m_fovSlidingRate = 1.0f;

    [SerializeField]
    [InspectorName("Enable Circular Rotation Movement")]
    bool m_enableCircleMovement;

    [SerializeField]
    [InspectorName("Circular rotation radius")]
    Vector2 m_circularRotationRadius = new Vector3(1,1);

    [SerializeField]
    [InspectorName("Circular rotation rate")]
    [Range(0.01f, 2.0f)]
    float m_circularRotationRate = 1.0f;

	[SerializeField, InspectorName("Enable Linear Movement")]
	bool m_enableLinearMovement = false;
	[SerializeField, InspectorName("Movement radius")]
	Vector3 m_linearMovementRadius = new Vector3(0,0,0);
	[SerializeField, InspectorName("Circular rotation rate"), Range(0.01f, 2.0f)]
	float m_linearMovementRate = 1.0f;

	[SerializeField]
    [InspectorName("Enable Axis Alignment Debug Test"), Tooltip("If enabled, will slowly rotate from X - 1 degree to X + 1 degree, where X is a series global rotation values at 180s and 360s. This is to test the popping that occurs with the crosshatch shader's stabilisation")]
    bool m_enableAxisAlignment = false; // Disables circular rotation
    [SerializeField]
    float m_axisAlignmentTestAngleRange = 2.0f; // Total range of axis test per-axis/direction
    [SerializeField]
    float m_axisAlignmentTestRotateRate = 1.0f; // 0 to 1 rate
    private float m_axisAlignmentT = 0;

    // Non-editor Vars: --
    // Can't be arsed to do the maths for moving a camera's lookatdir in a circle, just caching the original forward and using that
    Vector3 originalCameraForward;
	Vector3 originalCameraPos;

    // Start is called before the first frame update
    void Start()
    {
        originalCameraForward = transform.forward * 3.0f;
		originalCameraPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_enableAxisAlignment && m_enableCircleMovement) {
            transform.LookAt(transform.position + originalCameraForward + transform.TransformVector(new Vector3(Mathf.Cos(Time.time * m_circularRotationRate), Mathf.Sin(Time.time * m_circularRotationRate)) * m_circularRotationRadius), Vector3.up);
        }

        if (m_enableFOVSliding) {
            if (m_Camera) {
                m_Camera.fieldOfView = Mathf.Lerp(m_fovSlidingRange.x, m_fovSlidingRange.y, (Mathf.Sin(Time.time * m_fovSlidingRate) + 1)/2.0f);
            }
        }

		if (m_enableLinearMovement) {
			transform.position = originalCameraPos + (m_linearMovementRadius * Mathf.Sin(Time.time * m_linearMovementRate));
		}

        if (m_enableAxisAlignment) {
            Vector2 range = new Vector2(359.9f, 0.1f);
            m_axisAlignmentT += Time.deltaTime * m_axisAlignmentTestRotateRate;
            if (m_axisAlignmentT > 1) {
                m_axisAlignmentT -= 1.0f;
            }

            float yawAngle = Mathf.LerpAngle(range.x, range.y, m_axisAlignmentT);//(m_axisAlignmentT - 0.5f) * 2.0f;

            Vector3 currentEulers = transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(currentEulers.x, yawAngle, currentEulers.z);
        }
    }
}
