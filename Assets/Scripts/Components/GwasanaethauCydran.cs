using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CydrannauEndid {
	
	// Caches ENTITY components for quicker access than Unity's GetComponent calls
	// Handles adding of new entiy components
	public class GwasanaethauCydran : MonoBehaviour {
		
		// typeof is fast but only works for explicitly declared types (compile-time),
		// GetType() respects polymorphism, is more like C++ dynamic_cast (runtime)
		Dictionary<System.Type, CydranEndid> m_componentsCache = new Dictionary<System.Type, CydranEndid>();

		[HideInInspector] public Endid m_endid;

		public void Start() {
			CacheComponents();
		}

		public void Awake() {
			CacheComponents();
		}

		public void CacheComponents() {
			m_endid = GetComponent<Endid>();
			foreach (CydranEndid component in GetComponents<CydranEndid>()) {
				m_componentsCache.TryAdd(component.GetType(), component);
			}
		}

		// For a given gameobject, tries to get its utilities component.
		public static GwasanaethauCydran GetComponentUtilities(GameObject gameObject) {
			if (gameObject != null) {
				gameObject.TryGetComponent(out GwasanaethauCydran utilityComponent);
				if (!utilityComponent) {
					utilityComponent = gameObject.AddComponent<GwasanaethauCydran>();
				}
				return utilityComponent;
			}
			return null;
		}

		// Explicit typing
		public void RegisterNewComponentInstance<T>(T component) where T : CydranEndid {
			if (!component)
				return;

			if (m_componentsCache.ContainsKey(typeof(T))) {
				Debug.LogError("Attempted to add duplicate component (type " + typeof(T).ToString() + "! Only one component per type is permitted (currently).");
				Destroy(component);
			} else {
				m_componentsCache.Add(typeof(T), component);
			}
		}

		public void RegisterNewComponentInstance(CydranEndid component) {
			if (!component)
				return;
			System.Type componentType = component.GetType();
			if (m_componentsCache.ContainsKey(component.GetType())) {
				Debug.LogError("Attempted to add duplicate component (type " + componentType.ToString() + "! Only one component per type is permitted (currently).");
				Destroy(component);
			} else {
				m_componentsCache.Add(componentType, component);
			}
		}

		public void OnComponentDestroyed<T>() where T : CydranEndid {
			m_componentsCache.Remove(typeof(T));
		}

		// Gets the component of the specified type; returns nullptr otherwise (?)
		public T GetEntityComponent<T>() where T : CydranEndid {
			m_componentsCache.TryGetValue(typeof(T), out CydranEndid component);
			return (T)component;
		}

		public CydranEndid GetEntityComponent(System.Type type) {
			m_componentsCache.TryGetValue(type, out CydranEndid component);
			return component;
		}
	}
}