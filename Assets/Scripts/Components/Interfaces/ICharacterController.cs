using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CydrannauEndid {
	public interface ICharacterController {
		public enum ControllerType {
			Kinematic,
			KinematicSimple,
			Flying,
			Rigidbody
		}

		public abstract ControllerType GetControllerType();

		public abstract Vector3 GetVelocity();
		public abstract void SetVelocity(Vector3 velocity);
		// Timeless impulse; essentially just a mass-relative instant acceleration
		public abstract void ApplyImpulse(Vector3 forceVector, bool ignoreMass = false);

		public void OnMove(Vector2 value);
		public void OnLook(Vector2 value);
		public void OnJump(bool value);
		public void OnDash(bool value);
		public void OnCrouch(bool value);
		public void OnPrimary(InputValue value);
		public void OnSecondary(InputValue value);
		public void OnTertiary(InputValue value);
		public void OnQuaternary(InputValue value);

		public void OnDebugTrigger(bool value);
	}
}