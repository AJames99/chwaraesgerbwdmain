#define DEBUG_NODWEDDION

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodweddionEndid;
using DataBank;

using DataBank.ItemSystem;

namespace CydrannauEndid {
	// An interface to allow component-attributes to be retrieved from Charon JSON data (previously marked with #)
	// Rhyngwyneb ("Interface")
	public interface RNodweddionCydran {

		// Given an entity definition, should be implemented such that component-specific attributes (e.g. max HP on the health component)
		// are added to the attributes component.
		public bool AdioNodweddionCydran(CydranEndidNodweddion attributeComponent);

		// Add the component attribute callbacks (or any attribute callback, technically)
		public virtual void AdioAdalwadNodwedd(CydranEndidNodweddion cydNod) { }

		// Remove all component attribute callbacks
		public virtual void DileuAdalwadNodwedd() { }
	}

	public class CydranEndidNodweddion : CydranEndid {
		[System.Serializable]
		public class CofnodNodwedd<T> where T : System.IEquatable<T> {
			[SerializeField]
			public string enwNodwedd;
			[SerializeField]
			public GwerthNodwedd<T> gwerth;
		}
		
		[SerializeField] protected List<CofnodNodwedd<int>> m_cofnodionNodweddInt;
		[SerializeField] protected List<CofnodNodwedd<float>> m_cofnodionNodweddFloat;
		[SerializeField] protected List<CofnodNodwedd<bool>> m_cofnodionNodweddBool;

		protected Dictionary<string, NodweddEndid> m_nodweddion = new Dictionary<string, NodweddEndid>();

		public override EMathCydran CaelMathCydran() {
			return EMathCydran.Attribute;
		}

		public void AdioNodwedd(NodweddEndid attribute) {
			if (attribute == null) {
				return;
			}

			// Trim any JSON attribute flags (this is Obsolete now!!)
			attribute.m_enwNodwedd = attribute.m_enwNodwedd.Replace("#", string.Empty);
			attribute.m_enwNodwedd = attribute.m_enwNodwedd.Replace("!", string.Empty);

			if (m_nodweddion.ContainsKey(attribute.m_enwNodwedd.ToLowerInvariant())) {
				Debug.LogWarning("Attempted to add duplicate attribute " + attribute.m_enwNodwedd + " to the attribute component! Not a fatal error, but this shouldn't be happening if the component Init methods are set up correctly.");
				return;
			}
			m_nodweddion.Add(attribute.m_enwNodwedd.ToLowerInvariant(), attribute);
		}

		// TODO check if this needs to be ref
		public NodweddEndid CaelNodwedd(string key) {
			return m_nodweddion.ContainsKey(key.ToLowerInvariant()) ? m_nodweddion[key.ToLowerInvariant()] : null;
		}

		// Apply a new modifier to an attibute on this component
		public bool AdioAddasyddNodwedd(EntityAttributeModifier modifier) {
			// This SHOULD only perform CeisioAdioNodwedd if the first part of the boolean expression fails.
			if (m_nodweddion.ContainsKey(modifier.m_attributeId.ToLowerInvariant()) || CeisioAdioNodwedd(modifier.m_attributeId)) {
				return m_nodweddion[modifier.m_attributeId.ToLowerInvariant()].AddModifier(modifier);
			}
			return false;
		}

		// Remove a modifier from an attribute on this component
		public bool DileuAddasyddNodwedd(Assets.AddasyddNodweddInstans instans) {
			AttributeModifierEntry diffiniadAddasydd = GameDataBankManager.instance.m_attributeDataBank.GetAttributeModifier(instans.EnwAddasydd);
			if (m_nodweddion.ContainsKey(diffiniadAddasydd.m_attributeId.ToLowerInvariant())) {
				return m_nodweddion[diffiniadAddasydd.m_attributeId.ToLowerInvariant()].RemoveModifier(diffiniadAddasydd);
			}
			return false;
		}

		public void CaelGwerthNodwedd<T>(string key, ref GwerthNodwedd<T> value) where T : System.IEquatable<T> {
			NodweddEndid<T> attribute = CaelNodwedd(key) as NodweddEndid<T>;
			if (attribute != null) {
				// Assign gwerth as a reference to the existing attribute's result value
				value = attribute.m_gwerthCanlynol;
			} else {
				// Otherwise, we want gwerth to contain the default value for this attribute
				AttributeEntry<T> entry = GameDataBankManager.instance.m_attributeDataBank.GetAttribute(key) as AttributeEntry<T>;
				if (entry != null) {
					value.m_gwerth = entry.m_baseValue;
				} else {
					// Controversial? We don't want any references to attr values where the attr doesn't exist, so force it tae be null.
					value = null;
				}
				// Otherwise, don't assign value. Leave as-is;
			}
		}
		
		public bool GalluAdioNodwedd(string enwNodwedd) {
			// No duplicates allowed.
			if (m_nodweddion.ContainsKey(enwNodwedd.ToLowerInvariant()))
				return false;

			AttributeEntry entry = GameDataBankManager.instance.m_attributeDataBank.GetAttribute(enwNodwedd);

			// If the attribute ain't in the register, we can't allow it.
			if (entry == null)
				return false;

			return true;
		}

		/// <summary>
		/// Tries to add an attribute with the provided base value
		/// </summary>
		/// <param name="enwNodwedd">The attribute ID</param>
		/// <param name="gwerth">A reference to an instantiated attribute to populate with the reference to the newly added attribute</param>
		/// <returns></returns>
		public bool CeisioAdioNodwedd<T>(string enwNodwedd, ref GwerthNodwedd<T> gwerth) where T : System.IEquatable<T> {
			if (!GalluAdioNodwedd(enwNodwedd)) {
				// If we failed due to a duplicate, set gwerth to the existing attribute's gwerth
				if (m_nodweddion.ContainsKey(enwNodwedd.ToLowerInvariant())) {
					gwerth = (m_nodweddion[enwNodwedd.ToLowerInvariant()] as NodweddEndid<T>).m_gwerthCanlynol;
				}
				
				return false;
			}

			// These type-casted versions of the gwerth reference should reflect their changes in gwerth.

			AttributeEntry<T> entry = GameDataBankManager.instance.m_attributeDataBank.GetAttribute(enwNodwedd) as AttributeEntry<T>;
			if (entry != null) {
				switch (gwerth) {
					case GwerthNodwedd<float>: {
						GwerthNodwedd<float> gwerthFloat = gwerth as GwerthNodwedd<float>;
						NodweddEndidFloat nodFloat = new NodweddEndidFloat(entry as AttributeEntryFloat, ref gwerthFloat);
						gwerthFloat = nodFloat.m_gwerthCanlynol;
						AdioNodwedd(nodFloat);
						break;
					}
					case GwerthNodwedd<int>: {
						GwerthNodwedd<int> gwerthInt = gwerth as GwerthNodwedd<int>;
						NodweddEndidInt nodInt = new NodweddEndidInt(entry as AttributeEntryInt, ref gwerthInt);
						gwerthInt = nodInt.m_gwerthCanlynol;
						AdioNodwedd(nodInt);
						break;
					}
					case GwerthNodwedd<bool>: {
						GwerthNodwedd<bool> gwerthFlag = gwerth as GwerthNodwedd<bool>;
						NodweddEndidFlag nodFlag = new NodweddEndidFlag(entry as AttributeEntryFlag, ref gwerthFlag);
						gwerthFlag = nodFlag.m_gwerthCanlynol;
						AdioNodwedd(nodFlag);
						break;
					}
					default: {
						return false;
					}
				}
			} else {
				return false;
			}

			return true;
		}
		
		/// <summary>
		/// Tries to add an attribute with the attribute's default value.
		/// </summary>
		/// <param name="enwNodwedd">The attribute ID</param>
		/// <returns></returns>
		public bool CeisioAdioNodwedd(string enwNodwedd) {
			if (!GalluAdioNodwedd(enwNodwedd)) {
				return false;
			}

			AttributeEntry entry = GameDataBankManager.instance.m_attributeDataBank.GetAttribute(enwNodwedd);

			if (entry != null) {
				switch (entry) {
					case AttributeEntryFloat: {
						AttributeEntryFloat entryFloat = entry as AttributeEntryFloat;
						NodweddEndidFloat nodwedd = new NodweddEndidFloat(entryFloat);
						AdioNodwedd(nodwedd);
						break;
					}
					case AttributeEntryInt: {
						AttributeEntryInt entryInt = entry as AttributeEntryInt;
						NodweddEndidInt nodwedd = new NodweddEndidInt(entryInt);
						AdioNodwedd(nodwedd);
						break;
					}
					case AttributeEntryFlag: {
						AttributeEntryFlag entryFlag = entry as AttributeEntryFlag ;
						NodweddEndidFlag nodwedd = new NodweddEndidFlag(entryFlag);
						AdioNodwedd(nodwedd);
						break;
					}
					default: {
						return false;
					}
				}
			} else {
				return false;
			}

			return true;
		}

		public override bool Init(ref List<string> issueStrings) {
			if (m_wediYmgychwyn)
				return true;

			issueStrings = new List<string>();

			m_wediYmgychwyn = true;

			//foreach (string attrIdStr in dataCydranNodwedd.m_nodweddion.Keys) {
			//	m_attributes.Add(attrIdStr, dataCydranNodwedd.m_nodweddion[attrIdStr]);
			//}

			foreach (CofnodNodwedd<int> cofNodInt in m_cofnodionNodweddInt) {
				CeisioAdioNodwedd(cofNodInt.enwNodwedd, ref cofNodInt.gwerth);
			}
			foreach (CofnodNodwedd<bool> cofNodBool in m_cofnodionNodweddBool) {
				CeisioAdioNodwedd(cofNodBool.enwNodwedd, ref cofNodBool.gwerth);
			}
			foreach (CofnodNodwedd<float> cofNodFloat in m_cofnodionNodweddFloat) {
				CeisioAdioNodwedd(cofNodFloat.enwNodwedd, ref cofNodFloat.gwerth);
			}

			return m_wediYmgychwyn;
		}

		public override void DebugPrint() {
			string debugStr = "AttributeComponent on " + gameObject.name;
			foreach (string key in m_nodweddion.Keys) {
				debugStr += ("\n\tAttribute " + m_nodweddion[key].ToString());
			}
			Debug.Log(debugStr);
		}

		public bool AdioAdalwadNewidNodweddion(string enwNodwedd, NodweddEndid.ArNewidGwerth adalwad) {
			NodweddEndid nod;
			if (m_nodweddion.TryGetValue(enwNodwedd, out nod)) {
				nod.m_onValueChangedDelegates += adalwad;
			}
			return false;
		}

		// Removes an attribute callback
		public void DileuAdalwadNewidNodweddion(string enwNodwedd, NodweddEndid.ArNewidGwerth adalwad) {
			NodweddEndid nod;
			if (m_nodweddion.TryGetValue(enwNodwedd, out nod)) {
				nod.m_onValueChangedDelegates -= adalwad;
			} else {
				Debug.LogWarning("DileuAdalwadNewidNodweddion: Unable to find attribute \"" + enwNodwedd + "\"!");
			}
		}

#if DEBUG_NODWEDDION
		const int kPadding = 2;
		private void DrawDebugDataBox(int xPos, ref int yPos, int width, int height, string text) {
			GUI.Box(new Rect(xPos, yPos, width, height),
				text);
			yPos += height + kPadding;
		}

		public void OnGUI() {
			const int xPos = 1920 - 450;
			int yPos = 0;

			foreach (string key in m_nodweddion.Keys) {
				DrawDebugDataBox(xPos, ref yPos, 400, 50, m_nodweddion[key].ToString());
			}
		}
#endif
	}
}