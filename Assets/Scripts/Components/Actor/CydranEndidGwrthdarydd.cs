using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CydrannauEndid {
	public class CydranEndidGwrthdarydd : CydranEndid {
		public override EMathCydran CaelMathCydran() {
			return EMathCydran.Collider;
		}

		public enum EColliderType {
			Sphere,
			Capsule
		}

		[HideInInspector] public EColliderType m_colliderType;

		[SerializeField] public Collider m_collider;

		public override bool Init(ref List<string> issueStrings) {
			issueStrings = null;
			if (m_wediYmgychwyn)
				return true;
			m_wediYmgychwyn = true;

			switch (m_collider) {
				case CapsuleCollider: {
					m_colliderType = EColliderType.Capsule;
					break;
				}
				case SphereCollider: {
					m_colliderType = EColliderType.Sphere;
					break;
				}
			}

			/*if (entityEntry != null) {
				string colliderType = "";
				m_wediYmgychwyn &= entityEntry.TryGetParameterValueString("type", ref colliderType);
				float radius = 1.0f, height = 1.0f; // TODO when component defaults are added, remove these
				bool centreOnRoot = false;

				entityEntry.TryGetParameterValueFloat("radius", ref radius);
				entityEntry.TryGetParameterValueFloat("height", ref height);
				entityEntry.TryGetParameterValueBool("centreOnRoot", ref centreOnRoot);

				switch (colliderType) {
					case "capsule": {
						m_colliderType = EColliderType.Capsule;
						CapsuleCollider capsuleCollider = gameObject.AddComponent<CapsuleCollider>();
						capsuleCollider.radius = radius;
						capsuleCollider.height = height;
						capsuleCollider.center = centreOnRoot ? new Vector3(0, height / 2.0f, 0) : Vector3.zero;
						m_collider = capsuleCollider;
						break;
					}
					case "sphere": {
						m_colliderType = EColliderType.Sphere;
						SphereCollider sphereCollider = gameObject.AddComponent<SphereCollider>();
						sphereCollider.radius = radius;
						sphereCollider.center = centreOnRoot ? new Vector3(0, radius / 2.0f, 0) : Vector3.zero;
						m_collider = sphereCollider;
						break;
					}
				}

				bool isTriggerParam = true;
				entityEntry.TryGetParameterValueBool("isTrigger", ref isTriggerParam);
				m_collider.isTrigger = isTriggerParam;

			} else {
				Debug.LogError("Init failed on " + ToString() + "! Entry was null");
				Destroy(this);
			}*/

			return m_wediYmgychwyn;
		}
	}
}