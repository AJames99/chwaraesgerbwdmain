using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DataBank;
using DataBank.ItemSystem;
using NodweddionEndid;

namespace CydrannauEndid {
	public class CydranEndidIechyd : CydranEndid, RNodweddionCydran {

		// Component Attributes:
		[SerializeField] GwerthNodwedd<int> m_nodwedd_IechydCyfan;

		public enum MaxHealthModifyBehaviour : byte {
			FullyRestoreOnIncrease,	// If increasing Max HP, refill health entirely
			FullyRestoreOnAny,		// If changing Max HP at all, refill health entirely
			KeepProportion,			// Keep the same %age of health after the change, storing any fractional part in m_fractionalPart
			AddIncreaseToHealth,	// If max hp is increased, increase health by the same amount (e.g. MAX/CURRENT: 10/6 + 4max = 14/10)
			AddAnyChangeToHealth,	// Change health by the same amount as Max HP (Can kill?)
			Clip					// Don't change HP unless Max HP is less than it, in which case clip/clamp
		}
		
		// Regular health cannot go above this, but overheal can be applied
		private int m_maxHealthRaw;
		// Health value; excludes fractional parts and overheal
		private int m_healthRaw;
		// Represents the non-integral part of health; keeps HP integral but allows non-integral changes over time etc.
		private float m_fractionalPartRaw;
		// Guarded bool to totally prevent "isDead but health > 0"
		private bool m_isDeadRaw;
		// Represents excess health
		private int m_overHealRaw;

		CydranEndidNodweddion m_attributeComponent;

		// Decides how current HP is handled when changing Max HP
		[SerializeField] MaxHealthModifyBehaviour maxHealthModBehaviour = MaxHealthModifyBehaviour.Clip;
		// Determines whether or not a change in max HP could potentially kill (If false, HP can only be reduced as low as 1) (No effect with some Max HP modify behaviours)
		[SerializeField] bool m_maxHPChangeCanKill = false;

		// Won't trigger OnHealed or OnDamaged, but does do the Death Check
		protected int m_maxHealth {
			get { return m_healthRaw; }
			set { OnAssignMaxHealth(value); }
		}

		protected bool m_isDead {
			get { return m_isDeadRaw; }
			set { OnAssignIsDead(value); }
		}

		protected int m_health{
			get { return m_healthRaw; }
			set { OnAssignHealth(value); }
		}

		protected float m_fractionalPart {
			get { return m_fractionalPartRaw; }
			set { OnAssignFractionalHealth(value); }
		}

		protected float m_healthDecimal {
			get { return m_health + m_fractionalPart; }
		}

		public override void Reset() {
			base.Reset();
			CheckDead();
		}

		public override EMathCydran CaelMathCydran() { return EMathCydran.Health; }

		public override bool Init(ref List<string> issueStrings) {
			if (m_wediYmgychwyn)
				return true;
			m_wediYmgychwyn = true;

			GwirioGwasanaethau();

			issueStrings = null;

			m_attributeComponent = m_gwasanaethau.GetEntityComponent<CydranEndidNodweddion>();

			if (m_attributeComponent != null) {
				// Attributes component must be initialised. 
				if (!m_attributeComponent.m_wediYmgychwyn) {
					Debug.Log("HealthComponent could not be initialised yet; AttributeComponent must be initialised first");
					m_wediYmgychwyn = false;
					return m_wediYmgychwyn;
				}

				AdioNodweddionCydran(m_attributeComponent);

			} else {
				m_wediYmgychwyn = false;
				Debug.LogError("Init failed on " + ToString() + "! Entry or AttributeComponent was null");
				Destroy(this);
			}

			return m_wediYmgychwyn;
		}

		public bool AdioNodweddionCydran(CydranEndidNodweddion attributeComponent) {
			if (attributeComponent != null) {
				//NodweddEndidInt nodIechydCyfan = NodweddEndidInt.CreateEntityAttributeFromData("IechydCyfan", m_nodwedd_IechydCyfan);
				//attributeComponent.AdioNodwedd(nodIechydCyfan);
				attributeComponent.CeisioAdioNodwedd("IechydCyfan", ref m_nodwedd_IechydCyfan);
				return true;
			}
			return false;
		}

		public void AdioAdalwadNodwedd(CydranEndidNodweddion cydNod) {
			cydNod.AdioAdalwadNewidNodweddion("IechydCyfan", OnMaxHPAttributeChanged);
		}

		public void DileuAdalwadNodwedd() {
			m_attributeComponent.DileuAdalwadNewidNodweddion("IechydCyfan", OnMaxHPAttributeChanged);
		}

		private void OnDamaged() {
			// 
		}

		private void OnHealed() {
			//
		}

		private void OnDeath() {
			//
		}

		// By default, set health to max HP.
		private void OnResurrect() {
			FullyRestoreHealth();
		}


		private void CheckDead() {
			if (m_maxHealth <= 0) {
				m_isDead = true;
			}
		}

		// ----------------------
		// Outward facing methods
		// ----------------------
		public bool IsDead() {
			return m_isDead;
		}

		// Tries to set a health value, returns the difference in health as a result of the operation
		public int SetHealth(int health) {
			int deltaHealth = SetHealthSilent(health);

			if (deltaHealth > 0) {
				OnHealed();
			} else if (deltaHealth < 0) {
				OnDamaged();
			}

			return deltaHealth;
		}

		// Tries to set a health value including fractional part, returns same difference as SetHealth(int)
		public float SetHealth(float health) {
			// Including fractional part
			float oldHealth = m_healthDecimal;

			int healthIntegral;
			float healthFractional;
			health.GetIntegralFractional(out healthFractional, out healthIntegral);
			
			// Defer to SetHealth
			SetHealth(healthIntegral);
			// Setting raw should be fine since modifying the property will do checks we've already done here
			m_fractionalPartRaw = healthFractional;

			return oldHealth - m_healthDecimal;
		}

		// Tries to set a health value without triggering OnHeal or OnDamaged
		public int SetHealthSilent(int health) {
			int oldHealth = m_health;
			m_health = health;

			// The true delta; the resulting difference after checks & restrictions are applied
			int deltaHealth = health - oldHealth;

			return deltaHealth;
		}

		// Sets health to max
		public void FullyRestoreHealth() {
			// OnAssign handlers will do the rest
			m_health = m_maxHealth;
		}


		// ---------------------------------------
		// Behaviour for setting restrained values (Don't touch the properties! Only the raws)
		// ---------------------------------------
		private void OnAssignHealth(int health) {
			// Negative values aren't allowed
			health = Mathf.Max(health, 0);

			// Resurrection is not allowed via SetHealth et al; don't update anything
			if (health > 0 && m_isDead) {
				return;
			}

			// Don't allow overheal via SetHealth et al; restrict range
			EnforceMaximumHealth();

			CheckDead();
		}

		private void OnAssignMaxHealth(int maxHealth) {
			// Changing Max HP is pretty much always permitted, but the behaviour it uses may not always apply if dead etc.

			// Values < 1 aren't allowed.
			maxHealth = Mathf.Max(maxHealth, 1);

			// Nothing should be done if the values are identical
			if (maxHealth == m_maxHealth) {
				return;
			}

			bool isIncreasing = m_maxHealth < maxHealth;
			int oldMaxHealth = m_maxHealth;
			m_maxHealthRaw = maxHealth;

			switch (maxHealthModBehaviour) {
				case MaxHealthModifyBehaviour.FullyRestoreOnIncrease: {
					if (isIncreasing) {
						FullyRestoreHealth();
					}
					break;
				}
				case MaxHealthModifyBehaviour.FullyRestoreOnAny: {
					FullyRestoreHealth();
					break;
				}
				case MaxHealthModifyBehaviour.KeepProportion: {
					// Keep the same proportion
					float healthProportionOld = (m_fractionalPart + m_health) / oldMaxHealth;
					float healthNew = m_maxHealth * healthProportionOld;
					SetHealth(healthNew);
					break;
				}
				case MaxHealthModifyBehaviour.AddIncreaseToHealth: {
					// Offset health by the same additive increase to Max HP 
					if (isIncreasing) {
						int deltaMaxHP = oldMaxHealth - m_maxHealth;
						SetHealthSilent(m_health + deltaMaxHP);
					}
					break;
				}
				case MaxHealthModifyBehaviour.AddAnyChangeToHealth: {
					// Offset health by the same additive change to Max HP
					int deltaMaxHP = oldMaxHealth - m_maxHealth;

					// Health after offset:
					int offsetHealth = m_health + deltaMaxHP;

					// Only if this flag is true can health be reduced to 0 via max health change.
					if (m_maxHPChangeCanKill) {
						offsetHealth = Mathf.Max(offsetHealth, 1);
					}
					SetHealthSilent(offsetHealth);
					break;
				}
				case MaxHealthModifyBehaviour.Clip: {
					// Just keep health within the new 0-to-maxHP bounds
					int clippedHealth = Mathf.Clamp(m_health, m_maxHPChangeCanKill ? 0 : 1, m_maxHealth);
					SetHealthSilent(clippedHealth);
					break;
				}

				default: {
					break;
				}
			}
		}

		private void OnAssignIsDead(bool isDead) {
			if (isDead) {
				// The death is new; update var and trigger handler
				if (!m_isDead) {
					m_isDeadRaw = isDead;
					OnDeath();
				}
			} else {
				// The un-death is new; update var and trigger the second coming.
				if (m_isDead) {
					OnResurrect();
				}
			}
		}

		private void OnAssignFractionalHealth(float fractionalHealth) {
			float fraction = fractionalHealth;
			// Fractional health must be: -1 < X < 1 (naturally)
			// Any excess must be separated and put into the integral health
			if (Mathf.Abs(fractionalHealth) >= 1) { // check this works with floaty pointy fucky wucky precision
				int integral;
				fractionalHealth.GetIntegralFractional(out fraction, out integral);

				m_health += integral;
			}
			m_fractionalPartRaw = fraction;

			// Enforce: intHealth + fracHealth < maxHP
			EnforceMaximumHealth();
		}

		// Simply ensures HP(integral + fractional) is <= max HP
		private void EnforceMaximumHealth() {
			float excessHP = m_maxHealth - m_healthDecimal;
			if (excessHP > 0) {
				m_healthRaw = m_maxHealth;
				m_fractionalPartRaw = 0.0f;
			}
		}

		void OnMaxHPAttributeChanged(NodweddEndid nod) {
			// TODO m_maxHealth could just be an attribute with some callbacks and rules
			m_maxHealth = (nod as NodweddEndidInt).m_gwerthCanlynol.m_gwerth;
		}

		// These are made redundant by the new callback setup
		//void AddAttributeCallbacks() {
		//	NodweddEndidInt maxHPAttr = m_attributeComponent.CaelNodwedd("MaximumHealth") as NodweddEndidInt;
		//	maxHPAttr.m_onValueChangedDelegates += OnMaxHPAttributeChanged;
		//}

		//void RemoveAttributeCallbacks() {
		//	if (m_attributeComponent) {
		//		NodweddEndidInt maxHPAttr = m_attributeComponent.CaelNodwedd("MaximumHealth") as NodweddEndidInt;
		//		maxHPAttr.m_onValueChangedDelegates -= OnMaxHPAttributeChanged;
		//	}
		//}

		public override void OnDestroy() {
			base.OnDestroy();
			DileuAdalwadNodwedd();
		}
	}
}