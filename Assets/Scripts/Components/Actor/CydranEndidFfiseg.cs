using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DataBank;
using NodweddionEndid;
using DataBank.ItemSystem;

namespace CydrannauEndid {
	public class CydranEndidFfiseg : CydranEndid, RNodweddionCydran {
		public override EMathCydran CaelMathCydran() { return EMathCydran.Physics; }

		CydranEndidNodweddion m_cydranNodweddion;

		// Mass
		public int m_m�s = 100;
		// Drag
		public float m_llusgo = 1.0f;
		
		//
		public float m_knockBackMultiplierSelf = 1.0f;
		//
		public float m_knockBackMultiplierOther = 1.0f;

		// Movement speed (ATTR)
		[SerializeField] public GwerthNodwedd<float> m_nodwedd_cyflymderSymudiad = new GwerthNodwedd<float>(10.0f);
		// Friction (ATTR)
		[SerializeField] public GwerthNodwedd<float> m_nodwedd_ffrithiant = new GwerthNodwedd<float>(1.4f);

		public override bool Init(ref List<string> issueStrings) {
			if (m_wediYmgychwyn)
				return true;
			m_wediYmgychwyn = true;
			issueStrings = new List<string>();

			m_cydranNodweddion = m_gwasanaethau.GetEntityComponent<CydranEndidNodweddion>();

			if (m_cydranNodweddion != null) {
				// Attributes component must be initialised. 
				if (!m_cydranNodweddion.m_wediYmgychwyn) {
					issueStrings.Add("PhysicsComponent could not be initialised yet; AttributeComponent must be initialised first");
					return false;
				} else {
					AdioNodweddionCydran(m_cydranNodweddion);
				}

			} else {
				issueStrings.Add("Init failed on " + ToString() + "! Attribute component was null");
				Destroy(this);
			}

			return m_wediYmgychwyn;
		}

		public bool AdioNodweddionCydran(CydranEndidNodweddion cydNod) {
			bool bPass =	cydNod.CeisioAdioNodwedd("CyflymderSymudiad", ref m_nodwedd_cyflymderSymudiad);
			bPass &=		cydNod.CeisioAdioNodwedd("Ffrithiant", ref m_nodwedd_ffrithiant);
			return bPass;
		}
	}
}