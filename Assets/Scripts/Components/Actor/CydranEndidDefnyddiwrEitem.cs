#define DEBUG_ITEM_USER

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CydrannauEndid {

	// Physical slots on an entity - not the inventory, more like "item ports" which items can be equipped to.
	[System.Serializable]
	public class ItemUserSlot {

		// Items and slots both have equip regions.
		// In order for an item to be equippable in a particular slot, the slot and item must share
		// at least 1 equip region. If no equip regions are defined, it should be treated as unrestricted ("Any").
		[System.Serializable]
		public enum EquipRegion {
			ANNILYS = -1,
			Pen = 0,
			Llygaid,
			Corff,
			Cefn,
			Coesau,
			Traed,
			Tarian,
			Erfyn,
			Eitemdefnyddiadwy,
		};

		// Used to map slots to things like art (place helmet on head bone for the "Head" slot etc.)
		public enum SlotType {
			Pen,
			Corff,
			Dwylo,
			Coesau,
			Traed,
			Cefn
		};

		public enum SlotAvailability {
			NoSlotsAvailable = 0,		// There is no way for the item to be equipped
			EmptySlotsAvailable,		// There are empty slots that can take this item
			OccupiedSlotsAvailable,		// There are occupied slots that could accept this item if the current item is moved out
		};

		[SerializeField, InspectorName("Slot Name")] public string m_sSlotName;
		[SerializeField, InspectorName("Slot Bone")] public SlotType m_slotType;

		// Describes the items that can go into this slot; empty means any.
		[SerializeField] public List<EquipRegion> m_equipRegions;// { get; protected set; }

		// The item currently equipped to this slot
		public Eitem m_eitem { get; private set; }

		/// <summary>
		/// Returns whether this slot can take the provided item.
		/// </summary>
		/// <param name="eitem"></param>
		/// <param name="ignoreOccupancy">Whether to ignore whether there's an item in the slot or not</param>
		/// <returns></returns>
		public bool YnDerbynEitem(Eitem eitem, bool ignoreOccupancy = true) {
			// Either the slot or item has no restrictions
			if ((eitem.m_equipRegions != null ? eitem.m_equipRegions.Count : 0) < 1 || (m_equipRegions != null ? m_equipRegions.Count : 0) < 1)
				return ignoreOccupancy || m_eitem == null;

			foreach (EquipRegion region in eitem.m_equipRegions) {
				if (m_equipRegions.Contains(region)) {
					if (!(ignoreOccupancy || m_eitem == null)) {
						return false;
					}
						
					return true;
				}
			}
			return false;
		}

		// Is this slot occupied?
		public bool YdyLlawn() { return m_eitem != null; }

		public void RhowchEitem(Eitem eitem, CydranEndidDefnyddiwrEitem defnyddiwr) {
			// Notify the existing item of its removal			
			if (m_eitem != null) {
				Debug.Log("Removed item " + m_eitem.m_enwEitem + " from slot " + m_sSlotName);
				
				// Try swapping to the next valid slot
				if(!m_eitem.TrySwitchSlot(defnyddiwr, this)) {
					m_eitem.ArDynnu(defnyddiwr);
				}
				Debug.Log("...and replaced with item " + eitem.m_enwEitem + " in slot " + m_sSlotName);
			} else {
				Debug.Log("Added item " + eitem.m_enwEitem + " to slot " + m_sSlotName);
			}

			m_eitem = eitem;
			m_eitem.ArRhoi(defnyddiwr, this);
		}

		public void TynnwchEitem(CydranEndidDefnyddiwrEitem defnyddiwr) {
			if (m_eitem != null) {
				Debug.Log("Removed item " + m_eitem.m_enwEitem + " from slot " + m_sSlotName);
				m_eitem.ArDynnu(defnyddiwr);
			}
		}
	}

	public class CydranEndidDefnyddiwrEitem : CydranEndid {
		// Equip slots.
		[SerializeField] public List<ItemUserSlot> m_slots = new List<ItemUserSlot>();

		// Items to equip (on spawn)
		[SerializeField] public List<Eitem> m_eitemauIwRhoi = new List<Eitem>();

		public override EMathCydran CaelMathCydran() { return EMathCydran.ItemUser; }

		public override bool Init(ref List<string> issueStrings) {
			if (m_wediYmgychwyn)
				return true;
			m_wediYmgychwyn = true;
			issueStrings = new List<string>();

			// Sort such that the most restricted items are (attempted to be) fitted first
			m_eitemauIwRhoi.Sort(new SortItemByRestriction());
			m_eitemauIwRhoi.Reverse();

			List<Eitem> itemsPendingEquip = new List<Eitem>(m_eitemauIwRhoi);
			foreach (Eitem eitem in itemsPendingEquip) {
				if (CeisioRhoiEitem(eitem)) {
					m_eitemauIwRhoi.Remove(eitem);
				}
			}

			return m_wediYmgychwyn;
		}

		/// <summary>
		/// Attempts to equip an item.
		/// </summary>
		/// <param name="eitem">The item to equip</param>
		/// <param name="symudEraill">If true, will move other items to other acceptable slots to make space for this item, if this item cannot go in any slots without doing so. This will not currently move the item to another occupied slot, due to Crazy Recursion being needed for that</param>
		/// <returns></returns>
		public bool CeisioRhoiEitem(Eitem eitem, bool symudEraill = false) {
			// TODO implement lowest-acceptance prioritising: Equip to the least accepting slot (e.g. shoes will go to the shoe-only slot, rather than the "any item" slot)
			ItemUserSlot slotToEquipTo = GetSlotForItem(eitem, false, true);
			if (slotToEquipTo != null) {
				slotToEquipTo.RhowchEitem(eitem, this);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Returns whether or not there are acceptable slots for the provided item.
		/// </summary>
		/// <param name="eitem">The item in question</param>
		/// <param name="excludeItemsCurrentSlot">Whether to ignore the item's current slot (if equipped to this user)</param>
		/// <returns></returns>
		public bool HasSlotsForItem(Eitem eitem, out ItemUserSlot.SlotAvailability availability, bool excludeItemsCurrentSlot = true) {
			availability = ItemUserSlot.SlotAvailability.NoSlotsAvailable;
			foreach (ItemUserSlot slot in m_slots) {
				// (if applicable) If this slot is currently holding this item, skip
				if (excludeItemsCurrentSlot && slot.m_eitem == eitem) {
					continue;
				}

				// If this slot could take this item, ignore occupancy
				if (slot.YnDerbynEitem(eitem, true)) {
					if (slot.YdyLlawn()) {
						// Slot is occupied
						availability = ItemUserSlot.SlotAvailability.OccupiedSlotsAvailable;
					} else { 
						// Slot is empty and accepts us
						availability = ItemUserSlot.SlotAvailability.EmptySlotsAvailable;
						return true;
					}
				}
			}

			// Checked all slots, no empty was found - but was an occupied accepting slot found?
			if (availability == ItemUserSlot.SlotAvailability.OccupiedSlotsAvailable) {
				return true;
			}

			return false;
		}

		// CURRENTLY IGNOREOCCUPANCY IS UNIMPLEMENTED!!
		// checkForShufflableEquippedItems - if no valid slot is found, check if we can move any items in any valid slots to other slots
		//		(note: this only shuffles to EMPTY valid slots for the existing/slotted item)
		public ItemUserSlot GetSlotForItem(Eitem eitem, bool ignoreOccupancy = false, bool prioritiseHighestRestriction = true, bool checkForShufflableEquippedItems = true) {
			/*foreach (ItemUserSlot slot in m_slots) {
				// Does this slot accept the item? Regardless of occupancy
				if (slot.YnDerbynEitem(eitem, true)) {
					bool moveSuccessful = false;
					// Is the slot occupied, and do we want to move that currently equipped item?
					if (slot.YdyLlawn() && ignoreOccupancy) {
						// Is there an EMPTY slot available for the item we want to move?
						ItemUserSlot.SlotAvailability availability;
						if (HasSlotsForItem(slot.m_eitem, out availability, true)) {
							if (availability == ItemUserSlot.SlotAvailability.EmptySlotsAvailable) {
								// Move the equipped item to another slot
								RhowchEitem(slot.m_eitem, false);
								moveSuccessful = true;
							}
						}
					}

					if (moveSuccessful) {
						// Old item was successfully moved elsewhere, equip this one now.
						RhowchEitem(eitem, slot);
					}
				}
			}*/

			if (!prioritiseHighestRestriction) {
				foreach (ItemUserSlot slot in m_slots) {
					if (slot.YnDerbynEitem(eitem, false)) {
						return slot;
					}
				}
			} else {
				int highestRestrictionCount = -1;
				ItemUserSlot mostRestrictedSlot = null;
				foreach (ItemUserSlot slot in m_slots) {
					if (slot.YnDerbynEitem(eitem, false)) {
						int slotCount = slot.m_equipRegions == null ? 0 : slot.m_equipRegions.Count;
						if (slotCount > highestRestrictionCount) {
							highestRestrictionCount = slotCount;
							mostRestrictedSlot = slot;
						}
					}
				}

				// Didn't find a slot, do a second pass to see if any items equipped in slots valid for THIS item could be moved to other slots
				/*if (mostRestrictedSlot == null) {
					foreach (ItemUserSlot slot in m_slots) {
						if (slot.YnDerbynEitem(eitem, false)) {
							if(slot.m_eitem != null) {
								if(GetSlotForItem())
							}
						}
					}
				}*/

				return mostRestrictedSlot;
			}
			return null;
		}

		public void RhowchEitem(Eitem eitem, bool symudEraill = false) {
			ItemUserSlot slot = GetSlotForItem(eitem, symudEraill);
			slot.RhowchEitem(eitem, this);
		}

		// Only works if the slot is empty
		public void RhowchEitem(Eitem eitem, ItemUserSlot slot) {
			if (!slot.YdyLlawn()) {
				slot.RhowchEitem(eitem, this);
			}
		}

#if DEBUG_ITEM_USER
		const int kPadding = 20;
		private void DrawDebugDataBox(int xPos, ref int yPos, int width, int height, string text) {
			GUI.Box(new Rect(xPos, yPos, width, height),
				text);
			yPos += height + kPadding;
		}

		public void OnGUI() {
			int yPos = 300;
			foreach (ItemUserSlot slot in m_slots) {
				DrawDebugDataBox(50, ref yPos, 300, 60, slot.m_sSlotName + "(" + slot.m_slotType.ToString() + ") : " + slot.m_eitem);
			}
		}
#endif
	}
}