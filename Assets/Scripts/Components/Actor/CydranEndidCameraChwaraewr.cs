
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using CydrannauEndid;

namespace CydrannauEndid {
	public class CydranEndidCameraChwaraewr : CydranEndid {

		// Used as the "base" camera transform - only things like look will affect the rotation, not camera shake etc.
		Transform m_cameraBaseTransform;

		// Things like camera shake can be applied to this to affect all cameras (including UI, First-person cameras)
		Transform m_mainCameraTransform;

		Transform m_characterCameraBone;

		Animator m_cameraAnimator;

		AudioListener m_audioListener;

		Camera m_mainCamera;
		Camera m_firstPersonCamera;
		Camera m_UICamera;

		[SerializeField]
		float m_yawOffset;

		[SerializeField]
		float m_pitchOffset;

		[SerializeField]
		float m_FOV = 90.0f;

		[SerializeField]
		float m_verticalOffset;

		string[] mainCameraExcludedLayers = new string[] {
			"UI",
			"FirstPerson",
			"UIWorldSpace",
		};
		
		public override EMathCydran CaelMathCydran() { return EMathCydran.PlayerCamera; }

		public override bool Init(ref List<string> issueStrings) {
			if (m_wediYmgychwyn) {
				return true;
			}

			issueStrings = new List<string>();

			CydranEndidCelfCymeriad characterArtComponent;
			if (characterArtComponent = m_gwasanaethau.GetEntityComponent<CydranEndidCelfCymeriad>()) {
				if (!characterArtComponent.m_wediYmgychwyn) {
					return m_wediYmgychwyn;
				}

				m_characterCameraBone = characterArtComponent.m_cameraBoneTransform;
			}

			m_wediYmgychwyn = true;



			m_FOV = Mathf.Clamp(m_FOV, 1, 180);

			if (m_cameraBaseTransform == null) {
				if (m_characterCameraBone != null) {
					m_cameraBaseTransform = GameProcessManager.InstantiateEmpty("Player Camera Base", m_characterCameraBone).transform;
				} else {
					m_cameraBaseTransform = GameProcessManager.InstantiateEmpty("Player Camera Base", transform).transform;
					m_cameraBaseTransform.localPosition = m_cameraBaseTransform.localPosition.WithY(m_verticalOffset);
				}
			}

			if (m_mainCameraTransform == null) {
				m_mainCameraTransform = GameProcessManager.InstantiateEmpty("Main Camera", m_cameraBaseTransform).transform;
			}

			if (m_mainCamera == null && m_mainCameraTransform != null) {
				m_mainCamera = m_mainCameraTransform.gameObject.AddComponent<Camera>();
				if (Camera.main != null) {
					Camera.main.tag = "Untagged";
				}
				m_mainCamera.tag = "MainCamera";
				UniversalAdditionalCameraData URPCameraData = m_mainCamera.GetUniversalAdditionalCameraData();
				URPCameraData.renderPostProcessing = true;
				URPCameraData.stopNaN = false;
				m_mainCamera.cullingMask = ~LayerMask.GetMask(mainCameraExcludedLayers);
				m_mainCamera.fieldOfView = m_FOV;
			}


			return m_wediYmgychwyn;
		}

		public void SetCameraPitch(float pitch) {
			m_cameraBaseTransform.localRotation = Quaternion.Euler(Vector3.right * (pitch + m_pitchOffset));
		}

		public Camera GetMainCamera() {
			return m_mainCamera;
		}

		public Transform GetBaseCameraTransform() {
			return m_cameraBaseTransform;
		}
	}
}