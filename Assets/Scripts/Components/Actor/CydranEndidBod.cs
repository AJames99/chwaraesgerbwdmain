using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CydrannauEndid {
	public class CydranEndidBod : CydranEndid {
		// Types
		// IF THIS COMPILES I WILL SHIT MYSELF
		public enum T�mBod {
			None = 0,
			Player,
			EnemyGroup1
		}
		//public static Dictionary<ActorTeam, string> actorTeamStrings = new Dictionary<ActorTeam, string>() {
		//	{ ActorTeam.None, "None" },
		//	{ ActorTeam.Player, "Player" },
		//	{ ActorTeam.EnemyGroup1, "EnemyGroup1" }
		//};
		//public static ActorTeam TeamFromString(string str, out bool bSuccess) {
		//	bSuccess = false;
		//	foreach (ActorTeam team in actorTeamStrings.Keys) {
		//		if (actorTeamStrings[team].Equals(str)) {
		//			bSuccess = true;
		//			return team;
		//		}
		//	}
		//	return ActorTeam.None;
		//}
		
		// Public Methods
		#region publicMethods
		public T�mBod CaelTiim() { return m_tiim; }
		public bool YdyCynghreiriad(T�mBod tiimBodArall) {
			if (tiimBodArall == T�mBod.None || m_tiim == T�mBod.None) {
				return false;
			}
			
			return m_tiim == tiimBodArall;
		}
		#endregion

		// Private Vars
		#region privateVars
		[SerializeField] private T�mBod m_tiim = 0;
		#endregion

		public override EMathCydran CaelMathCydran() { return EMathCydran.Actor; }

		public override bool Init(ref List<string> issueStrings) {
			if (m_wediYmgychwyn)
				return true;

			issueStrings = null;
			m_wediYmgychwyn = true;

			return m_wediYmgychwyn;
		}

		//public override System.Type[] GetInitPrerequisites() { return new System.Type[] { typeof(AttributeComponent) }; }
	}
}