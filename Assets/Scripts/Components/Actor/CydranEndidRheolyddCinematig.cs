#define ENABLE_REDUCED_CONTROL_AT_HIGH_SPEED
//#define ENABLE_DEBUG


using System.Collections.Generic;
using System.Collections;
using UnityEngine.InputSystem;
using UnityEngine;
using CydrannauEndid;
using NodweddionEndid;
using UnityEditor;

namespace CydrannauEndid {
#if ENABLE_DEBUG
	[CustomEditor(typeof(CydranEndidRheolyddCinematig))]
	public class KinematicControllerComponentEditor : Editor {
		public override void OnInspectorGUI() {
			//base.OnInspectorGUI();
			DrawDefaultInspector();

			CydranEndidRheolyddCinematig kinematicController = (CydranEndidRheolyddCinematig)target;
			if (kinematicController) {
				if (kinematicController.m_pauseFixedUpdate) {
					if (GUILayout.Button("Step FixedUpdate")) {
						kinematicController.StepFixedUpdate();
					}
				}
			}
		}
	}
#endif

	public class CydranEndidRheolyddCinematig : CydranEndid, ICharacterController, RNodweddionCydran {
		// Types -----

		// Global constants -----
		private const float kGravityAmount = 24.0f;

		// Public Methods/Accessors -----
		#region publicMethods
		public ICharacterController.ControllerType GetControllerType() {
			return ICharacterController.ControllerType.Kinematic;
		}

		public Vector3 GetVelocity() {
			return m_velocity;
		}

		public void SetVelocity(Vector3 velocity) {
			m_velocity = velocity;
		}

		// TODO check this against actual physics equations; timeless impulses are weird
		public void ApplyImpulse(Vector3 forceVector, bool ignoreMass = false) {
			if (ignoreMass) {
				// F = a
				m_velocity += forceVector;
			} else {
				// F = m * a
				Vector3 acceleration = forceVector / m_physicsComponent.m_m�s;
				m_velocity += acceleration;
			}

			// Set this to true to allow impulses to un-ground us.
			// May need some more testing to figure out if this can result in undesired behaviour
			m_isBeingLaunched = true;
		}

		#endregion

		// Editor Debug Fields
#if ENABLE_DEBUG
		// Is the fixedupdate currently paused?
		[InspectorName("Enable FixedUpdate stepping")] public bool m_pauseFixedUpdate = false;
		// Should the paused fixedupate step 1 update?
		[HideInInspector] public bool m_stepFixedUpdate = false;
		[InspectorName("Show collision debug info")] public bool m_showCollisionDebug = true;

		public struct DebugRay {
			public DebugRay(Vector3 pos, Vector3 dir) { position = pos; direction = dir; }
			public Vector3 position;
			public Vector3 direction;
		}

		private DebugRay?[] m_collisionNormalRays = new DebugRay?[32];
		private DebugRay? m_velocityBeforeCollisions = null;
		private DebugRay? m_velocityAfterCollisions = null;
		private DebugRay? m_displacementBeforeCollisions = null;
		private DebugRay? m_displacementAfterCollisions = null;
		private int m_collisionNormalRayCount = 0;
		private Vector3 m_capsulePositionDebug = Vector3.zero;
		private Vector3 m_capsuleSweepDir = Vector3.zero;

		public void StepFixedUpdate() {
			m_stepFixedUpdate = true;
		}

		private void DrawRay(DebugRay? ray, Color color) {
			if (ray != null) {
				DebugRay nonNullRay = (DebugRay)ray;
				Debug.DrawRay(nonNullRay.position, nonNullRay.direction, color);
			}
		}

		void UpdateDebugRayPositions() {
			if (m_velocityAfterCollisions.HasValue) {
				m_velocityAfterCollisions = new DebugRay(m_velocityAfterCollisions.Value.position + m_capsulePositionDebug, m_velocityAfterCollisions.Value.direction);
			}
			if (m_velocityBeforeCollisions.HasValue) {
				m_velocityBeforeCollisions = new DebugRay(m_velocityBeforeCollisions.Value.position + m_capsulePositionDebug, m_velocityBeforeCollisions.Value.direction);
			}
			if (m_displacementAfterCollisions.HasValue) {
				m_displacementAfterCollisions = new DebugRay(m_displacementAfterCollisions.Value.position + m_capsulePositionDebug, m_displacementAfterCollisions.Value.direction);
			}
			if (m_displacementBeforeCollisions.HasValue) {
				m_displacementBeforeCollisions = new DebugRay(m_displacementBeforeCollisions.Value.position + m_capsulePositionDebug, m_displacementBeforeCollisions.Value.direction);
			}
			m_collisionNormalRayCount = Mathf.Min(m_collisionNormalRayCount, m_collisionNormalRays.Length);
			for (int i = 0; i < m_collisionNormalRayCount; i++) {
				if (m_collisionNormalRays[i] != null) {
					DebugRay ray = (DebugRay)m_collisionNormalRays[i];
					m_collisionNormalRays[i] = new DebugRay(ray.position + m_capsulePositionDebug, ray.direction);
				}
			}
		}
#endif

		public override EMathCydran CaelMathCydran() { return EMathCydran.KinematicCharacterController; }

		public override bool Init(ref List<string> issueStrings) {
			if (m_wediYmgychwyn)
				return true;
			issueStrings = new List<string>();
			m_wediYmgychwyn = true;

			m_attributeComponent = m_gwasanaethau.GetEntityComponent<CydranEndidNodweddion>();
			m_physicsComponent = m_gwasanaethau.GetEntityComponent<CydranEndidFfiseg>();
			CydranEndidGwrthdarydd colliderComponent = m_gwasanaethau.GetEntityComponent<CydranEndidGwrthdarydd>();
			m_wediYmgychwyn &= m_attributeComponent.m_wediYmgychwyn;

			AdioNodweddionCydran(m_attributeComponent);

			m_colliderType = colliderComponent.m_colliderType;
			m_collider = colliderComponent.m_collider;

			// Grounding rays should be based around the bottom of the collider.
			// For a buffer of distance, offset upwards by half the ray max distance
			switch (m_colliderType) {
				case CydranEndidGwrthdarydd.EColliderType.Sphere: {
						SphereCollider sphereCollider = m_collider as SphereCollider;
						kGroundingRayGroupOffset = sphereCollider.center.y - sphereCollider.radius + (kGroundingRayDistanceCheck / 2);
						kGroundingRayGroupSpread = sphereCollider.radius;
						break;
					}
				case CydranEndidGwrthdarydd.EColliderType.Capsule: {
						CapsuleCollider capsuleCollider = m_collider as CapsuleCollider;
						kGroundingRayGroupOffset = capsuleCollider.center.y - (capsuleCollider.height / 2) + (kGroundingRayDistanceCheck / 2);
						kGroundingRayGroupSpread = capsuleCollider.radius;
						break;
					}
			}

			m_groundingRayPositions = new Vector3[] {
				new Vector3(0, kGroundingRayGroupOffset, 0),
				new Vector3(kGroundingRayGroupSpread, kGroundingRayGroupOffset, 0),
				new Vector3(-kGroundingRayGroupSpread, kGroundingRayGroupOffset, 0),
				new Vector3(0, kGroundingRayGroupOffset, kGroundingRayGroupSpread),
				new Vector3(0, kGroundingRayGroupOffset, -kGroundingRayGroupSpread),
			};

			kExcludedLayersFromPhysics = LayerMask.GetMask(kExcludedLayerNames);

			m_playerCameraComponent = m_gwasanaethau.GetEntityComponent<CydranEndidCameraChwaraewr>();
			m_cameraTransform = m_playerCameraComponent.GetBaseCameraTransform();
			m_characterArtComponent = m_gwasanaethau.GetEntityComponent<CydranEndidCelfCymeriad>();

			m_characterArtComponent.m_animationEventCallbacksInt["SetFooting"] += OnSetFooting;

			return m_wediYmgychwyn;
		}

		//public void AdioNodweddionCydran(DataCydranEndid entityEntry, CydrannauEndid.CydranEndidNodweddion attributeComponent) {
		//	DataCydranIechyd cydIechyd = entityEntry as DataCydranIechyd;
		//	if (cydIechyd != null && attributeComponent != null) {
		//		EntityAttributeInt nodIechydCyfan = EntityAttributeInt.CreateEntityAttributeFromData("IechydCyfan", cydIechyd.m_nodwedd_IechydCyfan);
		//		attributeComponent.AddAttribute(nodIechydCyfan);
		//	}
		//}

		public bool AdioNodweddionCydran(CydranEndidNodweddion cydNod) {
			bool bPass = cydNod.CeisioAdioNodwedd("CyfernodCyflymiadAwyr", ref m_nodwedd_cyfernodCyflymiadAwyr);
			bPass &= cydNod.CeisioAdioNodwedd("CyfernodArafuAwyr", ref m_nodwedd_cyfernodArafuAwyr);
			bPass &= cydNod.CeisioAdioNodwedd("ManylrwyddRheolaethAwyr", ref m_nodwedd_manylrwyddRheolaethAwyr);
			bPass &= cydNod.CeisioAdioNodwedd("CryfderNaid", ref m_nodwedd_cryfderNaid);
			bPass &= cydNod.CeisioAdioNodwedd("FfactorSbonc", ref m_nodwedd_ffactorSbonc);
			bPass &= cydNod.CeisioAdioNodwedd("FfactorFfrithiantSglefrioTarian", ref m_nodwedd_ffactorFfrithiantSglefrioTarian);

			// We want to assign these to the existing attribute if there is one, otherwise the attribute's default according to the databank
			cydNod.CaelGwerthNodwedd("Ffrithiant", ref m_nodwedd_ffrithiant);
			cydNod.CaelGwerthNodwedd("GalluSglefrioTarian", ref m_nodwedd_galluSglefrioTarian);

			return bPass;
		}

		//public void AdioAdalwadNodwedd(CydranEndidNodweddion cydNod) {
		//
		//}
		//
		//public void DileuAdalwadNodwedd() {
		//	
		//}

		#region KinematicController
		// Component Attributes: (Defaults are set; should be updated via the callback functions registered above)
		[SerializeField] GwerthNodwedd<float> m_nodwedd_cyfernodCyflymiadAwyr = new GwerthNodwedd<float>(3.0f);
		[SerializeField] GwerthNodwedd<float> m_nodwedd_cyfernodArafuAwyr = new GwerthNodwedd<float>(8.0f);
		[SerializeField] GwerthNodwedd<float> m_nodwedd_manylrwyddRheolaethAwyr = new GwerthNodwedd<float>(8.0f);
		[SerializeField] GwerthNodwedd<float> m_nodwedd_cryfderNaid = new GwerthNodwedd<float>(10.0f);
		[SerializeField] GwerthNodwedd<float> m_nodwedd_ffactorSbonc = new GwerthNodwedd<float>(0.5f);
		[SerializeField] GwerthNodwedd<float> m_nodwedd_ffactorFfrithiantSglefrioTarian = new GwerthNodwedd<float>(0.1f);

		// Other attributes (From the attributes component)
		GwerthNodwedd<bool> m_nodwedd_galluSglefrioTarian = new GwerthNodwedd<bool>(false);
		GwerthNodwedd<float> m_nodwedd_ffrithiant = new GwerthNodwedd<float>(1.0f);

		// Constants ------------
		// If moving under this speed, apply effectively infinite friction to halt the player immediately
		private const float kFrictionSpeedThresholdStop = 0.1f;

		// Apply a friction multiplier if travelling under this speed horizontally
		private const float kIncreaseFrictionSpeedThreshold = 15.0f;
		private const float kIncreaseFrictionFactor = 8.0f;

		// The proportion of the base (forward) movement speed we must be travelling to initiate a shield slide
		private const float kShieldSlideSpeedMinimumFactor = 1.5f;
		// While shield sliding, if our speed drops to this proportion of base movement speed we will stop sliding
		private const float kShieldSlideStopSpeedMinimumFactor = 1.0f;

		// Attenuate our control down when at high speeds
#if ENABLE_REDUCED_CONTROL_AT_HIGH_SPEED
		// As speed goes from the X component to the Y component, the multiplier's influence goes from 0 to kReducedControlMultiplier_Factor
		private readonly Vector2 kReducedControlMultiplier_SpeedBounds = new Vector2(15.0f, 20.0f);
		// If 1, control will be eliminated if trying to accelerate opposite to velocity (press S while moving forward).
		// Reducing this value will increase the control factor when decelerating like this.
		private const float kReducedControlMultiplier_Factor = 0.85f;

		// The range either side of -1 where the dot product will be "rounded up" to -1, when considering the reduced control effect
		private const float kReducedControlMultiplier_DotProdRange = 0.1f;
#endif

		// For grounding, we check downwards from 5 positions (centre, forward, back, left, right - all in local space, but only horizontally (roll and pitch invariant))
		// Grounding ray group centre = this value, downwards from the local origin
		private float kGroundingRayGroupOffset = -0.4f;
		// The distance away from the centre that the 4 directionally offset rays will be
		private float kGroundingRayGroupSpread = 0.4f;
		private Vector3[] m_groundingRayPositions;
		private const float kGroundingRayDistanceCheck = 0.2f; // Also used to calculate ray start pos
		private const float kGroundingRayDistanceCheckBuffer = 0.01f; // True ray check is done using kGroundingRayDistanceCheck + this

		// To achieve DUSK-like camera flipping (i.e. allowing the player's pitch
		// to be unlimited when in midair, and rotating it back into the locked range
		// upon landing)
		private const float kCameraFlipRecoveryRate = 1000.0f; // deg/sec

		private const float kMaxSlopeAngle = 45.0f;

		// TODO test if this needs to be a double - must've made it a double for some reason...
		private const double kSlopePenetrationIgnoreLimit = 0.005;

		private string[] kExcludedLayerNames = new string[] {
			"TransparentFX",
			"Ignore Raycast",
			"Water",
			"UI",
			"FirstPerson",
			"Hitbox",
			"Items",
			"UIWorldSpace",
		};
		private LayerMask kExcludedLayersFromPhysics;

		//private const float kGroundAccelerationCoeff = 250.0f;
		private const float kGroundAccelerationCoeff = 50.0f;

		// Members --------------

#if ENABLE_DEBUG
		Vector3 m_debugWishDir;
#endif

		Vector3 m_velocity;
		CydranEndidCameraChwaraewr m_playerCameraComponent; // TODO create a camera interface and use that to make this applicable to NPCs too
		Transform m_cameraTransform;
		CydranEndidNodweddion m_attributeComponent;
		CydranEndidFfiseg m_physicsComponent;

		// Inputs
		private Vector2 m_lookInput;
		private Vector2 m_moveInput;
		private bool m_isCrouching;

		private float m_pitch = 0;
		private float m_yaw = 0;
		private float m_degreesThisJump = 0; // Used for counting spins

		private float m_shieldSlideTilt = 0.0f;

		private readonly RaycastHit[] m_collisionResults = new RaycastHit[32];
		private readonly Collider[] m_collisionResultsOverlap = new Collider[32];

		// Camera flipping angle counter
		private float m_cameraFlipDegreesToCompensate = 0.0f;

		// Grounding
		private bool m_isGrounded;
		private bool m_isBeingLaunched;
		private bool m_isRamping;

		// Peristence data
		private bool m_isGroundedInPrevFrame;
		private bool m_isGonnaJump;
		private float m_verticalVelocityBeforeLanding = 0.0f;
		private bool m_isShieldSliding;

		// Component Params
		[SerializeField]
		float m_cyflymderCyfanDiofyn = 10.0f;
		[SerializeField]
		float m_cyflymderCyfan�l = 7.0f;
		[SerializeField]
		float m_ychwanegiadYmlaenRheolaethAwyr = 2.0f;
		[SerializeField]
		float m_trothwyRampio = 0.8f;
		[SerializeField]
		float m_cyflymderCyfanTrawiadGlanio = 20.0f;
		[SerializeField]
		float m_lleiafswmCyflymderISboncen = 20.0f;
		[SerializeField]
		float m_shieldSlideAccelerationFactor = 1.6f;

		CydranEndidGwrthdarydd.EColliderType m_colliderType;
		Collider m_collider;

		CydranEndidCelfCymeriad m_characterArtComponent;

		// Animator params
		float m_animator_moveSpeedForward = 0;

#if ENABLE_DEBUG
		const int kPadding = 10;
		private void DrawDebugDataBox(int xPos, ref int yPos, int width, int height, string text) {
			GUI.Box(new Rect(xPos, yPos, width, height),
				text);
			yPos += height + kPadding;
		}

		void OnGUI() {
			Vector3 unitsPerSecond = m_velocity;
			unitsPerSecond.y = 0;
			GUI.Box(new Rect(Screen.width / 2f - 50, Screen.height / 2f + 50, 200, 60),
				("Horizontal speed:\n" + Mathf.Round(unitsPerSecond.magnitude * 100) / 100).ToString());

			int yPos = 300;
			DrawDebugDataBox(50, ref yPos, 150, 40, "Is Grounded: " + m_isGrounded);
			DrawDebugDataBox(50, ref yPos, 150, 40, "Is Ramping: " + m_isRamping);
			DrawDebugDataBox(50, ref yPos, 350, 40, "High Speed Reduced Control Factor: " + m_velocity.magnitude.RemapClamped(kReducedControlMultiplier_SpeedBounds.x, kReducedControlMultiplier_SpeedBounds.y, 0.0f, kReducedControlMultiplier_Factor).ToString());
			DrawDebugDataBox(50, ref yPos, 250, 40, "Degress to Compensate: " + m_cameraFlipDegreesToCompensate);
			DrawDebugDataBox(50, ref yPos, 150, 40, "Is Sliding: " + m_isShieldSliding);

			// Draw horizontal speed as a line
			var mid = new Vector2(Screen.width / 2, Screen.height / 2);
			var v = m_cameraTransform.InverseTransformDirectionHorizontal(m_velocity) * unitsPerSecond.magnitude * 3f;
			if (v.WithY(0).magnitude > 0.0001) {
				Drawing.DrawLine(mid, mid + Vector2.up * -v.z + Vector2.right * v.x, Color.red, 3f);
			}

			// Draw input direction
			var w = m_cameraTransform.InverseTransformDirectionHorizontal(m_debugWishDir) * 100;
			if (w.magnitude > 0.001) {
				Drawing.DrawLine(mid, mid + Vector2.up * -w.z + Vector2.right * w.x, Color.blue, 2f);
			}
		}

		void OnDrawGizmos() {
#if ENABLE_DEBUG
			if (m_pauseFixedUpdate && m_showCollisionDebug) {
				// Draw debug rays
				DrawRay(m_velocityBeforeCollisions, Color.red);
				DrawRay(m_velocityAfterCollisions, new Color(1, 0.5f, 0));
				DrawRay(m_displacementBeforeCollisions, Color.blue);
				DrawRay(m_displacementAfterCollisions, Color.cyan);
				for (int i = 0; i < m_collisionNormalRayCount; i++) {
					if (m_collisionNormalRays[i] != null) {
						DrawRay(m_collisionNormalRays[i], Color.yellow);
					}
				}

				if (m_colliderType == CydranEndidGwrthdarydd.EColliderType.Capsule) {
					Vector3 top, bottom;
					CapsuleCollider capsule = m_collider as CapsuleCollider;
					top = m_capsulePositionDebug + capsule.center + new Vector3(0, (capsule.height / 2) - capsule.radius, 0);
					bottom = m_capsulePositionDebug + capsule.center - new Vector3(0, (capsule.height / 2) - capsule.radius, 0);
					
					Gizmos.color = new Color(Color.red.r, Color.red.g, Color.red.b, 0.5f);
					Gizmos.DrawSphere(top, capsule.radius);
					Gizmos.DrawSphere(bottom, capsule.radius);

					Gizmos.color = new Color(Color.yellow.r, Color.yellow.g, Color.yellow.b, 0.5f);
					Gizmos.DrawSphere(top + m_capsuleSweepDir, capsule.radius);
					Gizmos.DrawSphere(bottom + m_capsuleSweepDir, capsule.radius);
				}
			}
#endif
		}
#endif
		protected void OnSetFooting(int foot) {
			m_characterArtComponent.m_mainAnimator.SetInteger("footing", foot);
		}

		public void Update() {
			// Don't update rotation if the cursor is free
			if (Cursor.lockState != CursorLockMode.Locked)
				m_lookInput = Vector2.zero;

			m_pitch = m_pitch.Wrap180();

			float pitchDelta = m_lookInput.y;

			if (m_cameraFlipDegreesToCompensate > 0) {
				float degDelta = kCameraFlipRecoveryRate * Time.deltaTime;

				// Prevent overcompensation
				if (m_pitch + degDelta > 90.0f || m_pitch + degDelta < 0.0f) {
					m_pitch += degDelta;
					m_cameraFlipDegreesToCompensate -= degDelta;
				} else {
					m_cameraFlipDegreesToCompensate = 0.0f;
				}
			}

			// Clamp pitch if grounded
			if (m_isGrounded && !m_isGonnaJump) {
				// TODO double check what the FUCK this is? both ifs seem to do the same thing as eachother?
				if ((m_pitch > 90f || m_pitch < -90f) && pitchDelta > 0f) {
					m_pitch += pitchDelta;
				} else if (m_pitch + pitchDelta > -87f && m_pitch + pitchDelta < 87f) {
					m_pitch += pitchDelta;
				}
			} else {
				m_pitch += pitchDelta;
			}

			if (!m_isGrounded) {
				m_degreesThisJump += pitchDelta;
			} else {
				m_degreesThisJump = 0; // TODO move this to on-land
			}

			// Invert yaw if upside down
			m_yaw += (m_pitch < 90.0f && m_pitch > -90.0f) ? m_lookInput.x : -m_lookInput.x;

			// Rotate the camera's pitch and the entity's yaw
			m_playerCameraComponent.SetCameraPitch(m_pitch);
			transform.localRotation = Quaternion.Euler(Vector3.up * m_yaw);

			// --- Interp animator values that change different to their reference values
			// Interp slower if no W/S input is being made, to ensure run -> idle is smoother.
			float moveSpeedLerpRate = Mathf.Abs(m_moveInput.y) > 0.1f ? 5.0f : 2.0f;
			m_animator_moveSpeedForward = m_animator_moveSpeedForward.InterpAdditive(m_cameraTransform.InverseTransformDirectionHorizontal(m_velocity).z, moveSpeedLerpRate, Time.deltaTime);

			// Set animator values
			m_shieldSlideTilt = m_shieldSlideTilt.InterpAdditive(m_moveInput.x, 4.0f, Time.deltaTime);
			m_characterArtComponent.m_mainAnimator.SetFloat("moveSpeedHorizontal", m_velocity.ToHorizontal().magnitude);    // XZ movement; disregards vertical (Y) movement.
			m_characterArtComponent.m_mainAnimator.SetFloat("shieldSlideTilt", m_shieldSlideTilt);
			m_characterArtComponent.m_mainAnimator.SetBool("isGrounded", m_isGrounded);
			m_characterArtComponent.m_mainAnimator.SetBool("isSliding", m_isShieldSliding);
			m_characterArtComponent.m_mainAnimator.SetBool("wantsToJump", m_isGonnaJump);                           // i.e. Holding space
			m_characterArtComponent.m_mainAnimator.SetFloat("moveSpeedForward", m_animator_moveSpeedForward);       // W/S movement - forward and backward relative to view
		}

		public void FixedUpdate() {
#if ENABLE_DEBUG
			if (m_pauseFixedUpdate) {
				if (m_stepFixedUpdate) {
					m_collisionNormalRayCount = 0;
					m_velocityBeforeCollisions = null;
					m_velocityAfterCollisions = null;
					m_displacementBeforeCollisions = null;
					m_displacementAfterCollisions = null;

					m_stepFixedUpdate = false;
				} else {
					return;
				}
			}
#endif

			Vector3 positionStartOfUpdate = transform.position.WithY(0);

			if (!m_isGrounded) {
				m_verticalVelocityBeforeLanding = m_velocity.VerticalComponent() < 0 ? m_velocity.VerticalComponent() : m_verticalVelocityBeforeLanding;
			}

			// The direction the player is inputting
			Vector3 wishDir = new Vector3(m_moveInput.x, 0.0f, m_moveInput.y);
			wishDir = m_cameraTransform.TransformDirectionHorizontal(wishDir);
#if ENABLE_DEBUG
			m_debugWishDir = wishDir;
#endif

			Vector3 groundNormal;
			m_isGrounded = IsGrounded(out groundNormal);

			// Upon landing, if we're beyond the normal limits, set the degrees we need to rotate by to reach forward facing angle
			if (!m_isGroundedInPrevFrame && m_isGrounded && (m_pitch > 87f || m_pitch < -87f)) {
				if (m_pitch > 0) {
					m_cameraFlipDegreesToCompensate = 360f - m_pitch;
				} else {
					m_cameraFlipDegreesToCompensate = -m_pitch;
				}
			}

			// Check if we should be shield sliding
			if (m_isCrouching) {
				if (m_nodwedd_galluSglefrioTarian.m_gwerth) {
					float horizontalSpeed = m_velocity.ToHorizontal().magnitude;
					if (m_isShieldSliding) {
						// If we're already shield sliding, make sure our speed qualifies us for continuing
						m_isShieldSliding = horizontalSpeed > kShieldSlideStopSpeedMinimumFactor * m_cyflymderCyfanDiofyn;
					} else {
						// If we're not already shield sliding, make sure our speed qualifies us for starting
						m_isShieldSliding = horizontalSpeed > kShieldSlideSpeedMinimumFactor * m_cyflymderCyfanDiofyn;
					}
				} else {
					// Bar the player from shield sliding if they can't.
					m_isShieldSliding = false;
				}
			}

			bool bounceForNext = false;
			if (m_isGrounded && !m_isRamping) {
				if (m_isGroundedInPrevFrame) {
					// Don't apply friction if just landed or about to jump (allows bhopping)
					if (!m_isGonnaJump && !m_isBeingLaunched) {
						ApplyFriction(ref m_velocity, Time.fixedDeltaTime, wishDir);
					}
				} else {
					// Set anim var: Land impact blend 

					// Set anim trigger: Land
				}

				// If our impact with the ground was fast enough, trigger a bounce
				if (-m_verticalVelocityBeforeLanding > m_lleiafswmCyflymderISboncen) {
					bounceForNext = true;
				}

				// If shield sliding, apply gravity.
				if (m_isShieldSliding) {
					m_velocity += Gravity.Down * kGravityAmount * Time.fixedDeltaTime;
				}

				if (!m_isGonnaJump) {
					float accelerationCoeff = kGroundAccelerationCoeff;
					if (m_isShieldSliding) {
						float coeff = Vector3.Dot(m_velocity, wishDir) > 0 ? m_nodwedd_cyfernodCyflymiadAwyr.m_gwerth : m_nodwedd_cyfernodArafuAwyr.m_gwerth;
						accelerationCoeff = m_shieldSlideAccelerationFactor * coeff;
					}
					Accelerate(ref m_velocity, wishDir, accelerationCoeff, Time.fixedDeltaTime);
				}

				// Crop up horizontal velocity component
				if (!m_isBeingLaunched) {
					m_velocity = Vector3.ProjectOnPlane(m_velocity, groundNormal);
				}

				// Jumping
				if (m_isGonnaJump) {
					m_velocity += groundNormal * m_nodwedd_cryfderNaid.m_gwerth;

					m_characterArtComponent.m_mainAnimator.SetTrigger("Jump");
				}

			} else {
				float coeff = Vector3.Dot(m_velocity, wishDir) > 0 ? m_nodwedd_cyfernodCyflymiadAwyr.m_gwerth : m_nodwedd_cyfernodArafuAwyr.m_gwerth;

				Accelerate(ref m_velocity, wishDir, coeff, Time.fixedDeltaTime);

				// TODO verify what this was for
				if (Mathf.Abs(m_moveInput.x) > 0.0001f) {
					ApplyAirControl(ref m_velocity, wishDir, Time.fixedDeltaTime);
				}

				m_velocity += Gravity.Down * kGravityAmount * Time.fixedDeltaTime;
			}

			Vector3 displacement = m_velocity * Time.fixedDeltaTime;

			// Removed since ResolveCollisions should deal with this situation anyway via sweeping
			/*
			// TODO iterate on this, hollowing still occurs!! ---- !!
			// If we're moving too fast for our collider, make sure we don't hollow through any colliders
			float colliderExtentMinimum = 0;
			switch (m_colliderType) {
				case CydranEndidGwrthdarydd.EColliderType.Sphere: {
					colliderExtentMinimum = (m_collider as SphereCollider).radius;
					break;
				}
				case CydranEndidGwrthdarydd.EColliderType.Capsule: {
					// TODO cache this instead
					CapsuleCollider capsule = m_collider as CapsuleCollider;
					colliderExtentMinimum = Mathf.Min(capsule.radius, capsule.height);
					break;
				}
			}

			
			if (displacement.magnitude > colliderExtentMinimum) {
				ClampDisplacement(ref m_velocity, ref displacement, transform.position);
			}
			*/

			ResolveCollisions(ref displacement, ref m_velocity);
			transform.position += displacement;

#if ENABLE_DEBUG
			// Update ray positions to match the position of the capsule at the start of this update
			UpdateDebugRayPositions();
#endif

			if (m_isBeingLaunched) {
				m_isBeingLaunched = false;
			}

			// [ Move bob blending goes here ]

			// [ Step noise and distance tracking goes here]

			if (bounceForNext) {
				ApplyImpulse(new Vector3(0, -m_verticalVelocityBeforeLanding * m_nodwedd_ffactorSbonc.m_gwerth, 0), true);
				m_isGrounded = false;
			}

			m_isGroundedInPrevFrame = m_isGrounded;
		}

		void Accelerate(ref Vector3 velocity, Vector3 accelDir, float accelCoeff, float deltaTime) {
			// Apply Quake 3 CPMA projection
			float projSpeed = Vector3.Dot(velocity, accelDir); // TODO this could be fucky if velocity.y != 0

			Vector3 velDirHorz = velocity.ToHorizontal().normalized;
			float speedHorz = velocity.ToHorizontal().magnitude;
			float dot = Vector3.Dot(velDirHorz, accelDir);

#if ENABLE_REDUCED_CONTROL_AT_HIGH_SPEED
			//if (m_isGrounded) {
			//	accelCoeff *= velocity.magnitude.RemapClamped(kReducedControlMultiplier_SpeedBounds.x, kReducedControlMultiplier_SpeedBounds.y, 0.0f, kReducedControlMultiplier_Factor);
			//}

			// If we're trying to decelerate...
			if (dot < 0) {
				float absDot = Mathf.Abs(dot);
				// As we go from the minimum speed to the maximum speed, lerp the "factor" from 0 to full
				float speedFactor = speedHorz.RemapClamped(kReducedControlMultiplier_SpeedBounds.x, kReducedControlMultiplier_SpeedBounds.y, 0.0f, kReducedControlMultiplier_Factor);

				// The more we're trying to decelerate (i.e. the more aligned our wish dir is to the inverse of velocity),
				// the more of a limit we apply, ranging from 0 to the above calculated value
				float adjustedDot = absDot.RemapClamped(0.0f, 1.0f - kReducedControlMultiplier_DotProdRange, 0.0f, 1.0f);

				float accelerationMultiplier = Mathf.Clamp01(1 - (adjustedDot * speedFactor));

				accelCoeff *= accelerationMultiplier;
			}
#endif
			float speedLimitToUse = m_cyflymderCyfanDiofyn;
			if (m_moveInput.y < 0) {
				speedLimitToUse = m_cyflymderCyfan�l;
			}/* else if (Mathf.Abs(m_moveInput.x) > 0.001f) {
				speedLimitToUse = m_maxSpeedFactorLateral;
			}*/

			// How much speed we need to add (in that direction) to reach the speed limit
			float speedToMax = speedLimitToUse - projSpeed;
			if (speedToMax < 0) {
				return;
			}

			// maxSpeed * deltaT => a = v / t
			// accelCoeff => ad hoc approach to make movement feel better
			float accelAmount = accelCoeff * speedLimitToUse * deltaTime;

			// If we are accelerating in a way that we exceed the speed limit, crop it to max
			accelAmount = Mathf.Clamp(accelAmount, 0, speedToMax);

			velocity += accelDir * accelAmount;
		}

		private void ApplyFriction(ref Vector3 velocity, float deltaT, Vector3 wishDir) {
			float speed = velocity.magnitude;
			if (speed <= 0.00001f) {
				return; // TODO surely we should just stop? or is that done elsewhere
			}

			// Don't drop below threshold.
			// If we're crouching and going beyond the minimum slide limit and we allow sliding, use sliding friction
			float downLimit = Mathf.Max(speed, kFrictionSpeedThresholdStop);

			float friction = m_nodwedd_ffrithiant.m_gwerth;
			if (m_isShieldSliding) {
				// If sliding, use the reduced sliding friction
				friction *= m_nodwedd_ffactorFfrithiantSglefrioTarian.m_gwerth;
			} else if (speed < kIncreaseFrictionSpeedThreshold) {
				// If travelling slow enough and not sliding, increase friction to allow for responsive movement at low speeds.
				friction *= kIncreaseFrictionFactor;
			}

			float dropAmount = speed - (downLimit * friction * deltaT);
			if (dropAmount < 0) {
				dropAmount = 0;
			}

			velocity *= (dropAmount / speed); // TODO maybe test if this could be done better?
		}

		void ApplyAirControl(ref Vector3 velocity, Vector3 accelDir, float deltaT) {
			// This only happens in the horizontal plane
			// TODO: Verify that these work with various gravity values (nonvertical gravity?)
			Vector3 playerDirHorz = velocity.ToHorizontal().normalized;
			float playerSpeedHorz = velocity.ToHorizontal().magnitude;

			float dot = Vector3.Dot(playerDirHorz, accelDir);
			if (dot > 0.0f) {
				float k = m_nodwedd_manylrwyddRheolaethAwyr.m_gwerth;
				k *= dot * dot * deltaT;

				// Like Quake 3 CPMA, increased direction change rate when we only hold W.
				// If we want pure forward movement, we are given more air control
				bool isPureForward = Mathf.Abs(m_moveInput.x) < 0.0001f && Mathf.Abs(m_moveInput.y) > 0;
				if (isPureForward) {
					k *= m_ychwanegiadYmlaenRheolaethAwyr;
				}

				// TODO check if order of operations is correct here (i think it is? encase in parentheses if so)
				// A little bit closer to accelDir
				playerDirHorz = playerDirHorz * playerSpeedHorz + accelDir * k;
				playerDirHorz.Normalize();

				velocity = (playerDirHorz * playerSpeedHorz).ToHorizontal() + (Gravity.Up * velocity.VerticalComponent());
			}
		}

		/// Calculates the displacement required to not be in other colliders. Modifies incoming displacement and velocity.
		/// displacement: velocity * deltaT
		void ResolveCollisions(ref Vector3 displacement, ref Vector3 velocity) {
			switch (m_colliderType) {
				case CydranEndidGwrthdarydd.EColliderType.Capsule: {
						CapsuleCollider capsule = m_collider as CapsuleCollider;
						float height = capsule.height;
						float radius = capsule.radius;
						Vector3 centre = capsule.center; // Local space

#if ENABLE_DEBUG
					m_capsulePositionDebug = transform.position;

					Vector3 offsetDebug = new Vector3(0,0.1f,0);
					// Draw displacement and vel before collisions are applied
					if (m_showCollisionDebug) {
						m_displacementBeforeCollisions = new DebugRay() { position = centre + offsetDebug, direction = displacement };
						offsetDebug.y += 0.1f;
						m_velocityBeforeCollisions = new DebugRay() { position = centre + offsetDebug, direction = velocity };
						offsetDebug.y += 0.1f;
					}
#endif

						// Rotation invariant - will need to be updated if we ever rotate capsules in a way other than yaw
						Vector3 point1 = transform.position + centre + Vector3.up * -height * 0.5f;
						Vector3 point2 = point1 + Vector3.up * height;

						Vector3 direction = displacement.normalized;
						float distanceMax = displacement.magnitude;
						Vector3 originalDisplacement = displacement.Copy();
#if ENABLE_DEBUG
					m_capsuleSweepDir = direction * distanceMax;
#endif
						int colliderCount = Physics.CapsuleCastNonAlloc(point1, point2, radius, direction, m_collisionResults, distanceMax, ~kExcludedLayersFromPhysics, QueryTriggerInteraction.Ignore);

						for (int i = 0; i < colliderCount; ++i) {
							Collider otherCollider = m_collisionResults[i].collider;
							Transform otherTransform = otherCollider.transform;

							// Skip ourselves
							if (otherTransform.gameObject.Equals(capsule.gameObject)) {
								continue;
							}

							Vector3 collisionNormal;
							float collisionDistance;
							// TODO - Shouldn't need to add centre here, testing now...
							if (Physics.ComputePenetration(capsule, transform.position + /*centre +*/ originalDisplacement, transform.rotation,
									otherCollider, otherTransform.position, otherTransform.rotation, out collisionNormal, out collisionDistance)) {

								// Ignore very small penetrations; required to stand still on slopes
								if (collisionDistance < kSlopePenetrationIgnoreLimit) {
									continue;
								}

#if ENABLE_DEBUG
							// Draw collision normal
							if (m_showCollisionDebug) {
								m_collisionNormalRays[i] = new DebugRay() { position = centre, direction = collisionNormal * collisionDistance };
								m_collisionNormalRayCount++;
							}
#endif

								// Shift displacement out of the collider
								displacement += collisionNormal * collisionDistance;

								// Clip down the velocity component which is in the direction of penetration
								velocity -= Vector3.Project(velocity, collisionNormal);
							}
						}

#if ENABLE_DEBUG
					// Draw final displacement and velocity
					if (m_showCollisionDebug) {
						m_displacementAfterCollisions = new DebugRay() { position = centre + offsetDebug, direction = displacement };
						offsetDebug.y += 0.1f;
						m_velocityAfterCollisions = new DebugRay() { position = centre + offsetDebug, direction = velocity };
						offsetDebug.y += 0.1f;
					}
#endif
						//colliderCount = Physics.OverlapCapsuleNonAlloc(point1 + displacement, point2 + displacement, radius, m_collisionResultsOverlap, ~kExcludedLayersFromPhysics, QueryTriggerInteraction.Ignore);
						//Vector3 collisionNormal;
						//float collisionDistance;
						//for (int i = 0; i < colliderCount; ++i) {
						//	if(Physics.ComputePenetration(capsule, 
						//}

						break;
					}
				default: {
						Debug.Log("Collision resolving for is only implemented for capsules rn!!");
						break;
					}
			}
		}

		// Check a raycast from each of the defined ray positions downwards,
		// if any hit, you're grounded!! (go to your room).
		bool IsGrounded(out Vector3 groundNormal) {
			// If vertical speed (w.r.t gravity's direction) is greater than our jump velocity,
			// ground us but mark us as ramping. This allows for sliding up inclines when moving fast, as
			// the velocity is deflected according to the incline's normal, giving us upward speed.
			// Also allow downward sliding, if applicable.
			float verticalSpeed = m_velocity.VerticalComponent();
			bool isGrounded = false;
			groundNormal = -Gravity.Down;

			// If being launched (by impulses etc.), return false.
			if (m_isBeingLaunched && verticalSpeed > 0) {
				return false;
			}

			// If moving upwards fast enough (via ramping), set the ramping flag
			m_isRamping = verticalSpeed > m_trothwyRampio * m_nodwedd_cryfderNaid.m_gwerth && isGrounded;

			int groundingRayHits = 0;
			groundNormal = Vector3.zero;

			foreach (Vector3 groundingRayPosition in m_groundingRayPositions) {
				// Not sure why we had ghost jumps originally, TODO test with and without
				/*if(groundinRayPosition == ghostJumpRayPos && isGrounded)
				   continue;*/
				RaycastHit hit;
				Vector3 rayPosWorld = transform.TransformPoint(groundingRayPosition);
				if (Physics.Raycast(rayPosWorld, Gravity.Down, out hit, kGroundingRayDistanceCheck + kGroundingRayDistanceCheckBuffer, ~kExcludedLayersFromPhysics)) {
					if (Vector3.Angle(Gravity.Up, hit.normal) <= kMaxSlopeAngle) {
						groundNormal += hit.normal;
						groundingRayHits++;
						isGrounded = true;
					}
					Debug.DrawRay(rayPosWorld, Gravity.Down * (kGroundingRayDistanceCheck + kGroundingRayDistanceCheckBuffer), Color.red, Time.fixedDeltaTime);
				} else {
					Debug.DrawRay(rayPosWorld, Gravity.Down * (kGroundingRayDistanceCheck + kGroundingRayDistanceCheckBuffer), Color.cyan, Time.fixedDeltaTime);
				}
			}

			// Set the normal to be an average of all the hits' normals
			if (isGrounded) {
				groundNormal /= groundingRayHits;
			}

			return isGrounded;
		}

		#endregion

		#region InputMethods
		void ICharacterController.OnMove(Vector2 value) {
			m_moveInput = value;
		}

		void ICharacterController.OnLook(Vector2 value) {
			m_lookInput = value;
			// Mouse up = look upwards
			m_lookInput.y = -m_lookInput.y;
		}

		void ICharacterController.OnJump(bool value) {
			m_isGonnaJump = value;
		}

		void ICharacterController.OnDash(bool value) {

		}

		void ICharacterController.OnCrouch(bool value) {
			// Uncrouching should always cancel shield sliding
			if (!value) {
				m_isShieldSliding = false;
			}
			m_isCrouching = value;
		}

		void ICharacterController.OnPrimary(InputValue value) {

		}

		void ICharacterController.OnSecondary(InputValue value) {

		}

		void ICharacterController.OnTertiary(InputValue value) {

		}

		void ICharacterController.OnQuaternary(InputValue value) {

		}

		void ICharacterController.OnDebugTrigger(bool value) {
#if ENABLE_DEBUG
			m_pauseFixedUpdate = !m_pauseFixedUpdate;
#endif
		}
		#endregion
	}
}