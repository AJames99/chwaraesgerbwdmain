using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodweddionEndid;


namespace CydrannauEndid {
	public class CydranEndidStatwsBod : CydranEndid {
		public override EMathCydran CaelMathCydran() { return EMathCydran.ActorStatus; }

		public override bool Init(ref List<string> issueStrings) {
			issueStrings = null;
			return true;
		}
	}
}