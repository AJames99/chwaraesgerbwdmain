using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CydrannauEndid {
	public class CydranEndidCelfCymeriad : CydranEndid {
		public override EMathCydran CaelMathCydran() {
			return EMathCydran.CharacterArt;
		}

		[SerializeField]
		public Transform m_cameraBoneTransform;

		[SerializeField]
		GameObject m_characterArtInstance;

		[SerializeField]
		public Animator m_mainAnimator;

		public delegate void OnAnimationEventInt(int value);
		public Dictionary<string, OnAnimationEventInt> m_animationEventCallbacksInt;

		public override bool Init(ref List<string> issueStrings) {
			if (m_wediYmgychwyn)
				return true;
			m_wediYmgychwyn = true;
			issueStrings = new List<string>();

			m_animationEventCallbacksInt = new Dictionary<string, OnAnimationEventInt>();
			m_animationEventCallbacksInt.Add("SetFooting", null);

			// Evt listener will likely be on the same gameobject as the animator:
			AnimationEventListener eventListener = m_mainAnimator.GetComponent<AnimationEventListener>();
			if (eventListener != null) {
				eventListener.m_characterArtComponent = this;
			}

			/*string artAssetPath = "";
			if (entityEntry.TryGetParameterValueString("assetPath", ref artAssetPath)) {
				GameObject assetPrefab = Resources.Load<GameObject>(artAssetPath);
				
				if (assetPrefab != null) {
					m_characterArtInstance = GameProcessManager.InstantiateObject(assetPrefab, transform);

					if (m_characterArtInstance != null) {
						string cameraBoneName = "";
						if (entityEntry.TryGetParameterValueString("cameraBoneName", ref cameraBoneName)) {
							m_cameraBoneTransform = m_characterArtInstance.transform.Find(cameraBoneName);
						}

						m_mainAnimator = m_characterArtInstance.GetComponent<Animator>();
						AnimationEventListener eventListener = m_characterArtInstance.AddComponent<AnimationEventListener>();
						eventListener.m_characterArtComponent = this;
					} else {
						m_wediYmgychwyn = false;
					}
				} else {
					m_wediYmgychwyn = false;
				}
			}*/

			return m_wediYmgychwyn;
		}

		public void AnimationEventInt(string key, int value) {
			m_animationEventCallbacksInt[key].Invoke(value);
		}
	}
}