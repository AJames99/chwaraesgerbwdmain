using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CydrannauEndid {
    public class PerceptionComponent : CydranEndid {
        public override EMathCydran CaelMathCydran() { return EMathCydran.Perception; }

		public override bool Init(ref List<string> issueStrings) {
			issueStrings = null;
			return true;
		}
	}
}