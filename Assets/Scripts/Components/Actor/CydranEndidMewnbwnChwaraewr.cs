
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CydrannauEndid {
	public class CydranEndidMewnbwnChwaraewr : CydranEndid {
		public override EMathCydran CaelMathCydran() { return EMathCydran.PlayerInput; }

		//[SerializeField]
		//protected InputActionAsset m_playerInputActionAsset;
		
		[SerializeField] protected PlayerInput m_playerInput;

		protected ICharacterController m_controller;

		const float kLookSensitivityBase = 0.1f;

		[SerializeField]
		// Input value, parameter
		float m_lookSensitivityParam;
		
		// Final value; scaled
		float m_lookSensitivity;

		[SerializeField]
		LayerMask m_debugLayerMask;

		#region UNUSEDINPUTTRACKINGSTUFF
		/*public enum ControllerInputs {
			MOVE,
			MOVEWORLDSPACE,
			JUMP,
			LOOK,
			CROUCH,
			ATTACK1,
			ATTACK2,
			ATTACK3,
			ABILITY1,
			ABILITY2,
			ABILITY3,
			ABILITY4
		}

		Dictionary<ICharacterController.ControllerInputs, bool> m_inputsHeld = new Dictionary<ICharacterController.ControllerInputs, bool>() {
			{ ICharacterController.ControllerInputs.JUMP, false },
			{ ICharacterController.ControllerInputs.CROUCH, false },
			{ ICharacterController.ControllerInputs.ATTACK1, false },
			{ ICharacterController.ControllerInputs.ATTACK2, false },
			{ ICharacterController.ControllerInputs.ATTACK3, false },
			{ ICharacterController.ControllerInputs.ABILITY1, false },
			{ ICharacterController.ControllerInputs.ABILITY2, false },
			{ ICharacterController.ControllerInputs.ABILITY3, false },
			{ ICharacterController.ControllerInputs.ABILITY4, false },
		};*/
		#endregion

		private void Awake() {
			if (m_playerInput)
				m_playerInput.actions.Enable();

			// Lock the cursor to allow for look
			Cursor.lockState = CursorLockMode.Locked;
		}

		public override bool Init(ref List<string> issueStrings) {
			if (m_wediYmgychwyn) {
				return true;
			}

			issueStrings = new List<string>();
			m_wediYmgychwyn = true;

			m_controller = m_gwasanaethau.GetEntityComponent<CydranEndidRheolyddCinematig>();
			if (m_controller == null) {
				issueStrings.Add("Error initialising PlayerInputComponent: Couldn't find the KinematicControllerComponent or ICharacterController on this entity!");
				m_wediYmgychwyn = false;
			}

			m_playerInput = GetComponent<PlayerInput>();
			if (!m_playerInput) {
				m_playerInput = gameObject.AddComponent<PlayerInput>();
			}

			if (m_playerInput != null) {
				//m_playerInput.actions = m_playerInputActionAsset;

				// God I fuckin hate Unity. Half the code necessary to make PlayerInput work is in the
				// component's private OnEnable and OnDisable functions...
				// Call actions.Enable() in Awake to make it work.
				//m_playerInput.notificationBehavior = PlayerNotifications.SendMessages;

				m_lookSensitivity = m_lookSensitivityParam * kLookSensitivityBase;

			} else {
				//issueStrings.Add("Error initialising PlayerInputComponent: couldn't load the asset specified in the inputActionAssetPath param! (File probably doesn't exist/path is wrong)");
				issueStrings.Add("Error initialising PlayerInputComponent: No PlayerInput component was found/assigned!");
				m_wediYmgychwyn = false;
			}


			return m_wediYmgychwyn;
		}

		public void OnMove(InputValue inputValue) {
			m_controller.OnMove(inputValue.Get<Vector2>());
		}

		public void OnLook(InputValue inputValue) {
			m_controller.OnLook(inputValue.Get<Vector2>() * m_lookSensitivity);
		}

		public void OnAttack1(InputValue inputValue) {
			Camera main = Camera.main;
			if (main) {
				RaycastHit[] results = new RaycastHit[32];
				Vector3 start = main.transform.position;
				int nResults = Physics.RaycastNonAlloc(start, main.transform.forward, results, 100.0f, m_debugLayerMask, QueryTriggerInteraction.Ignore);
				if (nResults > 0) {
					Vector3 instanceWorldPos = results[0].point;
					EntityInstancingManager.instance.InstantiateEntity("StrobeExplosionTest", EntityInstanceParameters.Params(instanceWorldPos));
				}
			}
		}

		public void OnAttack2(InputValue inputValue) {

		}

		public void OnAttack3(InputValue inputValue) {

		}

		public void OnPrimary(InputValue inputValue) {

		}

		public void OnSecondary(InputValue inputValue) {

		}

		public void OnTertiary(InputValue inputValue) {

		}

		public void OnQuaternary(InputValue inputValue) {

		}

		public void OnJump(InputValue inputValue) {
			m_controller.OnJump(inputValue.Get<float>() > 0.5f);
		}

		public void OnCrouch(InputValue inputValue) {
			m_controller.OnCrouch(inputValue.Get<float>() > 0.5f);
		}

		public void OnInteract(InputValue inputValue) {

		}

		public void OnSlot1(InputValue inputValue) {
			CydranEndidDefnyddiwrEitem defnyddiwrEitem = m_gwasanaethau.GetEntityComponent<CydranEndidDefnyddiwrEitem>();
			if(defnyddiwrEitem != null) {
				defnyddiwrEitem.m_slots[0].TynnwchEitem(defnyddiwrEitem);
			}
		}

		public void OnSlot2(InputValue inputValue) {

		}

		public void OnSlot3(InputValue inputValue) {

		}

		public void OnSlot4(InputValue inputValue) {

		}

		public void OnSlot5(InputValue inputValue) {

		}

		public void OnInventory(InputValue inputValue) {

		}

		public void OnScroll(InputValue inputValue) {

		}

		public void OnDebugTrigger(InputValue inputValue) {
			m_controller.OnDebugTrigger(inputValue.Get<float>() > 0.5f);
		}
	}
}