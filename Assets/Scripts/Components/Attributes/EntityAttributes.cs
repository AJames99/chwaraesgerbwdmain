using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets;
using DataBank;
using DataBank.ItemSystem;

namespace NodweddionEndid {

	// Determines how stacking multiple modifiers on the same attribute works
	// For now, the order determines which order they apply in SO MAKE SURE IT'S NOT REORDERED
	public enum MathStacNodwedd : byte {
		FIRST = 0,
		AdditiveBefore = 1,
		MultiplicativeBefore,
		AdditiveAfter,
		MultiplicativeAfter,
		COUNT
	}

	[System.Serializable]
	public abstract class GwerthNodwedd {
		
	}

	// Represents an attribute value; used to keep a cached local copy of an attribute's value
	[System.Serializable]
	public class GwerthNodwedd<T> : GwerthNodwedd where T : System.IEquatable<T> {
		[SerializeField] public T m_gwerth;
		public GwerthNodwedd(T gwerth) {
			m_gwerth = gwerth;
		}

		// Won't work well with reference types, but primitives will be fine.
		public GwerthNodwedd(GwerthNodwedd<T> arall) {
			m_gwerth = arall.m_gwerth;
		}
	}

	// Game-code side representation of entity attributes.
	// Should be created from an entry in the attribute databank
	public abstract class NodweddEndid {
		// String identifiers for the attribute and modifier, taken from the databank
		public string m_enwNodwedd;

		public NodweddEndid(DataBank.ItemSystem.AttributeEntry entry) {
			m_enwNodwedd = entry.m_id;
		}

		public abstract bool AddModifier(EntityAttributeModifier modifier);
		public abstract bool RemoveModifier(AttributeModifierEntry modifier);

		// When the attribute's value changes due to modifiers, run any delegates for the attribute
		public delegate void ArNewidGwerth(NodweddEndid nod);
		public ArNewidGwerth m_onValueChangedDelegates;

		public override string ToString() {
			return m_enwNodwedd;
		}
	}

	#region Attributes
	public abstract class NodweddEndid<T> : NodweddEndid where T : System.IEquatable<T> {

		public virtual GwerthNodwedd<T> m_gwerthSylfaenol { get; protected set; }
		public virtual GwerthNodwedd<T> m_gwerthCanlynol { get; private set; }

		protected EntityAttributeModifierSet m_modifiers = new EntityAttributeModifierSet();

		/// diystyruGwerthSylfaenol ("Override value"): See above.
		public NodweddEndid(DataBank.ItemSystem.AttributeEntry<T> entry, ref GwerthNodwedd<T> diystyruGwerth) : base(entry) {
			if (diystyruGwerth != null) {
				// Create a base value 
				m_gwerthSylfaenol = new GwerthNodwedd<T>(diystyruGwerth.m_gwerth);
				// Assign the incoming value class to the result value
				m_gwerthCanlynol = diystyruGwerth;
			} else {
				m_gwerthSylfaenol = new GwerthNodwedd<T>(entry.m_baseValue);
				m_gwerthCanlynol = new GwerthNodwedd<T>(entry.m_baseValue);
			}
		}

		public NodweddEndid(DataBank.ItemSystem.AttributeEntry<T> entry) : base(entry) {
			m_gwerthSylfaenol = new GwerthNodwedd<T>(entry.m_baseValue);
			m_gwerthCanlynol = new GwerthNodwedd<T>(entry.m_baseValue);
		}

		// Any method that overrides this must call this after its own overridden code
		public abstract void EvaluateAttributeValue();

		public override bool AddModifier(EntityAttributeModifier modifier) {
			if (m_modifiers.AddModifier(modifier)) {
				EvaluateAttributeValue();
				return true;
			}
			return false;
		}

		public override bool RemoveModifier(AttributeModifierEntry modifier) {
			if (m_modifiers.RemoveModifier(modifier)) {
				EvaluateAttributeValue();
				return true;
			}
			return false;
		}

		// To simplify tracking changes to result value
		protected void SetResultValue(T resultValue) {
			if (!m_gwerthCanlynol.Equals(resultValue)) {
				m_gwerthCanlynol.m_gwerth = resultValue;
				if (m_onValueChangedDelegates != null) {
					m_onValueChangedDelegates.Invoke(this);
				}
			}
		}

		public override string ToString() {
			return base.ToString() + " : " + m_gwerthCanlynol.m_gwerth + " (base: " + m_gwerthSylfaenol.m_gwerth + "), modifiers count: " + m_modifiers.modifierCount;
		}
	}

	public class NodweddEndidFlag : NodweddEndid<bool> {
		[HideInInspector] public int m_throthwyIBasio; // m_baseValue must be above this to output true.

		public NodweddEndidFlag(DataBank.ItemSystem.AttributeEntryFlag entry, ref GwerthNodwedd<bool> diystyruGwerth) : base(entry, ref diystyruGwerth) {
			m_throthwyIBasio = entry.m_thresholdToPass;
		}

		public NodweddEndidFlag(DataBank.ItemSystem.AttributeEntryFlag entry) : base(entry) {
			m_throthwyIBasio = entry.m_thresholdToPass;
		}

		public override void EvaluateAttributeValue() {
			int evaluatedValue = m_gwerthSylfaenol.m_gwerth ? 1 : 0;
			MathStacNodwedd stage = MathStacNodwedd.FIRST;

			while (stage < MathStacNodwedd.COUNT) {
				// Iterate, applying all additive modifiers before multiplicative
				for (int i = 0; i < m_modifiers.modifierCount; i++) {
					EntityAttributeModifierInt modifier = (EntityAttributeModifierInt)m_modifiers.m_attributeModifiers[i];
					if (modifier.m_stackType != stage)
						continue;

					switch (modifier.m_stackType) {
						case MathStacNodwedd.AdditiveAfter:
						case MathStacNodwedd.AdditiveBefore: {
							evaluatedValue += modifier.m_modifierValue;
							break;
						}
						// This assumes float modifiers are expressed as FACTORS, not percentages or quant decimals
						//case AttributeStackType.MultiplicativeAfter:
						//case AttributeStackType.MultiplicativeBefore: {
						//	evaluatedValue *= modifier.m_modifierValue;
						//	break;
						//}
					}
				}
				stage++;
			}

			SetResultValue(evaluatedValue > m_throthwyIBasio);
		}

		public static NodweddEndidFlag CreateEntityAttributeFromData(string attributeID, GwerthNodwedd<bool> value) {
			DataBank.ItemSystem.AttributeEntryFlag entry = GameDataBankManager.instance.m_attributeDataBank.GetAttribute(attributeID) as AttributeEntryFlag;
			if (entry != null) {
				NodweddEndidFlag attr;
				if (value != null) {
					attr = new NodweddEndidFlag(entry, ref value);
					return attr;
				} else {
					attr = new NodweddEndidFlag(entry);
					return attr;
				}
			}
			return null;
		}
	}

	public class NodweddEndidFloat : NodweddEndid<float> {
		[HideInInspector] public float m_minValue;
		[HideInInspector] public float m_maxValue;
		[HideInInspector] public bool m_hasMin;
		[HideInInspector] public bool m_hasMax;

		public NodweddEndidFloat(DataBank.ItemSystem.AttributeEntryFloat entry, ref GwerthNodwedd<float> gwerth) : base(entry, ref gwerth) {
			m_minValue = entry.m_minValue;
			m_maxValue = entry.m_maxValue;
			m_hasMax = entry.m_hasMax;
			m_hasMin = entry.m_hasMin;
		}

		public NodweddEndidFloat(DataBank.ItemSystem.AttributeEntryFloat entry) : base(entry) {
			m_minValue = entry.m_minValue;
			m_maxValue = entry.m_maxValue;
			m_hasMax = entry.m_hasMax;
			m_hasMin = entry.m_hasMin;
		}

		public override void EvaluateAttributeValue() {
			float evaluatedValue = m_gwerthSylfaenol.m_gwerth;
			Debug.Log("EvaluateAttributeValue - eval value reset to " + evaluatedValue);
			MathStacNodwedd stage = MathStacNodwedd.FIRST;
 

			while (stage < MathStacNodwedd.COUNT) {
				// Iterate, applying all additive modifiers before multiplicative
				for (int i = 0; i < m_modifiers.modifierCount; i++) {
					EntityAttributeModifierFloat modifier = (EntityAttributeModifierFloat)m_modifiers.m_attributeModifiers[i];
					if (modifier.m_stackType != stage)
						continue;

					switch (modifier.m_stackType) {
						case MathStacNodwedd.AdditiveAfter:
						case MathStacNodwedd.AdditiveBefore: {
							evaluatedValue += modifier.m_modifierValue;
								Debug.Log("EvaluateAttributeValue - eval additively modified to " + evaluatedValue + "(by " + modifier.m_modifierValue + ")");
								break;
						}
						// This assumes float modifiers are expressed as FACTORS, not percentages or quant decimals
						case MathStacNodwedd.MultiplicativeAfter:
						case MathStacNodwedd.MultiplicativeBefore: {
							evaluatedValue *= modifier.m_modifierValue;
								Debug.Log("EvaluateAttributeValue - eval multiplicatively modified to " + evaluatedValue + "(by " + modifier.m_modifierValue + ")");
								break;
						}
					}
				}
				stage++;
			}

			if (m_hasMin) {
				evaluatedValue = Mathf.Max(m_minValue, evaluatedValue);
			}
			if (m_hasMax) {
				evaluatedValue = Mathf.Min(m_maxValue, evaluatedValue);
			}
			SetResultValue(evaluatedValue);
		}

		public static NodweddEndidFloat CreateEntityAttributeFromData(string attributeID, GwerthNodwedd<float> value) {
			DataBank.ItemSystem.AttributeEntryFloat entry = GameDataBankManager.instance.m_attributeDataBank.GetAttribute(attributeID) as AttributeEntryFloat;
			if (entry != null) {
				NodweddEndidFloat attr;
				if (value != null) {
					attr = new NodweddEndidFloat(entry, ref value);
				} else {
					attr = new NodweddEndidFloat(entry);
				}
				return attr;
			}
			return null;
		}
	}

	public class NodweddEndidInt : NodweddEndid<int> {
		public int m_minValue;
		public int m_maxValue;
		public bool m_hasMin;
		public bool m_hasMax;

		public NodweddEndidInt(DataBank.ItemSystem.AttributeEntryInt entry, ref GwerthNodwedd<int> gwerth) : base(entry, ref gwerth) {
			m_minValue = entry.m_minValue;
			m_maxValue = entry.m_maxValue;
			m_hasMax = entry.m_hasMax;
			m_hasMin = entry.m_hasMin;
		}

		public NodweddEndidInt(DataBank.ItemSystem.AttributeEntryInt entry) : base(entry) {
			m_minValue = entry.m_minValue;
			m_maxValue = entry.m_maxValue;
			m_hasMax = entry.m_hasMax;
			m_hasMin = entry.m_hasMin;
		}

		public override void EvaluateAttributeValue() {
			int evaluatedValue = m_gwerthSylfaenol.m_gwerth;
			MathStacNodwedd stage = MathStacNodwedd.FIRST;

			while (stage < MathStacNodwedd.COUNT) {
				// Iterate, applying all additive modifiers before multiplicative
				for (int i = 0; i < m_modifiers.modifierCount; i++) {
					Debug.Log(m_modifiers.m_attributeModifiers[i].m_stackType.ToString() + ", " + m_modifiers.m_attributeModifiers[i].m_attributeId + ", " + m_modifiers.m_attributeModifiers[i].m_modifierId);
					EntityAttributeModifier modifier = m_modifiers.m_attributeModifiers[i];
					EntityAttributeModifierInt modifierInt = modifier as EntityAttributeModifierInt;
					EntityAttributeModifierFloat modifierFloat = modifier as EntityAttributeModifierFloat;

					if (modifier.m_stackType != stage)
						continue;
			
					if (modifierFloat == null & modifierInt == null)
						continue;


					switch (modifier.m_stackType) {
						case MathStacNodwedd.AdditiveAfter:
						case MathStacNodwedd.AdditiveBefore: {
								if (modifierFloat != null) {
									evaluatedValue += (int)modifierFloat.m_modifierValue;
								} else {
									evaluatedValue += (int)modifierInt.m_modifierValue;
								}
							
							break;
						}
						// This assumes float modifiers are expressed as FACTORS, not percentages or quant decimals
						case MathStacNodwedd.MultiplicativeAfter:
						case MathStacNodwedd.MultiplicativeBefore: {
								if (modifierFloat != null) {
									evaluatedValue = (int)((float)evaluatedValue * modifierFloat.m_modifierValue);
								} else {
									evaluatedValue *= (int)modifierInt.m_modifierValue;
								}
								break;
						}
					}
				}
				stage++;
			}

			if (m_hasMin) {
				evaluatedValue = Mathf.Max(m_minValue, evaluatedValue);
			}
			if (m_hasMax) {
				evaluatedValue = Mathf.Min(m_maxValue, evaluatedValue);
			}
			SetResultValue(evaluatedValue);
		}

		public static NodweddEndidInt CreateEntityAttributeFromData(string attributeID, GwerthNodwedd<int> value) {
			DataBank.ItemSystem.AttributeEntryInt entry = GameDataBankManager.instance.m_attributeDataBank.GetAttribute(attributeID) as AttributeEntryInt;
			if (entry != null) {
				NodweddEndidInt attr;
				if (value != null) {
					attr = new NodweddEndidInt(entry, ref value);
				} else {
					attr = new NodweddEndidInt(entry);
				}
				return attr;
			}
			return null;
		}
	}
	#endregion

	#region Modifiers
	// This was going to be the way to add modifiers to items, but it made more sense to just do that in the database and reference that instead.
	//[System.Serializable]
	//public class AddasyddNodwedd {
	//	[SerializeField, InspectorName("Attribute Modifier ID"), Tooltip("The ID of the attribute modifier to use. Must match one loaded from the Charon DB.")]
	//	public string enwAddasyddNodwedd;
	//
	//	[SerializeField, InspectorName("Attribute Modifier Value"), Tooltip("The value that the modifier should apply; interpreted to int/float/string depending on the modifier definition.")]
	//	public string gwerthAddasydd;
	//}

	public abstract class EntityAttributeModifier {
		public MathStacNodwedd m_stackType;
		public string m_attributeId;
		public string m_modifierId;

		public static MathStacNodwedd GetStackTypeFromDBEnum(Assets.MathStac stackType) {
			switch (stackType) {
				case MathStac.CynAdiol:
					return MathStacNodwedd.AdditiveBefore;
				case MathStac.CynLluosiadol:
					return MathStacNodwedd.MultiplicativeBefore;
				case MathStac.ArOolAdiol:
					return MathStacNodwedd.AdditiveAfter;
				case MathStac.ArOolCynlluosiadol:
					return MathStacNodwedd.MultiplicativeAfter;
			}
			return MathStacNodwedd.COUNT;
		}

		// TODO this could be in the constructors? Can we overload/override type?
		public static EntityAttributeModifier CreateModifierFromInstance(Assets.AddasyddNodweddInstans instance) {
			AttributeModifierEntry diffiniadAddasydd = GameDataBankManager.instance.m_attributeDataBank.GetAttributeModifier(instance.EnwAddasydd);
			switch (diffiniadAddasydd) {
				case AttributeModifierEntryFloat: {
						AttributeModifierEntryFloat attributeModifierEntryFloat = diffiniadAddasydd as AttributeModifierEntryFloat;
						EntityAttributeModifierFloat modifier = new EntityAttributeModifierFloat() {
							m_attributeId = attributeModifierEntryFloat.m_attributeId,
							m_modifierId = instance.EnwAddasydd,
							m_stackType = attributeModifierEntryFloat.m_stackType,
							m_modifierValue = float.Parse(instance.Gwerth.Gwerth, System.Globalization.NumberStyles.Float)
						};
						return modifier;
					}
				case AttributeModifierEntryInt: {
						AttributeModifierEntryInt attributeModifierEntryFloat = diffiniadAddasydd as AttributeModifierEntryInt;
						EntityAttributeModifierInt modifier = new EntityAttributeModifierInt() {
							m_attributeId = attributeModifierEntryFloat.m_attributeId,
							m_modifierId = instance.EnwAddasydd,
							m_stackType = attributeModifierEntryFloat.m_stackType,
							m_modifierValue = int.Parse(instance.Gwerth.Gwerth, System.Globalization.NumberStyles.Integer)
						};
						return modifier;
					}
				default: {
						Debug.LogError("Failed to create an attribute modifier of id " + instance.EnwAddasydd);
						return null;
					}
			}
		}
	}

	public abstract class EntityAttributeModifier<T> : EntityAttributeModifier {
		public abstract T m_modifierValue { get; set; }
	}

	public class EntityAttributeModifierFloat : EntityAttributeModifier<float> {
		public override float m_modifierValue { get; set; }
	}
	public class EntityAttributeModifierInt : EntityAttributeModifier<int> {
		public override int m_modifierValue { get; set; }
	}

	// Fixed amount of modifiers for any attribute;
	// no idea how to declare dict<..., AttributeModifierEntry[]> such that the values are a fixed size array
	public class EntityAttributeModifierSet {
		const int maxModifiers = 16;
		protected int m_modifierCount = 0;
		public int modifierCount { get { return m_modifierCount; } }
		public List<EntityAttributeModifier> m_attributeModifiers = new List<EntityAttributeModifier>(maxModifiers);

		public bool AddModifier(EntityAttributeModifier modifier) {
			if (modifier != null) {
				if (m_modifierCount < maxModifiers - 1) {
					m_attributeModifiers.Add(modifier);
					++m_modifierCount;
					return true;
				}
			}
			return false;
		}
		public bool RemoveModifier(AttributeModifierEntry modifier) {
			if (modifier != null) {
				if (m_modifierCount > 0) {
					EntityAttributeModifier result = m_attributeModifiers.Find(
						delegate (EntityAttributeModifier i) {
							return i.m_attributeId == modifier.m_attributeId && i.m_modifierId == modifier.m_modifierId;
						}
					);
					if (result != null) {
						m_attributeModifiers.Remove(result);
						--m_modifierCount;
					}
					return true;
				} else {
					Debug.LogError("Attribute Error: Entry set count was 0 and a remove was attempted with a non-null value!");
				}
			}
			return false;
		}
	}

	#endregion
}