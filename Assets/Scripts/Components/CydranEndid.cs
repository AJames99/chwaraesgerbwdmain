using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CydrannauEndid {
	// Base entity component type; components are added to entities to manage various properties & behaviour
	public abstract class CydranEndid : MonoBehaviour {
		// Must contain every component type (unfortunately)
		// Make sure to add to GameDataBankModuleEntities.m_componentTypeMapping too!
		public enum EMathCydran : byte {
			INVALID = 0,
			FIRST = 1,
			EntityStateMachine = 1,
			Actor,
			Health,
			ActorStatus,
			Attribute,
			ItemUser,
			Perception,
			Physics,
			Debug,
			KinematicCharacterController,
			PlayerCamera,
			Collider,
			PlayerInput,
			CharacterArt,
			PhysicalItem,
			COUNT,
		}

		// Component utilities
		[HideInInspector] public GwasanaethauCydran m_gwasanaethau;

		public bool m_wediYmgychwyn { get; protected set; } = false;

		public abstract EMathCydran CaelMathCydran();

		// Run on game/scene start
		public virtual void Start() { /*Init();*/ }

		// Called when this component is added or reset to its default
		public virtual void Reset() { /*Init();*/ }

		// Returns whether init was successful; false means you should try again after init-ing other components
		public abstract bool Init(ref List<string> issueStrings);
		//{
			/*if (m_utils == null) {
				m_utils = gameObject.GetComponent<ComponentUtilities>();
				if (m_utils == null) {
					m_utils = gameObject.AddComponent<ComponentUtilities>();
				}
			}

			if (m_utils) {
				m_utils.RegisterNewComponentInstance(this);
			}*/
		//}

		public virtual void OnDestroy() {
			if (!m_gwasanaethau) {
				Debug.LogError("CydranEndid was destroyed without having a ComponentUtilities attached!");
			}
		}

		public virtual void DebugPrint() { }


		// Returns The set of entity components that must be initialised before this component
		// public virtual System.Type[] GetInitPrerequisites() { return null; }

		// MOVED TO INTERFACE RNodweddionCydran
		//public virtual bool AdioNodweddionCydran(CydranEndidNodweddion cydNod) { return true; }

		protected void GwirioGwasanaethau() {
			if (m_gwasanaethau == null) {
				m_gwasanaethau = GetComponent<GwasanaethauCydran>();
				if (m_gwasanaethau == null) {
					m_gwasanaethau = gameObject.AddComponent<GwasanaethauCydran>();
				}
			}
		}
	}
}