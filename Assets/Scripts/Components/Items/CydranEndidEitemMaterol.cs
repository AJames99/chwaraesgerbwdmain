using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CydrannauEndid;

public class CydranEndidEitemMaterol : CydranEndid {

	// Item definition; the item associated with this physicalised item
	[SerializeField] Eitem m_diffiniadEitem;

	public override EMathCydran CaelMathCydran() {
		return EMathCydran.PhysicalItem;
	}

	public override bool Init(ref List<string> issueStrings) {
		return true;
	}

	public void UpdateItemDef(Eitem eitemNewydd) {
		m_diffiniadEitem = eitemNewydd;
	}
}
