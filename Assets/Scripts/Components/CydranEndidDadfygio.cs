using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DataBank;

namespace CydrannauEndid {

	// Debug Component
	public class CydranEndidDadfygio : CydranEndid {
		List<CydranEndid> m_debugOnSpawn = new List<CydranEndid>();

		public override EMathCydran CaelMathCydran() {
			return EMathCydran.Debug;
		}

		public override bool Init(ref List<string> issueStrings) {
			issueStrings = null;

			//foreach (string str in entityEntry.m_parameters.Keys) {
			//	System.Type ComponentType = GameDataBankManager.instance.m_entityDataBank.GetComponentTypeFromString(str);
			//	CydranEndid component = m_gwasanaethau.GetEntityComponent(ComponentType);
			//	EntityComponentParameter<string> paramStr = entityEntry.m_parameters[str] as EntityComponentParameter<string>;
			//	if (paramStr != null) {
			//		if (paramStr.m_value.Contains("OnSpawn")) {
			//			debugOnSpawn.Add(component);
			//		}
			//	}
			//}

			foreach (CydranEndid comp in m_debugOnSpawn) {
				comp.DebugPrint();
			}

			return true;
		}

		// Members
		private string m_debugIdentifier;
	}
}