using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CydrannauEndid;

public class AnimationEventListener : MonoBehaviour {
	[HideInInspector] public CydranEndidCelfCymeriad m_characterArtComponent = null;

	private void Start() {
		m_characterArtComponent ??= GetComponent<CydranEndidCelfCymeriad>();
	}

	private void Awake() {
		m_characterArtComponent ??= GetComponent<CydranEndidCelfCymeriad>();
	}

	public void SetFooting(int foot) {
		if (m_characterArtComponent != null) {
			m_characterArtComponent.AnimationEventInt("SetFooting", foot);
		}
	}
}
