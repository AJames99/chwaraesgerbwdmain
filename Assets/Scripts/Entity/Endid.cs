using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CydrannauEndid;
using CydrannauEndid;

using DataBank;

// Basis for all entities!
public class Endid : MonoBehaviour
{
	[SerializeField]
	public string m_enwEndid;

	[HideInInspector]
	public string m_prefabPath;

	[HideInInspector] public GwasanaethauCydran m_utils;

	private void Start() {

	}

	public void Initialise() {
		m_utils = GwasanaethauCydran.GetComponentUtilities(gameObject);
		m_utils.CacheComponents();

		List<CydranEndid> existingComponents = new List<CydranEndid>(gameObject.GetComponents<CydranEndid>());
		existingComponents.Sort(new TrefnYmgychwyn());

		// Keep a list of components with the component attribute interface, so they can be initialised after the main inits are done
		// on all the components
		List<RNodweddionCydran> cydrannauGydaNodweddionCydran = new List<RNodweddionCydran>();

		// TODO sorting should make this more efficient, but it isn't necessary right now (just ensure component init statuses are checked if dependencies exist)
		// sort components list here so init is in the right order

		Dictionary<string, List<string>> componentIssueStrings = new Dictionary<string, List<string>>();

		bool initialisedAny = true;
		while (initialisedAny) {
			initialisedAny = false;
			List<CydranEndid> initialisedComponents = new List<CydranEndid>();
			foreach (CydranEndid entityComponent in existingComponents) {
				List<string> issueStrings = new List<string>();
				
				entityComponent.m_gwasanaethau = m_utils;

				if (entityComponent.Init(ref issueStrings)) {
					initialisedAny |= true;
					initialisedComponents.Add(entityComponent);
					if (entityComponent is RNodweddionCydran) {
						cydrannauGydaNodweddionCydran.Add(entityComponent as RNodweddionCydran);
					}
					Debug.Log("Successfully initialised component " + entityComponent.CaelMathCydran().ToString());
				} else {
					Debug.Log("Couldn't initialise component " + entityComponent.CaelMathCydran().ToString() + ", deferring to next loop");
				}

				if (issueStrings != null && issueStrings.Count > 0) {
					if (componentIssueStrings.ContainsKey(entityComponent.CaelMathCydran().ToString())) {
						componentIssueStrings[entityComponent.CaelMathCydran().ToString()].AddRange(issueStrings);
					} else {
						componentIssueStrings.Add(entityComponent.CaelMathCydran().ToString(), issueStrings);
					}
				}
			}

			if (componentIssueStrings != null && componentIssueStrings.Keys.Count > 0) {
				Debug.LogError("Errors were present when initialising entity \"" + m_enwEndid + "\":\n");
				foreach (string componentStr in componentIssueStrings.Keys) {
					string errorMsg = "(" + componentStr + "):";
					int i = 1;
					foreach (string issueStr in componentIssueStrings[componentStr]) {
						errorMsg += "\n\t" + i.ToString() + "). " + issueStr;
					}
					Debug.LogError(errorMsg);
				}
			}

			if (!initialisedAny) {
				Debug.Log("Couldn't initialise " + existingComponents.Count + " components! They failed and did not succeed after other components were initialised." );
			}

			foreach (CydranEndid toRemove in initialisedComponents) {
				existingComponents.Remove(toRemove);
			}
			if (existingComponents.Count < 1) {
				break;
			}
		}

		CydranEndidNodweddion nodweddion = m_utils.GetEntityComponent<CydranEndidNodweddion>();
		if (nodweddion != null) {
			foreach (RNodweddionCydran cydranGydaNodCyd in cydrannauGydaNodweddionCydran) {
				cydranGydaNodCyd.AdioAdalwadNodwedd(nodweddion);
			}
		}
	}

	// Init order
	class TrefnYmgychwyn : IComparer<CydranEndid> {
		public static List<System.Type> rhestrTrefnYmgychwyn = new List<System.Type> {
			typeof(CydranEndidNodweddion),
			typeof(CydranEndidBod),
			typeof(CydranEndidFfiseg),
			typeof(CydranEndidGwrthdarydd),
			typeof(CydranEndidCelfCymeriad),
			typeof(CydranEndidCameraChwaraewr),
			typeof(CydranEndidRheolyddCinematig),
			typeof(CydranEndidStatwsBod),
			typeof(PerceptionComponent),
			typeof(CydranEndidDefnyddiwrEitem),
			typeof(CydranEndidIechyd),
			typeof(CydranEndidMewnbwnChwaraewr),
			typeof(CydranEndidDadfygio),
		};

		public int Compare(CydranEndid x, CydranEndid y) {
			int indexX = rhestrTrefnYmgychwyn.IndexOf(x.GetType());
			int indexY = rhestrTrefnYmgychwyn.IndexOf(y.GetType());
			return indexX.CompareTo(indexY);
		}
	}
}
