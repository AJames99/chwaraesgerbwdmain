using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityIconScript : MonoBehaviour {
	
	//public Sprite m_pAbilityIconSprite;
	[SerializeField]
	protected Image m_pAbilityBorder;
	[SerializeField]
	protected RectMask2D m_pAbilityFillMask;
	[SerializeField]
	Vector2 m_v2FillPaddingMinMax = new Vector2(80, 0);

	[SerializeField]
	protected Image m_pAbilityIconImage;

	[SerializeField]
	protected Text m_pAmmoText;

	[SerializeField]
	protected Text m_pHotkeyText;

	[SerializeField]
	protected Image m_pCrossImage;

	protected int m_nAmmoCurrent, m_nAmmoMax;

	public Color m_colButtonDown;
	public Color m_colButtonUp;

	public void SetAmmoValue(int nAmmo) {
		m_nAmmoCurrent = nAmmo;
		RefreshAmmoText();
	}

	public void SetAmmoMax(int nAmmo) {
		m_nAmmoMax = nAmmo;
		RefreshAmmoText();
	}

	public void SetAmmoVisible(bool bVisible) {
		if (m_pAmmoText)
			m_pAmmoText.enabled = bVisible;
	}

	void RefreshAmmoText() {
		if (m_pAmmoText) {
			m_pAmmoText.text = m_nAmmoCurrent.ToString() + "/" + m_nAmmoMax.ToString();
		}
	}

	public void SetHotkeyText(string szHotkey) {
		if(m_pHotkeyText)
			m_pHotkeyText.text = szHotkey;
	}

	public void SetButtonPressed(bool bPressed) {
		m_pAbilityIconImage.color = bPressed ? m_colButtonDown : m_colButtonUp;
		m_pAbilityBorder.color = bPressed ? m_colButtonDown : m_colButtonUp;
		//m_pAmmoText.color = bPressed ? m_colButtonDown : m_colButtonUp;
	}

	public void SetAbilityIconSprite(Sprite pSprite) {
		m_pAbilityIconImage.sprite = pSprite;
	}

	// Where 0 is in use/just used, and 1 is off of cooldown.
	public void SetAbilityProgress(float flProportion) {
		m_pAbilityFillMask.padding = new Vector4(0, 0, 0, flProportion.RemapClamped(0, 1, m_v2FillPaddingMinMax.x, m_v2FillPaddingMinMax.y));
	}

	public void SetAbilityAvailable(bool bAvailable) {
		if(m_pCrossImage)
			m_pCrossImage.enabled = !bAvailable;
	}

	void Start() {
		m_nAmmoCurrent = m_nAmmoMax = 0;
		SetAbilityAvailable(true);
	}
}
