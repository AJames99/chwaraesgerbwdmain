﻿//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods {
	public static float Remap(this float value, float from1, float to1, float from2, float to2) {
		return (((value - from1) / (to1 - from1)) * (to2 - from2)) + from2;
	}

	public static float RemapClamped(this float value, float from1, float to1, float from2, float to2) {
		return Mathf.Clamp(
			(((value - from1) / (to1 - from1)) * (to2 - from2)) + from2,
			Mathf.Min(from2, to2),
			Mathf.Max(from2, to2));
	}

	public static float RemapClamped(this int value, float from1, float to1, float from2, float to2) {
		return RemapClamped((float)value, from1, to1, from2, to2);
	}

	public static float Remap(this int value, float from1, float to1, float from2, float to2) {
		return Remap((float)value, from1, to1, from2, to2);
	}

	public static void BroadcastAll(string fun, string tag, System.Object msg) {
		GameObject[] gos = GameObject.FindGameObjectsWithTag(tag);

		foreach (GameObject go in gos) {
			go.gameObject.BroadcastMessage(fun, msg, SendMessageOptions.DontRequireReceiver);

		}
	}

	public static int GetRandomIndex(this Object[] obj) {
		return Mathf.RoundToInt(Random.value * (obj.Length - 1));
	}

	public static T GetRandom<T>(this T[] obj) {
		return obj[Mathf.RoundToInt(Random.value * (obj.Length - 1))];
	}

	// value: this float value
	// target: the target value
	// rate: the rate per second of change from value to target
	// t: the time (s)
	public static float InterpAdditive(this float value, float target, float rate, float t) {
		if (target > value) {
			value = Mathf.Min(value + (rate * t), target);
		} else {
			value = Mathf.Max(value - (rate * t), target);
		}
		return value;
	}

	// deltaUnits: rate * deltaTime
	public static Vector3 LerpAdditive(this Vector3 value, Vector3 target, float deltaUnits) {
		Vector3 diff = target - value;
		if (diff.magnitude <= deltaUnits) {
			return target;
		}

		diff.Normalize();
		return value + (deltaUnits * diff);
	}

	public static Vector3 LerpWithMinimumRate(this Vector3 value, Vector3 target, float minimumDeltaUnits) {
		Vector3 lerpedValue = Vector3.Lerp(value, target, minimumDeltaUnits);
		Vector3 additiveLerpedValue = value.LerpAdditive(target, minimumDeltaUnits);
		float magnitudeDifference = lerpedValue.sqrMagnitude - additiveLerpedValue.sqrMagnitude;
		if (magnitudeDifference > 0) {
			return lerpedValue;
		}

		return additiveLerpedValue;
	}

	public static Quaternion LerpAdditive(this Quaternion value, Quaternion target, float deltaUnits) {
		return Quaternion.RotateTowards(value, target, deltaUnits);
	}

	public static void AdditiveLookat(this Transform value, Vector3 point, Vector3 up, float degrees) {
		Quaternion start = Quaternion.Euler(value.rotation.eulerAngles);
		value.LookAt(point, up);
		Quaternion end = value.rotation;
		if (Quaternion.Angle(start, end) <= degrees)
			return;
		
		value.rotation = Quaternion.RotateTowards(start, end, degrees);
	}

	//todo find out if there's a more efficient way involving modulo/remainder
	public static float Wrap180(this float value) {
		while (value > 180)
			value -= 360;
		while (value < -180)
			value += 360;

		return value;
	}

	public static float Wrap360Positive(this float value) {
		while (value > 360)
			value -= 360;
		while (value < 0)
			value += 360;

		return value;
	}

	public static Vector3 Copy(this Vector3 value) {
		return new Vector3(value.x, value.y, value.z);
	}

	// Outputs the integral and fractional parts of a float, doesn't modify self
	public static void GetIntegralFractional(this float value, out float fractional, out int integral) {
		integral = Mathf.FloorToInt(value);
		fractional = value % 1;
	}
}