﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugTextReadout : MonoBehaviour {

	Canvas parentCanvas;
	Text textBox;

	Dictionary<string, string> itemsToDisplay = new Dictionary<string, string>();

	[SerializeField]
	private bool billboard = true;
	[SerializeField]
	private bool flipBillboard = true;

	[SerializeField]
	private bool enlargeWhenHoveredOver = true;
	// Enlarge the text when the camera is within N degrees to its centre in screenspace.
	private float enlargeWithinAngle = 40f;
	const int normalFontSize = 25;
	const int enlargedFontSize = 50;

	Transform mainCamera;

	bool _debugEnabled = true;
	public bool debugEnabled {
		set {
			_debugEnabled = value;
			if(textBox)
				textBox.enabled = _debugEnabled;
		}
		get {
			return _debugEnabled;
		}
	}

	void Start() {
		parentCanvas = GetComponent<Canvas>();
		textBox = parentCanvas.GetComponentInChildren<Text>();
		mainCamera = Camera.main.transform;
		parentCanvas.worldCamera = Camera.main;
	}

	public void AddItem(string propertyName) {
		itemsToDisplay.Add(propertyName, "");
	}

	public void UpdateValue(string propertyName, string value) {
		itemsToDisplay[propertyName] = value;
		UpdateTextBox();
	}

	void UpdateTextBox() {
		string finalString = "";
		foreach (string name in itemsToDisplay.Keys) {
			finalString += name + ": " + itemsToDisplay[name] + "\n";
		}
		if(textBox)
			textBox.text = finalString;
	}

	private void Update() {
		if (billboard) {
			if (flipBillboard) {
				textBox.transform.LookAt(transform.position + (transform.position - mainCamera.position));
			} else {
				textBox.transform.LookAt(mainCamera);
			}
		}

		if (enlargeWhenHoveredOver) {
			textBox.fontSize = normalFontSize;
			float camToTxtAngle = Vector3.Angle(mainCamera.forward, (transform.position - mainCamera.transform.position).normalized);
			if (camToTxtAngle < enlargeWithinAngle) {
				textBox.fontSize = enlargedFontSize;
			}
		}
	}
}
