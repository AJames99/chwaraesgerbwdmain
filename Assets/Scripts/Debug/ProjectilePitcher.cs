using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePitcher : Damageable {

	[SerializeField]
	Projectile projectile;

	[SerializeField]
	Transform shotPoint;

	[SerializeField]
	float shotInterval = 1.0f;
	float shotTimer = 0f;

	[SerializeField]
	float force = 10f;

	public override void Die() {}

	public override IController GetCharacterController() {
		return null;
	}

	public override void Push(Vector3 force, Vector3? point = null) {}

	private void Awake() {
		shotTimer = 0f;
	}

	private void FixedUpdate() {
		if (Time.fixedTime > shotTimer) {
			shotTimer = Time.fixedTime + shotInterval;
			Shoot();
		}
	}

	void Shoot() {
		Projectile.InstanceProjectile(projectile, shotPoint.position + shotPoint.forward, shotPoint.forward, force, null, new Damage(Damage.DamageTypes.PROJECTILE, 5), false);
	}
}
