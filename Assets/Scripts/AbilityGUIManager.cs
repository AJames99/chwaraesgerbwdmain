using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityGUIManager : MonoBehaviour {


	//[SerializeField]
	//protected Controller m_pOwnerController;

	// Set me externally! from the controller
	public AbilityUser m_pOwnerAbilityUser;

	//List<AbilityIconScript> m_vAbilityIcons = new List<AbilityIconScript>();
	Dictionary<AbilitySlot.SLOTBINDING_t, AbilityIconScript> m_dicAbilityIcons = new Dictionary<AbilitySlot.SLOTBINDING_t, AbilityIconScript>();

	const string szAbilityIconPrefabPath = "Prefab/UI/AbilityIcon";

	[SerializeField]
	Vector2 m_v2LeftMostPoint = new Vector2(0,0);

	[SerializeField]
	Vector2 m_v2OffsetPerIcon = new Vector2(0,0);

	public void CreateAbilityIcons() {
		if (m_dicAbilityIcons.Keys.Count > 0) {
			foreach (AbilitySlot.SLOTBINDING_t slotBindings in m_dicAbilityIcons.Keys) {
				Destroy(m_dicAbilityIcons[slotBindings].gameObject);
			}
			m_dicAbilityIcons.Clear();
		}

		//if (m_pOwnerController) {
		//	m_pOwnerAbilityUser = m_pOwnerController.GetAbilityUser();
		//}

		Vector2 v2PositionCounter = m_v2LeftMostPoint;

		GameObject baseIcon = Resources.Load(szAbilityIconPrefabPath) as GameObject;

		if (null != m_pOwnerAbilityUser) {
			Dictionary<AbilitySlot.SLOTBINDING_t, AbilitySlot> dicSlots = m_pOwnerAbilityUser.GetAbilities();
			foreach (AbilitySlot.SLOTBINDING_t slotBinding in dicSlots.Keys) {
				//dicSlots[slotBinding]
				//m_vAbilityIcons.Add
				AbilityIconScript abilityIcon = Instantiate(baseIcon, transform).GetComponent<AbilityIconScript>();
				Ability pAbility = dicSlots[slotBinding].GetAbility();
				if (pAbility != null) {
					abilityIcon.SetAbilityIconSprite(Resources.Load(pAbility.m_szAbilityIconPath) as Sprite);
				}
				
				abilityIcon.SetHotkeyText(dicSlots[slotBinding].GetHotkeyString());

				abilityIcon.GetComponent<RectTransform>().anchoredPosition = new Vector2(v2PositionCounter.x, v2PositionCounter.y);

				abilityIcon.SetAmmoVisible(pAbility.GetResourceType() == Ability.ResourceType.AMMO);

				m_dicAbilityIcons.Add(slotBinding, abilityIcon);

				v2PositionCounter += m_v2OffsetPerIcon;
			}
		} else {
			Debug.LogError("CreateAbilityIcons failed! m_pOwnerAbilityUser was null.");
		}
	}

	public void SetProgressForSlot(AbilitySlot.SLOTBINDING_t slotBinding, float flProgress) {
		if (m_dicAbilityIcons.ContainsKey(slotBinding)) {
			m_dicAbilityIcons[slotBinding].SetAbilityProgress(flProgress);
		}
	}

	public void SetAmmoForSlot(AbilitySlot.SLOTBINDING_t slotBinding, int nAmmoCount, int nAmmoMax) {
		if (m_dicAbilityIcons.ContainsKey(slotBinding)) {
			m_dicAbilityIcons[slotBinding].SetAmmoMax(nAmmoMax);
			m_dicAbilityIcons[slotBinding].SetAmmoValue(nAmmoCount);
		}
	}

	public void SetAmmoCounterVisible(AbilitySlot.SLOTBINDING_t slotBinding, bool bVisible) {
		if (m_dicAbilityIcons.ContainsKey(slotBinding)) {
			m_dicAbilityIcons[slotBinding].SetAmmoVisible(bVisible);
		}
	}

	public void SetButtonPressedForSlot(AbilitySlot.SLOTBINDING_t slotBinding, bool bPressed) {
		if (m_dicAbilityIcons.ContainsKey(slotBinding)) {
			m_dicAbilityIcons[slotBinding].SetButtonPressed(bPressed);
		}
	}

	public void SetAbilityAvailableForSlot(AbilitySlot.SLOTBINDING_t slotBinding, bool bVisible) {
		if (m_dicAbilityIcons.ContainsKey(slotBinding)) {
			m_dicAbilityIcons[slotBinding].SetAbilityAvailable(bVisible);
		}
	}
}
