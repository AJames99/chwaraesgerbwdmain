using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileHitbox : Hitbox {
	[SerializeField]
	protected bool m_bDeactivateOnHitOther = true;

	protected Projectile m_pProjectile;

	public override Damageable GetDamageable() {
		if(m_pProjectile)
			return m_pProjectile.m_pOwner;
		return null;
	}

	protected override void Start() {
		base.Start();
		m_pProjectile = GetComponent<Projectile>();
	}

	protected override void Awake() {
		base.Awake();
		if (!m_pProjectile)
			m_pProjectile = GetComponent<Projectile>();
	}

	public Projectile GetProjectile() {
		return m_pProjectile;
	}

	public override Damage GetDamage(Hitbox pReceiverBox = null) {
		if (!m_pProjectile)
			return null;

		// Let base class decide which hitsound should be used
		Damage pBaseDmg = base.GetDamage(pReceiverBox);

		Damage pDmg = new Damage(m_pProjectile.m_pDamageInfo);
		pDmg.m_ding = pBaseDmg.m_ding;
		pDmg.amount = Mathf.CeilToInt(pDmg.amount * (m_bCanHitLocations ? pReceiverBox.GetMultiplier() : 1.0f));

		return pDmg;
	}

	public override void OnHit(Hitbox pOther) {
		if (!m_bIsActive)
			return;

		if (pOther.GetTeam() == GetTeam())
			return;

		if (m_bDeactivateOnHitOther)
			m_bIsActive = false;

		base.OnHit(pOther);

		if (m_pProjectile)
			m_pProjectile.OnHit(pOther, this);
	}

	public override int GetTeam() {
		if (!m_pProjectile)
			return -1;
		return m_pProjectile.m_nTeam;
	}

	public override string GetOwnerName() {
		if (m_pProjectile)
			return m_pProjectile.name;
		return base.GetOwnerName();
	}
}
