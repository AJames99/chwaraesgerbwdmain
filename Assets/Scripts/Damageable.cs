﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IProjectileUser {
	// Returns the worldspace position and worldspace direction of where projectiles should be fired from.
	public abstract void GetProjectileFiringSetup(out Vector3 vPosition, out Vector3 vDirection);
}

public class Damage {
	public enum DamageTypes {
		DEFAULT,
		PROJECTILE,
		MELEE,
		MAGIC,
		EXPLOSION,
		IMPACT
	}

	public DamageTypes damageType = DamageTypes.DEFAULT;

	public int amount = 0;

	public Vector3 origin = Vector3.zero;

	public enum DamageDing {
		NONE,
		NORMAL,
		CRITICAL
	}

	public DamageDing m_ding = DamageDing.NONE;

	public Damage(DamageTypes type = DamageTypes.DEFAULT, int dmg = 0, Vector3? origin = null, DamageDing ding = DamageDing.NONE) {
		damageType = type;
		amount = dmg;
		if(null != origin)
			this.origin = (Vector3) origin;
		m_ding = ding;
	}

	public Damage(Damage copy) {
		damageType = copy.damageType;
		amount = copy.amount;
		origin = copy.origin;
	}
}

public abstract class Damageable : MonoBehaviour, IProjectileUser {
	public const string npcTag = "NPC";
	public const string playerTag  = "Player";
	public const string damagingWallTag = "Wall";

	const float IMPACT_MINIMUM_INTERVAL = 0.2f;

	public int healthPoints = 100;
	public int maxHealthPoints = 100;

	[SerializeField]
	protected float damageResistanceFactor = 0f;

	public float characterMass = 1.0f;
	public float minimumMomentumToImpact = 25.0f; // How much momentum (mass * speed) is needed to launch this character?

	[SerializeField]
	protected AnimationCurve impactDamageCurve; // Determines the relationship between velocity and damage
	[SerializeField]
	protected Vector2 impactDamageVelocityBounds = new Vector2(5f, 50f); // Determines the min & max values mapped onto the curve
	[SerializeField]
	protected Vector2 impactDamageBounds = new Vector2(5f, 100f); // Determines the min & max values mapped onto the curve
	[HideInInspector]
	public float minimumLaunchTime = 0.25f; // The minimum amount of time a damageable should be airborne before returning to the ground - helps cancel immediate groundings.
	
	public float impactSoftHardSpeedBoundary = 0.5f; // The amount along the impact damage curve we must be to transition from soft sounds to hard sounds

	public bool overhealAllowed = true;
	public float maxOverhealFactor = 1.5f;
	public int overhealDecayAmount = 3;
	public float overhealDecayInterval = 0.5f;
	protected float overhealTimer = 0f;

	public const int PLAYERTEAM = 1;
	public const int ENEMYTEAM = 0;
	public int team = ENEMYTEAM;

	[HideInInspector]
	public AudioSource reusableAudioSource;
	[HideInInspector]
	public ClipPlayer clipPlayer;

	// First two values are health ratios (i.e. health / maxhealth) and the second two are multipliers for impacts with other damageables
	static Vector4 v4KnockbackMultiplier = new Vector4(0.1f, 1.5f, 3.0f, 0.5f);

	private bool _isDead = false;
	public bool isDead {
		set {
			_isDead = value;
			//DeathCheck();
			if (value) {
				Die();
			}
		}
		get {
			return _isDead;
		}
	}

	public virtual void Start() {
		clipPlayer = GetComponent<ClipPlayer>();
		if (!clipPlayer) {
			clipPlayer = gameObject.AddComponent<ClipPlayer>();
		}

		reusableAudioSource = GetComponent<AudioSource>();
		if (!reusableAudioSource) {
			//reusableAudioSource = gameObject.AddComponent<AudioSource>();
			//reusableAudioSource.rolloffMode = AudioRolloffMode.Logarithmic;
			//reusableAudioSource.minDistance = 15;
			//reusableAudioSource.maxDistance = 150;
			//reusableAudioSource.dopplerLevel = 0;
			//reusableAudioSource.spread = 0;
			//reusableAudioSource.spatialBlend = 1.0f;
			//reusableAudioSource.mute = reusableAudioSource.loop = reusableAudioSource.playOnAwake = false;

			reusableAudioSource = Instantiate(Resources.Load("Prefab/ReusableAudioSource") as GameObject, transform).GetComponent<AudioSource>();
		}
	}

	public abstract IController GetCharacterController();

	public bool makesDamageNumbers = true;
	//private int uniqueDamageNumberID;

	protected float flNextImpactTime = 0.0f;

	public virtual void GetProjectileFiringSetup(out Vector3 vPosition, out Vector3 vDirection) {
		vPosition = transform.position;
		vDirection = transform.forward;
	}

	public virtual void StruckByHitbox(Hitbox pBoxReceiver, Hitbox pBoxAttacker) {
		if (team != pBoxAttacker.GetTeam()) {
			Hurt(pBoxAttacker.GetDamage(pBoxReceiver));

			if (pBoxAttacker.GetDamageable() == PlayerCharacter.GetMainCharacter()) {
				PlayerCharacter.GetMainCharacter().DoHitsound(pBoxAttacker.GetDamage(pBoxReceiver));
			}

			//Debug.Log("Hitbox (" + pBoxReceiver.name + ") on " + pBoxReceiver.GetOwnerName() + " hit by hitbox(" + pBoxAttacker.name + ") on " + pBoxAttacker.GetOwnerName());
		}
	}

	public virtual void Hurt(Damage damage) {
		healthPoints = Mathf.Max(0, healthPoints - damage.amount);
		if (makesDamageNumbers)
			DamageNumberManager.MakeNumber(transform.position, damage.amount, this);
		
		if (damage.m_ding != Damage.DamageDing.NONE) {
			PlayerCharacter pPlayer = PlayerCharacter.GetMainCharacter();
			if (pPlayer) {
				pPlayer.DoHitsound(damage);
			}
		}

		DeathCheck();
	}

	public virtual void Heal(int hp) {
		healthPoints = Mathf.Min(overhealAllowed ? Mathf.CeilToInt(maxHealthPoints * maxOverhealFactor) : maxHealthPoints, healthPoints + hp);
		overhealTimer = Time.time + overhealDecayInterval;
	}

	protected virtual void Update() {
		if (overhealAllowed && healthPoints > maxHealthPoints) {
			if (Time.time > overhealTimer) {
				overhealTimer = Time.time + overhealDecayInterval;
				healthPoints = Mathf.Max(maxHealthPoints, healthPoints - overhealDecayAmount);
			}
		}
	}

	protected void DeathCheck() {
		if (healthPoints <= 0) {
			Die();
			_isDead = true;
		}
	}

	public virtual float GetForceMultiplier() {
		//return 1.0f;
		return (healthPoints / (float)maxHealthPoints).RemapClamped(v4KnockbackMultiplier.x, v4KnockbackMultiplier.y, v4KnockbackMultiplier.z, v4KnockbackMultiplier.w);
	}

	protected Vector3 impactVerticalBoost = Vector3.up * 12f;

	// I think this gets called twice, so half the velocities and forces in question?
	public virtual bool ImpactedBy(Damageable other) {
		IController hitController = other.GetCharacterController();
		IController thisController = GetCharacterController();
		Vector3 relativeMomentum = (hitController.GetVelocity() * other.characterMass) - (thisController.GetVelocity() * characterMass);
		Vector3 toUs = transform.position - other.transform.position;

		toUs.Normalize();

		// To ensure that each imapct is managed once and only once per interaction.
		if (Time.time < flNextImpactTime || Time.time < other.flNextImpactTime)
			return false;

		// Ensure the velocity is in the same direction as the vector between the two bodies
		float relativeMomentSq = relativeMomentum.sqrMagnitude * Mathf.Max(Vector3.Dot(toUs, relativeMomentum.normalized), 0f);
		if (relativeMomentSq > minimumMomentumToImpact*minimumMomentumToImpact && relativeMomentSq > other.minimumMomentumToImpact*other.minimumMomentumToImpact) {

			// This is v1.0. It appears to be the wrong way around, as-in the shield forceMultiplier is being applied to the wrong force
			//Vector3 momentumStolen = (hitController.GetVelocity() * other.characterMass * forceMultiplier) + impactVerticalBoost;// * 0.5f;
			//hitController.AddForce(-momentumStolen / other.characterMass);
			//thisController.AddForce(momentumStolen / characterMass);

			// v2.0: Elastic Collisions.
			// Force multipliers affect the forces *they* apply to *others*
			float m1 = characterMass;
			float m2 = other.characterMass;
			Vector3 u1 = thisController.GetVelocity();
			Vector3 u2 = hitController.GetVelocity();
			float fm1 = GetForceMultiplier();
			float fm2 = other.GetForceMultiplier();
			//Debug.Log("fm1 " + fm1 + ", fm2 " + fm2);

			Vector3 v1 = (u1 * (m1 - m2) / (m1 + m2)) + (u2 * fm2 * (2 * m2) / (m1 + m2));
			Vector3 v2 = (u1 * fm1 * (2 * m1) / (m1 + m2)) + (u2 * (m2 - m1)/(m1 + m2));

			//thisController.AddForce(v1);
			//hitController.AddForce(v2);
			thisController.SetVelocity(v1);
			hitController.SetVelocity(v2);
			//thisController.AddForce(impactVerticalBoost);
			hitController.AddForce(impactVerticalBoost);

			Debug.Log("Collision success: Processed by " + name + ", colliding with " + other.name);

			other.flNextImpactTime = flNextImpactTime = Time.time + IMPACT_MINIMUM_INTERVAL;

			//other.OnImpactedBy(this);

			OnLaunched();
			return true;
		}

		return false;
	}

	// If A handles a collision between A and B, let B know with this function.
	public virtual void OnImpactedBy(Damageable other) {
	
	}

	public virtual void ImpactedWall() {
		Vector3 velocity = GetCharacterController().GetVelocity();
		float speed = velocity.magnitude;
		if (speed > impactDamageVelocityBounds.x) {
			float remapped = speed.Remap(impactDamageVelocityBounds.x, impactDamageVelocityBounds.y, 0f, 1f);
			float curveValue = impactDamageCurve.Evaluate(remapped).RemapClamped(0f,1f, impactDamageBounds.x, impactDamageBounds.y);
			int damage = Mathf.CeilToInt(curveValue);
			Hurt(new Damage(Damage.DamageTypes.IMPACT, damage));
			GetCharacterController().SetVelocity(
				(-velocity * 0.1f).WithY(impactVerticalBoost.y)
			);
			OnHitWall(remapped);
			//GetCharacterController().AddForce(impactVerticalBoost, true);
			//Debug.Log(name + ".ImpactedWall() succeeded with " + damage + " damage.");
		} else {
			//Debug.Log(name + ".ImpactedWall() failed!");
		}
	}

	public virtual void OnLaunched() {}
	public virtual void OnHitWall(float speed01 = -1) {}

	public abstract void Push(Vector3 force, Vector3? point = null);

	public abstract void Die();


	protected void OnTriggerEnter(Collider other) {
		string hitTag = other.tag;
		switch (hitTag) {
			case playerTag:
			case npcTag:
				// Only look at other damageables' triggers.
				if (!other.isTrigger)
					return;
				Damageable hitDamageable = other.GetComponent<Damageable>(); // Assume it's guaranteed to have one, if not then don't tag it lmao
				hitDamageable.ImpactedBy(this); // Push the other guy, and let HIS code (this function) push us back??
				break;
			case damagingWallTag:
				ImpactedWall();
				break;
		}
	}
}
