using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIComponent : MonoBehaviour {
	[Serializable]
	public class MaterialParam {
		[SerializeField]
		public string materialName;
		[SerializeField]
		public string paramName;
		[SerializeField]
		private float _paramValue;

		[HideInInspector]
		public Material material;
		public float paramValue {
			set {
				_paramValue = value;
				material?.SetFloat(paramName, value);
			}
			get {
				return _paramValue;
			}
		}
	}

	[SerializeField]
	MeshRenderer meshRenderer;
	Material[] materials;

	[SerializeField]
	protected MaterialParam[] materialParams;

	// Start is called before the first frame update
	protected virtual void Start() {
		for (int i = 0; i < materialParams.Length; i++) {
			// Which material instances are compatible with the provided materialparams?
			List<Material> validAssociatedMats = new List<Material>();

			foreach (Material mat in meshRenderer.materials) {
				if (mat.name.Equals(materialParams[i].materialName + " (Instance)")) {
					materialParams[i].material = mat;
					//Debug.Log("Material assigned: " + mat.name);
					materialParams[i].material.SetFloat(materialParams[i].paramName, materialParams[i].paramValue);
					break;
				}
			}
		}
	}
}