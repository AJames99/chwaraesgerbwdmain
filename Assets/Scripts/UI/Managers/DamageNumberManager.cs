using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageNumberManager : MonoBehaviour {

	public static DamageNumberManager instance = null;

	public int maxActiveSystems = 16;
	static int currentHead = 0;

	DamageNumber[] damageNumbersPool;
	// Workaround to create a truly 1:1 data structure
	static Dictionary<int, Damageable> indicesToObjects = new Dictionary<int, Damageable>(16); //Maps indices in the pool to Damageable object IDs
	static Dictionary<Damageable, int> objectsToIndices = new Dictionary<Damageable, int>(16);

	[SerializeField]
	DamageNumber damageNumberPrefab;

	[SerializeField]
	Color defaultFGColour = Color.white;

	[SerializeField]
	Color defaultDropsShadowColour = new Color(0.37f, 0f, 0f);

	const string damageString = "{0}!";


	// DEBUG
	/*class TestDmg : Damageable {
		string name;
		public TestDmg(string name) {
			this.name = name;
		}

		public override void Die() {
	
		}

		public override void Push(Vector3 force, Vector3? point = null) {
	
		}
	}*/

	// Start is called before the first frame update
	void Awake() {
		if (!instance) {
			instance = this;
			instance.damageNumbersPool = new DamageNumber[maxActiveSystems];
			for (int i = 0; i < maxActiveSystems; i++) {
				instance.damageNumbersPool[i] = Instantiate(damageNumberPrefab, Vector3.zero, Quaternion.identity);
				instance.damageNumbersPool[i].gameObject.SetActive(false);
			}
		}
	}

	/*float timer_interval = 2;
	float timer = 0;
	float timer2 = 1;
	TestDmg tempyOne = new TestDmg("one");
	TestDmg tempyTwo = new TestDmg("two");
	private void Update() {
		if (Time.time > timer) {
			Vector2 randy = Random.insideUnitCircle * 5;
			//MakeNumber(new Vector3(randy.x, 3.0f, randy.y), Random.Range(15, 300), tempyOne);
			MakeNumber(new Vector3(-5.0f, 3.0f, 0.0f), 1, tempyOne);
			timer = Time.time + timer_interval;
		}
		if (Time.time > timer2) {
			MakeNumber(new Vector3(5.0f, 3.0f, 0.0f), 2, tempyTwo);
			timer2 = Time.time + timer_interval;
		}
	}*/

	int GetFirstAvailableIndex() {
		if (damageNumbersPool[currentHead].isInUse)
			damageNumbersPool[currentHead].MakeInactive();
		int available = currentHead;

		currentHead++;
		if (currentHead >= maxActiveSystems)
			currentHead = 0;
		return available;
	}


	public static void MakeNumber(Vector3 worldPosition, int damageAmount, Damageable dmgable) {
		MakeNumber(worldPosition, damageAmount, dmgable, instance.defaultFGColour, instance.defaultDropsShadowColour);
	}
	public static void MakeNumber(Vector3 worldPosition, int damageAmount, Damageable dmgable, Color fgColour, Color bgColour) {
		DamageNumber damageNumber;
		// First check to see if we're already in the dictionary:
		if (objectsToIndices.ContainsKey(dmgable)) {
			// If we are, make sure the damage number assigned to us isn't already in use by another Damageable
			int id = objectsToIndices[dmgable];
			if (instance.damageNumbersPool[id].isInUse && !indicesToObjects[id].Equals(dmgable)) { // If in use by another Damageable, get us a new ID
				id = instance.GetFirstAvailableIndex();
				damageNumber = instance.damageNumbersPool[id];
				indicesToObjects[id] = dmgable;
				objectsToIndices[dmgable] = id;
				//Debug.Log("In use by another DMGable; Getting new index. " + damageNumber.numberCurrent);
			} else { // If in use by this Damageable, 
				damageNumber = instance.damageNumbersPool[objectsToIndices[dmgable]];
				int lastDmg = damageNumber.numberCurrent + damageAmount;
				damageNumber.gameObject.SetActive(false); //Deactivate it so we can reactivate with the new value; resets numberCurrent so cache before
				damageNumber.numberCurrent = lastDmg;
				//Debug.Log("Either not in use or in use by this DMGable; Recycling index. " + damageNumber.numberCurrent);
			}
			
		} else {
			int id = instance.GetFirstAvailableIndex();
			// Quick check to see if this id is already defined, if so overwrite
			if (indicesToObjects.ContainsKey(id)) {
				indicesToObjects[id] = dmgable;
			} else {
				indicesToObjects.Add(id, dmgable);
			}
			objectsToIndices.Add(dmgable, id);
			damageNumber = instance.damageNumbersPool[id];
			damageNumber.gameObject.SetActive(false); //Deactivate real quick
			damageNumber.numberCurrent = damageAmount;

			//Debug.Log("This DMGable hasn't made any numbers yet; Getting new index. " + damageNumber.numberCurrent);
		}
		damageNumber.associatedText[0].color = fgColour;
		damageNumber.associatedText[1].color = bgColour;
		damageNumber.associatedText[0].text = string.Format(damageString, damageNumber.numberCurrent);
		damageNumber.associatedText[1].text = string.Format(damageString, damageNumber.numberCurrent);
		damageNumber.transform.position = worldPosition;
		damageNumber.gameObject.SetActive(true);
	}
}