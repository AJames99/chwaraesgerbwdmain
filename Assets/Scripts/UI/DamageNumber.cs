using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageNumber : MonoBehaviour {

	Animator animator;

	[HideInInspector]
	public bool isInUse = false;

	public TextMesh[] associatedText;

	static Transform mainCamera;

	bool initialised = false;

	[HideInInspector]
	public bool isUILayer = false;

	[HideInInspector]
	public int numberCurrent = 0;

	const string animatorTrigger = "Activate";
	public void Initialise() {
		animator = GetComponent<Animator>();
		if (!mainCamera && !isUILayer)
			mainCamera = Camera.main.transform;
	}

	private void Update() {
		if(!isUILayer)
			transform.LookAt(mainCamera);
	}

	private void Awake() {
		if (!initialised)
			Initialise();
		isInUse = true;
		animator.SetTrigger(animatorTrigger);
	}

	public void MakeInactive() {
		isInUse = false;
		gameObject.SetActive(false);
		numberCurrent = 0;
	}
}