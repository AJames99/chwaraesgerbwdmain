using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Yetman.PostProcess;


// A Handsome wrapper class for the health bar UI
public class UIHealthBar : UIComponent {

	const string percentParamName = "time";
	const string borderParamName = "borderstrength";
	const string overhealMatName = "UI Heart Overheal";
	int[] healthPercentIndices, healthBorderIndices;
	int overhealPercentIndex, overhealBorderIndex;
	bool lerping;
	float healthCurrent;
	int healthTarget;

	[SerializeField]
	float interpRate = 0.25f;

	[SerializeField]
	Vector2 borderStrengthBasedOnHealth = new Vector2(0.2f, 0.03f);

	[SerializeField]
	ParticleSystem heartFlutterSystem = null;
	[SerializeField]
	Animator heartPoofSystem = null;

	[SerializeField]
	DamageNumber hpBarDamageNumber;

	[HideInInspector]
	public int maxHP, maxOverhealHP;

	[Header("Post Process Effects")]
	[SerializeField]
	VolumeProfile ppVolume;
	GrayscaleEffect greyscalePP;
	InvertEffect invertPP;
	bool hasBothPPEffects = false;
	bool invertEffectOn = false;
	[SerializeField]
	float invertFlashDuration = 0.1f;
	private float invertFlashTimer = 0f;

	int damageLast = 0;

	protected override void Start() {
		base.Start();
		List<int> healthPercentIndices = new List<int>();
		List<int> healthBorderIndices = new List<int>();
		for (int i = 0; i < materialParams.Length; i++) {
			if (materialParams[i].materialName.Equals(overhealMatName)) {
				if (materialParams[i].paramName.Equals(percentParamName))
					overhealPercentIndex = i;
				else if (materialParams[i].paramName.Equals(borderParamName))
					overhealBorderIndex = i;
				continue;
			}

			if (materialParams[i].paramName.Equals(percentParamName))
				healthPercentIndices.Add(i);
			else if (materialParams[i].paramName.Equals(borderParamName))
				healthBorderIndices.Add(i);
		}
		this.healthPercentIndices = healthPercentIndices.ToArray();
		this.healthBorderIndices = healthBorderIndices.ToArray();
		lerping = false;

		if(ppVolume) {
			foreach (VolumeComponent comp in ppVolume.components) {
				if (comp.name.Equals("GrayscaleEffect"))
					greyscalePP = (GrayscaleEffect) comp;
				else if (comp.name.Equals("InvertEffect"))
					invertPP = (InvertEffect) comp;
			}
			if (invertPP && greyscalePP)
				hasBothPPEffects = true;
		}

		if (hpBarDamageNumber)
			hpBarDamageNumber.isUILayer = true; // Stop the face-to-camera and warping effects
	}

	private void Update() {
		if (lerping) {
			foreach (int index in healthPercentIndices) {
				materialParams[index].paramValue = Mathf.Min(healthCurrent / maxHP, 1);
			}
			materialParams[overhealPercentIndex].paramValue = (healthCurrent - maxHP) / (maxOverhealHP - maxHP);
			foreach (int index in healthBorderIndices) {
				materialParams[index].paramValue = Mathf.Min(healthCurrent, maxHP).Remap(0, maxHP, borderStrengthBasedOnHealth.x, borderStrengthBasedOnHealth.y);
			}
			materialParams[overhealBorderIndex].paramValue = (healthCurrent - maxHP).Remap(0, (maxOverhealHP - maxHP), borderStrengthBasedOnHealth.x, borderStrengthBasedOnHealth.y);

			healthCurrent = healthCurrent.InterpAdditive(healthTarget, interpRate, Time.deltaTime);
			if (Mathf.Abs(healthCurrent - healthTarget) <= 0.001f)
				lerping = false;
		}
		if (invertEffectOn) {
			greyscalePP.blend.value = invertPP.blend.value = (invertFlashTimer - Time.time).Remap(invertFlashDuration, 0f, 1f, 0f);
			if (Time.time > invertFlashTimer) {
				invertPP.active = false;
				greyscalePP.active = false;
				invertEffectOn = false;
			}
		}
	}

	/*public void SetHealth(float hp) {
		if (hpBarDamageNumber) {
			hpBarDamageNumber.gameObject.SetActive(false);
			int dmgAmount = Mathf.CeilToInt((hp - healthTarget) * maxHP);
			hpBarDamageNumber.associatedText[0].text = hpBarDamageNumber.associatedText[1].text = 
				string.Format(dmgAmount < 0 ? "{0}!" : "+{0}!", dmgAmount);
			hpBarDamageNumber.gameObject.SetActive(true);
		}

		healthTarget = hp;
		lerping = true;
		//Debug.Log("HEALTH %:" + hp);
		heartFlutterSystem.Play();
	}*/

	public void SetHealth(int hp, bool playEffects = true) {
		if (playEffects) {
			int dmgAmount = 0;
			if (hpBarDamageNumber) {
				dmgAmount = Mathf.CeilToInt((hp - healthTarget));
				if (hpBarDamageNumber.isInUse || hpBarDamageNumber.gameObject.activeSelf) {     // For combining multiple instances of damage into one
					dmgAmount += damageLast;
					//Debug.Log(damageLast);
				}

				// Poof FX on heal
				if (dmgAmount > 0 && heartPoofSystem)
					heartPoofSystem.SetTrigger("Poof");

				damageLast = dmgAmount;
				hpBarDamageNumber.MakeInactive();
				hpBarDamageNumber.associatedText[0].text = hpBarDamageNumber.associatedText[1].text =
					string.Format(dmgAmount < 0 ? "{0}!" : "+{0}!", dmgAmount);
				hpBarDamageNumber.gameObject.SetActive(true);
				Debug.Log("dmg amount = " + dmgAmount);
			} else {
				Debug.Log("Was undefined");
			}

			// Flash on damaged
			if (hasBothPPEffects && dmgAmount < 0) {
				invertFlashTimer = Time.time + invertFlashDuration;
				invertPP.active = true;
				invertPP.blend.value = 1.0f;
				greyscalePP.active = true;
				greyscalePP.blend.value = 1.0f;
				invertEffectOn = true;
			}
		}

		healthTarget = hp;
		lerping = true;
		
		if(playEffects)
			heartFlutterSystem.Play();
	}

	/*	public void SetHealthSnap(float hp) {
			healthCurrent = healthTarget = hp;
			lerping = true;
			heartFlutterSystem.Play();
		}
	*/
	public void SetHealthSnap(int hp) {
		healthCurrent = healthTarget = hp;
		lerping = true;
		heartFlutterSystem.Play();
	}
}
