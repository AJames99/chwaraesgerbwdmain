﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CydrannauEndid.States {
	public abstract class EntityState {

		// The age of this state (based on Update time interval)
		public float age = 0f;
		// The age of this state (based on FixedUpdate time interval)
		public float fixedAge = 0f;
		// The machine using this state
		public EntityStateMachine outer;

		public abstract string GetStateName();
		public virtual void OnEnter() { }
		public virtual void OnExit() { }

		public virtual void Update() {
			age += Time.deltaTime;
		}
		public virtual void FixedUpdate() {
			fixedAge += Time.fixedDeltaTime;
		}
	}

	public class Uninitialized : EntityState {
		public override string GetStateName() { return "Unitialized"; }
	}
}