﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CydrannauEndid.States {

	// TODO update this to match EntityComponent structure
	public class EntityStateMachine : CydranEndid {

		public override EMathCydran CaelMathCydran() {
			return EMathCydran.EntityStateMachine;
		}

		public EntityState state;
		private EntityState nextState;

		public DebugTextReadout debugText = null;

		private void Awake() {
			state = new Uninitialized();
			state.outer = this;
		}

		public void SetNextState(EntityState newNextState) {
			nextState = newNextState;
		}

		private void Update() {
			if (nextState != null) {
				SetState(nextState);
			}
			state.Update();
		}

		private void FixedUpdate() {
			state.FixedUpdate();
		}

		public void SetState(EntityState newState) {
			nextState = null;
			newState.outer = this;
			if (state == null)
				Debug.LogError("State machine does not have a non-null state!");

			state.OnExit();
			state = newState;
			state.OnEnter();
		}

		public override bool Init(ref List<string> issueStrings) {
			issueStrings = null;
			return true;
		}
	}
}