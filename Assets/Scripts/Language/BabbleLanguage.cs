﻿using System;
using System.Collections.Generic;

/// <summary>
/// Babble Languages define the properties of a language that produces 
/// "babble" speech - speech using a discrete relationship between
/// characters and phonemes that is largely nonsensical in nature but
/// produces a relatively unique spoken sentence for every unique set of
/// input characters. Based on games like Animal Crossing and Jalopy.
/// </summary>
public abstract class BabbleLanguage {
	public enum SyllableType {
		VOWEL,
		CONSONANTVOWEL,
		CONSONANT,
	}

	public struct Syllable {
		public string IPAKey;
		public SyllableType type;
	}

	public struct Substring {
		public string str;
		//Syllable syllable;
	}

	//char[] skipChars = { '-', '_', '' };

	//public Substring[] substrings;
	public Dictionary<Substring, Syllable> syllableLookupCV = new Dictionary<Substring, Syllable>();
	public Dictionary<Substring, Syllable> syllableLookupC = new Dictionary<Substring, Syllable>();
	public Dictionary<Substring, Syllable> syllableLookupV = new Dictionary<Substring, Syllable>();
	public Dictionary<Syllable, string> soundFileLookup = new Dictionary<Syllable, string>();

	public abstract BabbleLanguage GenerateLanguage();

	public virtual Substring[] DecomposeString(string inputString) {
		inputString = inputString.ToLower();
		List<Substring> outputChain = new List<Substring>();
		while (inputString.Length > 0) {
			// Try to find consonant-vowels first
			foreach (Substring substr in syllableLookupCV.Keys) {
				if (inputString.StartsWith(substr.str)) {   // If this substring is present at the start of the input
					outputChain.Add(substr);    // This substring definition to the output chain
					inputString = inputString.Substring(substr.str.Length - 1); // Trim input string to shave off the chars we've just identified
					continue;
				}
			}
			// Then Consonants
			foreach (Substring substr in syllableLookupC.Keys) {
				if (inputString.StartsWith(substr.str)) {   // If this substring is present at the start of the input
					outputChain.Add(substr);    // This substring definition to the output chain
					inputString = inputString.Substring(substr.str.Length - 1); // Trim input string to shave off the chars we've just identified
					continue;
				}
			}
			// Then vowels
			foreach (Substring substr in syllableLookupV.Keys) {
				if (inputString.StartsWith(substr.str)) {   // If this substring is present at the start of the input
					outputChain.Add(substr);    // This substring definition to the output chain
					inputString = inputString.Substring(substr.str.Length - 1); // Trim input string to shave off the chars we've just identified
					continue;
				}
			}
			// This is reached if no matching substring was found, skipping the character.
			inputString = inputString.Substring(1);
		}
		return outputChain.ToArray();
	}
}

public class EnglishBabble : BabbleLanguage {
	// These two get combined
	static string[] consonantDefinitions = { "b,b", "br,br", "bl,bl", "c,k", "cr,kr", "cl,kl", "ch,tʃ", "d,d", "dr,dr", "dl,dl", "f,f", "fr,fr", "fl,fl", "g,g", "gr,gr", "gl,gl", "h,h", "j,dʒ", "k,k", "kr,kr", "kl,kl", "l,l", "m,m", "n,n", "ng,ŋ", "p,p", "pr,pr", "pl,pl", "r,r", "s,s", "sb,sb", "st,st", "sd,sd", "sw,sw", "sg,sg", "sp,sp", "sk,sk", "sc,sk", "sl,sl", "sh,ʃ", "sht,ʃ", "t,t", "tr,tr", "th,θ", "v,v", "vl,vl", "w,w", "wr, r", "z,z", "y,j" };
	static string[] vowelDefinitions = { "a,ɑ", "ah,ɑ", "e,ɛ", "eh,ɛ", "i,i", "ee,i", "ih,ɪ", "o,ɒ", "oh,ɒ", "u,ə", "uh,ə", "oo,u", "er,ə" };

	// These get read raw
	static string[] consonantVowelDefinitions = { };

	public const string testString = "Hello world! I am a robot.";

	public override BabbleLanguage GenerateLanguage() {
		List<Substring> substringsList = new List<Substring>();
		foreach (string vowelDef in vowelDefinitions) {
			string[] split = vowelDef.Split(',');
			syllableLookupV.Add(new Substring() { str = split[0] }, new Syllable() { IPAKey = split[1], type = SyllableType.VOWEL });
		}
		foreach (string consDef in consonantDefinitions) {
			string[] split = consDef.Split(',');
			syllableLookupC.Add(new Substring() { str = split[0] }, new Syllable() { IPAKey = split[1], type = SyllableType.CONSONANT });
		}

		foreach (string definition in consonantDefinitions) {
			string cons_glyph = definition.Split(',')[0];
			string cons_IPA = definition.Split(',')[1];
			foreach (string vowelDef in vowelDefinitions) {
				string vowel_glyph = vowelDef.Split(',')[0];
				string vowel_IPA = vowelDef.Split(',')[1];

				syllableLookupCV.Add(new Substring() { str = cons_glyph + vowel_glyph }, new Syllable() { IPAKey = cons_IPA + vowel_IPA, type = SyllableType.CONSONANTVOWEL });
			}
			//substringsList.Add(new Substring() {str = ""});
		}

		return this;
	}

	/*public static void Main() {
		EnglishBabble en = new EnglishBabble();
		en.GenerateLanguage();
		Substring[] substrs = en.DecomposeString(EnglishBabble.testString);
		Console.WriteLine("\"");
		foreach (Substring substr in substrs) {
			Console.Write(substr.str + " ");
		}
		Console.Write(".\"");
	}*/
}
