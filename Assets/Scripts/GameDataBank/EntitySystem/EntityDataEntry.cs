/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodweddionEndid;


// THIS WHOLE THING IS REDUNDANT AND IT SUX, TOO MUCH DUPLICATION AND WORK FOR ADDING SIMPLE TINGS
// going forward, data will be loaded directly from charon since it's already a fuckin DATABASE BRO

namespace DataBank.EntitySystem {
	public class EntityDataEntry {
		public string m_entityId;
		public string m_baseEntityId;

		public EntityDataEntry m_parentEntity;

		public Dictionary<string, DataBank.EntitySystem.DataCydranEndid> m_components = new Dictionary<string, DataBank.EntitySystem.DataCydranEndid>();

		public EntityDataEntry(string entityId, EntityDataEntry baseEntity, DataBank.EntitySystem.DataCydranEndid[] cydrannau ) {
			m_entityId = entityId;
			if (baseEntity != null) {
				m_baseEntityId = baseEntity.m_entityId;
			}
			m_parentEntity = baseEntity;

			// Copy inherited components
			ApplyInheritance();

			foreach (DataBank.EntitySystem.DataCydranEndid cydran in cydrannau) {
				OverrideOrAddComponent(cydran.CaelMathCydran(), cydran);
			}
		}

		// Adds the base type's components to this
		public void ApplyInheritance() {
			if (m_parentEntity != null) {
				Debug.Log("Inheriting from parent entity " + m_parentEntity.m_entityId);
				foreach (string key in m_parentEntity.m_components.Keys) {
					OverrideOrAddComponent(key, m_parentEntity.m_components[key]);
				}
			}
		}

		// If the incoming component exists, overwrite any properties defined by the incoming component
		public void OverrideOrAddComponent(string key, DataBank.EntitySystem.DataCydranEndid component) {
			if (m_components.ContainsKey(key)) {
				Debug.Log("Component " + key + " already exists, overwriting with incoming component's properties");
				m_components[key] = component;
			} else {
				m_components.Add(key, component);
			}
		}

		public override string ToString() {
			string str = m_entityId;
			if (m_baseEntityId != null && m_baseEntityId.Length > 0) {
				str += "(" + m_baseEntityId + ")";
			}
			str += "[";
			int iters = 0;
			foreach (string key in m_components.Keys) {
				str += key + " : " + m_components[key].ToString();
				str += ", ";
				iters++;
			}
			if (iters > 0) {
				str.Remove(str.Length - 3, 2);
			}
			str += "]";
			return str;
		}
	}

	#region Parameters
	// All component params extemd this, so their inheritance
	// can be done in a type-agnostic way.
	public abstract class EntityComponentParameter {
		public string m_key;
		public bool m_overridable = true;
		public abstract EntityComponentParameter Clone();
		//public abstract System.Type GetParamType();
	}

	public class EntityComponentParameter<T> : EntityComponentParameter {
		public T m_value;
		public EntityComponentParameter(string key, T value, bool overridable = true) {
			m_key = key;
			m_value = value;
			m_overridable = overridable;
		}

		public override string ToString() {
			string str = "";
			if (!m_overridable) {
				str += "(!)";
			}
			str += m_value.ToString();
			return str;
		}

		public override EntityComponentParameter Clone() {
			return new EntityComponentParameter<T>(m_key, m_value, m_overridable);
		}

		//public override System.Type GetParamType() {
		//	return typeof(T);
		//}
	}
	#endregion

	#region Components

	// Old obsolete component from when components were abstract sets of values with
	[System.Obsolete]
	public class EntityComponentEntryOLD {
		public string m_componentType;
		// Param list:
		public Dictionary<string, EntityComponentParameter> m_parameters = new Dictionary<string, EntityComponentParameter>();
		public EntityComponentEntryOLD(string componentType, params EntityComponentParameter[] parameters) {
			m_componentType = componentType;

			foreach (EntityComponentParameter param in parameters) {
				OverrideOrAddParam(param.m_key, param);
			}
		}

		//Copy constructor
		public EntityComponentEntryOLD(EntityComponentEntryOLD other) {
			m_componentType=other.m_componentType;
			foreach (string key in other.m_parameters.Keys) {
				m_parameters.Add(key, other.m_parameters[key].Clone());
			}
		}

		// Since the entities are process hierarchically, they can just inherit their parent properties at face value
		// - they'll be set up to inherit from the parent anyway
		// 'this' is the inherited component, the derivedComponent param is the derived entity's component
		// overwrites 'this' parameters with derviedComponent parameters
		public void OverwriteProperties(EntityComponentEntryOLD derivedComponent) {
			if (derivedComponent != null) {
				foreach (string key in derivedComponent.m_parameters.Keys) {
					OverrideOrAddParam(key, derivedComponent.m_parameters[key]);
				}
			}
		}

		public void OverrideOrAddParam(string key, EntityComponentParameter parameter) {
			if (m_parameters.ContainsKey(key)) {
				if (m_parameters[key].m_overridable) {
					m_parameters[key] = parameter;
				}
			} else {
				m_parameters.Add(key, parameter);
			}
		}

		public bool TryGetParameter(string key, out EntityComponentParameter param) {
			if (m_parameters.ContainsKey(key)) {
				param = m_parameters[key];
				return true;
			} else {
				param = null;
				return false;
			}
		}

		public bool TryGetParameterValueFloat(string key, ref float value) {
			EntityComponentParameter param;
			if (TryGetParameter(key, out param) && (param is EntityComponentParameter<float>)) {
				value = (param as EntityComponentParameter<float>).m_value;
				return true;
			}
			return false;
		}

		public bool TryGetParameterValueInt(string key, ref int value) {
			EntityComponentParameter param;
			if (TryGetParameter(key, out param) && (param is EntityComponentParameter<int>)) {
				value = (param as EntityComponentParameter<int>).m_value;
				return true;
			}
			return false;
		}

		public bool TryGetParameterValueBool(string key, ref bool value) {
			EntityComponentParameter param;
			if (TryGetParameter(key, out param) && (param is EntityComponentParameter<bool>)) {
				value = (param as EntityComponentParameter<bool>).m_value;
				return true;
			}
			return false;
		}

		public bool TryGetParameterValueString(string key, ref string value) {
			EntityComponentParameter param;
			if (TryGetParameter(key, out param) && (param is EntityComponentParameter<string>)) {
				value = (param as EntityComponentParameter<string>).m_value;
				return true;
			}
			return false;
		}

		public override string ToString() {
			string str = "[";
			int iters = 0;
			foreach (string key in m_parameters.Keys) {
				str += key + " : " + m_parameters[key].ToString();
				str += ", ";
				iters++;
			}
			if(iters > 0)
				str.Remove(str.Length - 3, 2);

			str += "]";
			return str;
		}
	}
	
	// New class to represent Charon components
	public abstract class DataCydranEndid {
		// Get component type (string)
		public abstract string CaelMathCydran();
	}

	// Actor/Being
	public class DataCydranBod : DataCydranEndid {
		public override string CaelMathCydran() { return "Bod"; }

		public string m_tiim;

		public static DataCydranBod CreateFromData(Assets.CydranBod dataCydranBod) {
			DataCydranBod cydBod = new DataCydranBod();
			cydBod.m_tiim = dataCydranBod.Tiim;
			return cydBod;
		}
	}
	
	// Health
	public class DataCydranIechyd : DataCydranEndid {
		public override string CaelMathCydran() { return "Iechyd"; }

		// Max HP
		public int m_nodwedd_IechydCyfan;

		public static DataCydranIechyd CreateFromData(Assets.CydranIechyd dataCydranIechyd) {
			DataCydranIechyd cydIechyd = new DataCydranIechyd();
			cydIechyd.m_nodwedd_IechydCyfan = (int)dataCydranIechyd.ATTRIechydCyfan;
			return cydIechyd;
		}
	}

	// Attributes
	public class DataCydranNodwedd : DataCydranEndid {
		public override string CaelMathCydran() { return "Nodwedd"; }

		public Dictionary<string, NodweddEndid> m_nodweddion;

		public static DataCydranNodwedd CreateFromData(Assets.CydranNodweddion dataCydranNodwedd) {
			DataCydranNodwedd cydNod = new DataCydranNodwedd();
			
			foreach (Assets.Nodwedd nodwedd in dataCydranNodwedd.Nodweddion) {
				DataBank.ItemSystem.AttributeEntry entry = GameDataBankManager.instance.m_attributeDataBank.GetAttribute(nodwedd.EnwNodwedd);
				if (entry != null) {
					switch (nodwedd.Gwerth.MathNewidyn) {
						case Assets.GwerthNodweddMathNewidyn.Float: {
							ItemSystem.AttributeEntryFloat floatEntry = entry as ItemSystem.AttributeEntryFloat;
							float paramValue = float.Parse(nodwedd.Gwerth.Gwerth, System.Globalization.NumberStyles.Float);
							NodweddEndidFloat attributeFloat = new NodweddEndidFloat(floatEntry, true, paramValue);
							cydNod.m_nodweddion.Add(nodwedd.EnwNodwedd, attributeFloat);
							break;
						}
						case Assets.GwerthNodweddMathNewidyn.Int: {
							ItemSystem.AttributeEntryInt intEntry = entry as ItemSystem.AttributeEntryInt;
							int paramValue = int.Parse(nodwedd.Gwerth.Gwerth, System.Globalization.NumberStyles.Integer);
							NodweddEndidInt attributeint = new NodweddEndidInt(intEntry, true, paramValue);
							cydNod.m_nodweddion.Add(nodwedd.EnwNodwedd, attributeint);
							break;
						}
						case Assets.GwerthNodweddMathNewidyn.Bool: {
							ItemSystem.AttributeEntryFlag flagEntry = entry as ItemSystem.AttributeEntryFlag;
							bool paramValue = nodwedd.Gwerth.Gwerth.ToLowerInvariant().Contains("true");
							NodweddEndidFlag attributeFlag = new NodweddEndidFlag(flagEntry, true, paramValue);
							cydNod.m_nodweddion.Add(nodwedd.EnwNodwedd, attributeFlag);
							break;
						}
						case Assets.GwerthNodweddMathNewidyn.String: {
							Debug.LogError("Attempted to load an attribute of type string to an attribute component! Not yet implemented");
							break;
						}
					}
				}
			}

			return cydNod;
		}
	}

	// Kinematic Controller
	public class DataCydranRheolyddCinematig : DataCydranEndid {
		public override string CaelMathCydran() { return "RheolyddCinematig"; }

		// Air Accel Coeff
		public float m_nodwedd_cyfernodCyflymiadAwyr;
		// Air Decel Coeff
		public float m_nodwedd_cyfernodArafuAwyr;
		// Max speed factor default
		public float m_cyflymderCyfanDiofyn;
		// Max speed factor backwards
		public float m_cyflymderCyfanOol;
		// Air control precision
		public float m_nodwedd_manylrwyddRheolaethAwyr;
		// Additional forward air control
		public float m_ychwanegiadYmlaenRheolaethAwyr;
		// Jump strength
		public float m_nodwedd_cryfderNaid;
		// Ramp threshold
		public float m_trothwyRampio;
		// Maximum velocity to land-impact
		public float m_cyflymderCyfanTrawiadGlanio;
		// Restitution factor
		public float m_nodwedd_ffactorSbonc;
		// Minimum velocity to bounce
		public float m_lleiafswmCyflymderISboncen;
		// Shield sliding velocity
		public float m_nodwedd_ffactorFfrithiantSglefrioTarian;
		// Shield sliding acceleration factor
		public float m_ffactorCyflymiadSglefrioTarian;

		public static DataCydranRheolyddCinematig CreateFromData(Assets.CydranRheolyddCinematig dataCydranRheolyddCinematig) {
			DataCydranRheolyddCinematig cydRheolCin = new DataCydranRheolyddCinematig();
			cydRheolCin.m_nodwedd_cyfernodCyflymiadAwyr = dataCydranRheolyddCinematig.ATTRCyfernodCyflymiadAwyr;
			cydRheolCin.m_nodwedd_cyfernodArafuAwyr = dataCydranRheolyddCinematig.ATTRCyfernodArafuAwyr;
			cydRheolCin.m_cyflymderCyfanDiofyn = dataCydranRheolyddCinematig.CyflymderCyfanDiofyn;
			cydRheolCin.m_cyflymderCyfanOol = dataCydranRheolyddCinematig.CyflymderCyfanOol;
			cydRheolCin.m_nodwedd_manylrwyddRheolaethAwyr = dataCydranRheolyddCinematig.ATTRManylrwyddRheolaethAwyr;
			cydRheolCin.m_ychwanegiadYmlaenRheolaethAwyr = dataCydranRheolyddCinematig.YchwanegiadYmlaenRheolaethAwyr;
			cydRheolCin.m_nodwedd_cryfderNaid = dataCydranRheolyddCinematig.ATTRCryfderNaid;
			cydRheolCin.m_trothwyRampio = dataCydranRheolyddCinematig.TrothwyRampio;
			cydRheolCin.m_cyflymderCyfanTrawiadGlanio = dataCydranRheolyddCinematig.CyflymderCyfanTrawiadGlanio;
			cydRheolCin.m_nodwedd_ffactorSbonc = dataCydranRheolyddCinematig.ATTRFfactorSbonc;
			cydRheolCin.m_lleiafswmCyflymderISboncen = dataCydranRheolyddCinematig.LleiafswmCyflymderISboncen;
			cydRheolCin.m_nodwedd_ffactorFfrithiantSglefrioTarian = dataCydranRheolyddCinematig.ATTRFfactorFfrithiantSglefrioTarian;
			cydRheolCin.m_ffactorCyflymiadSglefrioTarian = dataCydranRheolyddCinematig.FfactorCyflymiadSglefrioTarian;
			return cydRheolCin;
		}
	}

	#endregion
}
*/