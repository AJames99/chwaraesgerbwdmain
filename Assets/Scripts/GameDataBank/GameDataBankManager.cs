using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using CydrannauEndid;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Assets;

namespace DataBank {
	// The ushort key used is unique per entry set, not unique in the entire DB.
	// This is because any reference to another entry via the ushort will be
	// done in a context where the type and location/module that it points to is known.
	// If this changes, we'll need to update the way we key this data

	// deleteme if not used in the end
	public interface IGameDataBankEntry {
		
	}

	// Different modules contain different data, and sometimes contain cross-referential data to other modules' IDs
	public abstract class GameDataBankModule {
		public class DataBank<T> {
			Dictionary<ushort, T> m_dataBank = new Dictionary<ushort, T>();
			Dictionary<string, ushort> m_dataBankManifest = new Dictionary<string, ushort>();
			Dictionary<ushort, string> m_dataBankManifestReverse = new Dictionary<ushort, string>();
			ushort m_keyCounter = 0;
			Queue<ushort> m_recycledKeys = new Queue<ushort>();

			public void ClearData() {
				m_dataBank.Clear();
				m_dataBankManifest.Clear();
				m_dataBankManifestReverse.Clear();
				m_keyCounter = 0;
				m_recycledKeys.Clear();
			}

			// Add data to the databank, with an identifier to allow fast string-based querying of the data
			public void AddData(string identifier, T data) {
				Debug.Assert(!m_dataBankManifest.ContainsKey(identifier));

				ushort key = GetUniqueKey();
				Debug.Assert(!m_dataBank.ContainsKey(key));
				m_dataBank[key] = data;
				m_dataBankManifest.Add(identifier, key);
				m_dataBankManifestReverse.Add(key, identifier);
			}

			public void RemoveData(ushort key) {
				if (m_dataBank.ContainsKey(key) && m_dataBankManifestReverse.ContainsKey(key)) {
					m_recycledKeys.Enqueue(key);
					m_dataBank.Remove(key);
					m_dataBankManifest.Remove(m_dataBankManifestReverse[key]);
					m_dataBankManifestReverse.Remove(key);
				}
			}

			public bool HasData(ushort key) {
				return m_dataBank.ContainsKey(key);
			}

			public bool HasData(string identifier) {
				return m_dataBankManifest.ContainsKey(identifier);
			}

			public T GetData(ushort key) {
				return m_dataBank[key];
			}

			public T GetData(string id) {
				return m_dataBank[m_dataBankManifest[id]];
			}

			public ushort GetKeyOfEntry(string identifier) {
				return m_dataBankManifest[identifier];
			}

			protected ushort GetUniqueKey() {
				ushort key = 0;
				if (m_recycledKeys.Count != 0) {
					key = m_recycledKeys.Dequeue();
					Debug.Assert(!m_dataBank.ContainsKey(key), "DataBank tried to recycle a key that was already present in the databank!");
				} else if (m_keyCounter < ushort.MaxValue) {
					// May need to do an overflow check here.
					key = m_keyCounter++;
				} else {
					Debug.LogError("DataBank exceeded ushort key size! Need to change ushort to a larger datatype.");
				}
				return key;
			}

			public void DebugPrint() {
				string strDataBankDebug = "";
				foreach (ushort key in m_dataBank.Keys) {
					strDataBankDebug += "\n\t";
					strDataBankDebug += key.ToString() + " : " + m_dataBank[key].ToString();
				}
				Debug.Log(strDataBankDebug);
			}
		}

		public abstract void ClearData();
		public abstract void DebugPrintDataBanks();
	}

	// Singleton; Used as the interface for all data querying in the game
	public class GameDataBankManager : MonoBehaviour {

		private static GameDataBankManager _instance;
		public static GameDataBankManager instance {
			get {
				if (_instance == null) {
					_instance = new GameObject().AddComponent<GameDataBankManager>();
					_instance.name = _instance.GetType().Name;
					DontDestroyOnLoad(_instance.gameObject);
				}
				return _instance;
			}
		}

		public void Start() {
			if (instance != null && instance != this) {
				Destroy(this);
			}
		}

		public void ResetDatabase() {
			// reset modules here?
		}

		public void OnLoaded() {
			GameProcessManager.instance.GameDatabaseLoaded();
		}

		public void SetGameData(GameData gameData) {
			m_gameData = gameData;
		}

		// Attempts to retrieve an entity (as prefab) from the database, by ID
		public GameObject CeisioCaelEndid(string enwEndid) {
			GameObject gameObj = m_entityDataBank.CaelEndid(enwEndid);
			if (gameObj == null) {
				Debug.LogWarning("Unable to retrieve entity of ID " + enwEndid + "; Either not in the manifest or the prefab is null.");
			}
			return gameObj;
		}

		public GameDataBankModuleItems m_itemDataBank = new GameDataBankModuleItems();
		public GameDataBankModuleEntities m_entityDataBank = new GameDataBankModuleEntities();
		public GameDataBankModuleAttributes m_attributeDataBank = new GameDataBankModuleAttributes();
		protected GameData m_gameData;
	}

	public class GameDataBankModuleItems : GameDataBankModule {
		// Item Manifest
		Dictionary<string, Eitem> m_amlwgEitemau = new Dictionary<string, Eitem>();
		Dictionary<string, Assets.DiffiniadEitem> m_amlwgDiffiniadauEitem = new Dictionary<string, Assets.DiffiniadEitem>();

		public override void ClearData() {
			
		}

		public override void DebugPrintDataBanks() {

		}

		// Returns the item (not physicalised) of the specified ID, if it exists in the database
		public Eitem CaelEitem(string enwEitem) {
			if (m_amlwgEitemau.ContainsKey(enwEitem)) {
				return m_amlwgEitemau[enwEitem];
			}
			return null;
		}

		public Assets.DiffiniadEitem CaelDiffiniadEitem(string enwEitem) {
			if (m_amlwgDiffiniadauEitem.ContainsKey(enwEitem)) {
				return m_amlwgDiffiniadauEitem[enwEitem];
			}
			return null;
		}

		public void LoadData(string subfolder, Assets.ReadOnlyList<Assets.DiffiniadEitem> diffiniadauEitem) {
			// Cache all the definitions in a dictionary for easy lookup
			foreach (Assets.DiffiniadEitem diffEitem in diffiniadauEitem) {
				m_amlwgDiffiniadauEitem.Add(diffEitem.EnwEitem, diffEitem);
			}

			// Load all the item prefabs in the appropriate resources subfolder
			Object[] entityObjects = Resources.LoadAll(subfolder, typeof(GameObject));
			foreach (Object obj in entityObjects) {
				Eitem eitem = obj as Eitem;
				if (eitem != null) {
					bool bDuplicate = m_amlwgEitemau.ContainsKey(eitem.m_enwEitem);
					if (!bDuplicate && eitem.m_enwEitem != null && eitem.m_enwEitem.Length > 0) {

						// Get the item definition's attributes:
						if (m_amlwgDiffiniadauEitem.ContainsKey(eitem.m_enwEitem)) {
							m_amlwgEitemau.Add(eitem.m_enwEitem, eitem);
						}
					} else {
						Debug.LogError("Could not load item of name " + eitem.name+ " and ID " + eitem.m_enwEitem + (bDuplicate ? "; an item of this ID already exits!" : "; ID string is empty!"));
					}
				}
			}
		}
	}

	public class GameDataBankModuleEntities : GameDataBankModule {
		// Maps Entity IDs to Entities (prefabs)
		Dictionary<string, GameObject> m_entityManifest = new Dictionary<string, GameObject>();

		public override void ClearData() {
			Debug.LogError("ClearData aint implemented yet!!!!");
		}

		public override void DebugPrintDataBanks() {
			//m_entityDefinitions.DebugPrint();
		}

		// Returns the entity (as a prefab/GameObject) with the specific ID; null if invalid/not present in the manifest.
		public GameObject CaelEndid(string enwEndid) {
			if (m_entityManifest.ContainsKey(enwEndid)) {
				return m_entityManifest[enwEndid];
			}
			return null;
		}

		public void LoadData(string subfolder) {
			Object[] entityObjects = Resources.LoadAll(subfolder, typeof(GameObject));
			foreach (Object obj in entityObjects) {
				GameObject gameObj = obj as GameObject;
				if (gameObj != null) {
					Endid endid;
					if (gameObj.TryGetComponent<Endid>(out endid)) {
						bool bDuplicate = m_entityManifest.ContainsKey(endid.m_enwEndid);
						if (!bDuplicate && endid.m_enwEndid != null && endid.m_enwEndid.Length > 0) {
							m_entityManifest.Add(endid.m_enwEndid, gameObj);
						} else {
							Debug.LogError("Could not load entity of name " + endid.name + " and ID " + endid.m_enwEndid + (bDuplicate ? "; an entity of this ID already exits!" : "; ID string is empty!"));
						}
					}
				}
			}
		}
	}

	public class GameDataBankModuleAttributes : GameDataBankModule {
		DataBank<ItemSystem.AttributeEntry> m_attributes = new DataBank<ItemSystem.AttributeEntry>();
		DataBank<ItemSystem.AttributeModifierEntry> m_attributeModifiers = new DataBank<ItemSystem.AttributeModifierEntry>();

		public override void ClearData() {
			m_attributes.ClearData();
			m_attributeModifiers.ClearData();
		}

		// Takes all attribute defs and attribute modifiers
		public void LoadData(Assets.ReadOnlyList<Assets.DiffiniadNodwedd> nodweddau, Assets.ReadOnlyList<Assets.AddasyddNodwedd> addasyddNodweddion) {
			ClearData();

			// Create attributes
			foreach (Assets.DiffiniadNodwedd diffiniadNodwedd in nodweddau) {
				ItemSystem.AttributeEntry attributeEntry = CreateAttributeEntry(diffiniadNodwedd);
				m_attributes.AddData(attributeEntry.m_id, attributeEntry);
			}

			// Create attribute modifiers
			foreach (Assets.AddasyddNodwedd addasyddNodwedd in addasyddNodweddion) {
				ItemSystem.AttributeModifierEntry modifierEntry = CreateAttributeModifierEntry(addasyddNodwedd);
				m_attributeModifiers.AddData(addasyddNodwedd.EnwAddasydd, modifierEntry);
			}
		}

		private ItemSystem.AttributeEntry CreateAttributeEntry(Assets.DiffiniadNodwedd nodwedd) {
			ItemSystem.AttributeEntry entry = null;
			switch (nodwedd.Diofyn.MathNewidyn) {
				case GwerthNodweddMathNewidyn.Float: {
					ItemSystem.AttributeEntryFloat entryFloat = new ItemSystem.AttributeEntryFloat(nodwedd.EnwNodwedd, float.Parse(nodwedd.Diofyn.Gwerth, System.Globalization.NumberStyles.Float));
					if (nodwedd.Lleiafswm != null) {
						entryFloat.m_hasMin = true;
						entryFloat.m_minValue = (float)nodwedd.Lleiafswm;
					}
					if (nodwedd.Uchafswm != null) {
						entryFloat.m_hasMax = true;
						entryFloat.m_maxValue = (float)nodwedd.Uchafswm;
					}
					entry = entryFloat;
					break;
				}
				case GwerthNodweddMathNewidyn.Int: {
					ItemSystem.AttributeEntryInt entryInt = new ItemSystem.AttributeEntryInt(nodwedd.EnwNodwedd, int.Parse(nodwedd.Diofyn.Gwerth, System.Globalization.NumberStyles.Integer));
					if (nodwedd.Lleiafswm != null) {
						entryInt.m_hasMin = true;
						entryInt.m_minValue = (int)nodwedd.Lleiafswm;
					}
					if (nodwedd.Uchafswm != null) {
						entryInt.m_hasMax = true;
						entryInt.m_maxValue = (int)nodwedd.Uchafswm;
					}
					entry = entryInt;
					break;
				}
				case GwerthNodweddMathNewidyn.Bool: {
					ItemSystem.AttributeEntryFlag entryBool = new ItemSystem.AttributeEntryFlag(nodwedd.EnwNodwedd, nodwedd.Diofyn.Gwerth.ToLowerInvariant().Contains("true"), nodwedd.ThrothwyIBasio ?? 1);
					entry = entryBool;
					break;
				}
				default: {
					Debug.LogError("Error type in attribute value when creating attribute entries!");
					break;
				}
			}

			return entry;
		}

		private ItemSystem.AttributeModifierEntry CreateAttributeModifierEntry(Assets.AddasyddNodwedd addasydd) {
			ItemSystem.AttributeModifierEntry entry = null;
			switch (addasydd.MathAddasydd) {
				case MathAddasydd.Float: {
					ItemSystem.AttributeModifierEntryFloat modifierFloat = new ItemSystem.AttributeModifierEntryFloat(
						addasydd.EnwNodwedd,
						addasydd.EnwAddasydd
						, float.Parse(addasydd.DiofynAddasydd, System.Globalization.NumberStyles.Float)
						, NodweddionEndid.EntityAttributeModifier.GetStackTypeFromDBEnum(addasydd.MathStacAddasydd));
					entry = modifierFloat;
					break;
				}
				case MathAddasydd.Int: {
					ItemSystem.AttributeModifierEntryInt modifierInt = new ItemSystem.AttributeModifierEntryInt(
						addasydd.EnwNodwedd,
						addasydd.EnwAddasydd
						, int.Parse(addasydd.DiofynAddasydd, System.Globalization.NumberStyles.Integer)
						, NodweddionEndid.EntityAttributeModifier.GetStackTypeFromDBEnum(addasydd.MathStacAddasydd));
					entry = modifierInt;
					break;
				}
			}
			return entry;
		}

		public override void DebugPrintDataBanks() {
			m_attributes.DebugPrint();
			m_attributeModifiers.DebugPrint();
		}

		public ItemSystem.AttributeEntry GetAttribute(string id) {
			if (m_attributes.HasData(id)) {
				return m_attributes.GetData(id);
			}
			return null;
		}

		public ItemSystem.AttributeModifierEntry GetAttributeModifier(string id) {
			if (m_attributeModifiers.HasData(id)) {
				return m_attributeModifiers.GetData(id);
			}
			return null;
		}

		public bool TryGetAttributeKey(string attributeId, ref ushort key) {
			if (m_attributes.HasData(attributeId)) {
				key = m_attributes.GetKeyOfEntry(attributeId);
				return true;
			}
			return false;
		}
	}
}