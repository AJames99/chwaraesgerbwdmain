using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodweddionEndid;

namespace DataBank.ItemSystem {
	// Database entry that represents an item
	public class ItemDataEntry : IGameDataBankEntry {
		
	}

	#region Attributes

	// Base for all entity attributes
	public abstract class AttributeEntry : IGameDataBankEntry {
		public string m_id;
		public bool m_isComponentAttribute = false;
		public AttributeEntry(string id) { m_id = id; }
	}

	public abstract class AttributeEntry<T> : AttributeEntry {
		public virtual T m_baseValue { get; set; }

		public AttributeEntry(string id, T baseValue) : base(id){ m_baseValue = baseValue; }

	}

	// Represents an attribute that resolves to either "true" or "false"
	// m_thresholdToPass can be modified to effectively make it take more stacks to remove/add the effect
	public class AttributeEntryFlag : AttributeEntry<bool> {
		public int m_thresholdToPass; // m_baseValue must be above this to output true.
		public override bool m_baseValue { get; set; }

		public AttributeEntryFlag(string id, bool baseValue, int threshold) : base(id, baseValue) { m_thresholdToPass = threshold; }

		public override string ToString() {
			return m_id + "["
				+ "default: " + m_baseValue + ", "
				+ "threshold: " + m_thresholdToPass
				+ "]";
		}
	}

	public class AttributeEntryFloat : AttributeEntry<float> {
		public float m_minValue;
		public float m_maxValue;
		public bool m_hasMin;
		public bool m_hasMax;

		public override float m_baseValue { get; set; }
		public AttributeEntryFloat(string id, float baseValue) : base(id, baseValue) { }

		public override string ToString() {
			string str = m_id + "["
				+ "default: " + m_baseValue + ", ";
			if (m_hasMax) {
				str += "max: " + m_maxValue;
			}
			if (m_hasMin) {
				str += "min: " + m_minValue;
			}
			str += "]";
			return str;
		}
	}

	public class AttributeEntryInt : AttributeEntry<int> {
		public int m_minValue;
		public int m_maxValue;
		public bool m_hasMin;
		public bool m_hasMax;

		public override int m_baseValue { get; set; }
		public AttributeEntryInt(string id, int baseValue) : base(id, baseValue) { }

		public override string ToString() {
			string str = m_id + "["
				+ "default: " + m_baseValue + ", ";
			if (m_hasMax) {
				str += "max: " + m_maxValue;
			}
			if (m_hasMin) {
				str += "min: " + m_minValue;
			}
			str += "]";
			return str;
		}
	}

	#endregion

	#region AttributeModifiers


	// Base for all attribute modifiers
	public abstract class AttributeModifierEntry : IGameDataBankEntry {
		public MathStacNodwedd m_stackType;
		public string m_attributeId;
		public string m_modifierId;

		public AttributeModifierEntry(string attributeId, string modifierId) {
			m_attributeId = attributeId;
			m_modifierId = modifierId;
		}
	}

	// Generic base that inherits the real base
	public abstract class AttributeModifierEntry<T> : AttributeModifierEntry {
		public AttributeModifierEntry(string attributeId, string modifierId, T modifierValue, MathStacNodwedd stackType) : base(attributeId, modifierId) {
			m_modifierValue = modifierValue;
			m_stackType = stackType;
		}

		public abstract T m_modifierValue { get; set; }
	}

	// Currently floats and ints are incompatible for simplicity's sake.
	// Float values represent MULTIPLIERS, FACTORS. Not quant decimals like 0.1 -> "increase by 10%"
	// If your stat modifier is +25%, your float should be 1.25
	public class AttributeModifierEntryFloat : AttributeModifierEntry<float> {
		public AttributeModifierEntryFloat(string attributeId, string modifierId, float modifierValue, MathStacNodwedd stackType) : base(attributeId, modifierId, modifierValue, stackType) { }
		public override float m_modifierValue { get; set; }

		public override string ToString() {
			string str = "["
				+ "attribute: " + m_attributeId + ", "
				+ "modifier value: " + m_modifierValue + ", "
				+ "stack type: " + m_stackType.ToString()
				+ "]";
			return str;
		}
	}

	// Flag/bool type is covered by +1/-1 in an int modifier. Will also work with non-1 values.
	public class AttributeModifierEntryInt : AttributeModifierEntry<int> {
		public AttributeModifierEntryInt(string attributeId, string modifierId, int modifierValue, MathStacNodwedd stackType) : base(attributeId, modifierId, modifierValue, stackType) { }
		public override int m_modifierValue { get; set; }

		public override string ToString() {
			string str = "["
				+ "attribute: " + m_attributeId + ", "
				+ "modifier value: " + m_modifierValue + ", "
				+ "stack type: " + m_stackType.ToString()
				+ "]";
			return str;
		}
	}
	#endregion
}