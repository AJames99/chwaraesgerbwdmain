using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkAbilityUser : AbilityUser {
	// Need this for abilities that aren't strictly governed by ammo but require a minimum
	public string szAmmoName;

	public MonkAbilityUser(Controller pController, Damageable pDamageable) : base("Monk", pController, pDamageable) {
		MonkAmmo_Medallion pAmmoMedallion = new MonkAmmo_Medallion(GetAmmoManager(), "medallion");

		AddAbilitySlot(AbilitySlot.SLOTBINDING_t.PRIMARY, "Primary", new MonkAbility_CastMedallion(this, GetAmmoManager(), pAmmoMedallion));
		AddAbilitySlot(AbilitySlot.SLOTBINDING_t.SECONDARY, "Secondary", new MonkAbility_MedallionVolley(this, GetAmmoManager(), pAmmoMedallion));
		AddAbilitySlot(AbilitySlot.SLOTBINDING_t.TERTIARY, "Tertiary", new MonkAbility_Dash(this));
		AddAbilitySlot(AbilitySlot.SLOTBINDING_t.QUATERNARY, "Quaternary", new MonkAbility_OrbSwing(this));

		szAmmoName = ((MonkAbility_CastMedallion)m_dicAbilityKeyValues[AbilitySlot.SLOTBINDING_t.PRIMARY].GetAbility()).m_szAmmoTypeName;
	}
}

public class MonkAmmo_Medallion : AmmoType {
	public MonkAmmo_Medallion(AmmoAbilityResource pManager, string szAmmoTypeName) : base(pManager, szAmmoTypeName, 10, AMMORECHARGETYPES.REGEN, true, true, 1, 0.8f, 1.0f) { }
}

public class MonkAbility_CastMedallion : AmmoAbility {

	protected const string szProjectile = "Prefab/Projectiles/MedallionProjectile";

	protected float m_flProjectileSpeed = 250;

	public MonkAbility_CastMedallion(AbilityUser pOwner, AmmoAbilityResource pAmmoManager, AmmoType pAmmoType) : base("monk_castmedallion", pOwner, pAmmoManager, pAmmoType) {
		m_flUseInterval = 0.4f;
		m_iCost = 1;
		m_pAudioBundleAbility = Resources.Load("ScriptableObjects/BundleMedallionAbilitySounds") as AudioBundleAbility;
	}

	public override bool Activate() {
		if (base.Activate()) {
			// Shoot a medal
			Vector3 vSpawnPoint, vLaunchDir;
			m_pOwner.m_pDamageable.GetProjectileFiringSetup(out vSpawnPoint, out vLaunchDir);
			Projectile pProjScript = Projectile.InstanceProjectile(szProjectile, vSpawnPoint, vLaunchDir,
				m_flProjectileSpeed, m_pOwner.m_pDamageable,
				new Damage(Damage.DamageTypes.PROJECTILE, 50, m_pOwner.m_pDamageable.transform.position), false);

			//pProjScript.m_pRigidBody.AddTorque(pProjScript.transform.rotation * new Vector3(0, 0, 1200), ForceMode.VelocityChange);
			//pProjScript.m_pRigidBody.maxAngularVelocity = 30;

			m_pOwner.DoAbilitySound(m_pAudioBundleAbility.abilityClips_activate, m_pAudioBundleAbility.m_flActivateVolume);

			m_pOwner.DoAbilitySound(m_pAudioBundleAbility.abilityClips_fireProjectile, m_pAudioBundleAbility.m_flFireVolume);

			return true;
		}
		return false;
	}

	// TODO: move these two to the ammo type itself
	public override void OnAmmoFull() {
		m_pOwner.DoAbilitySound(m_pAudioBundleAbility.abilityClips_ammoFull, m_pAudioBundleAbility.m_flRegenFullVolume);
	}

	public override void OnAmmoRestored() {
		m_pOwner.DoAbilitySound(m_pAudioBundleAbility.abilityClips_regenerateAmmo, m_pAudioBundleAbility.m_flRegenVolume);
	}
}

public class MonkAbility_Dash : CooldownAbility {
	protected float m_flChargeInterval;
	protected float m_flChargeReleaseTime;
	protected bool m_bCharging = false;

	protected HitboxSeriesOperator m_pSequencer;

	protected Vector2 m_v2BoostStrength;
	protected int m_nAmmoMinimum;

	string szBoostSeq = "Prefab/HitboxSequences/MonkBoost";

	public MonkAbility_Dash(AbilityUser pOwner) : base("monk_dash", pOwner) {
		m_bAvailableOnSpawn = true;
		m_flCooldownDuration = 4.0f;
		m_flChargeInterval = 0.5f;
		m_flChargeReleaseTime = 0;
		m_nAmmoMinimum = 4;
		m_v2BoostStrength = new Vector2(60.0f, 25.0f);
		m_pAudioBundleAbility = Resources.Load("ScriptableObjects/BundleMonkDashAbilitySounds") as AudioBundleAbility;
		m_pSequencer = new HitboxSeriesOperator(m_pOwner);
		m_pSequencer.AppendHitboxSeries(m_pOwner.LoadHitboxSeries(szBoostSeq));
		m_pSequencer.InitAll();
	}

	public override bool Activate() {
		if (!m_bCharging) {
			if (base.Activate()) {
				m_bCharging = true;
				m_flChargeReleaseTime = Time.time + m_flChargeInterval;
				return true;
			}
		}
		return false;
	}

	public override bool IsReady() {
		return base.IsReady() && m_pOwner.GetAmmoManager().HasAmmo(((MonkAbilityUser)m_pOwner).szAmmoName, m_nAmmoMinimum);
	}

	public override void Update() {
		if (m_bCharging) {
			if (Time.time > m_flChargeReleaseTime) {
				TPSController pTPSController = (TPSController)m_pOwner.m_pController;
				if (pTPSController) {
					pTPSController.OnDash(m_v2BoostStrength);
				}
				m_pSequencer.PlayAllSeries();
				m_pOwner.GetReusableAudioSource().PlayOneShot(m_pAudioBundleAbility.abilityClips_activate[m_pAudioBundleAbility.abilityClips_activate.GetRandomIndex()], m_pAudioBundleAbility.m_flActivateVolume);
				m_bCharging = false;
			}
		}
	}

	public override bool IsAbleToActivate() {
		return m_pOwner.GetAmmoManager().HasAmmo(((MonkAbilityUser)m_pOwner).szAmmoName, m_nAmmoMinimum);
	}
}

public class MonkAbility_OrbSwing : CooldownAbility {
	protected int m_nAmmoMinimum;

	protected HitboxSeriesOperator m_pSequencer;

	string szDiagonalRightHitboxSeq = "Prefab/HitboxSequences/MonkDiagonalRight";
	string szDiagonalLeftHitboxSeq = "Prefab/HitboxSequences/MonkDiagonalLeft";

	float m_flSecondHitDelay;
	float m_flSecondHitTimer;
	bool m_bSecondHitCharging;

	public MonkAbility_OrbSwing(AbilityUser pOwner) : base("monk_orbswing", pOwner) {
		m_bAvailableOnSpawn = true;
		m_flCooldownDuration = 3.0f;
		m_nAmmoMinimum = 2;
		//m_pAudioBundleAbility = Resources.Load("ScriptableObjects/BundleMonkDashAbilitySounds") as AudioBundleAbility;
		m_pSequencer = new HitboxSeriesOperator(m_pOwner);
		m_pSequencer.AppendHitboxSeries(m_pOwner.LoadHitboxSeries(szDiagonalRightHitboxSeq));
		m_pSequencer.AppendHitboxSeries(m_pOwner.LoadHitboxSeries(szDiagonalLeftHitboxSeq));
		m_pSequencer.InitAll();

		m_bSecondHitCharging = false;
		m_flSecondHitDelay = 0.5f;
		m_flSecondHitTimer = 0;
	}

	public override void Update() {
		base.Update();
		if (m_bSecondHitCharging) {
			if (Time.time > m_flSecondHitTimer) {
				ActivateSecond();
				m_bSecondHitCharging = false;
			}
		}
	}

	public override bool Activate() {
		if (base.Activate()) {
			//m_pSequencer.PlayAllSeries();
			m_pSequencer.PlaySeries(0);
			m_bSecondHitCharging = true;
			m_flSecondHitTimer = Time.time + m_flSecondHitDelay;
		}
		return false;
	}

	protected void ActivateSecond() {
		m_pSequencer.PlaySeries(1);
	}

	public override bool IsReady() {
		return base.IsReady() && !m_pSequencer.IsAnimating() && m_pOwner.GetAmmoManager().HasAmmo(((MonkAbilityUser)m_pOwner).szAmmoName, m_nAmmoMinimum);
	}

	public override bool IsAbleToActivate() {
		return m_pOwner.GetAmmoManager().HasAmmo(((MonkAbilityUser)m_pOwner).szAmmoName, m_nAmmoMinimum);
	}
}

public class MonkAbility_MedallionVolley : AmmoAbility {

	protected const string szProjectile = "Prefab/Projectiles/MedallionProjectile";

	new protected AudioBundleChargeableAbility m_pAudioBundleAbility;

	protected float m_flProjectileSpeed = 250;

	protected AbilityChargeState m_stateCharge;
	protected float m_flChargeTimer;

	protected float m_flChargeInterval, m_flChargeFireInterval;

	protected int m_nBaseDamage;

	protected int m_nChargedShots;
	protected int m_nChargedShotsMax;

	public MonkAbility_MedallionVolley(AbilityUser pOwner, AmmoAbilityResource pAmmoManager, AmmoType pAmmoType) : base("monk_medallionvolley", pOwner, pAmmoManager, pAmmoType) {
		m_flUseInterval = 0.6f;
		m_flChargeInterval = 0.5f;
		m_flChargeFireInterval = 0.11f;
		m_iCost = 1;
		m_nBaseDamage = 50;
		m_flChargeTimer = 0;
		m_nChargedShots = 0;
		m_nChargedShotsMax = 10;

		m_stateCharge = AbilityChargeState.NOTCHARGING;
		m_pAudioBundleAbility = Resources.Load("ScriptableObjects/BundleMedallionVolleyAbilitySounds") as AudioBundleChargeableAbility;
	}

	public override bool Activate() {
		//pProjScript.m_pRigidBody.AddTorque(pProjScript.transform.rotation * new Vector3(0, 0, 1200), ForceMode.VelocityChange);
		//pProjScript.m_pRigidBody.maxAngularVelocity = 30;

		if (m_stateCharge == AbilityChargeState.NOTCHARGING && m_pAmmoManager.HasAmmo(m_szAmmoTypeName) && IsReady()) {
			m_stateCharge = AbilityChargeState.CHARGING;
			m_flChargeTimer = Time.time + m_flChargeInterval;

			if (null != m_pOwner && null != m_pAudioBundleAbility) {
				m_pOwner.DoAbilitySound(m_pAudioBundleAbility.abilityClips_activate, m_pAudioBundleAbility.m_flActivateVolume);
				m_pOwner.DoAbilitySoundLooped(m_pAudioBundleAbility.abilityClips_chargeBegin, m_pAudioBundleAbility.m_flChargeBeginVolume);
			}

			// Stop regen of ammo
			m_pAmmoManager.SetBusyState(m_szAmmoTypeName, true);

			return true;
		}

		return false;
	}

	void FireProjectile() {
		if (Time.time > m_flChargeTimer) {
			Vector3 vSpawnPoint, vLaunchDir;
			m_pOwner.m_pDamageable.GetProjectileFiringSetup(out vSpawnPoint, out vLaunchDir);
			Projectile pProjScript = Projectile.InstanceProjectile(szProjectile, vSpawnPoint, vLaunchDir,
				m_flProjectileSpeed, m_pOwner.m_pDamageable,
				new Damage(Damage.DamageTypes.PROJECTILE, m_nBaseDamage, m_pOwner.m_pDamageable.transform.position), false);
			if (null != m_pOwner && null != m_pAudioBundleAbility)
				m_pOwner.DoAbilitySound(m_pAudioBundleAbility.abilityClips_fireProjectile, m_pAudioBundleAbility.m_flFireVolume);
			
			m_pAmmoManager.UseAmmo(m_szAmmoTypeName, m_iCost);

			if (m_nChargedShots-- <= 0 || !m_pAmmoManager.HasAmmo(m_szAmmoTypeName, m_iCost)) {
				m_nChargedShots = 0;
				m_stateCharge = AbilityChargeState.NOTCHARGING;
				m_pAmmoManager.SetBusyState(m_szAmmoTypeName, false);
				m_flUseTimer = Time.time + m_flUseInterval;
			} else {
				m_flChargeTimer = Time.time + m_flChargeFireInterval;
			}
		}
	}

	public override void Update() {
		base.Update();

		switch (m_stateCharge) {
			case AbilityChargeState.CHARGING:
				ChargeProjectile();
				break;
			case AbilityChargeState.RELEASING:
				FireProjectile();
				break;
		}
	}

	void ChargeProjectile() {
		if (Time.time >= m_flChargeTimer) {
			if (m_nChargedShots >= m_nChargedShotsMax || m_pAmmoManager.GetAmmo(m_szAmmoTypeName) <= m_nChargedShots) {
				m_nChargedShots = Mathf.Min(m_nChargedShotsMax, m_pAmmoManager.GetAmmo(m_szAmmoTypeName));
				m_stateCharge = AbilityChargeState.RELEASING;
				if (null != m_pOwner && null != m_pAudioBundleAbility) {
					m_pOwner.StopAbilitySoundLooped();
					if (m_nChargedShots >= m_nChargedShotsMax)
						m_pOwner.DoAbilitySound(m_pAudioBundleAbility.abilityClips_chargeMax, m_pAudioBundleAbility.m_flChargeMaxVolume);

					m_pOwner.DoAbilitySound(m_pAudioBundleAbility.abilityClips_chargeRelease, m_pAudioBundleAbility.m_flChargeReleaseVolume);
				}
			} else {
				m_nChargedShots++;
				m_flChargeTimer = Time.time + m_flChargeInterval;
			}
			if (null != m_pOwner && null != m_pAudioBundleAbility)
				m_pOwner.DoAbilitySound(m_pAudioBundleAbility.abilityClips_chargeIncrement, m_pAudioBundleAbility.m_flChargeIncrementVolume);
		}
	}

	public override void OnStopUsingAbility() {
		base.OnStopUsingAbility();
		if (m_stateCharge == AbilityChargeState.CHARGING) {
			m_stateCharge = AbilityChargeState.RELEASING;
			m_flChargeTimer = 0; // To cause instant release of the first medallion in the volley
			if (null != m_pOwner && null != m_pAudioBundleAbility) {
				m_pOwner.StopAbilitySoundLooped();
				m_pOwner.DoAbilitySound(m_pAudioBundleAbility.abilityClips_chargeRelease, m_pAudioBundleAbility.m_flChargeReleaseVolume);
			}
		} else {
			m_pAmmoManager.SetBusyState(m_szAmmoTypeName, false);
		}
	}
}