using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using UnityEngine.InputSystem;

public abstract class Ability {

	// Reource types are used to determine class casting, so don't fuckin misassign them!!!
	public enum ResourceType {
		NONE,
		COOLDOWN,
		METER,
		AMMO,
	}

	public enum AbilityChargeState {
		NOTCHARGING,
		CHARGING,
		RELEASING
	}

	public AbilityUser m_pOwner;

	public string m_szAbilityName;

	public Ability(string szAbilityName, AbilityUser pOwner) {
		m_szAbilityName = szAbilityName;
		m_pOwner = pOwner;
	}

	public AudioBundleAbility m_pAudioBundleAbility;

	public string m_szAbilityIconPath;

	public abstract ResourceType GetResourceType();
	public abstract bool IsReady();         // Do we have enough resources to use this ability and is our character's condition permitting of this ability.
	public abstract bool Activate();

	public abstract void ForceRestore();    // Forcibly fills the resources for this ability
	public abstract void ForceReset();      // Forcibly resets the resources for this abiltiy back to what they should be "on spawn"

	public virtual void Update() { }

	public virtual void OnFailToUse() {
		m_pOwner.OnFailToUse();
	}

	public virtual void OnStopUsingAbility() { }

	public virtual void OnBeginUsingAbility() { }

	public abstract float GetAbilityProgress();

	// Basically, if an ability is unable to be used because of *external factors*, returns false.
	// Triggers the cross-circle over an ability icon on the GUI
	public virtual bool IsAbleToActivate() {
		return true;
	}
}

// Manages the hitboxes defined by a HitboxSeries object
public class HitboxSeriesOperator {
	List<HitboxSeries> m_lstHitboxSeries;
	protected bool m_bIsAnimating;
	AbilityUser m_pOwner;

	public HitboxSeriesOperator(AbilityUser pOwner) {
		m_bIsAnimating = false;
		m_lstHitboxSeries = new List<HitboxSeries>();
		m_pOwner = pOwner;
	}

	public bool IsAnimating() {
		return m_bIsAnimating;
	}

	public void AppendHitboxSeries(HitboxSeries pHitboxSeries) {
		m_lstHitboxSeries.Add(pHitboxSeries);
	}

	public void ClearAllSeries() {
		m_lstHitboxSeries.Clear();
	}

	public void PlayAllSeries() {
		foreach (HitboxSeries pSeries in m_lstHitboxSeries) {
			pSeries.Play();
		}
	}

	public void PlaySeries(int iIndex) {
		m_lstHitboxSeries[iIndex].Play();
	}

	public void Update() {
		if (m_bIsAnimating) {
			bool bStillAnimating = false;
			foreach (HitboxSeries pSeries in m_lstHitboxSeries) {
				pSeries.Update();

				if (pSeries.IsAnimating())
					bStillAnimating = true;

			}
			if (!bStillAnimating)
				m_bIsAnimating = false;
		}
	}

	public void InitAll() {
		foreach (HitboxSeries pSeries in m_lstHitboxSeries) {
			pSeries.Init(m_pOwner.m_pDamageable);
		}
	}
}

public abstract class CooldownAbility : Ability {
	public float m_flCooldownDuration;  // Fixed value, determines length of CD
	public float m_flCooldownDoneTime = 0;      // Set to activation time + duration
	public bool m_bAvailableOnSpawn = false;

	public CooldownAbility(string szName, AbilityUser pOwner) : base(szName, pOwner) {
	}

	public override bool Activate() {
		if (IsReady()) {
			m_flCooldownDoneTime = Time.time + m_flCooldownDuration;
			return true;
		}

		OnFailToUse();
		return false;
	}

	public override bool IsReady() {
		return Time.time > m_flCooldownDoneTime;
	}

	public override ResourceType GetResourceType() {
		return ResourceType.COOLDOWN;
	}

	public override void ForceReset() {
		if (m_bAvailableOnSpawn)
			m_flCooldownDoneTime = 0;
		else
			m_flCooldownDoneTime = Time.time + m_flCooldownDuration;
	}

	public override void ForceRestore() {
		m_flCooldownDoneTime = 0;
	}

	public virtual void OnCooldownComplete() { }

	public override float GetAbilityProgress() {
		//if (IsReady())
		//	return 1.0f;

		return 1.0f - ((m_flCooldownDoneTime - Time.time) / m_flCooldownDuration);
	}
}

public abstract class AmmoAbility : Ability {

	public AmmoAbilityResource m_pAmmoManager; // Defined by a parent

	protected int m_iCost;  // Ammo cost.

	protected float m_flUseInterval;    // Cooldown.
	protected float m_flUseTimer = 0;

	protected AmmoType m_pAmmoType;
	public AmmoType GetAmmoType() {
		return m_pAmmoType;
	}

	public void SetAmmoType(AmmoType pType) {
		m_pAmmoType = pType;
	}

	// Pass in the ammo resource manager of the owner.
	public AmmoAbility(string szName, AbilityUser pOwner, AmmoAbilityResource pAmmo, AmmoType pAmmoType) : base(szName, pOwner) {
		m_pAmmoManager = pAmmo;
		SetAmmoType(pAmmoType);
		if (null != pAmmoType)
			m_szAmmoTypeName = pAmmoType.GetName();
	}

	public string m_szAmmoTypeName;

	public override bool IsReady() {
		return Time.time > m_flUseTimer;
	}

	public override void ForceReset() {
		m_flUseTimer = 0;
		m_pAmmoManager.RestoreAllAmmo(m_szAmmoTypeName);
	}

	public override void ForceRestore() {
		ForceReset();
	}

	public override ResourceType GetResourceType() {
		return ResourceType.AMMO;
	}

	public override bool Activate() {
		if (m_pAmmoManager.HasAmmo(m_szAmmoTypeName) && IsReady()) {
			m_pAmmoManager.UseAmmo(m_szAmmoTypeName, m_iCost);
			m_flUseTimer = Time.time + m_flUseInterval;
			return true;
		}
		return false;
	}

	public virtual void OnAmmoRestored() { }
	public virtual void OnAmmoFull() { }

	public override float GetAbilityProgress() {
		return m_pAmmoType.GetProgress();
	}
}

public class AmmoType {
	public AmmoType(AmmoAbilityResource pResourceManager, string szAmmoTypeName, int iMax, AMMORECHARGETYPES rechargeType, bool bUseInterruptsRecharge, bool bAllowDebt, int iRegenAmount = 1, float flRegenTime = 0, float flDelayTime = 0) {
		m_pResourceManager = pResourceManager;
		m_rechargeType = rechargeType;
		m_iAmmoCount = m_iAmmoMaximum = iMax;
		m_currentState = AMMORESOURCESTATES.DELAY;
		m_flRechargeInterval = flRegenTime;
		m_flRechargeStartDelay = flDelayTime;
		m_bUseResetsRecharge = bUseInterruptsRecharge;
		m_iRegenAmount = iRegenAmount;
		m_szAmmoTypeName = szAmmoTypeName;
		m_bOwnerBusy = false;
	}
	protected string m_szAmmoTypeName;
	protected AmmoAbilityResource m_pResourceManager;
	public int m_iAmmoMaximum;
	public int m_iAmmoCount;
	public float m_flRechargeInterval;
	protected float m_flRechargeCompleteTime = 0;
	public float m_flRechargeStartDelay;
	public float m_flRechargeStartDelayCompleteTime = 0;
	public bool m_bUseResetsRecharge;           // Whether or not using this ability will totally reset the delay AND interval of the auto regen.
	public int m_iRegenAmount;
	protected bool m_bAllowDebt;                // Allows use of this ability if the cost is greater than what we have left, so long as we have at least 1 ammo.

	protected bool m_bOwnerBusy; // Pauses ammo regeneration

	public enum AMMORECHARGETYPES {
		NONE,       // Doesn't automatically regenerate. Can still be regained via external sources.
		REGEN,      // Regenerates some amount at an interval.
		REGENFULL   // Regenerates all ammo at an interval.
	}
	protected AMMORECHARGETYPES m_rechargeType;

	public enum AMMORESOURCESTATES {
		DELAY,
		REGENCYCLE,
		MAXEDOUT
	}
	protected AMMORESOURCESTATES m_currentState;

	public void SetBusyState(bool bBusy) {
		m_bOwnerBusy = bBusy;
		if (bBusy) {
			m_currentState = AMMORESOURCESTATES.DELAY;
		} else {
			m_flRechargeStartDelayCompleteTime = Time.time + m_flRechargeStartDelay;
		}
	}

	public string GetName() { return m_szAmmoTypeName; }

	public void RegenerateAmmo() {
		m_iAmmoCount = Mathf.Min(m_iAmmoCount + m_iRegenAmount, m_iAmmoMaximum);
		OnAmmoRegen();
		if (IsFull())
			OnAmmoFull();
	}

	public void AddAmmo(int iAmount) {
		m_iAmmoCount = Mathf.Min(m_iAmmoCount + iAmount, m_iAmmoMaximum);
		OnAmmoRegen();
		if (IsFull())
			OnAmmoFull();
	}

	public void ReplenishAll() {
		m_iAmmoCount = m_iAmmoMaximum;
		OnAmmoFull();
	}

	public bool HasAmmo() {
		return m_iAmmoCount > 0;
	}

	public bool IsFull() {
		return m_iAmmoCount == m_iAmmoMaximum;
	}

	// Call externally from an update function.
	public void Update() {
		if (m_bOwnerBusy)
			return;

		switch (m_currentState) {
			case AMMORESOURCESTATES.DELAY:
				if (Time.time > m_flRechargeStartDelayCompleteTime) {
					m_currentState = AMMORESOURCESTATES.REGENCYCLE;
					m_flRechargeCompleteTime = Time.time + m_flRechargeInterval;
				}
				break;
			case AMMORESOURCESTATES.REGENCYCLE:
				if (Time.time > m_flRechargeCompleteTime) {
					RegenerateAmmo();
					if (IsFull()) {
						m_currentState = AMMORESOURCESTATES.MAXEDOUT;
					} else {
						m_currentState = AMMORESOURCESTATES.REGENCYCLE;
						m_flRechargeCompleteTime = Time.time + m_flRechargeInterval;
					}
				}
				break;
		}
	}

	public bool CanUse(int iCost) {
		return m_iAmmoCount >= iCost || (m_iAmmoCount > 0 && m_bAllowDebt);
	}

	public void Interrupt() {
		m_currentState = AMMORESOURCESTATES.DELAY;
		m_flRechargeStartDelayCompleteTime = Time.time + m_flRechargeStartDelay;
	}

	public bool UseAmmo(int iCount) {
		if (CanUse(iCount)) {
			if (m_bUseResetsRecharge)
				Interrupt();

			m_iAmmoCount = Mathf.Max(m_iAmmoCount - iCount, 0);
		}
		return false;
	}

	public void DepleteAll() {
		m_iAmmoCount = 0;
	}

	public void ForceAmmoTo(int iAmount) {
		m_iAmmoCount = Mathf.Clamp(iAmount, 0, m_iAmmoMaximum);
	}

	public void OnAmmoFull() {
		m_pResourceManager.OnAmmoFull(m_szAmmoTypeName);
	}

	public void OnAmmoRegen() {
		m_pResourceManager.OnAmmoRegen(m_szAmmoTypeName);
	}

	public float GetProgress() {
		switch (m_currentState) {
			case AMMORESOURCESTATES.DELAY:
			default:
				return 0;

			case AMMORESOURCESTATES.MAXEDOUT:
				return 1;

			case AMMORESOURCESTATES.REGENCYCLE:
				return 1 - ((m_flRechargeCompleteTime - Time.time) / m_flRechargeInterval);
		}
	}
}

// Manager for all of a character's ammo.
public class AmmoAbilityResource {
	protected Dictionary<string, AmmoType> m_dicAmmoTypes = new Dictionary<string, AmmoType>();

	protected AbilityUser m_pOwner;
	public AmmoAbilityResource(AbilityUser pOwner) {
		m_pOwner = pOwner;
	}

	public void AddAmmoType(string szAmmo, AmmoType pAmmo) {
		if (m_dicAmmoTypes.ContainsKey(szAmmo)) {
			Debug.LogError("Tried adding duplicate ammo type " + szAmmo + "! Aborting.");
			return;
		}

		m_dicAmmoTypes.Add(szAmmo, pAmmo);
		OnAmmoChanged();
	}

	public void RemoveAmmoType(string szAmmo) {
		if (m_dicAmmoTypes.ContainsKey(szAmmo)) {
			m_dicAmmoTypes.Remove(szAmmo);
		}
	}

	public bool UseAmmo(string szAmmo, int iAmount = 1) {
		if (m_dicAmmoTypes.ContainsKey(szAmmo)) {
			bool bSuccess = m_dicAmmoTypes[szAmmo].UseAmmo(iAmount);
			OnAmmoChanged();
			return bSuccess;
		}
		return false;
	}

	public void IncrementAmmo(string szAmmo, int iDesiredAmount = 1) {
		if (m_dicAmmoTypes.ContainsKey(szAmmo)) {
			m_dicAmmoTypes[szAmmo].AddAmmo(iDesiredAmount);
		}
		OnAmmoChanged();
	}

	public void DepleteAllAmmo(string szAmmo) {
		if (m_dicAmmoTypes.ContainsKey(szAmmo)) {
			m_dicAmmoTypes[szAmmo].DepleteAll();
		}
		OnAmmoChanged();
	}

	public void RestoreAllAmmo(string szAmmo) {
		if (m_dicAmmoTypes.ContainsKey(szAmmo)) {
			m_dicAmmoTypes[szAmmo].ReplenishAll();
		}
		OnAmmoChanged();
	}
	public void SetAmmo(string szAmmo, int iAmount) {
		if (m_dicAmmoTypes.ContainsKey(szAmmo)) {
			m_dicAmmoTypes[szAmmo].ForceAmmoTo(iAmount);
		}
		OnAmmoChanged();
	}

	public int GetAmmo(string szAmmo) {
		if (m_dicAmmoTypes.ContainsKey(szAmmo)) {
			return m_dicAmmoTypes[szAmmo].m_iAmmoCount;
		}
		return 0;
	}

	public bool HasAmmo(string szAmmo, int iAmount = 1) {
		if (m_dicAmmoTypes.ContainsKey(szAmmo))
			return m_dicAmmoTypes[szAmmo].CanUse(iAmount);
		return false;
	}

	public bool HasAmmoType(string szAmmo) {
		return (m_dicAmmoTypes.ContainsKey(szAmmo));
	}

	public bool IsFull(string szAmmo) {
		if (HasAmmoType(szAmmo)) {
			return m_dicAmmoTypes[szAmmo].IsFull();
		}
		return false;
	}

	public void Update() {
		foreach (AmmoType type in m_dicAmmoTypes.Values) {
			type.Update();
		}
	}

	public void OnAmmoFull(string szAmmoType) {
		m_pOwner.OnAmmoFull(szAmmoType);
		OnAmmoChanged();
	}
	public void OnAmmoRegen(string szAmmoType) {
		m_pOwner.OnAmmoRegen(szAmmoType);
		OnAmmoChanged();
	}

	public void SetBusyState(string szAmmo, bool bBusy) {
		if (HasAmmoType(szAmmo)) {
			m_dicAmmoTypes[szAmmo].SetBusyState(bBusy);
		}
	}

	void OnAmmoChanged() {
		m_pOwner.OnAmmoChanged();
	}
}

public class AbilitySlot {
	public enum SLOTBINDING_t {
		PRIMARY,
		SECONDARY,
		TERTIARY,
		QUATERNARY
	}

	public SLOTBINDING_t m_slotBinding;

	public string m_szSlotName;

	protected AbilityUser m_pAbilityUser;

	string GetMappingStringForAction(InputActionMap pMap, string szHandle) {
		InputAction pAction = pMap.FindAction(szHandle);
		if (pAction != null) {
			return pAction.GetBindingDisplayString(InputBinding.DisplayStringOptions.DontIncludeInteractions);
		}

		return "";
	}

	public string GetHotkeyString() {
		if (null != m_pAbilityUser) {
			InputActionMap pInputMap = m_pAbilityUser.m_pController.GetInputActions();
			switch (m_slotBinding) {
				case SLOTBINDING_t.PRIMARY:
					return GetMappingStringForAction(pInputMap, "Primary");
				case SLOTBINDING_t.SECONDARY:
					return GetMappingStringForAction(pInputMap, "Secondary");
				case SLOTBINDING_t.TERTIARY:
					return GetMappingStringForAction(pInputMap, "Tertiary");
				case SLOTBINDING_t.QUATERNARY:
					return GetMappingStringForAction(pInputMap, "Quaternary");
			}
		}
		return "";
	}

	protected Ability m_pEquippedAbility;

	public virtual Ability GetAbility() {
		return m_pEquippedAbility;
	}

	public virtual void TryUseAbility() {
		m_pEquippedAbility.Activate();
	}

	public virtual void SetAbility(Ability pAbility) {
		m_pEquippedAbility = pAbility;
		//if(null != m_pEquippedAbility)
		//	m_pEquippedAbility.m_pContainer = this;
	}

	public virtual bool RemoveAbility() {
		bool bRemoved = false;
		if (m_pEquippedAbility != null)
			bRemoved = true;
		m_pEquippedAbility = null;
		return bRemoved;
	}

	public virtual void OnStopUsingAbility() {
		if (null != GetAbility()) {
			GetAbility().OnStopUsingAbility();
		}
	}

	public virtual void OnBeginUsingAbility() {
		if (null != GetAbility()) {
			GetAbility().OnBeginUsingAbility();
		}
	}

	public AbilitySlot(SLOTBINDING_t slotBinding, string szSlotName, AbilityUser pAbilityUser, Ability pAbility = null) {
		m_slotBinding = slotBinding;
		m_szSlotName = szSlotName;
		m_pEquippedAbility = pAbility;
		m_pAbilityUser = pAbilityUser;
	}

	public bool IsAbleToActivate() {
		if (null != GetAbility())
			return GetAbility().IsAbleToActivate();

		return false;
	}
}

// The container for all things pertaining to abilities.
// To be accessed by inputs and controllers and NPC logic.
// Query for cooldowns, slots, what's equiped where, what abilities we have etc.
public class AbilityUser {
	public string m_szAbilityUserClassname;

	public Controller m_pController; // Owner movement
	public Damageable m_pDamageable; // Owner health, body etc.

	public AbilityGUIManager m_pGUIManager = null;

	const string szAbilityFailNoise = "Sounds/ability_fail";
	const float flAbilityFailVolume = 0.1f;
	AudioClip m_pAudioClipFailToUse;
	bool m_bCanPlayFailSound;
	const float flMinFailSoundInterval = 0.05f;
	float m_flFailSoundTimer;

	public AbilityUser(string szAbilityUserClassname, Controller pController, Damageable pDamageable) {
		m_szAbilityUserClassname = szAbilityUserClassname;
		m_pAmmoManager = new AmmoAbilityResource(this);
		m_pController = pController;
		m_pDamageable = pDamageable;

		if (null == m_pController)
			Debug.LogError("AbilityUser: Controller provided was null!");
		if (null == m_pDamageable)
			Debug.LogError("AbilityUser: Damageable provided was null!");

		m_pAudioClipFailToUse = Resources.Load(szAbilityFailNoise) as AudioClip;

		m_bCanPlayFailSound = true;
		m_flFailSoundTimer = 0;
	}

	protected Dictionary<AbilitySlot.SLOTBINDING_t, AbilitySlot> m_dicAbilityKeyValues = new Dictionary<AbilitySlot.SLOTBINDING_t, AbilitySlot>();

	AmmoAbilityResource m_pAmmoManager;
	public AmmoAbilityResource GetAmmoManager() { return m_pAmmoManager; }

	public Dictionary<AbilitySlot.SLOTBINDING_t, AbilitySlot> GetAbilities() {
		return m_dicAbilityKeyValues;
	}

	public void UseAbility(AbilitySlot.SLOTBINDING_t slot) {
		if (HasSlot(slot)) {
			m_dicAbilityKeyValues[slot].TryUseAbility();
			m_bCanPlayFailSound = false;
			m_flFailSoundTimer = Time.time + flMinFailSoundInterval;
			if (m_dicAbilityKeyValues[slot].GetAbility() != null) {
				if (m_dicAbilityKeyValues[slot].GetAbility().GetResourceType() == Ability.ResourceType.AMMO) {
					AmmoAbility pAbility = (AmmoAbility)m_dicAbilityKeyValues[slot].GetAbility();
					if (null != m_pGUIManager)
						m_pGUIManager.SetAmmoForSlot(slot, pAbility.GetAmmoType().m_iAmmoCount, pAbility.GetAmmoType().m_iAmmoMaximum);
				}
			}
		}
	}

	public void PollAmmoForSlot(AbilitySlot pSlot) {
		if (null == pSlot.GetAbility() || null == m_pGUIManager)
			return;

		if (pSlot.GetAbility().GetResourceType() == Ability.ResourceType.AMMO) {
			AmmoAbility pAbilityAmmo = (AmmoAbility)pSlot.GetAbility();
			if (null != m_pGUIManager)
				m_pGUIManager.SetAmmoForSlot(pSlot.m_slotBinding, pAbilityAmmo.GetAmmoType().m_iAmmoCount, pAbilityAmmo.GetAmmoType().m_iAmmoMaximum);
		}
	}

	public virtual void OnBeginUsingAbility(AbilitySlot.SLOTBINDING_t slot) {
		if (HasSlot(slot)) {
			m_dicAbilityKeyValues[slot].OnBeginUsingAbility();
			m_pGUIManager.SetButtonPressedForSlot(slot, true);
		}
	}

	public virtual void OnStopUsingAbility(AbilitySlot.SLOTBINDING_t slot) {
		if (HasSlot(slot)) {
			m_dicAbilityKeyValues[slot].OnStopUsingAbility();
			m_pGUIManager.SetButtonPressedForSlot(slot, false);
		}
	}

	public void AddAbilitySlot(AbilitySlot.SLOTBINDING_t binding, string szSlotName, Ability pAbility = null) {
		//AbilitySlot pSlot = new AbilitySlot(binding, szSlotName, pAbility);
		AbilitySlot pSlot = new AbilitySlot(binding, szSlotName, this, null);
		if (!HasSlot(binding)) {
			m_dicAbilityKeyValues.Add(binding, pSlot);
			EquipAbility(pAbility, binding);
			if (null != m_pGUIManager)
				m_pGUIManager.SetAmmoCounterVisible(binding, pAbility.GetResourceType() == Ability.ResourceType.AMMO);
		} else {
			Debug.LogError("Tried to add an ability slot with binding " + binding.ToString() + ", which already exists for this Ability User! Aborting.");
		}
	}

	public AbilitySlot RemoveAbilitySlot(AbilitySlot.SLOTBINDING_t binding) {
		if (HasSlot(binding)) {
			AbilitySlot pRemovedAbility = m_dicAbilityKeyValues[binding];
			m_dicAbilityKeyValues.Remove(binding);
			return pRemovedAbility;
		}
		return null;
	}

	public bool HasSlot(AbilitySlot.SLOTBINDING_t slotBinding) {
		return m_dicAbilityKeyValues.ContainsKey(slotBinding);
	}

	public bool EquipAbility(Ability pAbility, AbilitySlot.SLOTBINDING_t slotBinding) {
		if (HasSlot(slotBinding)) {
			m_dicAbilityKeyValues[slotBinding].RemoveAbility();
			m_dicAbilityKeyValues[slotBinding].SetAbility(pAbility);

			if (pAbility.GetResourceType() == Ability.ResourceType.AMMO) {
				AmmoAbility ammoAbility = (AmmoAbility)pAbility;
				if (!m_pAmmoManager.HasAmmoType(ammoAbility.m_szAmmoTypeName)) {
					m_pAmmoManager.AddAmmoType(ammoAbility.m_szAmmoTypeName, ammoAbility.GetAmmoType());
				}
			}

			return true;
		}
		return false;
	}

	public virtual void Update() {
		m_pAmmoManager.Update();
		foreach (AbilitySlot.SLOTBINDING_t binding in m_dicAbilityKeyValues.Keys) {
			AbilitySlot pSlot = m_dicAbilityKeyValues[binding];
			if (null != pSlot) {
				if (null != pSlot.GetAbility()) {
					pSlot.GetAbility().Update();
					m_pGUIManager.SetProgressForSlot(binding, pSlot.GetAbility().GetAbilityProgress());
					m_pGUIManager.SetAbilityAvailableForSlot(binding, pSlot.GetAbility().IsAbleToActivate());
				}
			}
		}
	}

	public AudioSource GetReusableAudioSource() {
		return m_pDamageable.reusableAudioSource;
	}

	public ClipPlayer GetClipPlayer() {
		return m_pDamageable.clipPlayer;
	}

	public void OnAmmoRegen(string szAmmoType) {
		foreach (AbilitySlot.SLOTBINDING_t slot in m_dicAbilityKeyValues.Keys) {
			Ability pAbility = m_dicAbilityKeyValues[slot].GetAbility();
			if (null != pAbility) {
				if (pAbility.GetResourceType() == Ability.ResourceType.AMMO) {
					AmmoAbility ammoAbility = (AmmoAbility)pAbility;
					if (ammoAbility.m_szAmmoTypeName.Equals(szAmmoType))
						ammoAbility.OnAmmoRestored();

					if (null != m_pGUIManager)
						m_pGUIManager.SetAmmoForSlot(slot, ammoAbility.GetAmmoType().m_iAmmoCount, ammoAbility.GetAmmoType().m_iAmmoMaximum);
				}
			}
		}
	}

	public void OnAmmoFull(string szAmmoType) {
		foreach (AbilitySlot.SLOTBINDING_t slot in m_dicAbilityKeyValues.Keys) {
			Ability pAbility = m_dicAbilityKeyValues[slot].GetAbility();
			if (null != pAbility) {
				if (pAbility.GetResourceType() == Ability.ResourceType.AMMO) {
					AmmoAbility ammoAbility = (AmmoAbility)pAbility;
					if (ammoAbility.m_szAmmoTypeName.Equals(szAmmoType))
						ammoAbility.OnAmmoFull();
				}
			}
		}
	}

	public virtual void OnFailToUse() {
		//m_pDamageable
		if (Time.time > m_flFailSoundTimer)
			m_bCanPlayFailSound = true;

		if (m_bCanPlayFailSound) {
			if (PlayerCharacter.GetMainCharacter() == m_pDamageable) {
				m_pDamageable.reusableAudioSource.PlayOneShot(m_pAudioClipFailToUse, flAbilityFailVolume);
			}
		}
	}

	public HitboxSeries LoadHitboxSeries(string szPath) {
		return GameObject.Instantiate(Resources.Load(szPath) as GameObject, m_pDamageable.transform).GetComponent<HitboxSeries>();
	}

	public void OnAmmoChanged() {
		foreach (AbilitySlot pAbilitySlot in m_dicAbilityKeyValues.Values) {
			if (null != pAbilitySlot.GetAbility())
				PollAmmoForSlot(pAbilitySlot);
		}
	}

	public void DoAbilitySound(AudioClip[] arrClips, float flVolume) {
		if (arrClips.Length > 0)
			GetReusableAudioSource().PlayOneShot(arrClips[arrClips.GetRandomIndex()], flVolume);
	}

	public void DoAbilitySoundLooped(AudioClip[] arrClips, float flVolume) {
		if (arrClips.Length > 0) {
			AudioSource pSource = GetReusableAudioSource();
			pSource.clip = arrClips[arrClips.GetRandomIndex()];
			pSource.volume = flVolume;
			pSource.Play();
		}
	}

	public void StopAbilitySoundLooped() {
		GetReusableAudioSource().Stop();
	}
}
